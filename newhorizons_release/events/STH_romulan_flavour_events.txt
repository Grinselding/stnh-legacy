############################
#
# Romulan Flavour Events
#
# Written by Walshicus
#
############################

namespace = STH_romulan_flavour

# Ruler Assassination Attempt
country_event = {
	id = STH_romulan_flavour.1
	title = STH_romulan_flavour.1.name
	desc = STH_romulan_flavour.1.desc
	picture = sth_GFX_evt_romulanCapital
	trigger = {
		has_country_flag = romulan_star_empire
		exists = leader
		leader = { 
			has_level < 4 
			NOT = {	has_trait = leader_trait_reverred }
		}
		used_naval_capacity_percent < 0.3
		NOT = { has_country_flag = romulan_coup }
		any_owned_leader = {
			NOT = { is_same_value = root.leader }
			OR = { leader_class = admiral leader_class = general }
		}
	}
	mean_time_to_happen = { years = 10 }
	immediate = {
		set_timed_country_flag = { flag = romulan_coup days = 3600 }
		leader = { save_event_target_as = ruler }
		random_owned_leader = {
			limit = {
				NOT = { is_same_value = root.leader }
				OR = { leader_class = admiral leader_class = general }
			}
			save_event_target_as = plotLeader
			set_leader_flag = plotLeader
		}
	}
	option = {
		name = STH_romulan_flavour.1.a
		hidden_effect = {
			random_list = {
				25 = { 
					modifier = { add = 25 leader = { has_level > 4 } }
					country_event = { id = STH_romulan_flavour.2 days = 14 random = 14 scopes = { from = event_target:plotLeader fromfrom = event_target:ruler } } 
				} #Win and gain positive trait
				25 = { country_event = { id = STH_romulan_flavour.3 days = 14 random = 14 scopes = { from = event_target:plotLeader fromfrom = event_target:ruler } } } #Win and gain negative trait
				25 = { country_event = { id = STH_romulan_flavour.4 days = 14 random = 14 scopes = { from = event_target:plotLeader fromfrom = event_target:ruler } } } #Lose and die - country destabilised
				25 = { country_event = { id = STH_romulan_flavour.5 days = 14 random = 14 scopes = { from = event_target:plotLeader fromfrom = event_target:ruler } } } #Lose and die - country happy
			}
		}
	}
}

country_event = {
	id = STH_romulan_flavour.2
	title = STH_romulan_flavour.2.name
	desc = STH_romulan_flavour.2.desc
	picture = sth_GFX_evt_romulanCapital
	is_triggered_only = yes
	immediate = {
		leader = { save_event_target_as = ruler }
		random_owned_leader = {
			limit = { has_leader_flag = plotLeader }
			save_event_target_as = plotLeader
		}
	}
	option = {
		name = STH_romulan_flavour.2.a
		add_modifier = { modifier = em_country_united days = 3600 }
		from = {
			kill_leader = { show_notification = no }
		}
		leader = { add_trait = leader_trait_reverred }
	}
}
country_event = {
	id = STH_romulan_flavour.3
	title = STH_romulan_flavour.3.name
	desc = STH_romulan_flavour.3.desc
	picture = sth_GFX_evt_romulanCapital
	is_triggered_only = yes
	immediate = {
		leader = { save_event_target_as = ruler }
		random_owned_leader = {
			limit = { has_leader_flag = plotLeader }
			save_event_target_as = plotLeader
		}
	}
	option = {
		name = STH_romulan_flavour.3.a
		add_modifier = {
			modifier = em_country_divided
			days = 3600
		}
		from = {
			kill_leader = { show_notification = no }
		}
		leader = { add_trait = leader_trait_revilled }
	}
}
country_event = {
	id = STH_romulan_flavour.4
	title = STH_romulan_flavour.4.name
	desc = STH_romulan_flavour.4.desc
	picture = sth_GFX_evt_romulanCapital
	is_triggered_only = yes
	immediate = {
		leader = { save_event_target_as = ruler }
		random_owned_leader = {
			limit = { has_leader_flag = plotLeader }
			save_event_target_as = plotLeader
		}
	}
	option = {
		name = STH_romulan_flavour.4.a
		add_modifier = {
			modifier = em_country_divided
			days = 3600
		}
		fromfrom = { kill_leader = { show_notification = no } }
		assign_leader = from
		event_target:plotLeader = { add_trait = leader_trait_revilled }
	}
}
country_event = {
	id = STH_romulan_flavour.5
	title = STH_romulan_flavour.5.name
	desc = STH_romulan_flavour.5.desc
	picture = sth_GFX_evt_romulanCapital
	is_triggered_only = yes
	immediate = {
		leader = { save_event_target_as = ruler }
		random_owned_leader = {
			limit = { has_leader_flag = plotLeader }
			save_event_target_as = plotLeader
		}
	}
	option = {
		name = STH_romulan_flavour.5.a
		add_modifier = {
			modifier = em_country_united
			days = 3600
		}
		fromfrom = { kill_leader = { show_notification = no } }
		assign_leader = from
		event_target:plotLeader = { add_trait = leader_trait_reverred }
	}
}

# Romulan Tal Shiar Inquisition 1 to 10
fleet_event = {
	id = STH_romulan_flavour.11
	title = STH_romulan_flavour.11.name
	desc = STH_romulan_flavour.11.desc
	picture = sth_GFX_evt_romulanTNGEra
	location = ROOT
	trigger = {
		owner = { has_country_flag = romulan_star_empire }
		exists = leader
		leader = { 
			has_level < 3
			NOT = { has_leader_flag = talShiarOrderInvestigated }
		}
		is_in_combat = no
	}
	mean_time_to_happen = { months = 3600 }
	immediate = { 
		leader = { 
			save_event_target_as = officer
			set_leader_flag = talShiarOrderInvestigated 
		}
	}
	option = {
		name = STH_romulan_flavour.11.a
		hidden_effect = {
			random_list = {
				80 = { fleet_event = { id = STH_romulan_flavour.12 days = 1 } } #Win and gain positive trait
				20 = { fleet_event = { id = STH_romulan_flavour.13 days = 1 } } #Lose and gain negative trait
			}
		}
	}
}
fleet_event = {
	id = STH_romulan_flavour.12
	title = STH_romulan_flavour.12.name
	desc = STH_romulan_flavour.12.desc
	picture = sth_GFX_evt_romulanCourt
	location = ROOT
	is_triggered_only = yes
	immediate = { leader = { save_event_target_as = officer } }
	option = {
		name = STH_romulan_flavour.12.a
		leader = { add_trait = leader_trait_resilient }	
		owner = { add_resource = { influence = 25 } }
	}
}
fleet_event = {
	id = STH_romulan_flavour.13
	title = STH_romulan_flavour.13.name
	desc = STH_romulan_flavour.13.desc
	picture = sth_GFX_evt_romulanCourt
	location = ROOT
	is_triggered_only = yes
	immediate = { leader = { save_event_target_as = officer } }
	option = {
		name = STH_romulan_flavour.13.a
		leader = { add_trait = leader_trait_arrested_development }
		owner = {
			add_resource = { influence = -25 }
		}
	}
}

# Romulan Tal Shiar TWO
fleet_event = {
	id = STH_romulan_flavour.14
	title = STH_romulan_flavour.14.name
	desc = STH_romulan_flavour.14.desc
	picture = sth_GFX_evt_romulanTNGEra
	location = ROOT
	trigger = {
		owner = { has_country_flag = romulan_star_empire }
		exists = leader
		leader = {
			has_level < 3
			NOT = {	has_leader_flag = talShiarInvestigated	}
		}
		is_in_combat = no
	}
	mean_time_to_happen = { months = 3600 }
	immediate = { 
		leader = {
			save_event_target_as = officer
			set_leader_flag = talShiarInvestigated
		}
	}
	option = {
		name = STH_romulan_flavour.14.a
		hidden_effect = {
			random_list = {
				60 = { fleet_event = { id = STH_romulan_flavour.15 days = 1 } } #Win and gain positive trait
				40 = { fleet_event = { id = STH_romulan_flavour.16 days = 1 } } #Lose and gain negative trait
			}
		}
	}
}
fleet_event = {
	id = STH_romulan_flavour.15
	title = STH_romulan_flavour.15.name
	desc = STH_romulan_flavour.15.desc
	picture = sth_GFX_evt_romulanInterrogation
	location = ROOT
	is_triggered_only = yes
	immediate = { leader = { save_event_target_as = officer } }
	option = {
		name = STH_romulan_flavour.15.a
		leader = { add_trait = leader_trait_resilient }	
		owner = {
			add_resource = { influence = 25 }
		}
	}
}
fleet_event = {
	id = STH_romulan_flavour.16
	title = STH_romulan_flavour.16.name
	desc = STH_romulan_flavour.16.desc
	picture = sth_GFX_evt_romulanInterrogation
	location = ROOT
	is_triggered_only = yes
	immediate = { leader = { save_event_target_as = officer } }
	option = {
		name = STH_romulan_flavour.16.a
		leader = { add_trait = leader_trait_arrested_development }
		owner = {
			add_resource = { influence = -25 }
		}
	}
}

##Obsidian spy lockbox
ship_event = {
	id = STH_romulan_flavour.17
	title = STH_romulan_flavour.17.name
	desc = STH_romulan_flavour.17.desc
	picture = sth_GFX_evt_romulanTNGEra
	location = ROOT
	trigger = {
		owner = { has_country_flag = romulan_star_empire }
		exists = leader
		leader = {
			has_level > 3
			NOT = { has_leader_flag = talShiarInvestigated }
		}
		is_in_combat = no
	}
	mean_time_to_happen = { months = 3600 }
	immediate = {
		leader = { 
			save_event_target_as = officer
			set_leader_flag = talShiarInvestigated 
		}
	}
	option = {
		name = STH_romulan_flavour.17.a
		hidden_effect = {
			random_list = {
				50 = { ship_event = { id = STH_romulan_flavour.18 days = 1 } } #Win and gain positive trait
				50 = { ship_event = { id = STH_romulan_flavour.19 days = 1 } } #Win and gain positive trait
			}
		}
	}
}
ship_event = {
	id = STH_romulan_flavour.18
	title = STH_romulan_flavour.18.name
	desc = STH_romulan_flavour.18.desc
	picture = sth_GFX_evt_romulanTNGEra
	location = ROOT
	is_triggered_only = yes
	immediate = { leader = { save_event_target_as = officer } }
	option = {
		name = STH_romulan_flavour.18.a
		leader = { add_trait = leader_trait_scout }	
		owner = { add_resource = { influence = 25 } }
	}
}
ship_event = {
	id = STH_romulan_flavour.19
	title = STH_romulan_flavour.19.name
	desc = STH_romulan_flavour.19.desc
	picture = sth_GFX_evt_romulanTNGEra
	location = ROOT
	is_triggered_only = yes
	immediate = { leader = { save_event_target_as = officer } }
	option = {
		name = STH_romulan_flavour.19.a
		leader = { add_trait = leader_trait_trickster }
		owner = { add_resource = { influence = 25 } }
	}
}

#An Odd Report
ship_event = {
	id = STH_romulan_flavour.20
	title = STH_romulan_flavour.20.name
	desc = STH_romulan_flavour.20.desc
	picture = sth_GFX_evt_romulanTNGEra
	location = ROOT
	trigger = {
		owner = { 
			is_country_type = default
			has_country_flag = romulan_star_empire
		}
		exists = leader
		leader = { 
			has_level < 3 
			leader_class = admiral
			NOT = { has_leader_flag = talShiarInvestigated }
		}
		is_in_combat = no
	}
	mean_time_to_happen = { months = 3600 }
	immediate = { 
		leader = { 
			save_event_target_as = officer 
			set_leader_flag = talShiarInvestigated
		}			
	}
	option = {
		name = STH_romulan_flavour.20.a
		hidden_effect = {
			random_list = {
				10 = { ship_event = { id = STH_romulan_flavour.21 days = 1 } } #Critical success
				40 = { ship_event = { id = STH_romulan_flavour.22 days = 1 } } #Success
				40 = { ship_event = { id = STH_romulan_flavour.23 days = 1 } } #Failure
				10 = { ship_event = { id = STH_romulan_flavour.24 days = 1 } } #Critical failure
			}
		}
	}
}
ship_event = {
	id = STH_romulan_flavour.21
	title = STH_romulan_flavour.21.name
	desc = STH_romulan_flavour.21.desc
	picture = sth_GFX_evt_romulanTNGEra
	location = ROOT
	is_triggered_only = yes
	immediate = { leader = { save_event_target_as = officer } }
	option = {
		name = STH_romulan_flavour.21.a
		leader = { add_trait = leader_trait_fleet_logistician }
		owner = { add_resource = { influence = 25 } }
	}
}
ship_event = {
	id = STH_romulan_flavour.22
	title = STH_romulan_flavour.22.name
	desc = STH_romulan_flavour.22.desc
	picture = sth_GFX_evt_romulanTNGEra
	location = ROOT
	is_triggered_only = yes
	immediate = { leader = { save_event_target_as = officer } }
	option = {
		name = STH_romulan_flavour.22.a
		owner = { add_resource = { influence = 10 } }
	}
}
ship_event = {
	id = STH_romulan_flavour.23
	title = STH_romulan_flavour.23.name
	desc = STH_romulan_flavour.23.desc
	picture = sth_GFX_evt_romulanTNGEra
	location = ROOT
	is_triggered_only = yes
	immediate = { leader = { save_event_target_as = officer } }
	option = {
		name = STH_romulan_flavour.23.a
		leader = { add_trait = leader_trait_substance_abuser }
		owner = { add_resource = { influence = -10 } }
	}
}
ship_event = {
	id = STH_romulan_flavour.24
	title = STH_romulan_flavour.24.name
	desc = STH_romulan_flavour.24.desc
	picture = sth_GFX_evt_romulanTNGEra
	location = ROOT
	is_triggered_only = yes
	immediate = { leader = { save_event_target_as = officer } }
	option = {
		name = STH_romulan_flavour.24.a
		kill_leader = { show_notification = no }
		owner = { add_resource = { influence = -25 } }
	}
}



# Romulan Literature
# The Wait
country_event = {
	id = STH_romulan_flavour.100
	title = STH_romulan_flavour.100.name
	desc = STH_romulan_flavour.100.desc
	picture = sth_GFX_evt_romulanCapital
	fire_only_once = yes
	trigger = { is_species_class = ROM }
	mean_time_to_happen = { years = 40 }
	option = {
		name = STH_romulan_flavour.100.a
		add_resource = { influence = 50 }
		add_monthly_resource_mult = { resource = society_research value = @tier3researchreward min = @tier3researchmin max = @tier3researchmax }
		add_modifier = { modifier = em_the_wait years = 10 }
	}
}


#Latrunculo
country_event = {
	id = STH_romulan_flavour.1000
	title = STH_romulan_flavour.1000.name
	desc = STH_romulan_flavour.1000.desc
	picture = sth_GFX_evt_latrunculo1
	trigger = { 
		is_species_class = ROM
		any_owned_fleet = {
			exists = leader
			is_fleet_idle = yes
			leader = { species = { is_species_class = ROM } }
			root = {
				any_owned_fleet = {
					exists = leader
					NOT = { is_same_value = prevprev }
					is_fleet_idle = yes
					solar_system = { is_same_value = prevprevprev.solar_system }
				}
			}
		}
	}
	mean_time_to_happen = { years = 20 }
	immediate = {
		random_owned_fleet = {
			limit = {
				exists = leader
				is_fleet_idle = yes
				leader = { species = { is_species_class = ROM } }
				root = {
					any_owned_fleet = {
						exists = leader
						NOT = { is_same_value = prevprev }
						is_fleet_idle = yes
						solar_system = { is_same_value = prevprevprev.solar_system }
					}
				}
			}
			save_event_target_as = romulanLeaderFleet1
			leader = { 
				save_event_target_as = romulanLeader1 
				set_timed_leader_flag = { flag = recentLatrunculo years = 25 }
			}
		}
		random_owned_fleet = {
			limit = {
				exists = leader
				exists = event_target:romulanLeaderFleet1
				NOT = { is_same_value = event_target:romulanLeaderFleet1 }
				is_fleet_idle = yes
				solar_system = { is_same_value = event_target:romulanLeaderFleet1.solar_system }
			}
			save_event_target_as = romulanLeaderFleet2
			leader = { 
				save_event_target_as = romulanLeader2 
				set_timed_leader_flag = { flag = recentLatrunculo years = 25 }
			}
		}
	}
	option = {
		name = STH_romulan_flavour.1000.a
		hidden_effect = {
			random_list = {
				#Leader 1 Win
				10 = {
					modifier = { add = 10 event_target:romulanLeader1 = { has_level > 2 } }
					modifier = { add = 10 event_target:romulanLeader1 = { has_level > 3 } }
					modifier = { add = 10 event_target:romulanLeader1 = { has_level > 4 } }
					modifier = { add = 10 event_target:romulanLeader1 = { has_trait = leader_trait_latrunculo_master } }
					random_list = {
						0 = {
							modifier = { add = 10 event_target:romulanLeader1 = { NOT = { has_trait = leader_trait_adaptable } } }
							country_event = { id = STH_romulan_flavour.1001 days = 7 scopes = { from = event_target:romulanLeader1 fromfrom = event_target:romulanLeader2 fromfromfrom = event_target:romulanLeaderFleet1 fromfromfromfrom = event_target:romulanLeaderFleet2 } }
						}
						0 = {
							modifier = { add = 10 event_target:romulanLeader1 = { NOT = { has_trait = leader_trait_latrunculo_master } } }
							country_event = { id = STH_romulan_flavour.1002 days = 7 scopes = { from = event_target:romulanLeader1 fromfrom = event_target:romulanLeader2 fromfromfrom = event_target:romulanLeaderFleet1 fromfromfromfrom = event_target:romulanLeaderFleet2 } }
						}
						10 = {
							country_event = { id = STH_romulan_flavour.1003 days = 7 scopes = { from = event_target:romulanLeader1 fromfrom = event_target:romulanLeader2 fromfromfrom = event_target:romulanLeaderFleet1 fromfromfromfrom = event_target:romulanLeaderFleet2 } }
						}
					}
				}
				#Leader 2 Win
				10 = {
					modifier = { add = 10 event_target:romulanLeader2 = { has_level > 2 } }
					modifier = { add = 10 event_target:romulanLeader2 = { has_level > 3 } }
					modifier = { add = 10 event_target:romulanLeader2 = { has_level > 4 } }
					modifier = { add = 10 event_target:romulanLeader2 = { has_trait = leader_trait_latrunculo_master } }
					random_list = {
						0 = {
							modifier = { add = 10 event_target:romulanLeader2 = { NOT = { has_trait = leader_trait_adaptable } } }
							country_event = { id = STH_romulan_flavour.1001 days = 7 scopes = { from = event_target:romulanLeader2 fromfrom = event_target:romulanLeader1 fromfromfrom = event_target:romulanLeaderFleet2 fromfromfromfrom = event_target:romulanLeaderFleet1 } }
						}
						0 = {
							modifier = { add = 10 event_target:romulanLeader2 = { NOT = { has_trait = leader_trait_latrunculo_master } } }
							country_event = { id = STH_romulan_flavour.1002 days = 7 scopes = { from = event_target:romulanLeader2 fromfrom = event_target:romulanLeader1 fromfromfrom = event_target:romulanLeaderFleet2 fromfromfromfrom = event_target:romulanLeaderFleet1 } }
						}
						10 = {
							country_event = { id = STH_romulan_flavour.1003 days = 7 scopes = { from = event_target:romulanLeader2 fromfrom = event_target:romulanLeader1 fromfromfrom = event_target:romulanLeaderFleet2 fromfromfromfrom = event_target:romulanLeaderFleet1 } }
						}
					}
				}
				#Stalemate
				10 = {
					modifier = { 
						add = 50 
						event_target:romulanLeader1 = { has_trait = leader_trait_latrunculo_master }
						event_target:romulanLeader2 = { has_trait = leader_trait_latrunculo_master }
					}
					country_event = { id = STH_romulan_flavour.1004 days = 7 scopes = { from = event_target:romulanLeader1 fromfrom = event_target:romulanLeader2 fromfromfrom = event_target:romulanLeaderFleet1 fromfromfromfrom = event_target:romulanLeaderFleet2 } }
				}
			}
		}
	}
}

#Latrunculo Win 1
country_event = {
	id = STH_romulan_flavour.1001
	title = STH_romulan_flavour.1001.name
	desc = STH_romulan_flavour.1001.desc
	picture = sth_GFX_evt_latrunculo1
	trigger = { exists = from }
	is_triggered_only = yes
	option = {
		name = STH_romulan_flavour.1001.a
		from = { add_trait = leader_trait_adaptable }
	}
}
#Latrunculo Win 2
country_event = {
	id = STH_romulan_flavour.1002
	title = STH_romulan_flavour.1001.name
	desc = STH_romulan_flavour.1001.desc
	picture = sth_GFX_evt_latrunculo1
	trigger = { exists = from }
	is_triggered_only = yes
	option = {
		name = STH_romulan_flavour.1001.a
		from = { add_trait = leader_trait_latrunculo_master	}
	}
}
#Latrunculo Win 3
country_event = {
	id = STH_romulan_flavour.1003
	title = STH_romulan_flavour.1001.name
	desc = STH_romulan_flavour.1001.desc
	picture = sth_GFX_evt_latrunculo1
	trigger = { exists = from }
	is_triggered_only = yes
	option = {
		name = STH_romulan_flavour.1001.a
		from = { add_experience = 250 }
	}
}
#Latrunculo Stalemate
country_event = {
	id = STH_romulan_flavour.1004
	title = STH_romulan_flavour.1004.name
	desc = STH_romulan_flavour.1004.desc
	picture = sth_GFX_evt_latrunculo1
	trigger = { exists = from }
	is_triggered_only = yes
	option = {
		name = STH_romulan_flavour.1004.a
		from = { add_experience = 250 }
	}
}
########################################
#										
# STNH Expedition Events 2 - Heroes
# Written by Russ						
# 										
# Version 1.0 - created(Russ 28.08.19)	
#	LOC TO BE UDPATED - re-using old Loc																			
########################################

namespace = STH_expedition


# Start Expedition
#Leader sent on mission
ship_event = {
	id = STH_expedition.2000
	title = "STH_expedition.4.name"
	desc = "STH_expedition.4.desc"
	picture = sth_GFX_evt_spaceBackground
	is_triggered_only = yes
	trigger = {
		is_multiplayer = no
	}
	immediate = {
		owner = { country_event = { id = STH_expedition.5000 } }
	}
	option = {
		name = STH_expedition.4.a
		trigger = {
			NOR ={
			has_ship_flag = hero_ship
			owner = { has_country_flag = sth_expedition_hero_1_ongoing }}
			}
		hidden_effect = { 	owner = { country_event = { id = STH_expedition.2005 days = 180 random = 30 } 
							set_country_flag = sth_expedition_hero_1_ongoing	} }
		fleet = { 
			leader = {
				add_trait = leader_trait_immortal
				set_timed_leader_flag = {
					flag = sth_expedition_hero_Leader_1
					days = 1100 }
				save_global_event_target_as = sth_expedition_hero_Leader_1
				#exile_leader_as = sth_expedition_Leader_1
			}
		}
		root = {
			hidden_effect = {
				save_event_target_as = sth_expedition_hero_ship			
				set_ship_flag = sth_expedition_hero_ship
					
				fleet = { 	set_fleet_flag = sth_expedition_hero_ship 
							#set_location = { target = event_target:deep_space_system distance = 10 angle = 100 }
							set_event_locked = yes
				}
			}
		}	
	}
	option = {
		name = "STH_expedition.4.d"
		owner = { set_timed_country_flag = {
			flag = sth_expedition_hero_reject
			days = 360 }
		}
	}
	option = {
		name = "STH_expedition.4.e"
		trigger = {
			has_ship_flag = hero_ship
		}
		owner = { country_event = { id = STH_expedition.2 days = 5 } }
	}
	option = {
		name = "STH_expedition.4.f"
		custom_tooltip = "STH_expedition.4.f.tooltip"
		owner = { country_event = { id = STH_expedition.5001 days = 360 } }
	}
}

# Year 1
country_event = {
	id = STH_expedition.2005
	hide_window = yes
	is_triggered_only = yes

	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	
	immediate ={
		set_timed_country_flag = {
			flag = STH_expedition_hero1_year_1
			days = 360
		}
		set_country_flag = sth_expedition_hero_1_ongoing

		random_list = {
			25 ={ log = "STH_expedition_hero - planet survey Y1" 
				country_event = { id = STH_expedition.2008 } }
			25 ={ log = "STH_expedition_hero - attack Y1" 
				country_event = { id = STH_expedition.2021 } }
			25 ={ log = "SHT_expedition_hero - ancient civ Y1"
				country_event = { id = STH_expedition.2011 } }
			25 ={ log = "STH_expedition_hero - SOS Y1" 
				country_event = { id = STH_expedition.2025 } }
		}
	}
}

# Year 2
country_event = {
	id = STH_expedition.2006
	hide_window = yes
	is_triggered_only = yes

	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }

	immediate ={
		remove_country_flag = STH_expedition_hero1_year_1
		set_timed_country_flag = {
			flag = STH_expedition_hero1_year_2
			days = 360
		}
		random_list = {
			20 ={ log = "STH_expedition_hero - SOS Y2" 
				country_event = { id = STH_expedition.2025 } }
			20 ={ log = "expedition_hero - primative culture Y2" 
				country_event = { id = STH_expedition.2015 } }
			20 ={ log = "expedition_hero - recovery mission Y3" 
				country_event = { id = STH_expedition.2024 } }
			20 ={ log = "expedition_hero - planet survey Y2" 
				country_event = { id = STH_expedition.2008 } }
			20 ={ log = "expedition_hero - attack Y2" 
				country_event = { id = STH_expedition.2021 } }
			20 ={ log = "expedition_hero - Moriaty Y2" 
				country_event = { id = STH_expedition.2030 } }
		}
	}
}

# Year 3
country_event = {
	id = STH_expedition.2007
	hide_window = yes
	is_triggered_only = yes

	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }

	immediate ={
		remove_country_flag = STH_expedition_hero1_year_2
		set_timed_country_flag = {
			flag = STH_expedition_hero1_year_3
			days = 360
		}
		random_list = {
			15 ={ log = "expedition_hero - planet survey Y3" 
				country_event = { id = STH_expedition.2008 } }
			15 ={ log = "expedition_hero - ancient civ Y3"
				country_event = { id = STH_expedition.2011 } }
			15 ={ log = "expedition_hero - attack Y3" 
				country_event = { id = STH_expedition.2021 } }
			15 ={ log = "expedition_hero - recovery mission Y3" 
				country_event = { id = STH_expedition.2024 } }
			15 ={ log = "expedition_hero - SOS Y3" 
				country_event = { id = STH_expedition.2025 } }
			15 ={ log = "expedition_hero - Trader Y3" 
				country_event = { id = STH_expedition.2028 }}
		}
	}
}

#Planet Survey
country_event = {
	id = STH_expedition.2008
	title = "STH_expedition.8.name"
	desc = "STH_expedition.8.desc"
	picture = sth_GFX_evt_theAndroid1
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }

	immediate ={
		if = {
			limit = {
				has_country_flag = STH_expedition_hero1_year_1
			}
			random_list = {
				90 ={
					country_event = {
						id = STH_expedition.2009
						days =3
					}
				}
				10 ={
					country_event = {
						id = STH_expedition.2010
						days =3
					}
				}
			}
		}
		if = {
			limit = {
				has_country_flag = STH_expedition_hero1_year_2
			}
			random_list = {
				40 ={
					country_event = {
						id = STH_expedition.2009
						days =3
					}
				}
				60 ={
					country_event = {
						id = STH_expedition.2010
						days =3
					}
				}
			}
		}
		if = {
			limit = {
				has_country_flag = STH_expedition_hero1_year_3
			}
			random_list = {
				50 ={
					country_event = {
						id = STH_expedition.2009
						days =3
					}
				}
				50 ={
					country_event = {
						id = STH_expedition.2010
						days =3
					}
				}
			}
		}
	}
	option ={
		name = "STH_expedition.8.a"
	}
}

#Planet Survey - experience
country_event = {
	id = STH_expedition.2009
	title = "STH_expedition.9.name"

	
	picture = sth_GFX_evt_theAndroid1
	
	desc = {
        exclusive_trigger = { owner = { has_country_flag = STH_expedition_hero1_year_1 }}
            text = STH_expedition.9.desc_01
    }
    desc = {
        exclusive_trigger = { owner = { has_country_flag = STH_expedition_hero1_year_2 }}
            text = STH_expedition.9.desc_02
	}
	desc = {
        exclusive_trigger = { owner = { has_country_flag = STH_expedition_hero1_year_3 }}
            text = STH_expedition.9.desc_03
    }
	
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}
		
	option ={
		name = STH_expedition.9.a
		trigger = { has_country_flag = STH_expedition_hero1_year_1 }
		country_event = {
			id = STH_expedition.2006
			days = 400 random = 30
		}
		event_target:sth_expedition_hero_Leader_1 = {
			add_experience = 100
		}
	}
	option ={
		name = STH_expedition.9.b
		trigger = { has_country_flag = STH_expedition_hero1_year_2 }
		country_event = {
			id = STH_expedition.2007
			days = 400 random = 30
		}
		event_target:sth_expedition_Leader_hero_1 = {
			add_experience = 200
		}
	}
	option ={
		name = STH_expedition.9.c
		trigger = { has_country_flag = STH_expedition_hero1_year_3 }
		country_event = {
			id = STH_expedition.2100
			days = 400 random = 30
		}
		event_target:sth_expedition_Leader_hero_1 = {
			add_experience = 1000
			add_trait = leader_trait_expertise_new_worlds
		}
	}
}

#Planet Survey - minerals (E1)
country_event = {
	id = STH_expedition.2010
	title = "STH_expedition.10.name"
	desc = "STH_expedition.10.desc"
	picture = sth_GFX_evt_cargoBay
	is_triggered_only = yes
	hide_window = no

	immediate ={}
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	option ={
		name = STH_expedition.10.a
		trigger = {
			has_country_flag = STH_expedition_hero1_year_1
		}
		country_event = {
			id = STH_expedition.6
			days = 400 random = 20
		}
		random_list = {
			25 ={ add_resource = {
				alloys = 500
			} }
			25 ={ add_resource = {
				minerals = 5000
			} }
			25 ={ add_resource = {
				sr_pergium = 50
			} }
			25 ={ add_resource = {
				sr_boronite = 30
			} }
		}
	}
	option ={
		name = STH_expedition.10.b
		trigger = {
			has_country_flag = STH_expedition_hero1_year_2
		}
		country_event = {
			id = STH_expedition.2007
			days = 400 random = 20
		}
		random_list = {
			25 ={ add_resource = {
				alloys = 500
			} }
			25 ={ add_resource = {
				minerals = 1000
			} }
			25 ={ add_resource = {
				sr_dilithium = 50
			} }
			25 ={ add_resource = {
				sr_boronite = 30
			} }
		}
	}
	option ={
		name = STH_expedition.10.c
		trigger = {
			has_country_flag = STH_expedition_hero1_year_3
		}
		country_event = {
			id = STH_expedition.2100
			days = 200 random = 20
		}
		random_list = {
			25 ={ add_resource = {
				alloys = 5500
			} }
			25 ={ add_resource = {
				minerals = 7000
			} }
			25 ={ add_resource = {
				sr_pergium = 10
			} }
			25 ={ add_resource = {
				sr_boronite = 10
			} }
		}
	}
}

# Investigate Ancient Civ
country_event = {
	id = STH_expedition.2011
	title = "STH_expedition.11.name"
	desc = "STH_expedition.11.desc"
	picture = sth_GFX_evt_buildingDestroyed
	is_triggered_only = yes
	hide_window = no
	
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={
		if = {
			limit = {
				has_country_flag = STH_expedition_hero1_year_1
			}
			random_list = {
				33 ={
					country_event = {
						id = STH_expedition.2012
						days =3
					}
				}
				33 ={
					country_event = {
						id = STH_expedition.2013
						days =3
					}
				}
				33 ={
					country_event = {
						id = STH_expedition.2014
						days =3
					}
				}
			}
		}
		if = {
			limit = {
				has_country_flag = STH_expedition_hero1_year_3
			}
			random_list = {
				33 ={
					country_event = {
						id = STH_expedition.2012
						days =3
					}
				}
				33 ={
					country_event = {
						id = STH_expedition.2013
						days =3
					}
				}
				33 ={
					country_event = {
						id = STH_expedition.2014
						days =3
					}
				}
			}
		}
	}
	option = {
		name = "STH_expedition.11.a"
	}
}

# Investigate Ancient Civ - Research
country_event = {
	id = STH_expedition.2012
	title = "STH_expedition.12.name"
	desc = "STH_expedition.12.desc"
	picture = sth_GFX_evt_buildingDestroyed
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={
		if = { 
			limit = {
				owner = { has_country_flag = STH_expedition_hero1_year_1 }
			}
			random_list = {
				33 ={ add_resource = {
					engineering_research = 250
				} }
				33 ={ add_resource = {
					society_research = 250
				} }
				33 ={  
					event_target:sth_expedition_Leader_hero_1 = {
						add_experience = 300
					}
				}
			}
		}
		if = { 
			limit = {
				owner = { has_country_flag = STH_expedition_hero1_year_3 }
			}
			random_list = {
				50 ={ owner = { add_resource = {
					engineering_research = 150 }
				} }
				50 ={ owner = { add_resource = {
					society_research = 140 }
				} }
			}
		}
	}

	option ={
		name = STH_expedition.12.a
		trigger = { has_country_flag = "STH_expedition_hero1_year_1" }
		country_event = { id = STH_expedition.2006 days = 400 random = 30 }
		
	}
	option ={
		name = STH_expedition.12.b
		trigger = { has_country_flag = "STH_expedition_hero1_year_3" } 
		country_event = { id = STH_expedition.2100 days = 200 random = 30 }
	}
	option ={
		name = STH_expedition.12.a
		trigger = { has_country_flag = "STH_expedition_hero1_year_2" }
		country_event = { id = STH_expedition.2007 days = 400 random = 30 }
		
	}
}

# Investigate Ancient Civ - weapons
country_event = {
	id = STH_expedition.2013
	title = "STH_expedition.13.name"
	desc = "STH_expedition.13.desc"
	picture = sth_GFX_evt_buildingDestroyed
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}

	option ={ # RESEARCH
		name = STH_expedition.13.a
		trigger = {
			has_country_flag = STH_expedition_hero1_year_1
		}
		owner = {
			add_monthly_resource_mult = {
				resource = engineering_research
				value = @tier1researchreward
				min = @tier1researchmin
				max = @tier1researchmax
			}
		}
		owner = { add_resource = { influence = -100 } }
		country_event = {
			id = STH_expedition.2006
			days = 360 random = 20	
		}
	}
	option ={ # RESEARCH
		name = STH_expedition.13.a
		trigger = {
			has_country_flag = STH_expedition_hero1_year_3
		}
		owner = {
			add_monthly_resource_mult = {
				resource = engineering_research
				value = @tier1researchreward
				min = @tier1researchmin
				max = @tier1researchmax
			}
		}
		owner = { add_resource = { physics_research = 100 engineering_research = 100 influence = -50 } }
		country_event = {
			id = STH_expedition.2100
			days = 360 random = 100	
		}
	}
	option ={ # BREAK WEAPONS DOWN
		name = STH_expedition.13.b
		trigger = {
			has_country_flag = STH_expedition_hero1_year_1
		}
		owner ={ add_resource = { physics_research = 150 influence = 150 }}
		country_event = {
			id = STH_expedition.2006
			days = 360 random = 20	
		}
	}
	option ={ # BREAK WEAPONS DOWN
		name = STH_expedition.13.b
		trigger = {
			has_country_flag = STH_expedition_hero1_year_3
		}
		owner ={ add_resource = { physics_research = 50 influence = 250 }}
		country_event = {
			id = STH_expedition.2100
			days = 200 random = 20	
		}
	}
}

# Investigate Ancient Civ - NOTHING
country_event = {
	id = STH_expedition.2014
	title = "STH_expedition.14.name"
	desc = "STH_expedition.14.desc"
	picture = sth_GFX_evt_buildingDestroyed
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}

	option ={ # RESEARCH
		name = STH_expedition.14.a
		trigger = { has_country_flag = STH_expedition_hero1_year_1 }
		country_event = { id = STH_expedition.2006 days = 380 random = 20 }
		event_target:sth_expedition_hero_Leader_1 = {
			add_experience = 100
		}
	}
	option ={ # RESEARCH
		name = STH_expedition.14.a
		trigger = { has_country_flag = STH_expedition_hero1_year_2 }
		country_event = { id = STH_expedition.2007 days = 380 random = 20 }
		event_target:sth_expedition_hero_Leader_1 = {
			add_experience = 100
		}
	}
	option ={ # RESEARCH
		name = STH_expedition.14.a
		trigger = { has_country_flag = STH_expedition_hero1_year_3 }
		country_event = { id = STH_expedition.2100 days = 200 random = 20 }
		event_target:sth_expedition_hero_Leader_1 = {
			add_experience = 500
		}
	}
}

# Primative Culture
country_event = {
	id = STH_expedition.2015
	title = "STH_expedition.15.name"
	desc = "STH_expedition.15.desc"
	picture = sth_GFX_evt_talax
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}

	option ={ # RESEARCH
		name = STH_expedition.15.a
		random_list = {
			60 ={ log = "STH_expedition.2015 - OK"
				country_event = { id = STH_expedition.2016 days = 5 random = 1 }					
				}
			40 ={ log = "STH_expedition.2015 - Disastor"
				country_event = { id = STH_expedition.2017 days = 5 random = 1 }	
			}
		}
	}
	option ={ # IGNORE
		name = STH_expedition.15.b
		country_event = {
			id = STH_expedition.2007
			days = 360 random = 30
		}
	}
}

# Primative Culture 1
country_event = {
	id = STH_expedition.2016
	title = "STH_expedition.16.name"
	desc = "STH_expedition.16.desc"
	picture = sth_GFX_evt_caretakerPlanet
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}

	option ={ # RESEARCH
		name = STH_expedition.16.a
		country_event = { id = STH_expedition.2007 }
		random_list = {
			33 ={ add_resource = {
				society_research = 150
			} }
			33 ={ add_resource = {
				minerals = 50
			} }
			33 ={ add_resource = {
				influence =  100
			} }
		}
	}
}

# Primative Culture 2
country_event = {
	id = STH_expedition.2017
	title = "STH_expedition.17.name"
	picture = sth_GFX_evt_caretakerPlanet
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	desc = {
        exclusive_trigger = { owner = { has_country_flag = "united_federation_of_planets" }}
            text = STH_expedition.17.desc_01
    }
    desc = {
        exclusive_trigger = { owner = { NOT = { has_country_flag = "united_federation_of_planets" } } }
            text = STH_expedition.17.desc_02
    }

	immediate ={}

	option ={ # PRIME DIRECTIVE
		name = STH_expedition.17.a
		trigger = { has_country_flag = "united_federation_of_planets" }
		add_resource = {
			sr_pergium = 30
		}
		event_target:sth_expedition_Leader_1 = {
			add_experience = -500
			add_trait = leader_trait_careful
		}
		country_event = {
			id = STH_expedition.7
			days = 360 random = 30
		}
	}
	option ={ # LEAVE
		name = STH_expedition.17.b
		trigger = { NOT = { has_country_flag = "united_federation_of_planets" } }
		add_resource = {
			sr_pergium = 30
		}
		event_target:sth_expedition_Leader_1 = {
			add_experience = -500
			add_trait = leader_trait_careful
		}
		country_event = {
			id = STH_expedition.2007
			days = 360 random = 30
		}
	}
	option ={ # RESCUE MISSION
		allow = { has_resource = { type = unity amount >= 2000 } }
		name = STH_expedition.17.c
		add_resource = { unity = -2000 influence = 100 }
		random_list = {
			60 ={ country_event = { id = STH_expedition.18 days = 5	} }
			35 ={ country_event = { id = STH_expedition.19 days = 5 } }
			05 ={ country_event = { id = STH_expedition.20 days = 5 } }
		}
		event_target:sth_expedition_hero_Leader_1 = {
			add_trait = leader_trait_carefree
		}
	} 
}

# Primative Culture 2a
country_event = {
	id = STH_expedition.2018
	title = "STH_expedition.18.name"
	desc = "STH_expedition.18.desc"
	picture = sth_GFX_evt_caretakerPlanet
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}
	
	option ={ 
		name = "STH_expedition.18.a"
		add_resource = {
			unity = 300
		}
		country_event = {
			id = STH_expedition.2007
			days = 360 random = 30
		}
		event_target:sth_expedition_hero_Leader_1 = {
			add_experience = 1000
		}
	}
}

# Primative Culture 2b
country_event = {
	id = STH_expedition.2019
	title = "STH_expedition.19.name"
	desc = "STH_expedition.19.desc"
	picture = sth_GFX_evt_burning_settlement
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}

	option ={ # TRIED TO SAVE
		name = "STH_expedition.19.a"
		add_resource = {
			unity = 10
		}
		country_event = {
			id = STH_expedition.2007
			days = 360 random = 30
		}
		event_target:sth_expedition_hero_Leader_1 = {
			add_experience = 1000 }
	}
}

# Primative Culture 2c
country_event = {
	id = STH_expedition.2020
	title = "STH_expedition.20.name"
	desc = "STH_expedition.20.desc"
	picture = sth_GFX_evt_deadOfficer
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={
		if ={
			limit = {
				has_country_flag = STH_expedition_hero1_year_1
			}
			set_timed_country_flag = {
				flag = STH_expedition_hero_leader_1_dead
				days = 2000
			}
		}
		if ={
			limit = {
				has_country_flag = STH_expedition_hero2_year_1
			}
			set_timed_country_flag = {
				flag = STH_expedition_hero_leader_2_dead
				days = 2000
			}
		}
		if ={
			limit = {
				has_country_flag = STH_expedition_hero3_year_1
			}
			set_timed_country_flag = {
				flag = STH_expedition_hero_leader_3_dead
				days = 2000
			}
		}		
	}
	option ={ # DEAD
		name = "STH_expedition.20.a"
		add_resource = {
			unity = 10
			influence = -40
		}
		remove_country_flag = sth_expedition_hero_1_ongoing
		remove_country_flag = STH_expedition_hero1_year_1
		remove_country_flag = STH_expedition_hero1_year_2
		remove_country_flag = STH_expedition_hero1_year_3
		random_owned_fleet = {
			limit = {
				has_fleet_flag = sth_expedition_hero_ship
			}
			leader = { remove_trait = leader_trait_immortal	}
			destroy_fleet = THIS
		}
	}
}

# Ship Attacked (E1)
country_event = {
	id = STH_expedition.2021
	title = "STH_expedition.21.name"
	desc = "STH_expedition.21.desc"
	picture = sth_GFX_evt_spaceBattle3
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}
	option = {
		name = STH_expedition.21.a
		hidden_effect = { 
			random_list ={
			70 = { country_event = { id = STH_expedition.2022  } }
			30 = { country_event = { id = STH_expedition.2023  } } 
			}
		}	
	}
	option = {
		name = STH_expedition.21.b
		trigger = {
			has_country_flag = STH_expedition_hero1_year_1
		}
		hidden_effect = { country_event = { id = STH_expedition.2006 days = 400 random = 50 } }
	}
	option = {
		name = STH_expedition.21.c
		trigger = {
			has_country_flag = STH_expedition_hero1_year_2
		}
		hidden_effect = { country_event = { id = STH_expedition.2007 days = 400 random = 60 } }
	}
	option = {
		name = STH_expedition.21.d
		trigger = {
			has_country_flag = STH_expedition_hero1_year_3
		}
		hidden_effect = { country_event = { id = STH_expedition.2100 days = 200 random = 60 } }
	}
}

# Ship Attacked 1
country_event = {
	id = STH_expedition.2022
	title = "STH_expedition.22.name"
	desc = "STH_expedition.22.desc"
	picture = sth_GFX_evt_starshipAttacked
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}
	option = {
		name = STH_expedition.22.a
		trigger = {
			has_country_flag = STH_expedition_hero1_year_1
		}
		hidden_effect = { country_event = { id = STH_expedition.2006 days = 400 random = 20 } }
		event_target:sth_expedition_hero_Leader_1 ={
			add_experience = 200
		}
		add_resource = {
			alloys = 100	
		}
	}
	option = {
		name = STH_expedition.22.b
		trigger = {
			has_country_flag = STH_expedition_hero1_year_2
		}
		hidden_effect = { country_event = { id = STH_expedition.2007 days = 400 random = 20 } }
		event_target:sth_expedition_Leader_1 ={
			add_experience = 200
		}
		add_resource = {
			sr_dilithium = 10 alloys = 100 }
	}
	option = {
		name = STH_expedition.22.c
		trigger = {
			has_country_flag = STH_expedition_hero1_year_3
		}
		hidden_effect = { country_event = { id = STH_expedition.2100 days = 200 random = 20 } }
		event_target:sth_expedition_hero_Leader_1 ={
			add_experience = 1000
		}
		add_resource = {
			minerals = 100 alloys = 100 }
	}
}

# Ship Attacked 2 (destroyed) (E1)
country_event = {
	id = STH_expedition.2023
	title = "STH_expedition.23.name"
	desc = "STH_expedition.23.desc"
	picture = sth_GFX_evt_starshipExplosion
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}
	
	option = {
		name = STH_expedition.23.a
		add_resource = { influence = -100 }
		remove_country_flag = sth_expedition_hero_1_ongoing
		remove_country_flag = STH_expedition_hero1_year_1
		remove_country_flag = STH_expedition_hero1_year_2
		remove_country_flag = STH_expedition_hero1_year_3
		random_owned_fleet = {
			limit = {
				has_fleet_flag = sth_expedition_hero_ship
			}
			leader = { remove_trait = leader_trait_immortal	}
			destroy_fleet = THIS
		}
	}
}

# Recovery Mission
country_event = {
	id = STH_expedition.2024
	title = "STH_expedition.24.name"
	picture = sth_GFX_evt_alienProbe
	is_triggered_only = yes
	hide_window = no

	desc = {
        exclusive_trigger = { owner = { has_country_flag = STH_expedition_hero1_year_2 }}
            text = STH_expedition.24.desc_01
	}
	
    desc = {
        exclusive_trigger = { owner = { NOT = { has_country_flag = STH_expedition_hero1_year_3 } } }
            text = STH_expedition.24.desc_02
	}
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}
	option ={
		name = "STH_expedition.24.a"
		trigger = {
			has_country_flag = STH_expedition_hero1_year_2
		}
		event_target:sth_expedition_hero_Leader_1 ={
			add_experience = 1000
		}
		country_event = {
			id = STH_expedition.2007
			days = 360 random = 40
		}
	}
	option ={
		name = "STH_expedition.24.b"
		trigger = {
			has_country_flag = STH_expedition_hero1_year_3
		}
		event_target:sth_expedition_hero_Leader_1 ={
			add_experience = 1500
		}
		country_event = {
			id = STH_expedition.2100
			days = 360 random = 40
		}
	}
}

# SOS
country_event = {
	id = STH_expedition.2025
	title = "STH_expedition.25.name"
	desc = "STH_expedition.25.desc"
	picture = sth_GFX_evt_spaceBackground
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}

	option = {
		name = STH_expedition.25.a #Accept
		country_event = {
			id = STH_expedition.2026
			days = 3
		}
	}
	option = {
		name = STH_expedition.25.b #Decline - Good Guys
		trigger = { is_xenophilic = yes }
		add_resource = { unity = -50 }
		country_event = { id = STH_expedition.2027 days = 2 }	
	}
	option = {
		name = STH_expedition.25.c #Decline - Bad / Neutral Guys
		trigger = { is_xenophilic = no }
		country_event = { id = STH_expedition.2027 days = 2 }
	}
}

# SOS 1
country_event = {
	id = STH_expedition.2026
	title = "STH_expedition.26.name"
	desc = "STH_expedition.26.desc"
	picture = sth_GFX_evt_spaceBackground
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}

	option = {
		name = STH_expedition.26.a #Accept
		trigger = {
			has_country_flag = STH_expedition_hero1_year_1
		}
		owner = { add_resource = { physics_research = 250 } }
		event_target:sth_expedition_hero_Leader_1 ={
			add_experience = 200
		}
		country_event = {
			id = STH_expedition.2006
			days = 400 random = 25
		}
	}
	option = {
		name = STH_expedition.26.b #Accept
		trigger = {
			has_country_flag = STH_expedition_hero1_year_2
		}
		owner = { add_resource = { physics_research = 250 } }
		event_target:sth_expedition_hero_Leader_1 ={
			add_experience = 200
		}
		country_event = {
			id = STH_expedition.2007
			days = 400 random = 25
		}
	}
	option = {
		name = STH_expedition.26.c #Accept
		trigger = {
			has_country_flag = STH_expedition_hero1_year_3
		}
		owner = { add_resource = { physics_research = 250 } }
		event_target:sth_expedition_hero_Leader_1 ={
			add_experience = 200
		}
		country_event = {
			id = STH_expedition.2100
			days = 200 random = 25
		}
	}
}

# SOS 2
country_event = {
	id = STH_expedition.2027
	title = "STH_expedition.27.name"
	desc = "STH_expedition.27.desc"
	picture = sth_GFX_evt_exploding_ship
	show_sound = event_ship_explosion
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}

	option = {
		name = STH_expedition.27.a #Accept
		trigger = {
			has_country_flag = STH_expedition_hero1_year_1
		}
		country_event = {
			id = STH_expedition.2006
			days = 400 random = 25
		}
	}
	option = {
		name = STH_expedition.27.b #Accept
		trigger = {
			has_country_flag = STH_expedition_hero1_year_2
		}
		country_event = {
			id = STH_expedition.2007
			days = 400 random = 25
		}
	}
	option = {
		name = STH_expedition.27.c #Accept
		trigger = {
			has_country_flag = STH_expedition_hero1_year_3
		}
		country_event = {
			id = STH_expedition.2100
			days = 200 random = 25
		}
	}
}

# Trader Joe
country_event = {
	id = STH_expedition.2028
	title = "STH_expedition.28.name"
	desc = "STH_expedition.28.desc"
	picture = sth_GFX_evt_aMatterOfTime1
	#show_sound = event_ship_explosion
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}

	option = {
		name = STH_expedition.28.a #Accept
		country_event = {
			id = STH_expedition.2100
			days = 200 random = 50
		}
		owner = { add_resource ={ energy = -4000 sr_boronite = 100 } } 
	}
	option = {
		name = STH_expedition.28.b #Accept
		country_event = {
			id = STH_expedition.2100
			days = 200 random = 50
		}
		owner = { add_resource ={ minerals = -4000 sr_pergium = 100 } }
	}
	option = {
		name = STH_expedition.28.c #Accept
		country_event = {
			id = STH_expedition.2100
			days = 200 random = 50
		}
		owner = { add_resource ={ minerals = -100 alloys = 5000 } }
	}
	option = {
		name = STH_expedition.28.d #Accept
		country_event = {
			id = STH_expedition.2100
			days = 200 random = 50
		}
	}
}

# HOME - Exp 1
country_event = {
	id = STH_expedition.2100
	title = "STH_expedition.100.name"
	desc = "STH_expedition.100.desc"
	picture = sth_GFX_evt_caretakerHome
	#show_sound = event_ship_explosion
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={
		random_owned_ship = {
			limit = {
				has_ship_flag = sth_expedition_hero_ship
			}
			fleet = { 		
				remove_fleet_flag = sth_expedition_hero_ship 
				set_event_locked = no
				set_location = { target = owner.capital_scope distance = 300 angle = 100 }
				set_leader = event_target:sth_expedition_hero_Leader_1
				leader = { remove_trait = leader_trait_immortal	}
			}
			remove_ship_flag = sth_expedition_hero_ship
		}
		random_owned_fleet = {
			limit = {
				has_fleet_flag = sth_expedition_hero_ship
			}
			remove_fleet_flag = sth_expedition_hero_ship 
			set_event_locked = no
			set_location = { target = owner.capital_scope distance = 300 angle = 100 }
			set_leader = event_target:sth_expedition_hero_Leader_1
		}
	}

	option = {
		name = sth_congratulations #Congrats
		clear_global_event_target = sth_expedition_hero_Leader_1
		remove_country_flag = sth_expedition_hero_1_ongoing
		remove_country_flag = STH_expedition_hero1_year_3
	}
}


# Flag Holodeck moriarty Y2 
country_event = {
	id = STH_expedition.2030
	title = "STH_expedition.477.name"
	desc = "STH_expedition.477.desc"
	picture = sth_GFX_evt_holodeck1
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}
	option ={ # PLAY THE PROGRAMME
		name = "STH_expedition.477.a"
		country_event = { id = STH_expedition.2031 days = 5 random = 2 }
		event_target:sth_expedition_Leader_flag = {
			add_experience = -100
		}
	}
	option ={ # SWITCH OFF
	name = "STH_expedition.477.b"
	country_event = { id = STH_expedition.2007 days = 360 random = 30 }
	}
}

# Flag Holodeck moriarty Y2  - PLAY
country_event = {
	id = STH_expedition.2031
	title = "STH_expedition.478.name"
	desc = "STH_expedition.478.desc"
	picture = sth_GFX_evt_holodeck1
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}
	option ={ # PLAY THE PROGRAMME
		name = "STH_expedition.478.a"
		hidden_effect = { 
			random_list = {
				60 = { country_event = { id = STH_expedition.2032 days = 5  } } # RELIC
				40 = { country_event = { id = STH_expedition.2033 days = 5  } } # FAIL
			}
		}
	}
	option ={ # DESTROY THE PROGRAM
	name = "STH_expedition.478.b"
	country_event = { id = STH_expedition.2007 days = 360 random = 30 }
	add_resource = { influence = 100 physics_research = -150 }
	}
}

# Flag Holodeck moriarty Y2  - SUCCESS
country_event = {
	id = STH_expedition.2032
	title = "STH_expedition.479.name"
	desc = "STH_expedition.479.desc"
	picture = sth_GFX_evt_holodeck1
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}
	option ={ # NO RELIC
		name = "STH_expedition.479.a"
		trigger = {
			has_relic = r_hologram_bottle
		}
		add_resource = { physics_research = 100 engineering_research = 50 } 
		hidden_effect = { country_event = { id = STH_expedition.2007 days = 360 random = 30 } }
	}
	option ={ # RELIC
		name = "STH_expedition.479.b"
		trigger = {
			NOT = {
				has_relic = r_hologram_bottle
			}
		}
		add_relic = r_hologram_bottle
		hidden_effect = { country_event = { id = STH_expedition.2007 days = 360 random = 30 } }
	}
}

# Flag Holodeck moriarty Y2  - FAIL
country_event = {
	id = STH_expedition.2033
	title = "STH_expedition.2033.name"
	desc = "STH_expedition.2033.desc"
	picture = sth_GFX_evt_holodeck1
	is_triggered_only = yes
	hide_window = no
	trigger = { NOT = { has_country_flag = sth_expedition_recalled } }
	immediate ={}
	option ={ # NO RELIC
		name = "STH_expedition.2033.a"
		hidden_effect = { country_event = { id = STH_expedition.2007 days = 360 random = 30 } }
	}
}

country_event = {
	id = STH_expedition.5000
	hide_window = yes
	is_triggered_only = yes

	immediate = {
		random_system = {
			limit = {
				has_star_flag = "deep_space_system"
			}
			save_event_target_as = deep_space_system
		}
	}
}

country_event = {
	id = STH_expedition.5001
	title = "STH_expedition.5001.name"
	desc = "STH_expedition.5001.desc"
	picture = sth_GFX_evt_caretakerHome
	is_triggered_only = yes

	immediate = {
		random_owned_fleet = { 
			limit = { has_fleet_flag = sth_expedition_ship }
			set_location = root.capital_scope
			set_event_locked = no
		}
		random_owned_fleet = {
			limit = {
				has_fleet_flag = sth_expedition_hero_ship
			}
			remove_fleet_flag = sth_expedition_hero_ship 
			set_event_locked = no
			set_location = { target = owner.capital_scope distance = 300 angle = 100 }
			set_leader = event_target:sth_expedition_hero_Leader_1
			leader = { add_trait = leader_trait_immortal }
		}
		random_owned_ship = {
			limit = {
				has_ship_flag = sth_expedition_hero_ship
			}
			fleet = { 		
				remove_fleet_flag = sth_expedition_hero_ship 
				set_event_locked = no
				set_location = { target = owner.capital_scope distance = 300 angle = 100 }
				set_leader = event_target:sth_expedition_hero_Leader_1
			}
			remove_ship_flag = sth_expedition_hero_ship
		}
		random_owned_ship = {
			limit = { has_ship_flag = sth_expedition_ship }
			remove_ship_flag = sth_expedition_ship
			fleet = { 		
				remove_fleet_flag = sth_expedition_ship 
				set_event_locked = no
				set_location = { target = owner.capital_scope distance = 300 angle = 100 }
				set_leader = event_target:sth_expedition_Leader_1
				leader = { add_trait = leader_trait_immortal }
			}
		}
		random_owned_ship = {
			limit = { has_ship_flag = sth_expedition_ship2 }
			remove_ship_flag = sth_expedition_ship2
			fleet = { 		
				remove_fleet_flag = sth_expedition_ship2 
				set_event_locked = no
				set_location = { target = owner.capital_scope distance = 300 angle = 100 }
				set_leader = event_target:sth_expedition_Leader_2
				leader = { add_trait = leader_trait_immortal }
			}
		}
		random_owned_ship = {
			limit = { has_ship_flag = sth_expedition_ship3 }
			remove_ship_flag = sth_expedition_ship3
			fleet = { 		
				remove_fleet_flag = sth_expedition_ship3
				set_event_locked = no
				set_location = { target = owner.capital_scope distance = 300 angle = 100 }
				set_leader = event_target:sth_expedition_Leader_3
				leader = { add_trait = leader_trait_immortal }
			}
		}
	}

	option = {
		name = sth_congratulations
		set_timed_country_flag = { flag = sth_expedition_recalled days = 400 }
		hidden_effect = {
			remove_country_flag = STH_expedition_hero1_year_1
			remove_country_flag = STH_expedition_hero1_year_2
			remove_country_flag = STH_expedition_hero1_year_3
			remove_country_flag = sth_expedition_hero_1_ongoing
			remove_country_flag = STH_expedition1_year_1
			remove_country_flag = STH_expedition1_year_2
			remove_country_flag = STH_expedition1_year_3
			remove_country_flag = STH_expedition2_year_1
			remove_country_flag = STH_expedition2_year_2
			remove_country_flag = STH_expedition2_year_3
			remove_country_flag = STH_expedition3_year_1
			remove_country_flag = STH_expedition3_year_2
			remove_country_flag = STH_expedition3_year_3
			remove_country_flag = STH_expedition_flag_year_1
			remove_country_flag = STH_expedition_flag_year_2
			remove_country_flag = STH_expedition_flag_year_3
			remove_country_flag = STH_expedition_hero_leader_1_dead
			remove_country_flag = STH_expedition_hero_leader_2_dead
			remove_country_flag = STH_expedition_hero_leader_3_dead
			remove_country_flag = STH_expedition_leader_1_dead
			remove_country_flag = STH_expedition_leader_2_dead
			remove_country_flag = STH_expedition_leader_3_dead
			remove_country_flag = sth_expedition_1_ongoing
			remove_country_flag = sth_expedition_2_ongoing
			remove_country_flag = sth_expedition_3_ongoing
		}
	}
}
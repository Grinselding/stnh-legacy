########################################
#
# Hunter Mechanics
# Written & provided by Armaman
#
# Version 1.0 - created (26.07.2020)
#
########################################

namespace = STH_hirogen_mechanics
#Trigger for prey pop dying
country_event = {
	id = STH_hirogen_mechanics.1
	hide_window = yes
	is_triggered_only = yes
	trigger = {
		farmer_hunter = yes		
		any_owned_pop = {
			has_job = prey			
		}
	}	
	immediate = {
		every_owned_planet = {			
			limit = {
				any_owned_pop = {
					has_job = prey			
				}
			}			
			random_list = {
				50 = { } 
				50 = { 
					save_event_target_as = STH_hunter_mechanics_prey_pop_planet
					random_owned_pop = {
						limit = {							
							has_job = prey							
						}
						save_event_target_as = STH_hunter_mechanics_prey_pop						
					}
					if = {
						limit = {
							exists = owner
						}
						owner = { country_event = { id = STH_hirogen_mechanics.2 days = 7 random = 31} }
					}
				}
			}
		}		
	}
}

#Prey pop dies
country_event = {
	id = STH_hirogen_mechanics.2
	desc = "STH_hirogen_mechanics.2.desc"
	title = "STH_hirogen_mechanics.2.name"
	picture = "sth_GFX_evt_memorial1"
	show_sound = event_ghost_town
	is_triggered_only = yes
	location = event_target:STH_hunter_mechanics_prey_pop_planet
	trigger = {				
		farmer_hunter = yes
	}	
	option = {
		name = "STH_hirogen_mechanics.2.a"	
		event_target:STH_hunter_mechanics_prey_pop= {
			kill_pop = yes
		}
	}
}

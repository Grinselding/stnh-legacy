# ###########################################
# #
# # Nemesis Espionage Events
# # Written by Gemma Thomson
# #
# ###########################################

# namespace = operation

# # SUBTERFUGE (.1XXX)
# # SABOTAGE (.2XXX)
# # MANIPULATION (.3XXX)
# # PROVOCATION (.4XXX)
# # RANDOM EVENTS (.5XXX)

# @OperationBribeCost1 = -400
# @OperationBribeCost2 = -600
# @OperationBribeCost3 = -800

# # Number of days before a single event should (ideally) stand a chance of coming up again.
# @RandomOperationEventTimer = 600
# @RandomOperationEventTimerLong = 1000

# ###########################
# # SUBTERFUGE
# ###########################

# ### ACQUIRE ASSET

# # Install operatives
# event = {
#     id = operation.1000
#     title = operation.name
#     desc = operation.1000.desc
#     picture = GFX_evt_ancient_artifact #TODO_NEM: appeases the error log
#     show_sound = event_default #TODO_NEM: appeases the error log
#     is_triggered_only = yes

#     immediate = {
#         # GetOperationName = yes
#     }

#     option = { name = OK } #TODO_NEM: appeases the error log
# }
# # Target identified
# event = {
#     id = operation.1001
#     title = operation.name
#     desc = operation.1001.desc
#     picture = GFX_evt_ancient_artifact #TODO_NEM: appeases the error log
#     show_sound = event_default #TODO_NEM: appeases the error log
#     is_triggered_only = yes

#     immediate = {}

#     option = { name = OK } #TODO_NEM: appeases the error log
# }
# # Asset approached
# event = {
#     id = operation.1002
#     title = operation.name
#     desc = operation.1002.desc
#     picture = GFX_evt_ancient_artifact #TODO_NEM: appeases the error log
#     show_sound = event_default #TODO_NEM: appeases the error log
#     is_triggered_only = yes

#     immediate = {
#         random_list = { #TODO_NEM: fudging it
#             95 = {
#                 modifier = {
#                     factor = 1.2
#                     leader = { has_trait = leader_trait_adaptable } #TODO_NEM
#                 }
#                 owner = {
#                     country_event = { id = operation.1005 days = 0 }
#                 }
#             }
#             5 = {
#                 owner = {
#                     country_event = { id = operation.1006 days = 0 }
#                 }
#             }
#         }
#     }

#     option = { name = OK } #TODO_NEM: appeases the error log
# }
# country_event = {
#     id = operation.1005
#     title = operation.name
#     desc = operation.1005.desc #TODO_NEM: could be dynamic text
#     picture = GFX_evt_ancient_artifact #TODO_NEM
#     show_sound = event_default

#     is_triggered_only = yes

#     immediate = {
#         # acquire_asset_random = yes
#     }

#     option = { #Costly but safe extradition
#         name = operation.1005.a
#         add_resource = {
#             energy = -1000
#         }
#     }
#     option = { #Cheaper but riskier extradition
#         name = operation.1005.b
#         add_resource = {
#             energy = -500
#         }
#     }
#     option = { #Asset extradites themself
#         name = operation.1005.c
#         # trigger = {
#         #     from.target.owner = { is_gestalt = no }
#         # }
#     }
# }
# country_event = {
#     id = operation.1006
#     title = operation.name
#     desc = operation.1006.desc #TODO_NEM: could be dynamic text
#     picture = GFX_evt_ancient_artifact #TODO_NEM
#     show_sound = event_default

#     is_triggered_only = yes

#     immediate = { }

#     option = {
#         name = UNFORTUNATE
#     }
# }

# ###########################
# # RANDOM EVENTS
# ###########################
# # Triggered by operation_random_events_generic via e.g. operation_random_events_regular_regular
# # from = operation

# ### CONTACT LOST (regular vs. any)

# country_event = {
#     id = operation.5000
#     title = operation.5000.name
#     desc = operation.5000.desc
#     picture = GFX_evt_financial_instruments #TODO_NEM
#     show_sound = event_radio_chatter
#     # is_triggered_only = yes

#     trigger = {
#         always = no #TODO_NEM: Awaiting Operations script
#         # is_gestalt = no
#     }

#     immediate = {
#         from = { set_espionage_operation_progress_locked = yes }
#         set_timed_country_flag = {
#             flag = recent_op_contact_lost
#             days = @RandomOperationEventTimer
#         }
#     }

#     option = { #Find operatives
#         name = operation.5000.a
#         from.spynetwork = {
#             add_modifier = { modifier = spynetwork_extracting_operative } #Removed in subsequent events, or when the Spy Network disbands - whichever comes first
#         }
#         hidden_effect = {
#             random_list = {
#                 8 = {
#                     country_event = { id = operation.5001 days = 44 } #Re-establish contact
#                 }
#                 2 = {
#                     modifier = {
#                         factor = 0.5
#                         from = {
#                             NOR = {
#                                 has_espionage_category = op_cat_sabotage
#                                 has_espionage_category = op_cat_provocation
#                             }
#                         }
#                     }
#                     country_event = { id = operation.5002 days = 62 } #Contact lost
#                 }
#             }
#         }
#         ai_chance = {
#             factor = 10
#             # modifier = {
#             #     factor = 0
#             #     #TODO_NEM: AUG-24820 from.spynetwork = { spy power available < 12 }
#             # }
#             # modifier = {
#             #     factor = 2
#             #     #TODO_NEM: AUG-24820 from.spynetwork = { spy power available > 40 }
#             # }
#         }
#     }
#     option = {
#         name = DISAVOW
#         custom_tooltip = operation.5000.b.tooltip
#         from = {
#             add_modifier = { modifier = operation_contact_lost }
#         }
#         hidden_effect = {
#             from = { set_espionage_operation_progress_locked = no }
#         }
#         ai_chance = {
#             factor = 4
#         }
#     }
# }
# #Contact Lost: Re-established
# country_event = {
#     id = operation.5001
#     title = operation.5001.name
#     desc = operation.5001.desc
#     picture = GFX_evt_financial_instruments #TODO_NEM
#     show_sound = event_radio_chatter
#     is_triggered_only = yes

#     trigger = {
#         #TODO_NEM AUG-24820: 'Is running an operation' check, to ensure this Operation is still valid.
#     }

#     immediate = {
#         fromfrom.spynetwork = { remove_modifier = spynetwork_extracting_operative }
#         fromfrom = { set_espionage_operation_progress_locked = no }
#     } #TODO_NEM: See how this feels/if it works, otherwise stick it in 'after = {}'

#     option = { name = GOOD }
# }
# #Contact Lost: Proper Lost
# country_event = {
#     id = operation.5002
#     title = operation.5000.name
#     desc = operation.5002.desc
#     picture = GFX_evt_financial_instruments #TODO_NEM
#     show_sound = event_radio_chatter
#     is_triggered_only = yes

#     trigger = {
#         #TODO_NEM AUG-24820: 'Is running an operation' check, to ensure this Operation is still valid.
#     }

#     immediate = {
#         remove_modifier = spynetwork_extracting_operative
#     }

#     option = {
#         name = UNFORTUNATE
#         add_modifier = { modifier = operation_contact_lost }
#     }

#     after = { fromfrom = { set_espionage_operation_progress_locked = no } }
# }

# ### SPOOKED (operatives get paranoid; regular vs. regular)

# country_event = {
#     id = operation.5005
#     title = operation.5005.name
#     desc = operation.5005.desc
#     picture = GFX_evt_financial_instruments #TODO_NEM
#     show_sound = event_whispering
#     # is_triggered_only = yes

#     trigger = {
#         always = no #TODO_NEM: Awaiting Operations script
#         # is_gestalt = no
#         # from.target.owner = { is_gestalt = no }
#     }

#     immediate = {
#         from = { set_espionage_operation_progress_locked = yes }
#         set_timed_country_flag = {
#             flag = recent_op_operative_spooked
#             days = @RandomOperationEventTimer
#         }
#     }

#     option = { #Deny extra resources
#         name = operation.5005.a
#         hidden_effect = {
#             random_list = {
#                 6 = { }
#                 4 = {
#                     modifier = {
#                         factor = 0.5
#                         from = {
#                             NOR = {
#                                 has_espionage_category = op_cat_subterfuge
#                                 has_espionage_category = op_cat_provocation
#                             }
#                         }
#                     }
#                     operation_random_events_coverblown = yes
#                 }
#             }
#         }
#         ai_chance = {
#             factor = 10
#             # modifier = {
#             #     factor = 0
#             #     #TODO_NEM: AUG-24820 from.spynetwork = { spy power available < 12 }
#             # }
#         }
#     }
#     option = { #Grant resources
#         name = operation.5005.b
#         from = {
#             add_modifier = { modifier = spynetwork_operative_spooked days = 360 }
#         }
#         hidden_effect = {
#             random_list = {
#                 9 = { }
#                 1 = {
#                     modifier = {
#                         factor = 3
#                         from = {
#                             OR = {
#                                 has_espionage_category = op_cat_subterfuge
#                                 has_espionage_category = op_cat_provocation
#                             }
#                         }
#                     }
#                     operation_random_events_coverblown = yes
#                 }
#             }
#         }
#         ai_chance = {
#             factor = 10
#             # modifier = {
#             #     factor = 3
#             #     #TODO_NEM: AUG-24820 from.spynetwork = { spy power available > 40 }
#             # }
#         }
#     }
#     option = { #Bribery? (High income)
#         name = operation.5005.c
#         allow = {
#             has_monthly_income = {
#                 resource = energy
#                 value >= 40
#             }
#             from.target.owner = {
#                 NOR = {
#                     has_ethic = ethic_fanatic_xenophobe
#                     has_ethic = ethic_fanatic_authoritarian
#                 }
#             }
#         }
#         add_resource = { energy = @OperationBribeCost3 }
#         from.target = {
#             add_modifier = { modifier = spynetwork_operative_spooked }
#         }
#         ai_chance = {
#             factor = 10
#             # modifier = {
#             #     factor = 0.5
#             #     #TODO_NEM: AUG-24820 from.spynetwork = { spy power available < 40 }
#             # }
#         }
#     }
#     option = { #Bribery? (Medium income)
#         name = operation.5005.c
#         allow = {
#             has_monthly_income = {
#                 resource = energy
#                 value >= 10
#                 value < 40
#             }
#             from.target.owner = {
#                 NOR = {
#                     has_ethic = ethic_fanatic_xenophobe
#                     has_ethic = ethic_fanatic_authoritarian
#                 }
#             }
#         }
#         add_resource = { energy = @OperationBribeCost2 }
#         from.target = {
#             add_modifier = { modifier = spynetwork_operative_spooked }
#         }
#         ai_chance = {
#             factor = 10
#             # modifier = {
#             #     factor = 0.5
#             #     #TODO_NEM: AUG-24820 from.spynetwork = { spy power available < 40 }
#             # }
#         }
#     }
#     option = { #Bribery? (Low income)
#         name = operation.5005.c
#         allow = {
#             has_monthly_income = {
#                 resource = energy
#                 value < 10
#             }
#             from.target.owner = {
#                 NOR = {
#                     has_ethic = ethic_fanatic_xenophobe
#                     has_ethic = ethic_fanatic_authoritarian
#                 }
#             }
#         }
#         add_resource = { energy = @OperationBribeCost1 }
#         from.target = {
#             add_modifier = { modifier = spynetwork_operative_spooked }
#         }
#         ai_chance = {
#             factor = 10
#             # modifier = {
#             #     factor = 0.5
#             #     #TODO_NEM: AUG-24820 from.spynetwork = { spy power available < 40 }
#             # }
#         }
#     }

#     after = { from = { set_espionage_operation_progress_locked = no } }
# }

# ### APPREHENDED BY HUNTER-SEEKER DRONES (regular vs. gestalt)

# country_event = {
#     id = operation.5010
#     title = operation.5010.name
#     desc = operation.5010.desc
#     picture = GFX_evt_hive_mind #TODO_NEM
#     show_sound = event_default
#     # is_triggered_only = yes

#     trigger = {
#         always = no #TODO_NEM: Awaiting Operations script
#         # is_gestalt = no
#         # from.target.owner = { is_gestalt = yes }
#     }

#     immediate = {
#         from = {
#             set_espionage_operation_progress_locked = yes
#             target.owner = {
#                 set_country_flag = drones_apprehended_operatives@root
#             }
#         }
#         set_timed_country_flag = {
#             flag = recent_op_apprehended_by_drones
#             days = @RandomOperationEventTimer
#         }
#     }

#     # The friendlier the target, the less chance of mishap.
#     option = { #Needs diplomacy (very friendly)
#         name = operation.5010.a
#         exclusive_trigger = {
#             OR = {
#                 opinion_level = {
#                     who = from.target.owner
#                     level >= excellent
#                 }
#                 is_improving_relations_with = from.target.owner
#             }
#         }
#         random_list = {
#             8 = { country_event = { id = operation.5011 days = 4 } }
#             2 = { operation_random_events_coverblown = yes }
#         }
#     }
#     option = { #Needs diplomacy (friendly)
#         name = operation.5010.a
#         exclusive_trigger = {
#             opinion_level = {
#                 who = from.target.owner
#                 level >= neutral
#             }
#         }
#         random_list = { #TODO_NEM: check how this appears in-game
#             6 = { country_event = { id = operation.5011 days = 4 } }
#             4 = { operation_random_events_coverblown = yes }
#         }
#     }
#     option = { #Needs diplomacy (unfriendly)
#         name = operation.5010.a
#         exclusive_trigger = {
#             OR = {
#                 opinion_level = {
#                     who = from.target.owner
#                     level < neutral
#                 }
#                 from.target.owner = {
#                     has_country_flag = drones_apprehended_operatives@root
#                 }
#             }
#         }
#         random_list = {
#             4 = { country_event = { id = operation.5011 days = 4 } }
#             6 = { operation_random_events_coverblown = yes }
#         }
#     }

#     after = {
#         from = { set_espionage_operation_progress_locked = no }
#     }
# }
# country_event = {
#     id = operation.5011
#     title = operation.5011.name
#     desc = operation.5011.desc
#     picture = GFX_evt_hive_mind #TODO_NEM
#     show_sound = event_default
#     is_triggered_only = yes

#     immediate = { }

#     option = {
#         name = GOOD
#     }
# }

# ### MULTI-PHASIC FORCE FIELDS (drones suffer inteference; gestalt vs. regular)

# country_event = {
#     id = operation.5015
#     title = operation.5015.name
#     desc = operation.5015.desc
#     picture = GFX_evt_derelict_interior #TODO_NEM AUG-25618
#     show_sound = event_default #TODO_NEM AUG-25698
#     # is_triggered_only = yes

#     trigger = { always = no } #TODO_NEM: Awaiting Operations script

#     immediate = {
#         set_timed_country_flag = {
#             flag = recent_op_multiphasic_interference
#             days = @RandomOperationEventTimer
#         }
#     }

#     option = {
#         name = operation.5015.a
#         hidden_effect = {
#             locked_random_list = {
#                 2 = {
#                     modifier = {
#                         factor = 1.5
#                         relative_power = {
#                             who = from.target.owner
#                             category = technology
#                             value > equivalent
#                         }
#                     }
#                     # modifier = {
#                     #     factor = 1.5
#                     #     #TODO_NEM: decryption/encryption comparison check
#                     # }
#                     #No effect; Operation continues
#                 }
#                 2 = {
#                     modifier = {
#                         factor = 0.5
#                         relative_power = {
#                             who = from.target.owner
#                             category = technology
#                             value > superior
#                         }
#                     }
#                     from = { #Operation scope
#                         add_modifier = {
#                             modifier = operation_multiphasic_interference
#                             days = -1
#                         }
#                     }
#                 }
#                 1 = {
#                     country_event = { id = operation.5016 days = 3 } #Contact lost
#                 }
#             }
#         }
#     }
# }
# # Multi-Phasic Force Fields: contact lost
# country_event = {
#     id = operation.5016
#     title = operation.5016.name
#     desc = operation.5016.desc
#     picture = GFX_evt_derelict_interior #TODO_NEM AUG-25618
#     show_sound = event_default #TODO_NEM AUG-25698
#     is_triggered_only = yes

#     immediate = { }

#     option = {
#         name = UNFORTUNATE
#     }

#     after = {
#         if = { #If the target is also spying on ROOT, the target may earn an Asset.
#             limit = {
#                 fromfrom.target.owner = {
#                     has_spy_network = yes #TODO_NEM AUG-24366: currently just checks if *anyone* has a spy network
#                 }
#             }
#             fromfrom.target.owner = {
#                 country_event = { id = espionage.1040 days = 0 } #A Surprise Catch
#             }
#         }
#     }
# }

# ### MISTAKEN FOR FOOD (gestalt vs. hive)

# country_event = {
#     id = operation.5020
#     title = operation.5020.name
#     desc = operation.5020.desc
#     picture = GFX_evt_aggressive_flora
#     show_sound = event_alien_nature
#     # is_triggered_only = yes

#     trigger = { always = no } #TODO_NEM: Awaiting Operations script

#     immediate = {
#         set_timed_country_flag = {
#             flag = recent_op_drones_hunted
#             days = @RandomOperationEventTimer
#         }
#     }

#     option = { #Drop a lure (food)
#         name = operation.5020.a
#         exclusive_trigger = {
#             from.target.owner = { is_lithoid_empire = no }
#             resource_stockpile_compare = {
#                 resource = food
#                 value > 400
#             }
#         }
#         add_resource = { food = -200 }
#     }
#     option = { #Drop a lure (lithoids)
#         name = operation.5020.a
#         exclusive_trigger = {
#             from.target.owner = { is_lithoid_empire = yes }
#             resource_stockpile_compare = {
#                 resource = minerals
#                 value > 400
#             }
#         }
#         add_resource = { minerals = -200 }
#     }
#     option = { #Can't afford a lure
#         name = operation.5020.b
#         exclusive_trigger = {
#             OR = {
#                 AND = {
#                     from.target.owner = { is_lithoid_empire = no }
#                     resource_stockpile_compare = {
#                         resource = food
#                         value <= 400
#                     }
#                 }
#                 AND = {
#                     from.target.owner = { is_lithoid_empire = yes }
#                     resource_stockpile_compare = {
#                         resource = minerals
#                         value <= 400
#                     }
#                 }
#             }
#         }
#         from.spynetwork = {
#             add_modifier = {
#                 modifier = spynetwork_drones_hunted
#                 days = 280
#             }
#         }
#     }
# }

# ### TOXIC FIELD WORK (hive vs. hive)

# country_event = {
#     id = operation.5025
#     title = operation.5025.name
#     desc = {
#         text = operation.5025.desc
#         trigger = {
#             from.target.owner = {
#                 NOT = { has_country_flag = toxic_field_studied_by@root }
#             }
#         }
#     }
#     desc = {
#         text = operation.5025.desc.repeat
#         trigger = {
#             from.target.owner = { has_country_flag = toxic_field_studied_by@root }
#         }
#     }
#     picture = GFX_evt_toxic
#     show_sound = event_default
#     # is_triggered_only = yes

#     trigger = { always = no } #TODO_NEM: Awaiting Operations script

#     immediate = {
#         from = { set_espionage_operation_progress_locked = yes }
#         set_timed_country_flag = {
#             flag = recent_op_toxic_field
#             days = @RandomOperationEventTimerLong
#         }
#         from.target.owner = { save_event_target_as = toxic_field_country }
#     }

#     option = {
#         name = operation.5025.a
#         enable_special_project = {
#             name = "TOXIC_OPERATION_PROJECT"
#             location = from.target
#             owner = root
#         }
#         ai_chance = {
#             factor = 10
#         }
#     }
#     option = {
#         name = operation.5025.b
#         custom_tooltip = operation.5025.b.tooltip
#         from = { set_espionage_operation_progress_locked = no }
#         hidden_effect = {
#             random_list = {
#                 2 = {
#                     modifier = {
#                         factor = 0.6
#                         OR = {
#                             has_trait = trait_adaptive
#                             has_trait = trait_adaptive_lithoid
#                         }
#                     }
#                     modifier = {
#                         factor = 0.3
#                         OR = {
#                             has_trait = trait_extremely_adaptive
#                             has_trait = trait_survivor
#                         }
#                     }
#                     from.spynetwork = {
#                         add_modifier = {
#                             modifier = spynetwork_toxic_field
#                             days = 920 #Slightly less than @RandomOperationEventTimerLong. If changed, also update the time-delay directly below
#                         }
#                     }
#                     country_event = { id = operation.5027 days = 920 }
#                 }
#                 3 = {
#                     #No effect
#                 }
#             }
#         }
#         ai_chance = {
#             factor = 5
#         }
#     }
# }
# #Triggered upon completion of TOXIC_OPERATION_PROJECT
# #fromfromfrom = operation; toxic_field_country = operation.target
# country_event = {
#     id = operation.5026
#     title = operation.5026.name
#     desc = operation.5026.desc
#     picture = GFX_evt_toxic
#     show_sound = event_default
#     is_triggered_only = yes

#     trigger = {
#         #TODO_NEM AUG-24820: 'Is running an operation' check, to ensure this Operation is still valid.
#         exists = event_target:toxic_field_country
#     }

#     immediate = {
#         fromfromfrom = { set_espionage_operation_progress_locked = no }
#         event_target:toxic_field_country = { set_country_flag = toxic_field_studied_by@root }
#     }

#     option = {
#         name = GOOD
#     }
# }
# #Triggered on a time-delay after operation.5025
# country_event = {
#     id = operation.5027
#     hide_window = yes
#     is_triggered_only = yes

#     trigger = {
#         exists = event_target:toxic_field_country
#         has_spy_network = yes #TODO_NEM AUG-24366: currently just checks if *anyone* has a spy network; needs to check if ROOT still has one on event_target:toxic_field_country
#     }

#     immediate = {
#         event_target:toxic_field_country = { set_country_flag = toxic_field_studied_by@root }
#     }
# }

# ### EXCESS NOISE (machine vs. machine)

# country_event = {
#     id = operation.5030
#     title = operation.5030.name
#     desc = {
#         text = operation.5030.desc
#         trigger = {
#             from.target.owner = {
#                 NOT = { has_country_flag = excess_noise_studied_by@root }
#             }
#         }
#     }
#     desc = {
#         text = operation.5030.desc.repeat
#         trigger = {
#             from.target.owner = {
#                 has_country_flag = excess_noise_studied_by@root
#             }
#         }
#     }
#     picture = GFX_evt_sapient_AI
#     show_sound = event_scanner
#     # is_triggered_only = yes

#     trigger = { always = no } #TODO_NEM: Awaiting Operations script

#     immediate = {
#         from = { set_espionage_operation_progress_locked = yes }
#         set_timed_country_flag = {
#             flag = recent_op_excess_noise
#             days = @RandomOperationEventTimerLong
#         }
#         from.target.owner = { save_event_target_as = excess_noise_country }
#     }

#     option = {
#         name = operation.5030.a
#         enable_special_project = {
#             name = "NOISE_OPERATION_PROJECT"
#             location = from.target
#             owner = root
#         }
#         ai_chance = {
#             factor = 10
#         }
#     }
#     option = {
#         name = operation.5030.b
#         custom_tooltip = operation.5030.b.tooltip
#         from = { set_espionage_operation_progress_locked = no }
#         hidden_effect = {
#             random_list = {
#                 2 = {
#                     modifier = {
#                         factor = 0.6
#                         OR = {
#                             has_trait = trait_adaptive
#                             has_trait = trait_adaptive_lithoid
#                         }
#                     }
#                     modifier = {
#                         factor = 0.3
#                         OR = {
#                             has_trait = trait_extremely_adaptive
#                             has_trait = trait_survivor
#                         }
#                     }
#                     from.spynetwork = {
#                         add_modifier = {
#                             modifier = spynetwork_excess_noise
#                             days = 920 #Slightly less than @RandomOperationEventTimerLong. If changed, also update the time-delay directly below
#                         }
#                     }
#                     country_event = { id = operation.5032 days = 920 }
#                 }
#                 3 = {
#                     #No effect
#                 }
#             }
#         }
#         ai_chance = {
#             factor =5
#         }
#     }
# }
# #Triggered upon completion of NOISE_OPERATION_PROJECT
# #fromfromfrom = operation; excess_noise_country = operation.target
# country_event = {
#     id = operation.5031
#     title = operation.5031.name
#     desc = operation.5031.desc
#     picture = GFX_evt_sapient_AI
#     show_sound = event_default
#     is_triggered_only = yes

#     trigger = {
#         #TODO_NEM AUG-24820: 'Is running an operation' check, to ensure this Operation is still valid.
#         exists = event_target:excess_noise_country
#     }

#     immediate = {
#         fromfromfrom = { set_espionage_operation_progress_locked = no }
#         event_target:excess_noise_country = { set_country_flag = excess_noise_studied_by@root }
#     }

#     option = {
#         name = GOOD
#     }
# }
# #Triggered on a time-delay after operation.5030
# country_event = {
#     id = operation.5032
#     hide_window = yes
#     is_triggered_only = yes

#     trigger = {
#         exists = event_target:excess_noise_country
#         has_spy_network = yes #TODO_NEM AUG-24366: currently just checks if *anyone* has a spy network; needs to check if ROOT still has one on event_target:excess_noise_country
#     }

#     immediate = {
#         event_target:excess_noise_country = { set_country_flag = excess_noise_studied_by@root }
#     }
# }
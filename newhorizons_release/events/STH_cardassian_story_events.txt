###################################
#
# Cardassian Story Events
#
# Written by Walshicus, Russ, Felis
#
###################################
namespace = STH_cardassian_story

#Yearly Checks
event = {
	id = STH_cardassian_story.0
	hide_window = yes
	is_triggered_only = yes
	immediate = { 
		random_country = {
			limit = { has_country_flag = cardassian_union is_normal_country = yes }
			save_event_target_as = cardassian_country
			if = {
				limit = { 
					recently_lost_war = yes
					NOT = { has_country_flag = dominionChoice }
					exists = event_target:the_dominion
					has_communications = event_target:the_dominion
					any_neighbor_country = {
						OR  = {
							relative_power = { who = event_target:cardassian_country category = all value > equivalent }
							relative_power = { who = event_target:cardassian_country category = all value > superior }
						}
						NOT = { is_same_value = event_target:the_dominion }
					}
				}
				random_list = {
					10 = { country_event = { id = STH_cardassian_story.500 days = 80 random = 36 } set_country_flag = dominionChoice } 
					10 = { set_timed_country_flag = { flag = dominionChoice days = 800 } }
				}
			}
		}
	}
}

# Lightship Event - Created by Felis
# Spawning Event (HIDDEN)
event = {
	id = STH_cardassian_story.100
	hide_window = yes
	
	is_triggered_only = yes

	trigger = {
		years_passed >= 10
		NOT = { has_global_flag = lightship_spawned }
		any_country = {
			cardassian_empires = yes
			OR = {
				any_country = {
					has_country_flag = bajoran_republic
					has_communications = prev
				}
				any_country = {
					any_planet_within_border = {
						has_planet_flag = planet_bajor
					}
					has_communications = prev
				}
				any_planet_within_border = {
					has_planet_flag = planet_bajor
				}
			}
		}	
	}

	immediate = {
		log = "STH_cardassian_story.105: ancient contact"
		random_list = {
			100 = {}
			5 = {
				log = "STH_cardassian_story.105: ancient contact spawned"
				set_global_flag = lightship_spawned
				random_country = {
					limit = {
						cardassian_empires = yes
					}
					capital_scope = {
						planet_event = { id = STH_cardassian_story.101 days = 1 random = 350 }
					}
				}
			}
		}
	}
}

# Found a lightship crashsite
planet_event = {
	id = STH_cardassian_story.101
	title = "STH_cardassian_story.101.name"
	desc = "STH_cardassian_story.101.desc"
	picture = GFX_evt_bajoran_lightship
	show_sound = event_scanner
	location = ROOT
	trackable = yes

	is_triggered_only = yes

	immediate = {
		set_planet_flag = lightship_crashsite	
	}

	option = { 
		name = STH_cardassian_story.101.a # start digging
		ai_chance = { factor = 10 }
		enable_special_project = {
			name = "LIGHTSHIP_CRASHSITE_PROJECT"
			location = root
			owner = root.owner
		}
	}
	option = { 
		name = STH_cardassian_story.101.b # it's a waste of time
		ai_chance = { factor = 1 }
		owner = { 
			add_resource = { influence = 20 }
		}
	}
}

ship_event = {
	id = STH_cardassian_story.102
	title = "STH_cardassian_story.102.name"
	desc = "STH_cardassian_story.102.desc"
	picture = GFX_evt_cardassian_archaeologist
	show_sound = event_construction
	location = ROOT
	trackable = yes

	is_triggered_only = yes

	after = {
		owner = {
			add_monthly_resource_mult = {
				resource = engineering_research
				value = @tier3researchreward
				min = @tier3researchmin
				max = @tier3researchmax
			}
		}
	}

	option = {
		name = STH_cardassian_story.102.a # tell bajorans
		allow = {
			owner = {
				NOT = {
					any_planet_within_border = {
						has_planet_flag = planet_bajor
					}
				}
			}
		}
		custom_tooltip = STH_cardassian_story.102.a.tooltip
		owner = {
			add_monthly_resource_mult = {
				resource = unity
				value = @tier2unityreward
				min = @tier2unitymin
				max = @tier2unitymax
			}
		}
		ai_chance = {
			factor = 1
			modifier = {
				factor = 10
				OR = {
					owner = { has_country_flag = hebitian_union }
					owner = { has_country_flag = hebitian_country }
				}
			}
		}
		hidden_effect = {
			owner = {
				save_event_target_as = cardassian_country
				set_country_flag = lightship_revealed
			}
			random_country = {
				limit = {
					if = {
						limit = {
							any_country = {
								has_country_flag = bajoran_republic
								is_subject_type = member_world
							}
						}
						has_country_flag = united_federation_of_planets
					}
					else_if = {
						limit = {
							any_country = {
								has_country_flag = bajoran_republic
								is_subject_type = member_world_isu
							}
						}
						has_country_flag = interstellar_union
					}
					else_if = {
						limit = {
							any_country = {
								has_country_flag = bajoran_republic
								is_subject_type = dominion_member
							}
						}
						has_country_flag = the_dominion
					}
					else = {
						has_country_flag = bajoran_republic
					}
				}
				country_event = { id = STH_cardassian_story.105 }
			}
		}
	}
	option = {
		name = STH_cardassian_story.102.b # don't tell bajorans
		ai_chance = {
			factor = 1
			modifier = {
				factor = 10
				owner = { has_country_flag = cardassian_union }
			}
		}
		owner = {
			add_resource = { influence = 80 }
		}
		hidden_effect = {
			owner = {
				set_country_flag = lightship_hidden # doesn't do anything atm, needed for future follow-up events
			}
		}
	}
}

# It's a Hebitian burial vault
ship_event = {
	id = STH_cardassian_story.103
	title = "STH_cardassian_story.103.name"
	desc = "STH_cardassian_story.103.desc"
	picture = GFX_evt_cardassian_archaeologist
	show_sound = event_construction
	location = ROOT
	trackable = yes

	is_triggered_only = yes

	option = { 
		name = STH_cardassian_story.103.a # keep artifacts in a museum
		owner = {
			add_monthly_resource_mult = {
				resource = society_research
				value = @tier3researchreward
				min = @tier3researchmin
				max = @tier3researchmax
			}
			add_monthly_resource_mult = {
				resource = unity
				value = @tier2unityreward
				min = @tier2unitymin
				max = @tier2unitymax
			}
		}
	}
	option = { 
		name = STH_cardassian_story.103.b # sell artifacts
		owner = { add_resource = { sr_latinum = 250 } }
		hidden_effect = {
			owner = {
				set_country_flag = no_lightship # doesn't do anything atm, needed for future follow-up events
			}
		}
	}
}

# It's a warehouse full of self-sealing stem bolts
ship_event = {
	id = STH_cardassian_story.104
	title = "STH_cardassian_story.104.name"
	desc = "STH_cardassian_story.104.desc"
	picture = GFX_evt_stem_bolts
	show_sound = event_mystic_reveal
	location = ROOT
	trackable = yes

	is_triggered_only = yes

	option = { 
		name = STH_cardassian_story.104.a
		sth_great_artifact_reward = yes
		hidden_effect = {
			owner = {
				set_country_flag = no_lightship # doesn't do anything atm, needed for future follow-up events
			}
		}
	}
}

# Bajorans notified
country_event = {
	id = STH_cardassian_story.105
	title = "STH_cardassian_story.105.name"
	desc = "STH_cardassian_story.105.desc"
	picture = GFX_evt_bajoran_lightship
	show_sound = event_mystic_reveal

	is_triggered_only = yes

	option = { 
		name = STH_cardassian_story.105.a
		custom_tooltip = STH_cardassian_story.105.a.tooltip
		hidden_effect = {
			log = "STH_cardassian_story.105: ancient contact bajorans notified"
		}
	}
}

############################
#
# Occupation Chain 
# Written by Russ
#
############################

country_event = {
	id = STH_cardassian_story.200
	title = "STH_cardassian_story.200.name"
	desc = "STH_cardassian_story.200.desc"
	picture = sth_GFX_evt_cardassiaPrime
	show_sound = event_mystic_reveal

	trigger = {
		NOR = {
			has_country_flag = cardassian_bajoran_occupation_started
			has_country_flag = cardassian_bajoran_occupation_finished
			has_country_flag = cardassian_bajoran_occupation_rejected
		}
		exists = event_target:cardassian_union
		exists = event_target:bajoran_republic
		OR = {
			kelvin_era = yes
			discovery_era = yes
			original_series_era = yes
			wrath_of_khan_era = yes
			next_generation_era = yes
		}
		any_neighbor_country = {
			has_country_flag = bajoran_republic
			is_member_world = no
			num_owned_planets = 1
			relative_power = {
				who = event_target:cardassian_union
				category = fleet
				value <= pathetic
			}
		}
		has_country_flag = cardassian_union
		resource_income_compare = { resource = food value < 50 }
	}
	is_triggered_only = yes

	immediate = {
		random_country = {
			limit = { has_country_flag = bajoran_republic }
			set_country_flag =  bajor_occupation_country
			save_global_event_target_as = bajor_occupation_country
		}
		random_planet = {
			limit = { has_planet_flag = planet_bajor }
			set_planet_flag = bajor_occupation_planet
			save_global_event_target_as = bajor_occupation_planet
		}
	}
	option = { # Target Bajor
		name = STH_cardassian_story.200.a		
		random_country = {
			limit = { has_country_flag = bajoran_republic }
			country_event = { id = STH_cardassian_story.201 days = 14 random = 7 }
		}
		set_country_flag = cardassian_bajoran_occupation_started
		every_country = {
			limit = { has_communications = root NOT = { is_same_value = root } }
				country_event = { id = STH_cardassian_story.202 }
		}
	}
	option = { # Not Right Now
		name = STH_cardassian_story.200.b
		set_timed_country_flag = { flag = cardassian_bajoran_occupation_rejected days = 2000 }
	}
	option = { # No
		name = STH_cardassian_story.200.c
		add_resource = { influence = 50 unity = 10 }
		set_country_flag = cardassian_bajoran_occupation_rejected
	}
}

# Cardassian Contact
country_event = {
	id = STH_cardassian_story.201
	title = STH_cardassian_story.201.name
	desc = { 
		text = STH_cardassian_story.201.desc
	}
	diplomatic = yes
	picture_event_data = {
		portrait = event_target:cardassian_contact.ruler
		room = "cardassian_room"
	}
	is_triggered_only = yes
	force_open = yes
	immediate = {
		random_country = {
			limit = { has_country_flag = cardassian_union }
			save_event_target_as =  cardassian_contact
		}
		random_country = {
			limit = { has_country_flag = bajoran_republic }
			save_event_target_as =  bajoran_contact
		}
	}

	option = { # What??
		name = STH_cardassian_story.201.a
		is_dialog_only = yes
		response_text = STH_cardassian_story.201.a.response
	}

	option = { # Reject!!
		name = STH_cardassian_story.201.b
		ai_chance = { factor = 80 }
		response_text = STH_cardassian_story.201.b.response
		random_owned_planet = {
			limit = { has_planet_flag = planet_bajor }
			set_planet_flag = occupation_target
			save_event_target_as = occupation_target
			#owner = { set_country_flag = occupied_country }
		}
		add_opinion_modifier = {
			who = event_target:cardassian_union
			modifier = opinion_occupation_target
		}
		random_country = {
			limit = { has_country_flag = cardassian_union }
			set_country_flag = world_occupier
			add_opinion_modifier = {
				who = event_target:bajor_occupation_country
				modifier = opinion_cardassians_disdain
			}
			country_event = { id = STH_cardassian_story.203 }
			add_casus_belli = {
				type = cb_occupation
				who = event_target:bajoran_republic
				days = 600
			}
		}
		every_country = { # NOTIFICATION
			limit = { has_communications = event_target:cardassian_union NOT = { is_same_value = event_target:cardassian_union } }
			country_event = { id = STH_cardassian_story.202 days = 1 }
		}
	}
	option = { # Accept!!
		name = STH_cardassian_story.201.c
		set_country_flag = occupation_target
		response_text = STH_cardassian_story.201.c.response
		random_country = {
			limit = { has_country_flag = bajoran_republic }
			set_subject_of = {
				subject_type = vassal
				who = event_target:cardassian_union
			}
		}
	}
}

# Notification
country_event = {
	id = STH_cardassian_story.202
	title = STH_cardassian_story.202.name
	desc = STH_cardassian_story.202.desc
	picture = sth_GFX_evt_bajor_occupation
	is_triggered_only = yes
	trigger = { NOT = { has_country_flag = cardassian_union } }
	immediate = {}
	option = { # Condem
		name = STH_cardassian_story.202.a
		random_country = {
			limit = { has_country_flag = bajoran_republic }
			add_opinion_modifier = {
				who = PREV
				modifier = opinion_medium_boost
			}
		}
		random_country = {
			limit = { has_country_flag = cardassian_union }
			add_opinion_modifier = {
				who = PREV
				modifier = opinion_insult
			}
		}
	}
	option = { # approve
		name = STH_cardassian_story.202.b
		random_country = {
			limit = { has_country_flag = bajoran_republic }
			add_opinion_modifier = {
				who = PREV
				modifier = opinion_insult
			}
		}
		random_country = {
			limit = { has_country_flag = cardassian_union }
			add_opinion_modifier = {
				who = PREV
				modifier = opinion_medium_boost
			}
		}
	}
}

# It's War then
country_event = {
	id = STH_cardassian_story.203
	title = STH_cardassian_story.203.name
	desc = STH_cardassian_story.203.desc
	picture = sth_GFX_evt_bajor_occupation
	is_triggered_only = yes
	trigger = { }
	immediate = {}
	option = { 
		name = STH_cardassian_story.203.a
		declare_war = {
			target = event_target:bajoran_republic
			attacker_war_goal = wg_occupation
		}
		every_country = { # NOTIFICATION
			limit = { has_communications = event_target:cardassian_union NOT = { is_same_value = event_target:cardassian_union } }
			country_event = { id = STH_cardassian_story.206 days = 1 }
		}
	}
}

# A war has been won
# Root = Winner Warleader
# From = Loser Warleader
# FromFrom = War

# Cardassia Wins
country_event = {
	id = STH_cardassian_story.205
	title = STH_cardassian_story.205.name
	desc = STH_cardassian_story.205.desc
	picture = sth_GFX_evt_terok_nor
	is_triggered_only = yes
	trigger = { 
		root = { has_country_flag = cardassian_union }
		from = { has_country_flag = bajoran_republic }
	}
	immediate = {
		random_country = {
			limit = { has_country_flag = cardassian_union }
			save_event_target_as = cardassian_union
		}
		random_system = {
			limit = { has_star_flag = bajoran_homeworld }
			if = { 
				limit = { exists = starbase }
				starbase = { fleet = { destroy_fleet = this } }
			}
			create_starbase = { 
				size = starbase_starfortress 
				owner = event_target:cardassian_union
				module = gun_battery 
				module = trading_hub 
				module = module_armor
				building = ore_refinery
				building = offworld_trading_company
			}
		}
		random_planet = {
			limit = { has_planet_flag = planet_bajor }
			add_modifier = {
				modifier = occupation
			}
			owner = { set_country_flag = occupied_country }
			planet_event = { id = STH_bajoran_flavour.101 days = 400 random = 100 }
			solar_system = {
				every_fleet_in_system = { 
					limit = { 
						any_owned_ship = { 
							is_ship_class = shipclass_starbase 
						}
					}
					save_event_target_as = starbase_ds9
				}
			}		
			owner = {
				every_owned_ship = {
					limit = { exists = fleet fleet = { is_same_value = event_target:starbase_ds9 } }
					set_name = "Terok Nor"
				}
			}
		}
		random_planet = {
			limit = { has_planet_flag = planet_cardassia }
			add_modifier = {
				modifier = pm_havesting_occupied_world
			}
		}
		random_planet = {
			limit = { has_planet_flag = planet_bajor }
			if = { 
				limit = { NOT = { has_modifier = occupation } }
				add_modifier = {
					modifier = occupation
				}
			}
			planet_event = {
				id = STH_bajoran_flavour.100 days = 4000 random = 360 }
		}
	}
	option = { 
		name = STH_cardassian_story.205.a
		add_modifier = {
			modifier = em_occupying_force
			days = 5000
		}
		every_country = { # NOTIFICATION
			limit = { has_communications = event_target:cardassian_union NOR = { is_same_value = event_target:cardassian_union is_same_value = event_target:bajoran_republic } }
			country_event = { id = STH_cardassian_story.207 days = 1 }
		}
	}
}

# Notification
country_event = {
	id = STH_cardassian_story.206
	title = STH_cardassian_story.206.name
	desc = STH_cardassian_story.206.desc
	picture = sth_GFX_evt_terok_nor
	is_triggered_only = yes
	trigger = { }
	immediate = {}
	option = { 
		name = sth_understood
	}
}
# Notification
country_event = {
	id = STH_cardassian_story.207
	title = STH_cardassian_story.207.name
	desc = STH_cardassian_story.207.desc
	picture = sth_GFX_evt_terok_nor
	is_triggered_only = yes
	trigger = { }
	immediate = {}
	option = { 
		name = sth_understood
	}
}

# Dom - CARD Trigger
country_event = {
	id = STH_cardassian_story.499
	hide_window = yes
	is_triggered_only = yes
	trigger = {
		has_country_flag = cardassian_union
		has_global_flag = bajoran_wormhole_discovered
		years_passed > 200
		has_communications = event_target:the_dominion
		NOT = { has_country_flag = cardassian_union_dom_cooldown }
		is_subject = no
	}
	immediate = {
		country_event = { id = STH_cardassian_story.500 days = 30 random = 15 }
		From = { set_timed_country_flag = { flag = cardassian_enemy days = 600  } }
	}
}


# Dominion Chain
country_event = {
	id = STH_cardassian_story.500
	title = "STH_cardassian_story.500.name"
	desc = "STH_cardassian_story.500.desc"
	picture = sth_GFX_evt_cardassiaDominion1
	force_open = yes
	is_triggered_only = yes
	trigger = { exists = event_target:the_dominion } 
	immediate = { 
		random_neighbor_country = {
			limit = {
				OR = {
					relative_power = { who = root category = all value > equivalent }
					relative_power = { who = root category = all value > superior }
				}	
				NOT = { is_same_value = event_target:the_dominion }
			}
			save_event_target_as = threateningCountry
		}
	}
	option = {
		name = STH_cardassian_story.500.a #Pursue Dominion Join
		set_timed_country_flag = { flag = cardassian_union_dom_cooldown days = 3600 }
		hidden_effect = {
			event_target:the_dominion = { country_event = { id = STH_cardassian_story.501 days = 30 random = 7 scopes = { from = root fromfrom = event_target:threateningCountry } } }
		}

	}
	option = {
		name = STH_cardassian_story.500.b #Reject Dominion Join
		set_timed_country_flag = { flag = cardassian_union_dom_cooldown days = 3600 }
	}
}

country_event = {
	id = STH_cardassian_story.501
	title = "STH_cardassian_story.501.name"
	desc = "STH_cardassian_story.501.desc"
	picture = sth_GFX_evt_cardassiaDominion1
	force_open = yes
	is_triggered_only = yes
	trigger = { exists = from } 
	option = {
		name = STH_cardassian_story.501.a #Accept Offer
		from = { country_event = { id = STH_cardassian_story.502 days = 7 random = 3 scopes = { from = root fromfrom = fromfrom } } }
	}
	option = {
		name = STH_cardassian_story.501.b #Reject Offer
		allow = { is_ai = no }
		from = { country_event = { id = STH_cardassian_story.505 days = 7 random = 3 scopes = { from = root fromfrom = fromfrom } } }
	}
}

#Join
country_event = {
	id = STH_cardassian_story.502
	title = "STH_cardassian_story.502.name"
	desc = "STH_cardassian_story.502.desc"
	picture = sth_GFX_evt_cardassiaDominion2
	force_open = yes
	is_triggered_only = yes
	trigger = { exists = from } 
	option = { name = STH_cardassian_story.502.a } # For Cardassia
	option = { name = STH_cardassian_story.502.b } # For The Dominion
	after = {
		if = {
			limit = { is_ai = no }
			set_subject_of = { who = event_target:the_dominion subject_type = dominion_member_human }
			#Add Unhappy Pops Modifier
		}
		else = { set_subject_of = { who = event_target:the_dominion subject_type = dominion_member } }
		hidden_effect = {
			every_country = {
				limit = { has_communications = root is_assimilator = no NOR = { is_same_value = root is_same_value = from } }
				country_event = { id = STH_cardassian_story.503 scopes = { from = root fromfrom = from fromfromfrom = fromfrom } }
			}
			from = { country_event = { id = STH_cardassian_story.504 scopes = { from = root } } }
			# Ask the Dom for revenge against XXX
			random_list = {
				70 = { country_event = { id = STH_cardassian_story.510 days = 100 random = 30 } }
				30 = {}
			}
		}
	}
}

#Galaxy Notification
country_event = {
	id = STH_cardassian_story.503
	title = "STH_cardassian_story.503.name"
	desc = {
		trigger = { has_communications = fromfrom }
		text = "STH_cardassian_story.503.desc_01"
	}
	desc = {
		trigger = { NOT = { has_communications = fromfrom } }
		text = "STH_cardassian_story.503.desc_02"
	}
	picture = sth_GFX_evt_cardassiaDominion2
	force_open = yes
	is_triggered_only = yes
	trigger = { exists = from exists = fromfrom } 
	option = {
		name = STH_cardassian_story.503.a #Worrying
	}
	option = {
		name = STH_cardassian_story.503.b #Does not concern us
	}
}

#Dominion Notification
country_event = {
	id = STH_cardassian_story.504
	title = "STH_cardassian_story.504.name"
	desc = { text = "STH_cardassian_story.504.desc" }
	picture = sth_GFX_evt_cardassiaDominion2
	force_open = yes
	is_triggered_only = yes
	trigger = { exists = from } 
	option = {
		name = STH_cardassian_story.504.a #The Dominion will prevail
	}
}

#Dominion rejection
country_event = {
	id = STH_cardassian_story.505
	title = "STH_cardassian_story.505.name"
	desc = "STH_cardassian_story.505.desc"
	picture = sth_GFX_evt_cardassiaDominion1
	force_open = yes
	is_triggered_only = yes
	trigger = { exists = from } 
	option = { name = STH_cardassian_story.505.a } # We will prevail
	# option = { name = STH_cardassian_story.502.b } # For The Dominion
}

## Cardassian/Dom Revenge
country_event = {
	id = STH_cardassian_story.510
	title = "STH_cardassian_story.510.name"
	desc = "STH_cardassian_story.510.desc"
	picture = sth_GFX_evt_cardassiaDominion1
	force_open = yes
	is_triggered_only = yes
	trigger = { is_subject_type = dominion_member }
	immediate = {
		random_country = {
			limit = { has_country_flag = cardassian_enemy }
			save_event_target_as = cardassian_enemy
		}
	}
	option = { # Demand revenge.
		name = STH_cardassian_story.510.a
		random_country = {
			limit = { has_country_flag = the_dominion }
			country_event = { id = STH_cardassian_story.511 }
		}
	}  
	option = { name = STH_cardassian_story.510.b } # No revenge. 
}

## Dom Response
country_event = {
	id = STH_cardassian_story.511
	title = "STH_cardassian_story.511.name"
	desc = "STH_cardassian_story.511.desc"
	picture = sth_GFX_evt_cardassiaDominion1
	force_open = yes
	is_triggered_only = yes
	trigger = { has_country_flag = the_dominion }
	immediate = {
		random_country = {
			limit = { has_country_flag = cardassian_enemy }
			save_event_target_as = cardassian_enemy
		}
	}
	option = { # Accept
		name = STH_cardassian_story.511.a
		ai_chance = {
			factor = 2
			modifier = {
				factor = 0.1
				is_at_war = yes
			}
			modifier = {
				factor = 5
				used_naval_capacity_integer >= 75
			}
		}
		random_country = {
			limit = { has_country_flag = cardassian_union }
			country_event = { id = STH_cardassian_story.512 days = 5 }
		}
		# Moved to Dominion Events
		country_event = {
			id = STH_dominion_story.300 days = 30 random = 10
		}
	}  
	option = { # Reject
		name = STH_cardassian_story.511.b
		ai_chance = {
			factor = 1
			modifier = {
				factor = 1
				is_at_war = yes
			}
			modifier = {
				factor = 5
				used_naval_capacity_integer <= 45
			}
		}	
		random_country = {
			limit = { has_country_flag = cardassian_union }
			country_event = { id = STH_cardassian_story.513 days = 5 }
		}
	}
	option = { # Already at war.
		name = STH_cardassian_story.511.c
		trigger = { is_at_war = yes } 
		ai_chance = { factor = 5 }	
		random_country = {
			limit = { has_country_flag = cardassian_union }
			country_event = { id = STH_cardassian_story.514 days = 5 }
		}
	} 
}

## Cardassian/Dom Revenge - accept
country_event = {
	id = STH_cardassian_story.512
	title = "STH_cardassian_story.512.name"
	desc = "STH_cardassian_story.512.desc"
	picture = sth_GFX_evt_cardassiaDominion1
	force_open = yes
	is_triggered_only = yes
	trigger = { has_country_flag = cardassian_union is_subject_type = dominion_member }
	immediate = {
		random_country = {
			limit = { has_country_flag = cardassian_enemy }
			save_event_target_as = cardassian_dom_enemy
			set_timed_country_flag = {
				flag = cardassian_dom_enemy days = 600
			}
		}
	}
	option = { # Prepare for War.
		name = STH_cardassian_story.512.a
		ai_chance = { factor = 2 }
		add_opinion_modifier = {
			who = event_target:the_dominion
			modifier = opinion_medium_boost
		}
	}  
	#option = { name = STH_cardassian_story.512.b } # No revenge. 
}

## Cardassian/Dom Revenge - reject
country_event = {
	id = STH_cardassian_story.513
	title = "STH_cardassian_story.513.name"
	desc = "STH_cardassian_story.513.desc"
	picture = sth_GFX_evt_cardassiaDominion1
	force_open = yes
	is_triggered_only = yes
	trigger = { has_country_flag = cardassian_union is_subject_type = dominion_member }
	immediate = {
		random_country = {
			limit = { has_country_flag = cardassian_enemy }
			save_event_target_as = cardassian_dom_enemy
			remove_country_flag = cardassian_enemy 
		}
	}
	option = { # Frustration.
		name = STH_cardassian_story.513.a
		add_resource = { unity = 50 }
		add_opinion_modifier = {
			who = event_target:the_dominion
			modifier = opinion_insult
		}
	}  
	option = { # Fury.
		name = STH_cardassian_story.513.b
		add_resource = { unity = 100 }
		add_opinion_modifier = {
			who = event_target:the_dominion
			modifier = opinion_cardassians_disdain
		}
	}
	option = { # Accept.
		name = STH_cardassian_story.513.c
		random_country = {
			limit = { has_country_flag = the_dominion }
			add_opinion_modifier = {
				who = event_target:cardassian_union
				modifier = opinion_small_boost
			}
		}
	}
}
# Already at war.
country_event = {
	id = STH_cardassian_story.514
	title = "STH_cardassian_story.514.name"
	desc = "STH_cardassian_story.514.desc"
	picture = sth_GFX_evt_cardassiaDominion1
	force_open = yes
	is_triggered_only = yes
	trigger = { has_country_flag = cardassian_union is_subject_type = dominion_member }
	immediate = {
		random_country = {
			limit = { has_country_flag = cardassian_enemy }
			save_event_target_as = cardassian_dom_enemy
			remove_country_flag = cardassian_enemy 
		}
	}
	option = { # Frustration.
		name = STH_cardassian_story.514.a
		add_opinion_modifier = {
			who = event_target:the_dominion
			modifier = opinion_broke_guarantee
		}
	}  
	option = { # Ask again in 260.
		name = STH_cardassian_story.514.b
		hidden_effect = {
			country_event = {
				id = STH_cardassian_story.510 days = 360 
			}
		}
	}
}

#Hebitian Restoration
country_event = {
	id = STH_cardassian_story.1000
	title = "STH_cardassian_story.1000.name"
	desc = "STH_cardassian_story.1000.desc"
	picture = sth_GFX_evt_hebitianRestoration
	force_open = yes
	is_triggered_only = yes
	immediate = { save_event_target_as = cardassian_country }
	option = {
		name = STH_cardassian_story.1000.a
		custom_tooltip = "STH_cardassian_story.1000.a.tooltip"
		trigger = { 
			has_country_flag = cardassian_union
			NOT = { any_country = { has_country_flag = hebitian_union } } 
		}
		ai_chance = {
			factor = 0
			modifier = { factor = 1 has_country_flag = cardassian_union NOT = { any_country = { has_country_flag = hebitian_union } } }
		}
		hidden_effect = { 
			become_hebitian_union = yes
			every_country = {
				limit = { is_species_class = CAR NOR = { is_same_value = root has_country_flag = hebitian_country } }
				country_event = { id = STH_cardassian_story.1001 days = 1 }
			}
			every_country = {
				limit = { 
					NOR = { is_species_class = CAR is_same_value = root }
					has_established_contact = root
				}
				country_event = { id = STH_cardassian_story.1002 days = 7 random = 3 }
			}
		}
	}
	option = {
		name = STH_cardassian_story.1000.b
		custom_tooltip = "STH_cardassian_story.1000.b.tooltip"
		trigger = { NOT = { has_country_flag = cardassian_union } }
		ai_chance = {
			factor = 0
			modifier = { factor = 1 NOT = { has_country_flag = cardassian_union } }
		}
		hidden_effect = { 
			become_hebitian_country = yes 
			every_country = {
				limit = { is_species_class = CAR NOR = { is_same_value = root has_country_flag = hebitian_country } }
				country_event = { id = STH_cardassian_story.1001 days = 0 }
			}
		}
	}
	option = {
		name = STH_cardassian_story.1000.c
		hidden_effect = { }
	}
}



#Accept or reject Hebitian species name
country_event = {
	id = STH_cardassian_story.1001
	title = "STH_cardassian_story.1001.name"
	desc = "STH_cardassian_story.1001.desc"
	picture = sth_GFX_evt_hebitianRestoration
	force_open = yes
	is_triggered_only = yes
	trigger = {
		is_species_class = CAR
	}
	option = {
		name = STH_cardassian_story.1001.a #Accept
		ai_chance = {
			factor = 0
			modifier = { factor = 1 NOT = { has_country_flag = cardassian_union } }
		}
	}
	option = {
		name = STH_cardassian_story.1001.b #We are Cardassian!
		ai_chance = { factor = 1 }
		hidden_effect = {
			if = {
				limit = { any_country = { is_species_class = CAR any_owned_pop = { is_species = "Cardassian" } } }
				random_country = { 
					limit = { is_species_class = CAR any_owned_pop = { is_species = "Cardassian" } } 
					random_owned_pop = {
						limit = { is_species = "Cardassian" }
						species = { save_event_target_as = cardassian_species }
					}
				}
				every_owned_pop = {
					limit = { is_species = "Hebitian" }
					change_species = event_target:cardassian_species
				}
				change_dominant_species = { species = event_target:cardassian_species }
			}
			else = {
				modify_species = { species = this add_trait = trait_pc_nuked_preference }
				species = { rename_species = { name_list = cardassian_name } save_event_target_as = cardassian_species }
				modify_species = { species = this remove_trait = trait_pc_nuked_preference }
			}
		}
		change_dominant_species = { species = event_target:cardassian_species }
	}
}



#Notification to neighbours
country_event = {
	id = STH_cardassian_story.1002
	title = "STH_cardassian_story.1002.name"
	desc = "STH_cardassian_story.1002.desc"
	picture = sth_GFX_evt_hebitianRestoration
	force_open = yes
	is_triggered_only = yes
	trigger = { }
	option = {
		name = STH_cardassian_story.1002.a
	}
}
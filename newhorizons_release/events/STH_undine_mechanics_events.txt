########################
#
# Undine mechanic events
#
# Written by Walshicus
#
########################

namespace = STH_undine_mechanics

#Undine First Contact Event
country_event = {
	id = STH_undine_mechanics.1
	is_triggered_only = yes
	hide_window = yes
	trigger = {
		is_ai = no
		is_normal_country = yes
		FROM = { is_undine_empire = yes }
	}
	immediate = {
		establish_communications = FROM
		FROM = { establish_communications = root }
		country_event = { id = STH_undine_mechanics.3 days = 0 scopes = { from = from } }
	}
}
country_event = {
	id = STH_undine_mechanics.3
	title = "STH_undine_mechanics.3.name"
	desc = "STH_undine_mechanics.3.desc"
	is_triggered_only = yes
	diplomatic = yes
	picture_event_data = {
		portrait = event_target:undineLeader
		room = "undine_room"
	}
	immediate = {
		from = {
			if = {
				limit = { exists = leader } 
				leader = { save_event_target_as = undineLeader }
			}
			else = { save_event_target_as = undineLeader }
		}
	}
	option = {
		name = STH_undine_mechanics.3.a
		response_text = STH_undine_mechanics.3.a.response
	}	
}

#New Assimilate Gatekeeper
event = {
	id = STH_undine_mechanics.50
	is_triggered_only = yes
	hide_window = yes
	immediate = {
		every_country = {
			limit = { is_infester = yes any_owned_pop_species = { NOT = { is_species_class = UND } } }
			country_event = { id = STH_undine_mechanics.51 } #Pop Consume
		}
	}	
}

#Consume Pop
country_event = {
	id = STH_undine_mechanics.51
	is_triggered_only = yes
	hide_window = yes
	trigger = { is_infester = yes }
	immediate = {
		random_owned_pop = {
			limit = { NOT = { is_species_class = UND } }
			root = { add_resource = { minerals = 25 food = 25 energy = 10 society_research = 10 } }
			kill_pop = yes
		}
	}
}



#Undine Destroyed
country_event = {
	id = STH_undine_mechanics.1006
	hide_window = yes
	is_triggered_only = yes
	trigger = {
		is_undine_empire = yes
		exists = FROM
		FROM = { is_normal_country = yes is_undine_empire = no }
	}
	immediate = { 
		FROM = { country_event = { id = STH_undine_mechanics.1007 } }
	}
}

#Undine Destroyed
country_event = {
	id = STH_undine_mechanics.1007
	title = "STH_undine_mechanics.1007.name"
	desc = "STH_undine_mechanics.1007.desc"
	picture = sth_GFX_evt_undinePlanet1
	is_triggered_only = yes
	trigger = { }
	immediate = { }
	option = {
		name = "STH_undine_mechanics.1007.a" ##HAREL TODO REPLACE
		large_scaling_research_reward = { research_type = physics_research }
		large_scaling_research_reward = { research_type = society_research }
		large_scaling_research_reward = { research_type = engineering_research }
	}
}
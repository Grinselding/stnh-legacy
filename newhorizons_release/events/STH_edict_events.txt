#################################################
#						
# STNH Edict Events				
# Russ						
# 						
# Version 1.1 - created (24/07/19)						
# First Contact Day - Felis												
# Romulan Festival - Felis																
# Klingon Festival - ClingingMars									   		
#									    	
#						
#################################################

namespace = STH_edicts

# First Contact Day - Written by Felis
country_event = {
	id = STH_edicts.1
	title = "STH_edicts.1.name"
	desc = "STH_edicts.1.desc"
	picture = sth_GFX_evt_federationEarth	# Will find a pic from 2d team
	show_sound = event_mystic_reveal	# Implement 1st contact day theme
	is_triggered_only = yes

	trigger = {
		has_edict = edict_local_festival_1
	}

	option = { 
		name = STH_edicts.1.a
		add_monthly_resource_mult = {
			resource = unity
			value = @tier1unityreward
			min = 30
			max = 100
		}
	}
}

# Eitreih'hveinn Farmer Festival - Written by Felis
country_event = {
    id = STH_edicts.2
    title = "STH_edicts.2.name"
    desc = "STH_edicts.2.desc"
    picture = sth_GFX_evt_romulanCapital    # placeholder
    show_sound = event_mystic_reveal    # placeholder
    is_triggered_only = yes

    trigger = {
        has_edict = edict_local_festival_6
    }

    option = { 
        name = STH_edicts.2.a
        capital_scope = {
            add_modifier = {
                modifier = em_farmer_festival
                days = 360
            }
        }
    }
}

# Kot'Baval Call To Arms - Klingon unique
country_event = {
	id = STH_edicts.3
	title = "STH_edicts.3.name"
	desc = "STH_edicts.3.desc"
	picture = sth_GFX_evt_klingonLeadership1
	show_sound = event_mystic_reveal    # placeholder
	is_triggered_only = yes
	
	trigger = {
		has_country_flag = klingon_empire
		has_edict = edict_local_festival_2
	}

	option = { 
        name = STH_edicts.3.a
        capital_scope = {
            add_modifier = {
                modifier = em_kotbaval_festival
                days = 360
            }
        }
    }
}
	
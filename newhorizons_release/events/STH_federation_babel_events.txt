############################################################
#															
# Babel Events												
# Written by Russ											
# 															
# Version 0.1 - created(Russ 09.07.19)						
# Version 1.0 - Added to git (Russ 11.07.19)					
# Version 1.2 - Trigger updates (Russ 13.07.19)				
# Version 1.3 - Triggers added
# 									   						
# Babel One - federation formation assistance event      	
#															
#############################################################

namespace = STH_babel

country_event = {
	id = STH_babel.1
	hide_window = yes
	is_triggered_only = yes
	trigger = {
		is_ai = no
		years_passed > 14
		has_country_flag = federation_founder
		is_at_war = no
		#has_prefed_crisis_in_progress = no 
		NOR = { 
			has_country_flag = united_federation_of_planets 
			has_country_flag = babel_conference_occurred
			has_country_flag = terra_prime
			has_country_flag = terran_empire
			has_country_flag = demons_crisis
		}
		NOT = { has_global_flag = federation_in_progress }
		
	}
	
	immediate = {
		country_event = { id = STH_babel.11 }
		set_country_flag = babel_conference_occurred
	}
} 
					
#Conference underway
country_event = {
	id = STH_babel.2
	title = "STH_babel.2.name"
	desc = "STH_babel.2.desc"
	picture = sth_GFX_evt_heroDiplomacy1
	is_triggered_only = yes

	immediate = {
		set_country_flag = "babel_conf_started"	
		random_planet =  { limit = { has_planet_flag = babel } 
			save_event_target_as = babelLocation }
		random_country = { limit = { has_country_flag = andorian_empire }
			save_global_event_target_as = rivalAndor1 } 
		random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
			save_global_event_target_as = rivalEarth1 } 
		random_country = { limit = { has_country_flag = tellarian_technocracy }
			save_global_event_target_as = rivalTellar1 }					
		random_country = { limit = { vulcan_empires = yes }
			save_global_event_target_as = rivalVulcan1 }	
		random_country = { limit = { has_country_flag = andorian_empire }
			save_global_event_target_as = babelAndor } 
		random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
			save_global_event_target_as = babelEarth } 
		random_country = { limit = { has_country_flag = tellarian_technocracy }
			save_global_event_target_as = babelTellar }					
		random_country = { limit = { vulcan_empires = yes }
			save_global_event_target_as = babelVulcan }	
	}	
	option = {  
		name = STH_babel.2.a #Remove Rivals
		custom_tooltip = "STH_babel.2.a.tooltip"
		#Andor			
		random_country = { limit = { has_country_flag = andorian_empire }
			end_rivalry = event_target:rivalVulcan1 }
		random_country = { limit = { has_country_flag = andorian_empire }
			end_rivalry = event_target:rivalTellar1 }
		random_country = { limit = { has_country_flag = andorian_empire }
			end_rivalry = event_target:rivalEarth1 }
		#Earth		
		random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
			end_rivalry = event_target:rivalVulcan1 }
		random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
			end_rivalry = event_target:rivalAndor1 }	
		random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
			end_rivalry = event_target:rivalTellar1 }
		#Vulcan
		random_country = { limit = { vulcan_empires = yes }
			end_rivalry = event_target:rivalAndor1 }
		random_country = { limit = { vulcan_empires = yes }
			end_rivalry = event_target:rivalTellar1 }
		random_country = { limit = { vulcan_empires = yes }
			end_rivalry = event_target:rivalEarth1 }
		#Tellar				
		random_country = { limit = { has_country_flag = tellarian_technocracy }
			end_rivalry = event_target:rivalEarth1 }	
		random_country = { limit = { has_country_flag = tellarian_technocracy }
			end_rivalry = event_target:rivalVulcan1 }
		random_country = { limit = { has_country_flag = tellarian_technocracy }
			end_rivalry = event_target:rivalAndor1 }
				
		#Move on...				
			hidden_effect = { country_event = { id = STH_babel.4 days = 3 } }
		}
		
	option = {  
		name = STH_babel.2.b #End Bad
		custom_tooltip = "STH_babel.2.b.tooltip"
		#Andor
		hidden_effect = {
			random_country = { limit = { has_country_flag = andorian_empire }
				add_opinion_modifier = { 
					modifier = triggered_babel_bad
					who = event_target:babelVulcan } }
			random_country = { limit = { has_country_flag = andorian_empire }
				add_opinion_modifier = { 
					modifier = triggered_babel_bad 
					who = event_target:babelEarth } }	
			random_country = { limit = { has_country_flag = andorian_empire }
				add_opinion_modifier = { 
					modifier = triggered_babel_bad 
					who = event_target:babelTellar } }					
		#Earth
			random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
				add_opinion_modifier = { 
					modifier = triggered_babel_bad
					who = event_target:babelVulcan } }
			random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
				add_opinion_modifier = { 
					modifier = triggered_babel_bad
					who = event_target:babelTellar } }
			random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
				add_opinion_modifier = { 
					modifier = triggered_babel_bad
					who = event_target:babelAndor } }
		#Tellar					
			random_country = { limit = { has_country_flag = tellarian_technocracy }
				add_opinion_modifier = { 
					modifier = triggered_babel_bad
					who = event_target:babelVulcan } }
			random_country = { limit = { has_country_flag = tellarian_technocracy }
				add_opinion_modifier = { 
					modifier = triggered_babel_bad
					who = event_target:babelEarth } }
			random_country = { limit = { has_country_flag = tellarian_technocracy }
				add_opinion_modifier = { 
					modifier = triggered_babel_bad
					who = event_target:babelAndor } }		
		#Vulcan			
			random_country = { limit = { vulcan_empires = yes } 
				add_opinion_modifier = { 
					modifier = triggered_babel_bad
					who = event_target:babelAndor } }
			random_country = { limit = { vulcan_empires = yes } 
				add_opinion_modifier = { 
					modifier = triggered_babel_bad
					who = event_target:babelTellar } }						
			random_country = { limit = { vulcan_empires = yes } 
				add_opinion_modifier = { 
					modifier = triggered_babel_bad
					who = event_target:babelEarth } }
		}
	}
}

#Babel Completion
country_event = {
	id = STH_babel.4
	title = "STH_babel.4.name"
	desc = "STH_babel.4.desc"
	picture = sth_GFX_evt_heroDiplomacy1
	is_triggered_only = yes

	immediate = {
		set_country_flag = "babel_conf_complete"	
		random_country = { limit = { has_country_flag = andorian_empire }
			save_global_event_target_as = babelAndor1 } 
		random_country = { limit = { has_country_flag = united_earth }
			save_global_event_target_as = babelEarth1 } 
		random_country = { limit = { has_country_flag = tellarian_technocracy }
			save_global_event_target_as = babelTellar1 }					
		random_country = { limit = { vulcan_empires = yes }
			save_global_event_target_as = babelVulcan1 } 
	#Rivals part 2			
		random_country = { limit = { has_country_flag = andorian_empire }
			save_global_event_target_as = rivalAndor2 } 
		random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
			save_global_event_target_as = rivalEarth2 } 
		random_country = { limit = { has_country_flag = tellarian_technocracy }
			save_global_event_target_as = rivalTellar2 }					
		random_country = { limit = { vulcan_empires = yes }
			save_global_event_target_as = rivalVulcan2 }						
	}
			
	option = {  name = STH_babel.4.a #Best
		custom_tooltip = "STH_babel.4.a.tooltip"
		hidden_effect = {
	#Andor
		random_country = { limit = { has_country_flag = andorian_empire }
			add_opinion_modifier = { 
				modifier = triggered_babel 
				who = event_target:babelVulcan1 } }
		random_country = { limit = { has_country_flag = andorian_empire }
			add_opinion_modifier = { 
				modifier = triggered_babel 
				who = event_target:babelEarth1 } }	
		random_country = { limit = { has_country_flag = andorian_empire }
			add_opinion_modifier = { 
				modifier = triggered_babel 
				who = event_target:babelTellar1 } }					
	#Earth
		random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
			add_opinion_modifier = { 
				modifier = triggered_babel
				who = event_target:babelVulcan1 } }
		random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
			add_opinion_modifier = { 
				modifier = triggered_babel
				who = event_target:babelTellar1 } }
		random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
			add_opinion_modifier = { 
				modifier = triggered_babel
				who = event_target:babelAndor1 } }
	#Tellar					
		random_country = { limit = { has_country_flag = tellarian_technocracy }
			add_opinion_modifier = { 
				modifier = triggered_babel
				who = event_target:babelVulcan1 } }
		random_country = { limit = { has_country_flag = tellarian_technocracy }
			add_opinion_modifier = { 
				modifier = triggered_babel
				who = event_target:babelEarth1 } }
		random_country = { limit = { has_country_flag = tellarian_technocracy }
			add_opinion_modifier = { 
				modifier = triggered_babel
				who = event_target:babelAndor1 } }		
	#Vulcan			
		random_country = { limit = { vulcan_empires = yes }
			add_opinion_modifier = { 
				modifier = triggered_babel
				who = event_target:babelAndor1 } }
		random_country = { limit = { vulcan_empires = yes }
			add_opinion_modifier = { 
				modifier = triggered_babel
				who = event_target:babelEarth1 } }	
		random_country = { limit = { vulcan_empires = yes }
			add_opinion_modifier = { 
				modifier = triggered_babel
				who = event_target:babelTellar1 } }
		}
	#Move on...				
		hidden_effect = { country_event = { id = STH_babel.5 days = 3 } } 
	}
			
	option = {  name = STH_babel.4.b #Outrage
		owner = { add_resource = { influence = 100 } }
		remove_country_flag = babel_conf_started
		custom_tooltip = "STH_babel.4.b.tooltip"
		#Move on...				
			hidden_effect = { country_event = { id = STH_babel.22 days = 3 } } 
		hidden_effect = {
		#Andor			
		random_country = { limit = { has_country_flag = andorian_empire }
			add_opinion_modifier = {
			modifier = opinion_outraged
			who = event_target:rivalVulcan2 } }
		random_country = { limit = { has_country_flag = andorian_empire }
			add_opinion_modifier = {
			modifier = opinion_outraged
			who = event_target:rivalTellar2 } }
		random_country = { limit = { has_country_flag = andorian_empire }
			add_opinion_modifier = {
			modifier = opinion_outraged
			who = event_target:rivalEarth2 } }
		#Earth		
		random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
			add_opinion_modifier = {
			modifier = opinion_outraged
			who = event_target:rivalVulcan2 } }
		random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
			add_opinion_modifier = {
			modifier = opinion_outraged
			who = event_target:rivalAndor2 } }	
		random_country = { limit = { has_country_flag = united_earth NOT = { has_country_flag = terra_prime } }
			add_opinion_modifier = {
			modifier = opinion_outraged
			who = event_target:rivalTellar2 } }
		#Vulcan
		random_country = { limit = { vulcan_empires = yes }
			add_opinion_modifier = {
			modifier = opinion_outraged
			who = event_target:rivalAndor2 } }
		random_country = { limit = { vulcan_empires = yes }
			add_opinion_modifier = {
			modifier = opinion_outraged
			who = event_target:rivalTellar2 } }
		random_country = { limit = { vulcan_empires = yes }
			add_opinion_modifier = {
			modifier = opinion_outraged
			who = event_target:rivalEarth2 } }
		#Tellar				
		random_country = { limit = { has_country_flag = tellarian_technocracy }
			add_opinion_modifier = {
			modifier = opinion_outraged
			who = event_target:rivalEarth2 } }
		random_country = { limit = { has_country_flag = tellarian_technocracy }
			add_opinion_modifier = {
			modifier = opinion_outraged
			who = event_target:rivalVulcan2 } }
		random_country = { limit = { has_country_flag = tellarian_technocracy }
			add_opinion_modifier = {
			modifier = opinion_outraged
			who = event_target:rivalAndor2 } }	
		}
	}				
}
 

country_event = {
	id = STH_babel.5
	title = "STH_babel.5.name"
	desc = "STH_babel.5.desc"
	picture = sth_GFX_evt_heroDiplomacy1
	is_triggered_only = yes
	
	immediate = { 
		remove_country_flag = "babel_conf_complete"
		set_country_flag = "babel_one_success"
		remove_country_flag = babel_conf_started
	}

	option = { name = STH_babel.5.a
		owner = { add_resource = { influence = 50 } }
	}
	option = { 
		name = STH_babel.5.b # Form C.O.P
		owner = { add_resource = { influence = 50 } }
		country_event = { id = STH_babel.6 }
	}
}

# Form the Coalition of Planets
country_event = {
	id = STH_babel.6
	title = "STH_babel.6.name"
	desc = "STH_babel.6.desc"
	picture = sth_GFX_evt_heroDiplomacy1
	is_triggered_only = yes
	
	immediate = {
		random_country = { limit = { has_country_flag = andorian_empire }
			save_global_event_target_as = babelAndor_invite } 
		random_country = { limit = { has_country_flag = united_earth }
			save_global_event_target_as = babelEarth_invite } 
		random_country = { limit = { has_country_flag = tellarian_technocracy }
			save_global_event_target_as = babelTellar_invite }					
		random_country = { limit = { vulcan_empires = yes }
			save_global_event_target_as = babelVulcan_invite } 
	}

	option = { # Invite others
		name = STH_babel.6.a
		owner = { add_resource = { influence = 50 } }
		country_event = { id = STH_babel.12 days = 10 }

		random_country = {
			limit = { has_country_flag = andorian_empire }
			country_event = { id = STH_babel.21 days = 1 }
		}
		random_country = {
			limit = { vulcan_empires = yes }
			country_event = { id = STH_babel.21 days = 1 }
		}
		random_country = {
			limit = { OR = { has_country_flag = united_earth has_country_flag = united_human_remnant } }
			country_event = { id = STH_babel.21 days = 1 }
		}
		random_country = {
			limit = { has_country_flag = tellarian_technocracy }
			country_event = { id = STH_babel.21 days = 1 }
		}
	}
}

#Failed to complete in time
country_event = {
	id = STH_babel.10
	title = "STH_babel.10.name"
	desc = "STH_babel.10.desc"
	picture = sth_GFX_evt_heroDiplomacy1
	is_triggered_only = yes
	immediate = { remove_country_flag = "babel_conf_started" }
	option = { name = STH_babel.10.a
		trigger = { owner = { resource_stockpile_compare = { resource = influence value >= 50 } } }
		owner = { add_resource = { influence = -50 } }
	}
}

#Babel Project Enabler
country_event = {
	id = STH_babel.11
	title = "STH_babel.11.name"
	desc = "STH_babel.11.desc"
	picture = sth_GFX_evt_heroDiplomacy1
	is_triggered_only = yes
	immediate = { 		
		random_planet =  { limit = { has_planet_flag = babel } save_event_target_as = babelLocation } 
	}
	#Accept
	option = {
		name = STH_babel.11.a
			enable_special_project = { name = "BABEL_1_PROJECT" owner = root location = event_target:babelLocation }	
	}
	#Reject
	option = {
		name = STH_babel.11.b
		owner = { add_resource = { influence = 100 } }
	}
	#Accept Flagship
	option = {
		name = STH_babel.11.c
		trigger = {
			any_owned_leader = {
				OR = {
					has_trait = leader_trait_hero_ship_admiral
					has_leader_flag = sthero
				}
			}
		}
			enable_special_project = { name = "BABEL_1A_PROJECT" owner = root location = event_target:babelLocation }	
	}
}

# Form Coalition
country_event = {
	id = STH_babel.12
	hide_window = yes
	is_triggered_only = yes

	immediate = {
		remove_country_flag = babel_conf_started
		if = {
			limit = { has_country_flag = united_earth  }
			set_country_flag = coalition_of_planets_leader
			save_event_target_as = fed_cop_leader
		}
		else_if = {
			limit = { vulcan_empires = yes }
			set_country_flag = coalition_of_planets_leader
			save_event_target_as = fed_cop_leader
		}
		else_if = {
			limit = { has_country_flag = andorian_empire }
			set_country_flag = coalition_of_planets_leader
			save_event_target_as = fed_cop_leader
		}
		else_if = {
			limit = { has_country_flag = tellarian_technocracy }
			set_country_flag = coalition_of_planets_leader
			save_event_target_as = fed_cop_leader
		}

		random_country = {
			limit = { has_country_flag = cop_earth  }
			leave_alliance = { override_requirements = yes }
			join_alliance = { who = event_target:fed_cop_leader override_requirements = yes }
			save_event_target_as = cop_1
		}

		random_country = {
			limit = { has_country_flag = cop_vulcan  }
			leave_alliance = { override_requirements = yes }
			join_alliance = { who = event_target:fed_cop_leader override_requirements = yes }
			save_event_target_as = cop_2
		}

		random_country = {
			limit = { has_country_flag = cop_andoria  }
			leave_alliance = { override_requirements = yes }
			join_alliance = { who = event_target:fed_cop_leader override_requirements = yes }
			save_event_target_as = cop_3
		}

		random_country = {
			limit = { has_country_flag = cop_tellar  }
			leave_alliance = { override_requirements = yes }
			join_alliance = { who = event_target:fed_cop_leader override_requirements = yes }
			save_event_target_as = cop_4
		}

		federation = {
			set_federation_type = sth_cop_federation
			set_name =  "Coalition of Planets" 
		}
		set_federation_leader = event_target:fed_cop_leader
		country_event = { id = STH_babel.13 days = 2 }

		every_country = {
			limit = { has_communications = root NOT = { is_same_value = root } }
			country_event = { id = STH_babel.14 days = 2 }
		}
	}
}

# COP NOTIFICATION
country_event = {
	id = STH_babel.13
	title = STH_babel.13.name
	desc = STH_babel.13.desc
	picture = sth_GFX_evt_federationSigned
	is_triggered_only = yes
	fire_only_once = yes

	trigger = {}
	immediate = {}

	option = {
		name = sth_understood
	}
}

# COP NOTIFICATION - 3rd Party
country_event = {
	id = STH_babel.14
	title = STH_babel.14.name
	desc = STH_babel.14.desc
	picture = sth_GFX_evt_federationFlag
	is_triggered_only = yes
	fire_only_once = yes

	trigger = {}
	immediate = {}

	option = {
		name = sth_understood
		hidden_effect = {
			if = { # UE
				limit = {
					any_country = {
						OR = {
							has_country_flag = united_earth
							has_country_flag = united_human_remnant
						}
						is_ai = no
					}
				}
				random_country = {
					limit = {
						OR = {
							has_country_flag = united_earth
							has_country_flag = united_human_remnant
						}
					}
					country_event = { id = theFederation.1 days = 2000 random = 250 }
				}
			}
			if = { # ANDOR
				limit = {
					any_country = {
						has_country_flag = andorian_empire
						is_ai = no
					}
				}
				random_country = {
					limit = {
						has_country_flag = andorian_empire
					}
					country_event = { id = theFederation.2 days = 2000 random = 250 }
				}
			}
			if = { # ANDOR
				limit = {
					any_country = {
						has_country_flag = andorian_empire
						is_ai = no
					}
				}
				random_country = {
					limit = {
						has_country_flag = andorian_empire
					}
					country_event = { id = theFederation.3 days = 2000 random = 250 }
				}
			}
			if = { # TELLAR
				limit = {
					any_country = {
						has_country_flag = tellarian_technocracy
						is_ai = no
					}
				}
				random_country = {
					limit = {
						has_country_flag = tellarian_technocracy
					}
					country_event = { id = theFederation.4 days = 2000 random = 250 }
				}
			}
			if = { # VULCAN
				limit = {
					any_country = {
						vulcan_empires = yes
						is_ai = no
					}
				}
				random_country = {
					limit = {
						vulcan_empires = yes
					}
					country_event = { id = theFederation.2 days = 2000 random = 250 }
				}
			}
		}
	}
}


#Babel Project Abandon Option
country_event = {
	id = STH_babel.20
	hide_window = yes
	trigger = { 
		OR = { 
			has_special_project = "BABEL_1_PROJECT"
			has_special_project = "BABEL_1A_PROJECT"	
		}
		AND = {
			has_country_flag = united_federation_of_planets	
		}
	}
	fire_only_once = yes
	
	immediate = { 
		abort_special_project = {
			type = "BABEL_1_PROJECT"
		}
		abort_special_project = {
			type = "BABEL_1A_PROJECT"
		}
	}
}

# Form the Coalition of Planets - VOTE
country_event = {
	id = STH_babel.21
	title = "STH_babel.21.name"
	desc = "STH_babel.21.desc"
	picture = sth_GFX_evt_heroDiplomacy1
	is_triggered_only = yes
	
	immediate = {
	}
	
	option = { # JOIN - UE
		name = STH_babel.21.a
		trigger = { has_country_flag = united_earth }
		ai_chance = { factor = 100 }
		random_country = {
			limit = {
				OR = {
					has_country_flag = united_earth
					has_country_flag = united_human_remnant
				}
			}
			save_global_event_target_as = cop_earth
		}
		set_country_flag = cop_earth
	}
	option = { # REJECT - UE
		name = STH_babel.21.b
		set_country_flag = cop_earth_rejected
	}
	option = { # JOIN - VULCAN
		name = STH_babel.21.c
		trigger = { vulcan_empires = yes }
		ai_chance = { factor = 100 }
		random_country = {
			limit = {
				vulcan_empires = yes
			}
			save_global_event_target_as = cop_vulcan
		}
		set_country_flag = cop_vulcan
	}
	option = { # REJECT - VULCAN
		name = STH_babel.21.d
		trigger = { vulcan_empires = yes }
		set_country_flag = cop_vulcan_rejected
	}
	option = { # JOIN - Andoria
		name = STH_babel.21.e
		trigger = { has_country_flag = andorian_empire }
		ai_chance = { factor = 100 }
		random_country = {
			limit = {
				has_country_flag = andorian_empire
			}
			save_global_event_target_as = cop_andoria
		}
		set_country_flag = cop_andoria
	}
	option = { # REJECT - Andoria
		name = STH_babel.21.f
		trigger = { has_country_flag = andorian_empire }
		set_country_flag = cop_andoria_rejected
	}
	option = { # JOIN - Tellar
		name = STH_babel.21.g
		trigger = { has_country_flag = tellarian_technocracy }
		ai_chance = { factor = 100 }
		random_country = {
			limit = {
				has_country_flag = tellarian_technocracy
			}
			save_global_event_target_as = cop_tellar
		}
		set_country_flag = cop_tellar
	}
	option = { # REJECT - Tellar
		name = STH_babel.21.h
		trigger = { has_country_flag = tellarian_technocracy }
		set_country_flag = cop_tellar_rejected
	}
}

# COP NOTIFICATION
country_event = {
	id = STH_babel.22
	title = STH_babel.22.name
	desc = STH_babel.22.desc
	picture = sth_GFX_evt_heroDiplomacy1
	is_triggered_only = yes
	fire_only_once = yes

	trigger = {}
	immediate = {}

	option = {
		name = sth_understood
	}
}


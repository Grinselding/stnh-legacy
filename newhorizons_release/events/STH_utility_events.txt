namespace = STH_utility_events

###Fleet Stuff
country_event = {
	id = STH_utility_events.100
	hide_window = yes
	is_triggered_only = yes
	immediate = { 
		grab_fleet_power_to_variable_2 = yes
		if = {
			limit = { check_variable = { which = tempFleetValue2 value > tempFleetValue } }
			if = {
				limit = { num_ships >= 400 }
				while = { count = 5 random_owned_ship = { limit = { is_ship_class = shipclass_military } delete_ship = this } }
			}
			else_if = {
				limit = { num_ships >= 300 }
				while = { count = 4 random_owned_ship = { limit = { is_ship_class = shipclass_military } delete_ship = this } }
			}
			else_if = {
				limit = { num_ships >= 200 }
				while = { count = 3 random_owned_ship = { limit = { is_ship_class = shipclass_military } delete_ship = this } }
			}
			else_if = {
				limit = { num_ships >= 100 }
				while = { count = 2 random_owned_ship = { limit = { is_ship_class = shipclass_military } delete_ship = this } }
			}
			else = {
				random_owned_ship = { limit = { is_ship_class = shipclass_military } delete_ship = this }
			}
			country_event = { id = STH_utility_events.100 days = 1 }
		}
	}
}
# ##################################
# #
# # Nemesis Galactic Emperor Events
# #
# # Written by Henrik Thyrwall
# #
# ##################################

# namespace = emperor

# # Test Event
# country_event = {
# 	id = emperor.499
# 	hide_window = yes

# 	trigger = { always = no }

# 	immediate = {
# 		if = {
# 			limit = { is_galactic_council_established = no }
# 			set_council_size = 3
# 			random_playable_country = {
# 				limit = {
# 					is_galactic_community_member = yes 
# 					is_galactic_custodian = no
# 					is_part_of_galactic_council = no 
# 				}
# 				add_to_galactic_council = yes
# 			}
# 			random_playable_country = {
# 				limit = {
# 					is_galactic_community_member = yes 
# 					is_part_of_galactic_council = no 
# 					is_galactic_custodian = no
# 				}
# 				add_to_galactic_council = yes
# 			}
# 			random_playable_country = {
# 				limit = {
# 					is_galactic_community_member = yes 
# 					is_part_of_galactic_council = no 
# 					is_galactic_custodian = no
# 				}
# 				add_to_galactic_council = yes
# 			}
# 		}
# 		country_event = { id = emperor.1 }
# 	}
# }

# # Test Event
# country_event = {
# 	id = emperor.498
# 	hide_window = yes

# 	trigger = { always = no }

# 	immediate = {
# 		remove_from_galactic_community = yes
# 	}
# }

# # Test Event
# country_event = {
# 	id = emperor.497
# 	hide_window = yes

# 	trigger = { always = no }

# 	immediate = {
# 		set_timed_country_flag = { flag = imperial_crusade_target days = 3600 }
# 	}
# }

# # Test Event
# country_event = {
# 	id = emperor.496
# 	hide_window = yes

# 	trigger = { always = no }

# 	immediate = {
# 		add_to_galactic_community = yes
# 	}
# }

# # Test Event
# country_event = {
# 	id = emperor.495
# 	hide_window = yes

# 	trigger = { always = no }

# 	immediate = {
# 		set_global_flag = imperial_charter_granted
# 		add_modifier = { modifier = imperial_charter }
# 		random_playable_country = {
# 			limit = { is_galactic_emperor = yes }
# 			save_event_target_as = gal_emperor
# 		}
# 		random_playable_country = {
# 			limit = { has_modifier = imperial_charter }
# 			save_event_target_as = charter_corp
# 		}
# 		every_playable_country = {
# 			limit = { is_ai = no }
# 			country_event = { id = emperor.60 }
# 		}
# 	}
# }

# # Rise of the Galactic Empire - HIDDEN
# country_event = {
# 	id = emperor.1
# 	hide_window = yes

# 	is_triggered_only = yes

# 	immediate = {
# 		# Deal with existing Custodian resolutions
# 		if = {
# 			limit = {
# 				OR = {
# 					is_active_resolution = resolution_custodian_gdf
# 					is_active_resolution = resolution_custodian_expand_gdf
# 				}
# 			}
# 			set_timed_country_flag = { # To avoid duplicate IA events
# 				flag = gdf_turned_into_armada
# 				days = 11
# 			}
# 			country_event = { id = emperor.51 days = 10 }
# 		}

# 		# Coronation
# 		save_event_target_as = gal_emperor
# 		country_event = { id = emperor.2 }
# 	}
# }

# # Rise of the Galactic Empire
# country_event = {
# 	id = emperor.2
# 	title = "emperor.2.name"
# 	desc = {
# 		trigger = { is_gestalt = no }
# 		text = "emperor.2.normal.desc"
# 	}
# 	desc = {
# 		trigger = { is_machine_empire = yes }
# 		text = "emperor.2.machine.desc"
# 	}
# 	desc = {
# 		trigger = { is_hive_empire = yes }
# 		text = "emperor.2.hive.desc"
# 	}
# 	picture = GFX_evt_diplomatic_visit
# 	show_sound = event_announcement

# 	is_triggered_only = yes

# 	immediate = {
# 		remove_country_flag = custodianship_extended
# 		remove_country_flag = perpetual_custodianship
# 		set_galactic_emperor = yes
# 		random_country = {
# 			limit = { is_country_type = global_event }
# 			change_variable = {
# 				which = galactic_empires
# 				value = 1
# 			}
# 		}
# 		if = {
# 			limit = { is_gestalt = no }
# 			shift_ethic = "ethic_fanatic_authoritarian"
# 			change_government = {
# 				authority = auth_imperial
# 				civics = {
# 					civic = civic_galactic_sovereign
# 				}
# 			}
# 		}
# 		else = {
# 			change_government = {
# 				civics = {
# 					civic = civic_galactic_sovereign
# 				}
# 			}
# 		}
# 		set_name = random
# 		change_country_flag = {
# 			icon = {
# 				category = "special"
# 				file = "the_empire.dds"
# 			}
# 			background= {
# 				category = "backgrounds"
# 				file = "00_solid.dds"
# 			}
# 			colors={
# 				"red"
# 				"red"
# 				"null"
# 				"null"
# 			}
# 		}
# 		every_playable_country = {
# 			limit = {
# 				NOT = { is_same_value = root }
# 			}
# 			country_event = { id = emperor.3 }
# 		}
# 	}

# 	option = {
# 		name = emperor.2.a
# 	}
# }

# # Rise of the Galactic Empire (Others)
# country_event = {
# 	id = emperor.3
# 	title = "emperor.2.name"
# 	desc = {
# 		trigger = { 
# 			event_target:gal_emperor = { is_gestalt = no } 
# 		}
# 		text = "emperor.3.normal.desc"
# 	}
# 	desc = {
# 		trigger = { 
# 			event_target:gal_emperor = { is_machine_empire = yes }
# 		}
# 		text = "emperor.3.machine.desc"
# 	}
# 	desc = {
# 		trigger = { 
# 			event_target:gal_emperor = { is_hive_empire = yes }
# 		}
# 		text = "emperor.3.hive.desc"
# 	}
# 	picture = GFX_evt_diplomatic_visit
# 	show_sound = event_announcement

# 	is_triggered_only = yes

# 	after = {
# 		if = {
# 			limit = { is_galactic_community_member = yes }
# 			country_event = { id = emperor.4 }
# 		}
# 		if = {
# 			limit = { 
# 				is_galactic_community_member = yes
# 				has_federation = yes
# 			}
# 			country_event = { id = emperor.5 days = 16 }
# 		}
# 	}

# 	option = {
# 		trigger = { is_galactic_community_member = yes }
# 		name = emperor.3.a
# 	}
# 	option = {
# 		trigger = { is_galactic_community_member = no }
# 		name = emperor.3.b
# 	}
# }

# # Proclamation of Empire
# country_event = {
# 	id = emperor.4
# 	title = "emperor.4.title"
# 	desc = {
# 		trigger = { 
# 			event_target:gal_emperor = { is_gestalt = no } 
# 		}
# 		text = "emperor.4.a.desc"
# 	}
# 	desc = {
# 		trigger = { 
# 			event_target:gal_emperor = { is_machine_empire = yes }
# 		}
# 		text = "emperor.4.b.desc"
# 	}
# 	desc = {
# 		trigger = { 
# 			event_target:gal_emperor = { is_hive_empire = yes }
# 		}
# 		text = "emperor.4.c.desc"
# 	}

# 	diplomatic = yes

# 	picture_event_data = {
# 		portrait = event_target:gal_emperor
# 		planet_background = event_target:gal_emperor
# 		graphical_culture = event_target:gal_emperor
# 		city_level = event_target:gal_emperor
# 		room = event_target:gal_emperor.ruler
# 	}

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.4.a
# 	}
# }

# # Leave Federation or Leave Empire
# country_event = {
# 	id = emperor.5
# 	title = "emperor.5.name"
# 	desc = "emperor.5.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	trigger = {
# 		is_galactic_community_member = yes
# 		has_federation = yes
# 	}

# 	option = {
# 		name = emperor.5.a
# 		ai_chance = {
# 			factor = 100
# 		}	
# 		leave_alliance = { override_requirements = yes }
# 		default_hide_option = yes
# 	}
# 	option = {
# 		name = emperor.5.b
# 		ai_chance = {
# 			factor = 100
# 		}	
# 		remove_from_galactic_community = yes
# 		hidden_effect = {
# 			event_target:gal_emperor = {
# 				country_event = { id = emperor.6 }
# 			}
# 		}
# 	}
# }

# # Left Empire - Emperor informed
# country_event = {
# 	id = emperor.6
# 	title = "emperor.6.name"
# 	desc = "emperor.6.desc"
# 	picture = GFX_evt_announcement
# 	show_sound = event_announcement

# 	is_triggered_only = yes

# 	immediate = {
# 		add_opinion_modifier = {
# 			who = from
# 			modifier = opinion_traitor_to_empire
# 		}
# 	}

# 	option = {
# 		name = emperor.6.a
# 	}
# }

# # Imperial Armada Founded
# country_event = {
# 	id = emperor.50
# 	title = "emperor.50.name"
# 	desc = "emperor.50.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	trigger = {
# 		NOT = { has_country_flag = gdf_turned_into_armada }
# 	}

# 	immediate = {
# 		random_playable_country = {
# 			limit = { is_galactic_emperor = yes }
# 			save_event_target_as = gal_emperor
# 		}
# 	}

# 	option = {
# 		name = emperor.50.a
# 		trigger = {
# 			is_galactic_emperor = yes
# 			is_gestalt = no
# 		}
# 	}
# 	option = {
# 		name = emperor.50.b
# 		trigger = {
# 			is_galactic_emperor = yes
# 			is_gestalt = yes
# 		}
# 	}
# 	option = {
# 		name = emperor.50.c
# 		trigger = {
# 			is_galactic_emperor = no
# 		}
# 	}
# }

# # GDF Becomes Imperial Armada (HIDDEN)
# country_event = {
# 	id = emperor.51
# 	hide_window = yes

# 	is_triggered_only = yes

# 	immediate = {
# 		random_playable_country = {
# 			limit = { is_galactic_emperor = yes }
# 			save_event_target_as = gal_emperor
# 		}
# 		if = {
# 			limit = {
# 				 NOT = { is_active_resolution = resolution_custodian_expand_gdf }
# 			}
# 			pass_resolution = resolution_emperor_imperial_armada
# 		}
# 		else = {
# 			pass_resolution = resolution_emperor_imperial_armada
# 			pass_resolution = resolution_emperor_expand_ia
# 		}
# 		every_playable_country = {
# 			limit = { is_galactic_community_member = yes }
# 			country_event = { id = emperor.52 }
# 		}
# 	}
# }

# # GDF Becomes Imperial Armada
# country_event = {
# 	id = emperor.52
# 	title = "emperor.50.name"
# 	desc = "emperor.52.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	immediate = {
# 		random_playable_country = {
# 			limit = { is_galactic_emperor = yes }
# 			save_event_target_as = gal_emperor
# 		}
# 	}

# 	option = {
# 		name = emperor.50.a
# 		trigger = {
# 			is_galactic_emperor = yes
# 			is_gestalt = no
# 		}
# 	}
# 	option = {
# 		name = emperor.50.b
# 		trigger = {
# 			is_galactic_emperor = yes
# 			is_gestalt = yes
# 		}
# 	}
# 	option = {
# 		name = emperor.50.c
# 		trigger = {
# 			is_galactic_emperor = no
# 		}
# 	}
# }

# # Imperial Legions Founded
# country_event = {
# 	id = emperor.55
# 	title = "emperor.55.name"
# 	desc = "emperor.55.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	immediate = {
# 		random_playable_country = {
# 			limit = { is_galactic_emperor = yes }
# 			save_event_target_as = gal_emperor
# 		}
# 	}

# 	option = {
# 		name = emperor.55.a
# 		trigger = {
# 			is_galactic_emperor = yes
# 			is_gestalt = no
# 		}
# 	}
# 	option = {
# 		name = emperor.55.b
# 		trigger = {
# 			is_galactic_emperor = yes
# 			is_gestalt = yes
# 		}
# 	}
# 	option = {
# 		name = emperor.55.c
# 		trigger = {
# 			is_galactic_emperor = no
# 		}
# 	}
# }

# # Imperial Charter Granted
# country_event = {
# 	id = emperor.60
# 	title = "emperor.60.name"
# 	desc = "emperor.60.desc"
# 	picture = GFX_evt_ship_offloading_cargo
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	immediate = {
# 		random_playable_country = {
# 			limit = { is_galactic_emperor = yes }
# 			save_event_target_as = gal_emperor
# 		}
# 		random_playable_country = {
# 			limit = { has_modifier = imperial_charter }
# 			save_event_target_as = charter_corp
# 		}
# 	}

# 	option = {
# 		name = emperor.60.a
# 	}
# }

# # Imperial Concession Port Built
# planet_event = {
# 	id = emperor.61
# 	hide_window = yes

# 	is_triggered_only = yes

# 	trigger = {
# 		has_branch_office = yes
# 		branch_office_owner = { has_modifier = imperial_charter }
# 		last_building_changed = building_imperial_concession_port
# 	}

# 	immediate = {
# 		set_planet_flag = imp_concession_port
# 		random_playable_country = {
# 			limit = { is_galactic_emperor = yes }
# 			change_variable = {
# 				which = imp_concession_ports
# 				value = 1
# 			}
# 			country_event = { id = emperor.65 }
# 		}
# 	}
# }

# # Imperial Concession Port Removed
# planet_event = {
# 	id = emperor.62
# 	hide_window = yes

# 	is_triggered_only = yes

# 	trigger = {
# 		has_planet_flag = imp_concession_port
# 		NOT = { has_building = building_imperial_concession_port }
# 	}

# 	immediate = {
# 		remove_planet_flag = imp_concession_port
# 		random_playable_country = {
# 			limit = { is_galactic_emperor = yes }
# 			change_variable = {
# 				which = imp_concession_ports
# 				value = -1
# 			}
# 			country_event = { id = emperor.65 }
# 		}
# 	}
# }

# # Imperial Charter Country Destroyed
# country_event = {
# 	id = emperor.63
# 	hide_window = yes

# 	is_triggered_only = yes

# 	trigger = {
# 		has_modifier = imperial_charter
# 	}

# 	immediate = {
# 		remove_global_flag = imperial_charter_granted
# 		every_planet = {
# 			limit = { has_planet_flag = imp_concession_port }
# 			remove_planet_flag = imp_concession_port
# 		}
# 		random_playable_country = {
# 			limit = { is_galactic_emperor = yes }
# 			set_variable = {
# 				which = "imp_concession_ports"
# 				value = 0
# 			}
# 			add_imp_concession_ports_0 = yes
# 		}
# 	}
# }

# # Tally Imperial Concession Ports
# country_event = {
# 	id = emperor.65
# 	hide_window = yes

# 	is_triggered_only = yes

# 	immediate = {
# 		if = {
# 			limit = {
# 				check_variable = {
# 					which = imp_concession_ports
# 					value < 1
# 				}
# 			}
# 			add_imp_concession_ports_0 = yes
# 		}
# 		else_if = {
# 			limit = {
# 				check_variable = {
# 					which = imp_concession_ports
# 					value >= 1
# 				}
# 				check_variable = {
# 					which = imp_concession_ports
# 					value <= 2
# 				}
# 			}
# 			add_imp_concession_ports_1 = yes
# 		}
# 		else_if = {
# 			limit = {
# 				check_variable = {
# 					which = imp_concession_ports
# 					value >= 3
# 				}
# 				check_variable = {
# 					which = imp_concession_ports
# 					value <= 4
# 				}
# 			}
# 			add_imp_concession_ports_2 = yes
# 		}
# 		else_if = {
# 			limit = {
# 				check_variable = {
# 					which = imp_concession_ports
# 					value >= 5
# 				}
# 				check_variable = {
# 					which = imp_concession_ports
# 					value <= 6
# 				}
# 			}
# 			add_imp_concession_ports_3 = yes
# 		}
# 		else_if = {
# 			limit = {
# 				check_variable = {
# 					which = imp_concession_ports
# 					value >= 7
# 				}
# 				check_variable = {
# 					which = imp_concession_ports
# 					value <= 8
# 				}
# 			}
# 			add_imp_concession_ports_4 = yes
# 		}
# 		else_if = {
# 			limit = {
# 				check_variable = {
# 					which = imp_concession_ports
# 					value >= 9
# 				}
# 				check_variable = {
# 					which = imp_concession_ports
# 					value <= 10
# 				}
# 			}
# 			add_imp_concession_ports_5 = yes
# 		}
# 		else_if = {
# 			limit = {
# 				check_variable = {
# 					which = imp_concession_ports
# 					value >= 11
# 				}
# 				check_variable = {
# 					which = imp_concession_ports
# 					value <= 12
# 				}
# 			}
# 			add_imp_concession_ports_6 = yes
# 		}
# 		else_if = {
# 			limit = {
# 				check_variable = {
# 					which = imp_concession_ports
# 					value >= 13
# 				}
# 				check_variable = {
# 					which = imp_concession_ports
# 					value <= 14
# 				}
# 			}
# 			add_imp_concession_ports_7 = yes
# 		}
# 		else_if = {
# 			limit = {
# 				check_variable = {
# 					which = imp_concession_ports
# 					value >= 15
# 				}
# 				check_variable = {
# 					which = imp_concession_ports
# 					value <= 16
# 				}
# 			}
# 			add_imp_concession_ports_8 = yes
# 		}
# 		else_if = {
# 			limit = {
# 				check_variable = {
# 					which = imp_concession_ports
# 					value >= 17
# 				}
# 				check_variable = {
# 					which = imp_concession_ports
# 					value <= 18
# 				}
# 			}
# 			add_imp_concession_ports_9 = yes
# 		}
# 		else_if = {
# 			limit = {
# 				check_variable = {
# 					which = imp_concession_ports
# 					value >= 19
# 				}
# 			}
# 			add_imp_concession_ports_10 = yes
# 		}
# 	}
# }

# # Rebellion! (HIDDEN)
# event = {
# 	id = emperor.100
# 	hide_window = yes

# 	is_triggered_only = yes

# 	trigger = {
# 		imperial_authority <= 25
# 		any_playable_country = {
# 			is_galactic_community_member = yes
# 			is_galactic_emperor = no
# 			is_subject = no
# 			any_envoy = {
# 				has_envoy_task = { task = undermine_imperial_authority }
# 			}
# 		}
# 	}

# 	immediate = {
# 		random_playable_country = {
# 			limit = { is_galactic_emperor = yes }
# 			save_event_target_as = gal_emperor
# 		}
# 		# Find Rebel leader - non-AI first
# 		if = {
# 			limit = {
# 				any_playable_country = {
# 					is_ai = no
# 					is_galactic_community_member = yes
# 					is_galactic_emperor = no
# 					is_subject = no
# 					# is undermining Imperial Authority
# 				}
# 			}
# 			random_playable_country = {
# 				limit = {
# 					is_ai = no
# 					is_galactic_community_member = yes
# 					is_galactic_emperor = no
# 					is_subject = no
# 					# is undermining Imperial Authority
# 					NOT = {
# 						any_playable_country = {
# 							NOT = { is_same_value = prev }
# 							is_ai = no
# 							is_galactic_community_member = yes
# 							is_galactic_emperor = no
# 							# is undermining Imperial Authority
# 							relative_power = {
# 								who = prev
# 								value > equivalent
# 							}
# 						}
# 					}
# 				}
# 				country_event = { id = emperor.101 }
# 			}
# 		}
# 		else = {
# 			random_playable_country = {
# 				limit = {
# 					is_galactic_community_member = yes
# 					is_galactic_emperor = no
# 					is_subject = no
# 					# is undermining Imperial Authority
# 					NOT = {
# 						any_playable_country = {
# 							NOT = { is_same_value = prev }
# 							is_galactic_community_member = yes
# 							is_galactic_emperor = no
# 							# is undermining Imperial Authority
# 							relative_power = {
# 								who = prev
# 								value > equivalent
# 							}
# 						}
# 					}
# 				}
# 				country_event = { id = emperor.101 }
# 			}
# 		}
# 	}
# }

# # Rebellion! Take the lead?
# country_event = {
# 	id = emperor.101
# 	title = "emperor.101.name"
# 	desc = "emperor.101.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.101.a
# 		custom_tooltip = joined_galactic_empire_rebellion
# 		hidden_effect = {
# 			set_country_flag = empire_rebel
# 			save_event_target_as = rebel_leader

# 			# Subjects join
# 			if = {
# 				limit = { is_overlord = yes }
# 				every_subject = {
# 					country_event = { id = emperor.108 }
# 				}
# 			}

# 			# Recruit others
# 			if = {
# 				limit = {
# 					any_playable_country = {
# 						is_galactic_community_member = yes
# 						is_galactic_emperor = no
# 						is_subject = no
# 						NOR = {
# 							has_country_flag = empire_loyalist
# 							has_country_flag = empire_rebel
# 						}
# 					}
# 				}
# 				random_playable_country = {
# 					limit = {
# 						is_galactic_community_member = yes
# 						is_galactic_emperor = no
# 						is_subject = no
# 						NOR = {
# 							has_country_flag = empire_loyalist
# 							has_country_flag = empire_rebel
# 						}
# 					}
# 					country_event = { id = emperor.103 }
# 				}
# 			}

# 			# We stand alone
# 			else = {
# 				country_event = { id = emperor.104 }
# 			}
# 		}
# 	}
# 	option = {
# 		name = emperor.101.b
# 		custom_tooltip = joined_galactic_empire_loyalists
# 		hidden_effect = {
# 			set_country_flag = empire_loyalist
# 			if = {
# 				limit = {
# 					any_playable_country = {
# 						is_ai = no
# 						NOT = { is_same_value = root }
# 						is_galactic_community_member = yes
# 						is_galactic_emperor = no
# 						is_subject = no
# 						NOT = { has_country_flag = empire_loyalist }
# 						# is undermining Imperial Authority
# 					}
# 				}
# 				random_playable_country = {
# 					limit = {
# 						is_ai = no
# 						NOT = { is_same_value = root }
# 						is_galactic_community_member = yes
# 						is_galactic_emperor = no
# 						is_subject = no
# 						NOT = { has_country_flag = empire_loyalist }
# 						# is undermining Imperial Authority
# 						NOT = {
# 							any_playable_country = {
# 								NOT = { is_same_value = prev }
# 								NOT = { is_same_value = root }
# 								is_ai = no
# 								is_galactic_community_member = yes
# 								is_galactic_emperor = no
# 								# is undermining Imperial Authority
# 								relative_power = {
# 									who = prev
# 									value > equivalent
# 								}
# 							}
# 						}
# 					}
# 					country_event = { id = emperor.101 }
# 				}
# 			}
# 			else_if = {
# 				limit = {
# 					any_playable_country = {
# 						is_galactic_community_member = yes
# 						is_galactic_emperor = no
# 						is_subject = no
# 						NOT = { is_same_value = root }
# 						NOT = { has_country_flag = empire_loyalist }
# 						# is undermining Imperial Authority
# 					}
# 				}
# 				random_playable_country = {
# 					limit = {
# 						is_galactic_community_member = yes
# 						is_galactic_emperor = no
# 						is_subject = no
# 						NOT = { is_same_value = root }
# 						NOT = { has_country_flag = empire_loyalist }
# 						# is undermining Imperial Authority
# 						NOT = {
# 							any_playable_country = {
# 								NOT = { is_same_value = prev }
# 								NOT = { is_same_value = root }
# 								is_galactic_community_member = yes
# 								is_galactic_emperor = no
# 								NOT = { has_country_flag = empire_loyalist }
# 								# is undermining Imperial Authority
# 								relative_power = {
# 									who = prev
# 									value > equivalent
# 								}
# 							}
# 						}
# 					}
# 					country_event = { id = emperor.101 }
# 				}
# 			}
# 			# No one wanted to dance!
# 			else = {
# 				every_playable_country = {
# 					limit = { is_galactic_community_member = yes }
# 				}
# 				country_event = { id = emperor.102 }
# 			}
# 		}
# 	}
# }

# # No one joined Rebellion
# country_event = {
# 	id = emperor.102
# 	title = "emperor.102.name"
# 	desc = {
# 		trigger = { is_galactic_emperor = no }
# 		text = "emperor.102.a.desc"
# 	}
# 	desc = {
# 		trigger = { is_galactic_emperor = yes }
# 		text = "emperor.102.b.desc"
# 	}
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.102.a
# 		trigger = { is_galactic_emperor = no }
# 	}
# 	option = {
# 		name = emperor.102.b
# 		trigger = { is_galactic_emperor = yes }
# 	}
# }

# # Rebellion! Join?
# country_event = {
# 	id = emperor.103
# 	title = "emperor.101.name"
# 	desc = "emperor.103.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.103.a
# 		hidden_effect = {
# 			set_country_flag = empire_rebel

# 			# Form federation if it doesn't exist yet
# 			if = {
# 				limit = {
# 					event_target:rebel_leader = { has_federation = no }
# 				}
# 				join_alliance = { 
# 					who = event_target:rebel_leader
# 					name = NAME_Rebellion
# 					override_requirements = yes 
# 				}
# 				set_federation_leader = event_target:rebel_leader
# 				federation = { 
# 					set_federation_flag = empire_rebels
# 					add_cohesion = 150 
# 					add_federation_experience = 4200
# 				}
# 			}

# 			# Join existing federation
# 			else = {
# 				join_alliance = { who = event_target:rebel_leader override_requirements = yes }
# 			}

# 			# Subjects join
# 			if = {
# 				limit = { is_overlord = yes }
# 				every_subject = {
# 					country_event = { id = emperor.108 }
# 				}
# 			}

# 			# Recruit others
# 			if = {
# 				limit = {
# 					any_playable_country = {
# 						is_galactic_community_member = yes
# 						is_galactic_emperor = no
# 						is_subject = no
# 						NOR = {
# 							has_country_flag = empire_loyalist
# 							has_country_flag = empire_rebel
# 						}
# 					}
# 				}
# 				random_playable_country = {
# 					limit = {
# 						is_galactic_community_member = yes
# 						is_galactic_emperor = no
# 						is_subject = no
# 						NOR = {
# 							has_country_flag = empire_loyalist
# 							has_country_flag = empire_rebel
# 						}
# 					}
# 					country_event = { id = emperor.103 }
# 				}
# 			}
# 			else = {
# 				# Tally rebels
# 				event_target:rebel_leader = {
# 					country_event = { id = emperor.105 }
# 				}
# 			}
# 		}
# 	}
# 	option = {
# 		name = emperor.103.b
# 		hidden_effect = {
# 			set_country_flag = empire_loyalist

# 			# Recruit others
# 			if = {
# 				limit = {
# 					any_playable_country = {
# 						is_galactic_community_member = yes
# 						is_galactic_emperor = no
# 						is_subject = no
# 						NOR = {
# 							has_country_flag = empire_loyalist
# 							has_country_flag = empire_rebel
# 						}
# 					}
# 				}
# 				random_playable_country = {
# 					limit = {
# 						is_galactic_community_member = yes
# 						is_galactic_emperor = no
# 						is_subject = no
# 						NOR = {
# 							has_country_flag = empire_loyalist
# 							has_country_flag = empire_rebel
# 						}
# 					}
# 					country_event = { id = emperor.103 }
# 				}
# 			}
# 			# Tally rebels
# 			else = {
# 				event_target:rebel_leader = {
# 					country_event = { id = emperor.105 }
# 				}
# 			}
# 		}
# 	}
# }

# # We Stand Alone
# country_event = {
# 	id = emperor.104
# 	title = "emperor.104.name"
# 	desc = "emperor.104.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.104.a
# 	}
# }

# # Tally Rebels (HIDDEN)
# country_event = {
# 	id = emperor.105
# 	hide_window = yes

# 	is_triggered_only = yes

# 	immediate = {
# 		# Emperor's subjects join Loyalists
# 		event_target:gal_emperor = {
# 			if = {
# 				limit = { is_overlord = yes }
# 				every_subject = {
# 					country_event = { id = emperor.108 }
# 				}
# 			}
# 		}

# 		if = {
# 			# No one else joined
# 			limit = { has_federation = no }
# 			remove_from_galactic_community = yes
# 			country_event = { id = emperor.104 }

# 			event_target:gal_emperor = {
# 				country_event = { id = emperor.106 }
# 			}
# 		}
# 		else = {
# 			every_playable_country = {
# 				limit = {
# 					is_galactic_community_member = yes
# 					has_country_flag = empire_rebel
# 				}
# 				remove_from_galactic_community = yes
# 			}

# 			# Inform Emperor
# 			event_target:gal_emperor = {
# 				country_event = { id = emperor.106 }
# 			}


# 		}
# 	}
# }

# # Open Revolt!
# country_event = {
# 	id = emperor.106
# 	title = "emperor.106.name"
# 	desc = {
# 		trigger = {
# 			from = { has_federation = no }
# 			any_playable_country = {
# 				is_galactic_community_member = yes
# 				has_country_flag = empire_loyalist
# 			}
# 		}
# 		text = "emperor.106.a.desc"
# 	}
# 	desc = {
# 		trigger = {
# 			from = { has_federation = yes }
# 			any_playable_country = {
# 				is_galactic_community_member = yes
# 				has_country_flag = empire_loyalist
# 			}
# 		}
# 		text = "emperor.106.b.desc"
# 	}
# 	desc = {
# 		trigger = {
# 			NOT = {
# 				any_playable_country = {
# 					is_galactic_community_member = yes
# 					has_country_flag = empire_loyalist
# 				}
# 			}
# 		}
# 		text = "emperor.106.c.desc"
# 	}
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	immediate = {
# 		# Form Loyalist federation
# 		if = {
# 			limit = {
# 				any_playable_country = {
# 					is_galactic_community_member = yes
# 					has_country_flag = empire_loyalist
# 				}
# 			}
# 			random_playable_country = {
# 				limit = {
# 					is_galactic_community_member = yes
# 					has_country_flag = empire_loyalist
# 				}
# 				join_alliance = { 
# 					who = root
# 					name = NAME_Loyalists
# 					override_requirements = yes 
# 				}
# 				set_federation_leader = root
# 			}
# 			every_playable_country = {
# 				limit = {
# 					is_galactic_community_member = yes
# 					has_country_flag = empire_loyalist
# 					has_federation = no
# 				}
# 				join_alliance = { who = root override_requirements = yes }
# 			}
# 			federation = { 
# 				set_federation_flag = empire_loyalists
# 				set_federation_type = imperial_loyalists_federation
# 				add_cohesion = 200
# 				set_federation_law = centralization_imperial_loyalists
# 				set_federation_law = succession_type_none
# 				set_federation_law = succession_term_perpetual
# 				set_federation_law = fleet_contribution_none
# 				set_federation_law = treaties_separate_no_imperial_loyalists
# 				set_federation_law = declare_war_president_vote_imperial_loyalists
# 				set_federation_law = invite_members_president_vote_imperial_loyalists
# 				set_federation_law = kick_members_president_vote_imperial_loyalists
# 				set_federation_law = vote_weight_diplomatic_imperial_loyalists
# 				set_federation_law = allow_subjects_to_join_yes_imperial_loyalists
# 			}
# 		}

# 		add_imperial_authority = 50 # Disruptive elements left Empire

# 		# Declare War
# 		declare_war = {
# 			target = event_target:rebel_leader
# 			name = "NAME_The_Civil_War"
# 			attacker_war_goal = "wg_galactic_civil_war_loyalists"
# 		}

# 		# Inform non-members of civil war
# 		every_playable_country = {
# 			limit = {
# 				is_ai = no
# 				is_galactic_community_member = no
# 				NOT = { has_country_flag = empire_rebel }
# 			}
# 			country_event = { id = emperor.107 }
# 		}
# 	}

# 	option = {
# 		name = emperor.106.a
# 	}
# }

# # Galactic Civil War - Outsiders Informed
# country_event = {
# 	id = emperor.107
# 	title = "emperor.107.name"
# 	desc = {
# 		trigger = {
# 			event_target:rebel_leader = { has_federation = no }
# 		}
# 		text = "emperor.107.a.desc"
# 	}
# 	desc = {
# 		trigger = {
# 			event_target:rebel_leader = { has_federation = no }
# 		}
# 		text = "emperor.107.b.desc"
# 	}
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.107.a
# 	}
# }

# # Galactic Civil War - Subject joins Overlord
# country_event = {
# 	id = emperor.108
# 	title = "emperor.107.name"
# 	desc = {
# 		trigger = {
# 			overlord = { has_country_flag = empire_loyalist }
# 		}
# 		text = "emperor.108.a.desc"
# 	}
# 	desc = {
# 		trigger = {
# 			overlord = { has_country_flag = empire_rebel }
# 		}
# 		text = "emperor.108.b.desc"
# 	}
# 	desc = {
# 		trigger = {
# 			overlord = { is_galactic_emperor = yes }
# 		}
# 		text = "emperor.108.c.desc"
# 	}
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	immediate = {
# 		if = {
# 			limit = {
# 				overlord = { has_country_flag = empire_rebel }
# 			}
# 			set_country_flag = empire_rebel
# 			# Form federation if it doesn't exist yet
# 			if = {
# 				limit = {
# 					overlord = { has_federation = no }
# 				}
# 				join_alliance = { 
# 					who = event_target:rebel_leader
# 					name = NAME_Rebellion
# 					override_requirements = yes 
# 				}
# 				set_federation_leader = overlord
# 				federation = { 
# 					set_federation_flag = empire_rebels
# 					add_cohesion = 150 
# 					add_federation_experience = 4200
# 				}
# 			}

# 			# Join existing federation
# 			else = {
# 				join_alliance = { who = overlord override_requirements = yes }
# 			}
# 		}
# 		if = {
# 			limit = {
# 				overlord = { 
# 					OR = {
# 						has_country_flag = empire_loyalist
# 						is_galactic_emperor = yes
# 					}
# 				}
# 			}
# 			set_country_flag = empire_loyalist
# 		}
# 	}

# 	option = {
# 		name = emperor.108.a
# 	}
# }

# # Loyalist Victory (Emperor)
# country_event = {
# 	id = emperor.150
# 	title = "emperor.150.name"
# 	desc = "emperor.150.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	after = {
# 		every_playable_country = {
# 			limit = { has_country_flag = empire_loyalist }
# 			country_event = { id = emperor.151 }
# 		}
# 		every_playable_country = {
# 			limit = { has_country_flag = empire_rebel }
# 			country_event = { id = emperor.152 }
# 		}
# 		every_playable_country = {
# 			limit = {
# 				is_ai = no
# 				is_galactic_community_member = no
# 			}
# 			country_event = { id = emperor.153 }
# 		}
# 	}

# 	option = {
# 		name = emperor.150.a
# 		add_imperial_authority = 100
# 		hidden_effect = {
# 			set_country_flag = civil_war_leniency
# 			every_playable_country = {
# 				limit = { has_country_flag = empire_rebel }
# 				add_modifier = {
# 					modifier = "former_rebel"
# 					days = 2160
# 				}
# 			}
# 		}
# 	}
# 	option = {
# 		name = emperor.150.b
# 		add_imperial_authority = 100
# 		hidden_effect = {
# 			set_country_flag = civil_war_punishment
# 			every_playable_country = {
# 				limit = { has_country_flag = empire_rebel }
# 				add_modifier = {
# 					modifier = "former_rebel"
# 					days = 2160
# 				}
# 			}
# 		}
# 	}
# }

# # Loyalist Victory (Loyalists)
# country_event = {
# 	id = emperor.151
# 	title = "emperor.150.name"
# 	desc = {
# 		trigger = {
# 			from = { has_country_flag = civil_war_leniency }
# 		}
# 		text = "emperor.151.a.desc"
# 	}
# 	desc = {
# 		trigger = {
# 			from = { has_country_flag = civil_war_punishment }
# 		}
# 		text = "emperor.151.b.desc"
# 	}
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	immediate = {
# 		remove_country_flag = empire_loyalist
# 	}

# 	option = {
# 		name = emperor.151.a
# 	}
# }

# # Loyalist Victory (Rebels)
# country_event = {
# 	id = emperor.152
# 	title = "emperor.152.name"
# 	desc = {
# 		trigger = {
# 			from = { has_country_flag = civil_war_leniency }
# 		}
# 		text = "emperor.152.a.desc"
# 	}
# 	desc = {
# 		trigger = {
# 			from = { has_country_flag = civil_war_punishment }
# 		}
# 		text = "emperor.152.b.desc"
# 	}
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	immediate = {
# 		remove_country_flag = empire_rebel
# 	}

# 	option = {
# 		name = emperor.152.a
# 		trigger = {
# 			from = { has_country_flag = civil_war_leniency }
# 		}
# 	}
# 	option = {
# 		name = emperor.152.b
# 		trigger = {
# 			from = { has_country_flag = civil_war_punishment }
# 		}
# 	}
# }

# # Loyalist Victory (Others)
# country_event = {
# 	id = emperor.153
# 	title = "emperor.153.name"
# 	desc = "emperor.153.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.153.a
# 	}
# }

# # Rebel Victory (Rebels)
# country_event = {
# 	id = emperor.200
# 	title = "emperor.200.name"
# 	desc = "emperor.200.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.200.a
# 	}
# }

# # Rebel Victory (Loyalists)
# country_event = {
# 	id = emperor.201
# 	title = "emperor.200.name"
# 	desc = "emperor.201.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	immediate = {
# 		add_modifier = {
# 			modifier = "former_loyalist"
# 			days = 2160
# 		}
# 	}

# 	option = {
# 		name = emperor.201.a
# 	}
# }

# # Rebel Victory (Emperor)
# country_event = {
# 	id = emperor.202
# 	title = "emperor.200.name"
# 	desc = "emperor.202.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	immediate = {
# 		add_modifier = {
# 			modifier = "former_emperor"
# 			days = 2880
# 		}
# 	}

# 	option = {
# 		name = emperor.202.a
# 	}
# }

# # Rebel Victory (Others)
# country_event = {
# 	id = emperor.203
# 	title = "emperor.203.name"
# 	desc = "emperor.203.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.153.a
# 	}
# }

# # Status Quo (Emperor and Loyalists)
# country_event = {
# 	id = emperor.250
# 	title = "emperor.250.name"
# 	desc = "emperor.250.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	immediate = {
# 		remove_country_flag = empire_loyalist
# 	}

# 	option = {
# 		name = emperor.250.a
# 	}
# }

# # Status Quo (Rebels)
# country_event = {
# 	id = emperor.251
# 	title = "emperor.250.name"
# 	desc = "emperor.251.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	immediate = {
# 		remove_country_flag = empire_rebel
# 	}

# 	option = {
# 		name = emperor.251.a
# 	}
# }

# # Status Quo (Others)
# country_event = {
# 	id = emperor.252
# 	title = "emperor.250.name"
# 	desc = "emperor.252.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.153.a
# 	}
# }

# # Restore Community (Winner)
# country_event = {
# 	id = emperor.300
# 	title = "emperor.300.name"
# 	desc = "emperor.300.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	immediate = {
# 		save_event_target_as = dissolve_winner
# 		add_to_galactic_community = yes
# 	}

# 	option = {
# 		name = emperor.300.a
# 	}
# }

# # Restore Community (Imperials)
# country_event = {
# 	id = emperor.301
# 	title = "emperor.301.name"
# 	desc = "emperor.301.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.301.a
# 	}
# }


# # Restore Community (Others)
# country_event = {
# 	id = emperor.302
# 	title = "emperor.301.name"
# 	desc = "emperor.302.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.302.a
# 	}
# 	option = {
# 		name = emperor.302.b
# 	}
# }

# # Seize the Throne (Winners)
# country_event = {
# 	id = emperor.310
# 	title = "emperor.310.name"
# 	desc = "emperor.310.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	immediate = {
# 		add_to_galactic_community = yes
# 	}

# 	option = {
# 		name = emperor.310.a
# 	}
# }

# # Seize the Throne (Imperials)
# country_event = {
# 	id = emperor.311
# 	title = "emperor.310.name"
# 	desc = "emperor.311.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.311.a
# 	}
# }

# # Seize the Throne (New Emperor)
# country_event = {
# 	id = emperor.312
# 	title = "emperor.310.name"
# 	desc = "emperor.312.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.312.a
# 	}
# }

# # Empire Destroyed
# country_event = {
# 	id = emperor.400
# 	hide_window = yes

# 	is_triggered_only = yes

# 	trigger = {
# 		is_galactic_emperor = yes
# 	}

# 	immediate = {
# 		set_country_flag = silence_destroy_event
# 		every_playable_country = {
# 			limit = { is_ai = no }
# 			country_event = { id = emperor.401 }
# 		}
# 		set_galactic_emperor = no
# 		set_council_size = 0
# 	}
# }

# # Empire Destroyed
# country_event = {
# 	id = emperor.401
# 	title = "emperor.401.name"
# 	desc = "emperor.401.desc"
# 	picture = GFX_evt_partition
# 	show_sound = event_vote

# 	is_triggered_only = yes

# 	option = {
# 		name = emperor.401.a
# 	}
# }
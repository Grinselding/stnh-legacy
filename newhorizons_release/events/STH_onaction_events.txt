namespace = STH_onaction

#First Contact Gatekeeper
#TODO - update everything to use this
country_event = {
	id = STH_onaction.1
	hide_window = yes
	is_triggered_only = yes
	immediate = { 
		# print_scope_effect = yes
		if = {
			limit = { is_normal_country = yes from = { has_country_flag = edo_god_country any_owned_fleet = { has_fleet_flag = edo_god_fleet } } }
			# log = "should get first contact"
			from = { 
				random_owned_fleet = { 
					limit = { has_fleet_flag = edo_god_fleet }
					save_event_target_as = edo_god_fleet
				} 
			}
			country_event = { id = STH_galactic_features.1500 scopes = { from = root.from fromfrom = event_target:edo_god_fleet } }
			
		}
	}
}


#Meet Undine
country_event = {
	id = STH_onaction.11
	title = "action.10.name"
	desc = "action.15.desc"
	picture = GFX_evt_star_chart
	location = FROMFROMFROM
	is_triggered_only = yes
	trigger = {
		OR = { 
			is_country_type = default
			is_country_type = minorRace
		}
		exists = capital_scope
		FROM = { is_country_type = undine }
	}
	immediate = { 
		
	}
	option = {
		name = "action.10.a"
		establish_communications_no_message = FROM
		country_event = { id = STH_undine_crisis.101 scopes = { from = from fromfrom = fromfrom fromfromfrom = fromfromfrom } }
	}
}


# Kill Amoebas
event = {
	id = STH_onaction.100
	hide_window = yes
	is_triggered_only = yes
	trigger = {
		any_country = { is_country_type = amoeba }
	}
	immediate = { 
		every_country = {
			limit = { is_country_type = amoeba }
			destroy_country = yes
		}
	}
}

# A planets controller becomes a country not the same as the owner.
# Root = Planet
# From = Planet Owner
# FromFrom = Planet Controller (the one occupying)
###Transfer control
planet_event = {
	id = STH_onaction.150
	hide_window = yes
	is_triggered_only = yes
	trigger = {
		exists = FromFrom
		FromFrom = { exists = overlord is_member_world = yes }
	}
	immediate = { 
		FromFrom = { overlord = { save_event_target_as = overlordCountry } }
		set_controller = event_target:overlordCountry
	}
}



# Gender Bias Gatekeeper
event = {
	id = STH_onaction.199
	hide_window = yes
	is_triggered_only = yes
	trigger = { }
	immediate = {
		every_country = {
			limit = { misogynist = yes }
			country_event = { id = STH_onaction.200 }
		}
	}
}

# Misogynist and Misandrist Species
country_event = {
	id = STH_onaction.200
	hide_window = yes
	is_triggered_only = yes
	trigger = {
		OR = {
			misogynist = yes
			misandrist = yes
		}
	}
	immediate = {
		if = {
			limit = { misogynist = yes }
			every_pool_leader = {
				limit = { gender = female }
				set_is_female = no
				set_name = random
			}
			every_owned_leader = {
				limit = { gender = female }
				set_is_female = no
				set_name = random
			}
		}
		if = {
			limit = { misandrist = yes }
			every_pool_leader = {
				limit = { gender = male }
				set_is_female = yes
				set_name = random
			}
			every_owned_leader = {
				limit = { gender = male }
				set_is_female = yes
				set_name = random
			}
		}
	}
}

#More female scientists, more male soldiers
country_event = {
	id = STH_onaction.201
	hide_window = yes
	is_triggered_only = yes
	trigger = {
		exists = FROM
		femaleScienceMaleSoldier = yes
	}
	immediate = {		
		FROM = {
			if = {
				limit = { gender = female OR = { leader_class = admiral leader_class = general } }
				random_list = {
					10 = { recruitable = no }
					10 = { }
				}
			}
			if = {
				limit = { gender = male OR = { leader_class = scientist } }
				random_list = {
					10 = { recruitable = no }
					10 = { }
				}
			}
		}
	}
}

#Leader Species Traits
country_event = {
	id = STH_onaction.202
	hide_window = yes
	is_triggered_only = yes
	trigger = {
		OR = { 
			is_assimilator = yes
			is_former_drone_country = yes
			AND = { is_species_class = VID NOT = { has_global_flag = phageCure } }
			FROM = { species = { has_trait = trait_darsay_personality } }
		}
	}
	immediate = {
		if = {
			limit = { is_assimilator = yes }
			FROM = {
				add_trait = leader_trait_assimilated
				remove_trait = leader_trait_partially_assimilated
				set_borg_name = yes
			}
			every_pool_leader = { 
				limit = { NOT = { has_trait = leader_trait_assimilated } } 
				add_trait = leader_trait_assimilated 
				remove_trait = leader_trait_partially_assimilated
			}
			every_owned_leader = { 
				limit = { NOT = { has_trait = leader_trait_assimilated } } 
				add_trait = leader_trait_assimilated 
				remove_trait = leader_trait_partially_assimilated
			}
		}
		if = {
			limit = { is_former_drone_country = yes }
			FROM = {
				add_trait = leader_trait_partially_assimilated
				# set_borg_name = yes
			}
			every_pool_leader = { 
				limit = { NOT = { has_trait = leader_trait_partially_assimilated } } 
				add_trait = leader_trait_partially_assimilated 
				remove_trait = leader_trait_assimilated
			}
			every_owned_leader = { 
				limit = { NOT = { has_trait = leader_trait_partially_assimilated } } 
				add_trait = leader_trait_partially_assimilated 
				remove_trait = leader_trait_assimilated
			}
		}
		if = {
			limit = { FROM = { species = { has_trait = trait_darsay_personality } } }
			FROM = { 
				# add_trait = leader_trait_assimilated
				set_darsay_name = yes
			}
			# every_pool_leader = { limit = { NOT = { has_trait = leader_trait_assimilated } } add_trait = leader_trait_assimilated }
			# every_owned_leader = { limit = { NOT = { has_trait = leader_trait_assimilated } } add_trait = leader_trait_assimilated }
		}
		if = {
			limit = { is_species_class = VID NOT = { has_global_flag = phageCure } }
			FROM = { add_trait = leader_trait_the_phage }
			every_pool_leader = { limit = { NOT = { has_trait = leader_trait_the_phage } } add_trait = leader_trait_the_phage }
			every_owned_leader = { limit = { NOT = { has_trait = leader_trait_the_phage } } add_trait = leader_trait_the_phage }
		}
	}
}

# Expand Helix
planet_event = {
	id = STH_onaction.300
	hide_window = yes
	is_triggered_only = yes
	###TODO FIX HELIX EXPANSION
}

event = {
	id = STH_onaction.500
	hide_window = yes
	is_triggered_only = yes
	immediate = { 
		every_country = {	
			if = {	
				limit = { 
					OR = { 
						is_country_type = default
						is_country_type = minorRace
						is_country_type = ufp_member_world
					}	
					NOR = { 
						has_technology = tech_phaser_cannon_1
						has_technology = tech_phaser_1
						has_technology = tech_plasma_disruptor_beam_1
						has_technology = tech_antiproton_cannon_1
						has_technology = tech_polaron_beam_1
						has_technology = tech_tetryon_cannon_1
						has_technology = tech_disruptor_cannon_1
						has_technology = tech_plasma_cannon_1
					}
					NOR = { 
						has_technology = tech_engineering_starship-class_717
						has_technology = tech_engineering_bird-of-prey_716
					}					
				}
			give_technology = { tech = tech_engineering_starship-class_717 message = no }
			give_technology = { tech = tech_phaser_1 message = no }
			}
			if = {	
				limit = {
					OR = { 
						is_country_type = default
						is_country_type = minorRace
						is_country_type = ufp_member_world
					}			
					NOT = { has_technology = tech_engineering_starship-class_717 }
					OR = {
						has_technology = tech_phaser_cannon_1
						has_technology = tech_phaser_1
						has_technology = tech_plasma_disruptor_beam_1
						has_technology = tech_polaron_beam_1
						has_technology = tech_antiproton_cannon_1
					}
				}
				give_technology = { tech = tech_engineering_starship-class_717 message = no }
			}
			if = {	
				limit = {
					OR = { 
						is_country_type = default
						is_country_type = minorRace
						is_country_type = ufp_member_world
					}			
					NOT = { has_technology = tech_engineering_bird-of-prey_716 }
					OR = {
						has_technology = tech_disruptor_cannon_1
						has_technology = tech_plasma_cannon_1
						has_technology = tech_tetryon_cannon_1
					}
				}	
				give_technology = { tech = tech_engineering_bird-of-prey_716 message = no }
			}			
		}	
	}
}

#Remove Borg subjects
country_event = {
	id = STH_onaction.600
	hide_window = yes
	trigger = {
		is_assimilator = yes
		is_overlord = yes
	}
	mean_time_to_happen = { months = 1 }
	immediate = {
		every_subject = { 
			limit = { NOT = { has_country_flag = assimilation_cube_country } }
			set_subject_of = { who = none } 
		}
	}
}

##Leader level up reward to other leaders - Massively performance intensive, do not use
#country_event = {
#	id = STH_onaction.700
#	hide_window = yes
#	is_triggered_only = yes
#	trigger = {
#		exists = FROM
#		OR = {
#			AND = {
#				has_swapped_tradition = tr_perfection_3
#				FROM = { has_trait = leader_trait_assimilated }
#			}
#			FROM = { species = { is_species_class = DOM } }
#		}
#	}
#	immediate = {
#		if = {
#			limit = { has_swapped_tradition = tr_perfection_3 FROM = { has_trait = leader_trait_assimilated } }
#			every_owned_leader = { 
#				limit = { 
#					NOT = { is_same_value = FROM } 
#					has_trait = leader_trait_assimilated
#				}
#				add_experience = 100
#			}
#		}
#		if = {
#			limit = { FROM = { species = { is_species_class = DOM } } }
#			every_owned_leader = {
#				limit = { 
#					NOT = { is_same_value = FROM } 
#					species = { is_species_class = DOM }
#				}
#				add_experience = 100
#			}
#		}
#	}
#}

#Registry / NCC number setting on ship build
ship_event = {
	id = STH_onaction.800
	hide_window = yes  
	is_triggered_only = yes
	trigger = { 
		exists = owner 
		# owner = { uses_ship_registry = yes }
	}
	immediate = {
		set_ship_registry = yes
	}
}

#Registry / NCC number setting on startup
event = {
	id = STH_onaction.801
	hide_window = yes  
	is_triggered_only = yes
	trigger = { }
	immediate = {
		every_country = {
			every_owned_ship = {
				limit = {  
					OR = {
						# is_ship_class = shipclass_military
						is_ship_class = shipclass_colonizer
						is_ship_class = shipclass_constructor
						is_ship_class = shipclass_transport
						is_ship_class = shipclass_science_ship
					}
				}
				set_ship_registry = yes
			}
		}
	}
}


#Dead Leader Gets Ship Name
# Executed as a leader has died
# This = Country
# From = Leader
country_event = {
	id = STH_onaction.900
	title = STH_onaction.900.name
	desc = { text = STH_onaction.900.desc }
	picture = {
		trigger = { is_species_class = KDF }
		picture = sth_GFX_evt_leaderDeathKlingon
	}
	picture = {
		trigger = { starfleet_empire = yes }
		picture = sth_GFX_evt_leaderDeathFederation
	}
	picture = {
		trigger = { NOR = { starfleet_empire = yes is_species_class = KDF } }
		picture = sth_GFX_evt_leaderDeath
	}
	trigger = {
		is_assimilator = no
		is_changeling_empire = no
		exists = FROM
		FROM = { 
			has_skill > 4 
			NOT = { has_leader_flag = silentDeath }
		}
		any_owned_ship = { 
			is_ship_class = shipclass_military 
			NOT = { has_ship_flag = renamedInHonour }
			fleet = { is_alliance_fleet = no }
		}
	}
	is_triggered_only = yes
	immediate = {
		clone_leader = { 
			target = FROM 
			skill = 1
			effect = { 
				save_event_target_as = leaderTarget
				recruitable = no
			}
		}
		random_owned_ship = {
			limit = { 
				is_ship_class = shipclass_military 
				NOR = { 
					has_ship_flag = renamedInHonour 
					has_ship_flag = hero_ship
				}
				fleet = { is_alliance_fleet = no }
			}
			save_event_target_as = shipCandidate1
		}
		random_owned_ship = {
			limit = { 
				is_ship_class = shipclass_military 
				NOR = { 
					has_ship_flag = renamedInHonour 
					has_ship_flag = hero_ship
					is_same_value = event_target:shipCandidate1
				}
				fleet = { is_alliance_fleet = no }
			}
			save_event_target_as = shipCandidate2
		}
		random_owned_ship = {
			limit = { 
				is_ship_class = shipclass_military 
				NOR = { 
					has_ship_flag = renamedInHonour 
					has_ship_flag = hero_ship
					is_same_value = event_target:shipCandidate1
					is_same_value = event_target:shipCandidate2
				}
				fleet = { is_alliance_fleet = no }
			}
			save_event_target_as = shipCandidate3
		}
	}
	option = {
		name = STH_onaction.900.a
		trigger = { exists = event_target:shipCandidate1 }
		allow = { exists = event_target:shipCandidate1 }
		hidden_effect = {
			create_fleet = {
				effect = { 
					set_owner = root
					create_ship = { name = "" random_existing_design = constructor }
					last_created_ship = { set_name = "NAME_LEADERTARGET_NAME" save_event_target_as = shipTarget }
					set_location = root.capital_scope
				}
			}
			event_target:shipCandidate1 = {
				set_ship_flag = renamedInHonour
				set_name = "NAME_SHIPTARGET_NAME"
				set_ship_registry = yes
				add_modifier = { modifier = sh_named_ship days = -1 }
				event_target:shipTarget = { delete_ship = this }
			}
		}
	}
	option = {
		name = STH_onaction.900.b
		trigger = { exists = event_target:shipCandidate2 }
		allow = { exists = event_target:shipCandidate2 }
		hidden_effect = {
			create_fleet = {
				effect = { 
					set_owner = root
					create_ship = { name = "" random_existing_design = constructor }
					last_created_ship = { set_name = "NAME_LEADERTARGET_NAME" save_event_target_as = shipTarget }
					set_location = root.capital_scope
				}
			}
			event_target:shipCandidate2 = {
				set_ship_flag = renamedInHonour
				set_name = "NAME_SHIPTARGET_NAME"
				set_ship_registry = yes
				add_modifier = { modifier = sh_named_ship days = -1 }
				event_target:shipTarget = { delete_ship = this }
			}
		}
	}
	option = {
		name = STH_onaction.900.c
		trigger = { exists = event_target:shipCandidate3 }
		allow = { exists = event_target:shipCandidate3 }
		hidden_effect = {
			create_fleet = {
				effect = { 
					set_owner = root
					create_ship = { name = "" random_existing_design = constructor }
					last_created_ship = { set_name = "NAME_LEADERTARGET_NAME" save_event_target_as = shipTarget }
					set_location = root.owner.capital_scope
				}
			}
			event_target:shipCandidate3 = {
				set_ship_flag = renamedInHonour
				set_name = "NAME_SHIPTARGET_NAME"
				set_ship_registry = yes
				add_modifier = { modifier = sh_named_ship days = -1 }
				event_target:shipTarget = { delete_ship = this }
			}
		}
	}
	option = {
		name = STH_onaction.900.z
	}
	after = {
		hidden_effect = { event_target:leaderTarget = { kill_leader = { show_notification = no } } }
	}
}

# Dead Mirror Universe Leader gets a replacement
# Executed as a leader has died
# This = Country
# From = Leader
country_event = {
	id = STH_onaction.901
	title = STH_onaction.901.name
	desc = { text = STH_onaction.901.desc }
	picture = sth_GFX_evt_leaderDeath
	trigger = {
		exists = FROM
		FROM = { has_trait = leader_trait_defiant_captain }
		any_owned_ship = { has_ship_flag = "USS_Defiant" }
	}
	is_triggered_only = yes
	immediate = {
		if = {
			limit = { NOT = { exists = event_target:USS_Defiant } }
			random_owned_ship = {
				limit = { has_ship_flag = "USS_Defiant" }
				save_global_event_target_as = USS_Defiant
			}
		}
		clone_leader = { target = FROM traits = {} }
		last_created_leader = { save_event_target_as = oldLeader recruitable = no remove_trait = leader_trait_defiant_captain }
		create_leader = {
			name = random
			species = event_target:oldLeader.species
			class = scientist
			skill = random
			traits = { trait = leader_trait_defiant_captain trait = leader_trait_hero_ship_admiral }
		}
		last_created_leader = { save_global_event_target_as = defiant_leader set_leader_flag = sthero }
		event_target:USS_Defiant = { fleet = { assign_leader = event_target:defiant_leader } }
	}
	option = {
		name = STH_onaction.901.a
		hidden_effect = {
			event_target:oldLeader = { kill_leader = { show_notification = no } }
		}		
	}
}



#Set Vassal Status
event = {
	id = STH_onaction.1000
	hide_window = yes  
	is_triggered_only = yes
	trigger = {}
	immediate = {
		every_country = {
			limit = { is_subject = yes exists = overlord }
			overlord = { save_event_target_as = overlordCountry }
			country_event = { id = STH_onaction.1001 scopes = { from = event_target:overlordCountry } }
		}
	}
}

#Set Vassal Status
country_event = {
	id = STH_onaction.1001
	hide_window = yes  
	is_triggered_only = yes
	trigger = { exists = from }
	immediate = { set_appropriate_vassal_status = yes }
}

#Fed Member Colonies Transferred
planet_event = {
	id = STH_onaction.1200
	hide_window = yes  
	is_triggered_only = yes
	immediate = {  
		planet_event = { id = STH_onaction.1201 days = 1 }
	}
}
planet_event = {
	id = STH_onaction.1201
	hide_window = yes  
	is_triggered_only = yes
	pre_triggers = {
		has_owner = yes
	}
	trigger = { 
		owner = { 
			OR = { 
				is_subject_type = member_world 
				is_subject_type = member_world_human 
				is_subject_type = member_world_isu 
				is_subject_type = member_world_human_isu
			}   
		}
	}
	immediate = {  
		set_owner = root.owner.overlord
	}
}

#Cowboy Pops
pop_event = {
	id = STH_onaction.1500
	hide_window = yes  
	is_triggered_only = yes
	trigger = { OR = { is_species_class = FED is_species_class = SKA } exists = planet }
	immediate = {
		planet = {
			random_owned_pop = {
				if = { 
					limit = { has_pop_flag = cowboy_pop } 
					root = { set_pop_flag = cowboy_pop }
				}
			}
		}	
	}
}




# Give Various Traits as Needed
country_event = {
	id = STH_onaction.2000
	is_triggered_only = yes
	hide_window = yes
	trigger = { exists = FROM }
	immediate = {
		from = {
			if = {
				limit = { species = { has_trait = trait_chromodynamic_power_module } }
				add_trait = leader_trait_chromodynamic_power_module
			}
		}
		from = {
			if = {
				limit = { species = { has_trait = trait_synaptic_processors } }
				add_trait = leader_trait_synaptic_processors
			}
		}
		from = {
			if = {
				limit = { species = { has_trait = trait_shapeshifter } }
				add_trait = leader_trait_shapeshifter
			}
		}		
	}
}


ship_event = {
	id = STH_onaction.3000
	is_triggered_only = yes
	hide_window = yes
	trigger = { owner = { starfleet_empire = yes } }
	immediate = {
		ship_event = { id = STH_onaction.3001 days = 1 }
	}
}



# Starbase Names
ship_event = {
	id = STH_onaction.3001
	is_triggered_only = yes
	hide_window = yes
	trigger = { owner = { starfleet_empire = yes } }
	immediate = {
		solar_system = {
			every_fleet_in_system = { 
				limit = { 
					any_owned_ship = { 
						is_ship_class = shipclass_starbase 
					}
				}
				save_event_target_as = starbaseFleet
			}
		}
		owner = {
			change_variable = { which = "starbaseCount" value = 1 }
			every_owned_ship = {
				limit = { exists = fleet fleet = { is_same_value = event_target:starbaseFleet } }
				set_name = "Starbase [prev.starbaseCount]"
			}
		}
	}
}

### Planet defense annual spawn event
country_event = {
    id = STH_onaction.4000
    is_triggered_only = yes
    hide_window = yes
    trigger = { 
        has_technology = tech_engineering_02925
    }
    immediate = {
        every_owned_planet = {
            limit = { 
				OR = {
					has_building = building_mars_perimeter
					has_building = building_fighter_hangar_1 
				}
			}
            if = {
                limit = {
					count_armies = { limit = { army_type = orbital_fighter_army } count < 6 }
					has_ground_combat = no
					has_orbital_bombardment = no
					NOT = { any_owned_pop = { has_modifier = pop_recently_conquered } }
				}
                create_army = {
                    name = "NAME_Orbital_Fighter"
                    owner = root
                    type = "orbital_fighter_army"
                }
            }
        }
    }
}

# Destroy Agencies etc. on country death
country_event = {
	id = STH_onaction.5000
	is_triggered_only = yes
	hide_window = yes
	trigger = { 
		any_subject = {
			OR = {
				is_country_type = great_house
				is_country_type = agency
				is_country_type = naval_museum
			}
		}
	}
	immediate = {
		every_subject = {
			limit = { 
				OR = {
					is_country_type = agency
					is_country_type = naval_museum
				} 
			}
			destroy_country = yes
		}
		every_subject = {
			limit = { 
				OR = {
					is_country_type = great_house
				} 
			}
			set_country_type = pirate
		}
	}
}

# Game start event, activate Subclass modifiers to all planets in the galaxy
event = {
	id = STH_onaction.6000
	hide_window = yes
	is_triggered_only = yes
	
	immediate = {
		every_planet = {
			limit = { is_planet_subclass = yes }
			set_subclass = yes
		}
	}
}

### Terraform finish on action, removes any existing subclass modifier. TODO - install new modifiers when applicable, such as a tropical species terraforms a planet to wet
planet_event = {
	id = STH_onaction.6001
	hide_window = yes
	
	is_triggered_only = yes
	
	immediate = {
		THIS = { remove_subclass_modifiers = yes }
	}
}

### Every time a building is completed, destroyed, etc, allow unrestricted job shuffle for 3 days
planet_event = {
	id = STH_onaction.6100
	hide_window = yes
	is_triggered_only = yes
	immediate = { set_timed_planet_flag = { flag = allow_job_shuffle days = 3 } }
}

### Every year, allow unrestricted job shuffle for 3 days
event = {
	id = STH_onaction.6101
	hide_window = yes
	is_triggered_only = yes
	immediate = { every_playable_country = { country_event = { id = STH_onaction.6102 } } }
}

country_event = {
	id = STH_onaction.6102
	hide_window = yes
	is_triggered_only = yes
	immediate = { every_owned_planet = { set_timed_planet_flag = { flag = allow_job_shuffle days = 3 } } }
}

country_event = {
	id = STH_onaction.650
	hide_window = yes
	is_triggered_only = yes
	trigger = {
		FROM = { is_megastructure_type = STH_great_link_0 }
	}
	immediate = { 
		every_planet_within_border = {
			limit = { is_planet_class = pc_thegreatlink }
			remove_all_capital_buildings = yes
			add_building = building_link_capital_2
			add_district = district_great_link
			create_pop = { species = event_target:founderSpecies }
			create_pop = { species = event_target:founderSpecies }
			create_pop = { species = event_target:founderSpecies }
			create_pop = { species = event_target:founderSpecies }
			create_pop = { species = event_target:founderSpecies }
			create_pop = { species = event_target:founderSpecies }
									
		}
	}
}
# Food Deficit Workaround
country_event = { 
	id = STH_onaction.6500
	hide_window = yes
	is_triggered_only = yes

	trigger = {
		NOR = { 
			has_country_flag = the_borg_collective 
			has_country_flag = coppelius_council
			has_country_flag = cravic_imperative
			has_country_flag = pralor_auxiliary
		}
		is_borg_empire = no
		is_infester = no
		OR = {
			#owner = { has_modifier = food_deficit }
			owner = { resource_stockpile_compare = { resource = food value <= 0 } }
		}
	}
	immediate = {
		every_owned_pop = {
			limit = { is_organic_species = yes  }
			if = {
				limit = { NOT = { has_modifier = po_food_deficit  } }
				owner = { log = "[This.GetName] Food Deficit add" }
				add_modifier = { modifier = po_food_deficit }
			}
		}
	}
}
# Food Deficit Workaround
country_event = { 
	id = STH_onaction.6501
	hide_window = yes
	is_triggered_only = yes

	trigger = {
		is_normal_country = yes
		owner = { resource_stockpile_compare = { resource = food value > 0 } }
		NOR = { 
			has_country_flag = the_borg_collective 
			has_country_flag = coppelius_council
			has_country_flag = cravic_imperative
			has_country_flag = pralor_auxiliary
		}
		is_borg_empire = no
		is_infester = no
	}
	immediate = {
		every_owned_pop = {
			limit = { 
				is_organic_species = yes 
				has_modifier = po_food_deficit  
			}
			owner = { log = "[This.GetName] Food Deficit remove" }
			remove_modifier = po_food_deficit
		}
	}
}

# Envoy Modifier
country_event = { 
	id = STH_onaction.6510
	hide_window = yes
	is_triggered_only = yes

	trigger = {
		is_normal_country = yes
		NOR = {
			is_assimilator = no
			has_country_flag = undine
			has_country_flag = envoy_modifier_in_place
		}
	}
	immediate = {
		if = {
			limit = { is_envoys_improve = yes }
			set_timed_country_flag = { flag = envoy_modifier_in_place days = 360 }
			add_modifier = { modifier = em_envoy_improvement_bonus days = 364 }
		}
		if = {
			limit = { is_envoys_harm = yes }
			set_timed_country_flag = { flag = envoy_modifier_in_place days = 360 }
			add_modifier = { modifier = em_envoy_harm_bonus days = 364 }
		}
	}
}

# Borg pop clothing workaround
country_event = { 
	id = STH_onaction.6520
	hide_window = yes
	is_triggered_only = yes
	fire_only_once = yes

	trigger = {
		is_assimilator = yes
		is_ai = no
	}
	immediate = {
		every_owned_pop = {
			limit = {
				is_species_class = BRG
			}
			modify_species = {
				add_trait = "trait_pc_assimilated_preference"
				change_scoped_species = yes
			}
		}
	}
}
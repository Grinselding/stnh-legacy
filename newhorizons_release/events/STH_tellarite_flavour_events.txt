############################
#
# Tellarite Flavour Events
#
# Written by Walshicus / Kodiak
#
############################

namespace = STH_tellarite_flavour

country_event = {
	id = STH_tellarite_flavour.0
	hide_window = yes
	location = ROOT
	is_triggered_only = yes
	immediate = {
		if = {
			limit = { 
				is_species_class = TEL
				any_owned_fleet = { exists = leader } 
			}
			random_owned_fleet = {
				limit = {
					exists = leader
				}
				fleet_event = { id = STH_tellarite_flavour.1 days = 7200 random = 3600 }	
			}
		}
		else = {
			country_event = { id = STH_tellarite_flavour.0 days = 7000 random = 4500 }
		}
	}
}

fleet_event = {
	id = STH_tellarite_flavour.1
	title = STH_tellarite_flavour.1.name
	desc = STH_tellarite_flavour.1.desc
	picture = sth_GFX_evt_tellariteCruiser
	location = ROOT
	trigger = {
		owner = { is_species_class  = TEL }
		exists = leader
		NOT = { leader = { has_leader_flag = hadDebate } }
		is_in_combat = no
	}
	is_triggered_only = yes
	immediate = {
		leader = { save_event_target_as = officer }
	}
	option = {
		name = STH_tellarite_flavour.1.a
		hidden_effect = {
			random_list = {
				50 = { fleet_event = { id = STH_tellarite_flavour.2 days = 7 } } #Win and gain positive and negative trait
				50 = { fleet_event = { id = STH_tellarite_flavour.3 days = 7 } } #Lose but gain positive trait
			}
			owner = { country_event = { id = STH_tellarite_flavour.0 days = 10000 random = 4500 } }
		}
	}
}

fleet_event = {
	id = STH_tellarite_flavour.2
	title = STH_tellarite_flavour.2.name
	desc = STH_tellarite_flavour.2.desc
	picture = sth_GFX_evt_tellariteCaptain
	location = ROOT
	is_triggered_only = yes
	immediate = {
		leader = { 
			save_event_target_as = officer 
			set_leader_flag = hadDebate
		}
	}
	option = {
		name = STH_tellarite_flavour.2.a
		leader = { add_trait = leader_trait_stubborn }
		owner = { 
			add_resource = { influence = 25 }
			add_monthly_resource_mult = { resource = society_research value = @tier1researchreward min = @tier1researchmin max = @tier1researchmax }
		}
	}
}

fleet_event = {
	id = STH_tellarite_flavour.3
	title = STH_tellarite_flavour.3.name
	desc = STH_tellarite_flavour.3.desc
	picture = sth_GFX_evt_tellariteCaptain
	location = ROOT
	is_triggered_only = yes
	immediate = {
		leader = { 
			save_event_target_as = officer 
			set_leader_flag = hadDebate
		}
	}
	option = {
		name = STH_tellarite_flavour.3.a
		leader = { add_trait = leader_trait_adaptable }
		owner = { 
			add_resource = { influence = 25 }
			add_monthly_resource_mult = { resource = society_research value = @tier1researchreward min = @tier1researchmin max = @tier1researchmax }
		}
	}
}


#Remembering the Voice Wars
country_event = {
	id = STH_tellarite_flavour.100
	hide_window = yes
	fire_only_once = yes
	mean_time_to_happen = { months = 12 }
	trigger = {
		years_passed >= 50
		is_species_class = TEL
		any_subject = {
			OR = { is_subject_type = member_world is_subject_type = member_world_human }
			is_species_class = TEL
		}
	}
	immediate = {
		every_country = {
			limit = { 
				is_species_class = TEL
				any_subject = {
					OR = { is_subject_type = member_world is_subject_type = member_world_human }
					is_species_class = TEL
				}
			}
			country_event = { id = STH_tellarite_flavour.101 }
		}
	}
}
country_event = {
	id = STH_tellarite_flavour.101
	title = STH_tellarite_flavour.101.name
	desc = STH_tellarite_flavour.101.desc
	picture = sth_GFX_evt_federationTellar
	is_triggered_only = yes
	trigger = { }
	option = {
		name = STH_tellarite_flavour.101.a
		enable_special_project = { name = "TELLARITE_1_PROJECT" location = root.capital_scope owner = root }
	}
	option = {
		name = STH_tellarite_flavour.101.b
		add_resource = { influence = -25 }
	}
}
country_event = {
	id = STH_tellarite_flavour.102
	title = STH_tellarite_flavour.102.name
	desc = STH_tellarite_flavour.102.desc
	picture = sth_GFX_evt_federationTellar
	is_triggered_only = yes
	option = {
		name = STH_tellarite_flavour.102.a
		every_owned_pop = {
			limit = { species = { is_species_class = TEL } }
			add_modifier = { modifier = po_voice_wars_rememberance days = 1080 }
		}
	}
}

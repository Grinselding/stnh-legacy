############################
#
# Bajoran Flavour Events
#
# Written by Walshicus & Russ
#
############################

namespace = STH_bajoran_flavour

# Bajoran Literature
# Gaudaal's Lament
country_event = {
	id = STH_bajoran_flavour.1
	title = STH_bajoran_flavour.1.name
	desc = STH_bajoran_flavour.1.desc
	picture = sth_GFX_evt_vedekAssembly
	fire_only_once = yes
	trigger = { is_species_class  = BAJ years_passed < 100 }
	mean_time_to_happen = { years = 10 }
	option = {
		name = STH_bajoran_flavour.1.a
		add_resource = { influence = 50 }
		add_monthly_resource_mult = { resource = society_research value = @tier3researchreward min = @tier3researchmin max = @tier3researchmax }
		add_modifier = { modifier = em_gaudaals_lament years = 10 }
	}
}

# STH_bajoran_flavour.125 - Non War trigger
country_event = {
	id = STH_bajoran_flavour.125
	hide_window = yes
	mean_time_to_happen = { years = 5 }
	trigger = { 
		any_owned_planet = {
			has_planet_flag = planet_bajor
			any_pop = { is_species_class  = BAJ }
			solar_system = { exists = starbase }
			owner = { NOT = { has_country_flag = bajoran_republic } }
			has_any_unrest_modifier = yes
			planet_stability < 75
		}	
	}
	immediate = {
		random_planet = {
			limit = { has_planet_flag = planet_bajor }
			planet_event = { id = STH_bajoran_flavour.101 days = 600 random = 60 } set_planet_flag = bajoran_resistance
		}
	}
}

planet_event = {
	id = STH_bajoran_flavour.100
	hide_window = yes
	is_triggered_only = yes
	trigger = { 
		this = { 
			has_planet_flag = planet_bajor
			any_pop = { is_species_class  = BAJ }
			solar_system = { exists = starbase }
		}
		from = { NOT = { has_country_flag = bajoran_republic } }	
	}
	immediate = {
		this = { planet_event = { id = STH_bajoran_flavour.101 days = 200 random = 60 } set_planet_flag = bajoran_resistance }
	}
}

planet_event = {
	id = STH_bajoran_flavour.101
	title = STH_bajoran_flavour.101.name
	desc = STH_bajoran_flavour.101.desc
	picture = sth_GFX_evt_vedekAssembly
	is_triggered_only = yes
	location = root
	pre_triggers = {
		has_owner = yes
	}
	trigger = { 
		has_planet_flag = planet_bajor
		any_pop = { is_species_class  = BAJ }
		solar_system = { exists = starbase }		
		owner = { NOT = { has_country_flag = bajoran_republic } }
	}
	immediate = {
		random_country = {
			limit = { is_overlord_to = event_target:bajoran_republic }
			set_country_flag = bajoran_overlords
			save_event_target_as = bajoran_overlords
		}
		add_modifier = {
			modifier = "pm_jammed_subspace_comms"
			days = -1
		}
	}
	option = {
		name = STH_bajoran_flavour.101.a # dispatch away team
		hidden_effect = {
			add_modifier = {
				modifier = "pm_bajoran_resistance_cells"
				days = -1
			}
			root = { planet_event = { id = STH_bajoran_flavour.102 days = 120 random = 30 } }
		}	
	}
	option = {
		name = STH_bajoran_flavour.101.b # do nothing
		hidden_effect = {
			add_modifier = {
				modifier = "pm_bajoran_resistance_cells_large"
				days = -1
			}
			root = { planet_event = { id = STH_bajoran_flavour.103 days = 250 random = 60 } }
		}	
	}	
}

planet_event = {
	id = STH_bajoran_flavour.102
	title = STH_bajoran_flavour.102.name
	desc = STH_bajoran_flavour.102.desc
	picture = sth_GFX_evt_vedekAssembly
	location = root
	is_triggered_only = yes
	pre_triggers = {
		has_owner = yes
	}
	trigger = { 
		has_planet_flag = planet_bajor
		any_pop = { is_species_class  = BAJ }
		solar_system = { exists = starbase }		
		owner = { NOT = { has_country_flag = bajoran_republic } }
	}
	option = {
		name = STH_bajoran_flavour.102.a # go to starbase
		hidden_effect = {
			root = { planet_event = { id = STH_bajoran_flavour.103 days = 60 random = 30 } }
		}
	}
}

planet_event = {
	id = STH_bajoran_flavour.103
	title = STH_bajoran_flavour.103.name
	desc = STH_bajoran_flavour.103.desc
	picture = GFX_evt_space_debris
	location = root
	is_triggered_only = yes
	pre_triggers = {
		has_owner = yes
	}
	trigger = { 
		has_planet_flag = planet_bajor
		any_pop = { is_species_class  = BAJ }
		solar_system = { exists = starbase }		
		owner = { NOT = { has_country_flag = bajoran_republic } }
	}
	immediate = {
		random_pop = { limit = { is_species_class = BAJ } kill_pop = yes }
		solar_system = {
			if = { 
				limit = { exists = starbase }
				starbase = { fleet = { destroy_fleet = this } }
			}	
		}
		random_country = {
			limit = { is_overlord_to = event_target:bajoran_republic }
			set_country_flag = bajoran_overlords
			save_event_target_as = bajoran_overlords
		}
	}
	option = {
		name = STH_bajoran_flavour.103.a # let the resistance be
		ai_chance = { factor = 30 }
		solar_system = {
			create_starbase = { 
				size = starbase_starfortress 
				owner = root.owner
				module = gun_battery 
				module = trading_hub 
				module = module_armor
				building = ore_refinery
				building = offworld_trading_company
			}
			solar_system = {
				every_fleet_in_system = { 
					limit = { 
						any_owned_ship = { 
							is_ship_class = shipclass_starbase 
						}
					}
					save_event_target_as = starbase_ds9
				}
			}		
		}
		owner = {
			every_owned_ship = {
				limit = { exists = fleet fleet = { is_same_value = event_target:starbase_ds9 } }
				set_name = "Terok Nor"
			}
		}
		hidden_effect = { 
			remove_modifier = pm_bajoran_resistance_cells 
			remove_modifier = pm_bajoran_resistance_cells_large
			remove_modifier = pm_jammed_subspace_comms
			add_modifier = {
				modifier = "pm_bajoran_resistance_cells_large"
				days = 1500
			}
			owner = { country_event = { id = STH_bajoran_flavour.104 days = 1500 random = 100 } }
			random_country = {
				limit = { has_country_flag = cardassian_union }
				country_event = { id = STH_bajoran_flavour.120 days = 360 random = 90 } 
				country_event = { id = STH_bajoran_flavour.120 days = 700 random = 90 } 
			}		
		}
	}
	option = {
		name = STH_bajoran_flavour.103.b # crush the resistance
		solar_system = {
			create_starbase = { 
				size = starbase_starfortress 
				owner = root.owner
				module = gun_battery 
				module = trading_hub 
				module = module_armor
				building = ore_refinery
				building = offworld_trading_company
			}
			solar_system = {
				every_fleet_in_system = { 
					limit = { 
						any_owned_ship = { 
							is_ship_class = shipclass_starbase 
						}
					}
					save_event_target_as = starbase_ds9
				}
			}		
		}
		owner = {
			every_owned_ship = {
				limit = { exists = fleet fleet = { is_same_value = event_target:starbase_ds9 } }
				set_name = "Terok Nor"
			}
		}
		random_pop = { limit = { is_species_class = BAJ } kill_pop = yes }
		hidden_effect = { 
			remove_modifier = pm_bajoran_resistance_cells 
			remove_modifier = pm_bajoran_resistance_cells_large
			remove_modifier = pm_jammed_subspace_comms
			add_modifier = {
				modifier = "pm_bajoran_resistance_cells"
				days = 1500
			}
			random_country = {
				limit = { has_country_flag = cardassian_union has_country_flag = bajoran_overlords }
				country_event = { id = STH_bajoran_flavour.120 days = 360 random = 90 } 
				country_event = { id = STH_bajoran_flavour.120 days = 700 random = 90 }		
				random_list = {
					80 = { country_event = { id = STH_bajoran_flavour.104 days = 1000 random = 100 } }
					20 = { country_event = { id = STH_bajoran_flavour.130 days = 800 random = 100 } }
				} 
			}	
		}
	}
	option = {
		allow = { owner = { OR = { has_ethic = ethic_fanatic_xenophobe has_ethic = ethic_fanatic_militarist has_ethic = ethic_fanatic_authoritarian has_ethic = ethic_authoritarian } } }
		name = STH_bajoran_flavour.103.c ### purge all bajorans 
		solar_system = {
			create_starbase = { 
				size = starbase_starfortress 
				owner = root.owner
				module = gun_battery 
				module = trading_hub 
				module = module_armor
				building = ore_refinery
				building = offworld_trading_company 
			}
			solar_system = {
				every_fleet_in_system = { 
					limit = { 
						any_owned_ship = { 
							is_ship_class = shipclass_starbase 
						}
					}
					save_event_target_as = starbase_ds9
				}
			}		
		}
		owner = {
			every_owned_ship = {
				limit = { exists = fleet fleet = { is_same_value = event_target:starbase_ds9 } }
				set_name = "Terok Nor"
			}
		}
		#every_owned_species = { limit = { is_species_class = BAJ } kill_pop = yes }
		every_owned_pop = { limit = { is_species_class = BAJ } kill_pop = yes }
		hidden_effect = { remove_modifier = pm_bajoran_resistance_cells remove_modifier = pm_bajoran_resistance_cells_large remove_modifier = pm_jammed_subspace_comms }
		owner = { add_resource = { influence = 100 } }
	}
	option = { # Open peace talks
		name = STH_bajoran_flavour.103.d
		allow = { has_resource = { type = influence amount >= 350 } has_resource = { type = energy amount >= 4500 } }
		owner = { add_resource = { influence = -350 energy = -4500 } }
		enable_special_project = { name = CARDASSIAN_1_PROJECT location = THIS }
	}
}

# Bajor Freedom -starts
country_event = {
	id = STH_bajoran_flavour.104
	title = STH_bajoran_flavour.104.name
	desc = STH_bajoran_flavour.104.desc
	picture = sth_GFX_evt_terok_nor
	is_triggered_only = yes

	trigger = {
		any_planet = {
			has_planet_flag = planet_bajor
			any_pop = { is_species_class  = BAJ }
			solar_system = { exists = starbase }
			owner = { NOT = { has_country_flag = bajoran_republic } }
		} 	
	}

	immediate = {
		random_country = {
			limit = { has_country_flag = cardassian_union }
			save_event_target_as = bajoran_overlords
		}

		random_country = {
			limit = { has_country_flag = bajoran_republic }
			add_casus_belli = {
				type = cb_occupation_subject
				who = event_target:cardassian_union
				days = 2000
			}
		}
		random_country = {
			limit = { has_country_flag = cardassian_union }
			country_event = { id = STH_bajoran_flavour.120 days = 200 random = 90 } 
			country_event = { id = STH_bajoran_flavour.120 days = 600 random = 90 } 
		}
	}
	option = {
		name = STH_bajoran_flavour.104.a
		country_event = { id = STH_bajoran_flavour.105 days = 90 random = 10 }
		random_country = {
			limit = { has_country_flag = bajoran_overlords }
			remove_modifier = em_occupying_force
			capital_scope = {
				remove_modifier = pm_havesting_occupied_world
				add_modifier = {
					modifier = terrorist_attack
				}
			}
		}
	}
}

# Bajor Freedom -War
country_event = {
	id = STH_bajoran_flavour.105
	title = STH_bajoran_flavour.105.name
	desc = STH_bajoran_flavour.105.desc
	picture = sth_GFX_evt_bajor_occupation
	is_triggered_only = yes

	trigger = {
		any_planet = {
			has_planet_flag = planet_bajor
			any_pop = { is_species_class  = BAJ }
			solar_system = { exists = starbase }
			owner = { NOT = { has_country_flag = bajoran_republic } }
		} 	
	}

	immediate = {
		random_country = {
			limit = { has_country_flag = cardassian_union }
			save_event_target_as = bajoran_overlords
		}

		random_country = {
			limit = { has_country_flag = bajoran_republic }
			add_casus_belli = {
				type = cb_occupation_subject
				who = event_target:bajoran_overlords
				days = 2000
			}
		}
	}
	option = {
		name = STH_bajoran_flavour.105.a
		random_country = {
			limit = { has_country_flag = cardassian_union }
			country_event = { id = STH_bajoran_flavour.109 days = 3 } 
		}
		random_country = {
			limit = { has_country_flag = bajoran_republic }
			remove_country_flag = occupied_country
		}
		every_country = { # NOTIFICATION
			limit = { has_communications = event_target:bajoran_republic NOT = { is_same_value = event_target:bajoran_republic } }
			country_event = { id = STH_bajoran_flavour.106 days = 1 }
		}
	}
}

# notified
country_event = {
	id = STH_bajoran_flavour.106
	title = "STH_bajoran_flavour.106.name"
	desc = "STH_bajoran_flavour.106.desc"
	picture = sth_GFX_evt_terok_nor
	show_sound = event_mystic_reveal

	is_triggered_only = yes

	option = { 
		name = STH_bajoran_flavour.106.a
	}
}

# A war has been won
# Root = Winner Warleader
# From = Loser Warleader
# FromFrom = War

# Bajor Wins
country_event = {
	id = STH_bajoran_flavour.107
	title = STH_bajoran_flavour.107.name
	desc = STH_bajoran_flavour.107.desc
	picture = sth_GFX_evt_bajor_occupation
	is_triggered_only = yes
	trigger = { 
		root = { has_country_flag = bajoran_republic }
		from = { has_country_flag = cardassian_union }
	}
	immediate = {
		random_country = {
			limit = { has_country_flag = occupied_country }
			remove_country_flag = occupied_country
		}
		random_planet = {
			limit = { has_planet_flag = planet_bajor }
			remove_modifier = occupation
			add_modifier = {
				modifier = pop_liberation_fever
				days = 1000
			}
			add_planet_devastation = 35
		}
		random_country = {
			limit = { has_country_flag = cardassian_union }
			remove_country_flag = occupation_target_country
			remove_country_flag = bajoran_overlords

		}
	}
	option = { 
		name = STH_bajoran_flavour.107.a
		every_country = { # NOTIFICATION
			limit = { has_communications = event_target:cardassian_union NOR = { is_same_value = event_target:bajoran_republic has_country_flag = cardassian_union } }
			country_event = { id = STH_bajoran_flavour.108 days = 1 }
		}
		random_country = { # NOTIFY CARDASSIA
			limit = { has_country_flag = cardassian_union }
			country_event = { id = STH_bajoran_flavour.115 days = 1 }
		}
	}
}

# notified
country_event = {
	id = STH_bajoran_flavour.108
	title = "STH_bajoran_flavour.108.name"
	desc = "STH_bajoran_flavour.108.desc"
	picture = sth_GFX_evt_terok_nor
	show_sound = event_mystic_reveal

	is_triggered_only = yes

	option = { 
		name = sth_understood
	}
}

# Cardassia Notified withdraw demand
country_event = {
	id = STH_bajoran_flavour.109
	title = STH_bajoran_flavour.109.name
	desc = { 
		text = STH_bajoran_flavour.109.desc
	}
	diplomatic = yes
	picture_event_data = {
		portrait = event_target:bajoran_leader
		room = bajoran_room	
	}
	is_triggered_only = yes
	force_open = yes

	immediate = {
		random_planet = {
			limit = { has_planet_flag = planet_bajor }
			create_country = {
				name = "Bajoran Resistance"
				type = default
				ignore_initial_colony_error = yes
				civics = { civic = "civic_prophet_1" civic = "civic_prophet_2" }
				authority = auth_oligarchic
				name_list = "Bajoran"
				ethos = { ethic = "ethic_pacifist" ethic = "ethic_fanatic_spiritualist" }
				origin = "origin_spiritualist"
				species = event_target:bajoranSpecies
				flag = {
					icon = { category = "trek" file = "Bajor.dds" }
					background = { category = "backgrounds" file = "circle.dds" }
					colors = { "customcolor1454" "customcolor1806" "null" "null" }
				}
				ship_prefix = BV
				effect = {
					set_graphical_culture = bajoran_01
					set_country_flag = bajoran_republic
					set_country_flag = custom_start_screen
					set_country_flag = generic_ent
					set_country_flag = alpha_beta_empire
					set_country_flag = init_spawned
					set_country_flag = sth_medium_galaxy
					set_country_flag = botf_minor
					save_global_event_target_as = bajoran_republic
				}
			}
			set_owner = event_target:bajoran_republic
			remove_modifier = pm_bajoran_resistance_cells
			remove_modifier = pm_bajoran_resistance_cells_large
			remove_modifier = occupation
			add_modifier = {
				modifier = pop_liberation_fever
				days = 1000
			}
		}
		random_country = {
			limit = { has_country_flag = bajoran_republic }
			save_global_event_target_as = bajoran_republic
			add_appropriate_start_techs = yes
			give_technology = { tech = "tech_society_words_379" message = no }
			species = { save_global_event_target_as = bajoranSpecies }
			set_country_type = minorRace
			establish_communications_no_message = root.owner
			add_opinion_modifier = {
				who = event_target:cardassian_union
				modifier = opinion_world_occupied
			}
		}
		random_country = {
			limit = { has_country_flag = bajoran_republic }
			remove_country_flag = occupied_country
			hidden_effect = {
				country_event = { id = STH_bajoran_mechanics.1 days = 360 }
				add_casus_belli = {
					type = cb_occupation_subject
					who = event_target:cardassian_union
					days = 2000
				}
			}
			create_leader = {
				class = governor
				name = "Kalem Apren"
				species = event_target:bajoranSpecies
				gender = male 
				set_age = 60 
				skill = 2
				traits = { trait = leader_trait_army_veteran trait = leader_trait_agrarian_upbringing }
				effect = { set_leader_flag = kalemapren }
			}
		last_created_leader = { save_event_target_as = bajoran_leader }
			create_leader = {
				class = admiral
				name = "Li Nalas"
				species = event_target:bajoranSpecies
				gender = male 
				set_age = 25
				skill = 2
				traits = { trait = leader_trait_army_veteran trait = leader_trait_agrarian_upbringing }
				effect = { set_leader_flag = linalas }
			}
			create_leader = {
				class = scientist
				name = "Mora Pol"
				species = event_target:bajoranSpecies
				gender = male 
				set_age = 45
				skill = 2
				traits = { trait = leader_trait_doctor trait = leader_trait_expertise_biology }
				effect = { set_leader_flag = morapol }
			}
		}
		random_system = {
			limit = { has_star_flag = bajoran_homeworld }
			starbase = {
				set_owner = event_target:bajoran_republic
			}
		}
		random_country = {
			limit = { has_country_flag = cardassian_union }
			save_event_target_as = cardassian_contact
			remove_country_flag = world_occupier
			add_opinion_modifier = {
				who = event_target:bajoran_republic
				modifier = opinion_cardassians_disdain
			}
			remove_modifier = em_occupying_force
			capital_scope = {
				remove_modifier = pm_havesting_occupied_world
			}
		}
		every_country = {
			limit = { has_communications = event_target:cardassian_union }
			establish_communications_no_message = event_target:bajoran_republic
			establish_contact = {
				who = event_target:bajoran_republic
				location = event_target:bajoran_system
			}
		}
	}

	option = { # What??
		name = STH_bajoran_flavour.109.a
		is_dialog_only = yes
		response_text = STH_bajoran_flavour.109.a.response
	}
	option = { # Release Bajor
		name = STH_bajoran_flavour.109.b
		ai_chance = { factor = 30 }
		hidden_effect = {
			random_country = {
				limit = { has_country_flag = bajoran_republic }
			}
			country_event = { id = STH_bajoran_flavour.111 days = 2 }
			country_event = { id = STH_bajoran_flavour.116 days = 1 }
		}
	}
	option = { # War with Bajor
		name = STH_bajoran_flavour.109.c
		ai_chance = { factor = 10 }
		hidden_effect = {
			random_country = {
				limit = { has_country_flag = bajoran_republic }
				country_event = { id = STH_bajoran_flavour.110 days = 2 }
			}	
		}
		country_event = { id = STH_bajoran_flavour.116 days = 1 }
		add_casus_belli = {
			type = cb_occupation
			who = event_target:bajoran_republic
			days = 360
		}
	}
}

# War it is then
country_event = {
	id = STH_bajoran_flavour.110
	title = "STH_bajoran_flavour.110.name"
	desc = "STH_bajoran_flavour.110.desc"
	picture = sth_GFX_evt_cardassian_ship
	show_sound = event_mystic_reveal

	is_triggered_only = yes
	immediate = {
		create_fleet = {
			name = "Higa Metar Resistance Cell" 
			effect = {
				set_owner = event_target:bajoran_republic	
				set_location = root.capital_scope
			}
		}
		last_created_fleet = {
			create_ship = { name = random design = "NAME_Reaver" graphical_culture = "pirate_01" }
			create_ship = { name = random design = "NAME_Reaver" graphical_culture = "pirate_01" }
			create_ship = { name = random design = "NAME_Reaver" graphical_culture = "pirate_01" }
			create_ship = { name = random design = "NAME_Reaver" graphical_culture = "pirate_01" }
			create_ship = { name = random design = "NAME_Reaver" graphical_culture = "pirate_01" }
			create_ship = { name = random random_existing_design = corvette graphical_culture = "bajoran_01" }
			create_ship = { name = random random_existing_design = corvette graphical_culture = "bajoran_01" }
			create_ship = { name = random random_existing_design = cruiser graphical_culture = "bajoran_01" }
			create_ship = { name = random random_existing_design = cruiser graphical_culture = "bajoran_01" }
		}
		create_fleet = {
			name = "Shakaar Resistance Cell" 
			effect = {
				set_owner = event_target:bajoran_republic	
				set_location = root.capital_scope
			}
		}
		last_created_fleet = {
			create_ship = { name = random design = "NAME_Yridian_Destroyer" }
			create_ship = { name = random design = "NAME_Miradorn_Raider" }
			create_ship = { name = random design = "NAME_Miradorn_Raider" }
			create_ship = { name = random design = "NAME_Miradorn_Raider" }
			create_ship = { name = random design = "NAME_Miradorn_Raider" }
			create_ship = { name = random random_existing_design = corvette graphical_culture = "bajoran_01" }
			create_ship = { name = random random_existing_design = corvette graphical_culture = "bajoran_01" }
			create_ship = { name = random random_existing_design = cruiser graphical_culture = "bajoran_01" }
			create_ship = { name = random random_existing_design = cruiser graphical_culture = "bajoran_01" }
		}
		create_fleet = {
			name = "Ornathia Resistance Cell" 
			effect = {
				set_owner = event_target:bajoran_republic	
				set_location = root.capital_scope
			}
		}
		last_created_fleet = {
			create_ship = { name = random design = "NAME_Yridian_Destroyer" }
			create_ship = { name = random design = "NAME_Amarie_Cruiser" }
			create_ship = { name = random design = "NAME_Miradorn_Raider" }
			create_ship = { name = random random_existing_design = corvette graphical_culture = "bajoran_01" }
			create_ship = { name = random random_existing_design = corvette graphical_culture = "bajoran_01" }
			create_ship = { name = random random_existing_design = cruiser graphical_culture = "bajoran_01" }
			create_ship = { name = random random_existing_design = cruiser graphical_culture = "bajoran_01" }
		}
	}
	option = { 
		name = STH_bajoran_flavour.110.a
		declare_war = {
			target = event_target:cardassian_union
			attacker_war_goal = wg_end_occupation
		}
	}
}

# Bajor Released
country_event = {
	id = STH_bajoran_flavour.111
	title = "STH_bajoran_flavour.111.name"
	desc = "STH_bajoran_flavour.111.desc"
	picture = sth_GFX_evt_bajor_occupation
	show_sound = event_mystic_reveal

	is_triggered_only = yes
	immediate = {
		random_subject = {
			limit = { has_country_flag = bajoran_republic }
			set_subject_of = { who = none }
		}
		random_country = {
			limit = { has_country_flag = bajoran_republic }
			set_name = "Bajoran Provisional Government"
			country_event = { id = STH_bajoran_flavour.114 days = 1200 }
		}
	}
	option = { 
		name = STH_bajoran_flavour.111.a
		every_country = { # NOTIFICATION
			limit = { has_communications = event_target:cardassian_union NOR = { is_same_value = event_target:bajoran_republic has_country_flag = cardassian_union } }
			country_event = { id = STH_bajoran_flavour.112 days = 1 }
		}
		random_country = {
			limit = { has_country_flag = cardassian_union }
			remove_country_flag = occupation_target_country
			remove_country_flag = world_occupier
			country_event = { id = STH_bajoran_flavour.113 days = 3 }
		}
		random_planet = {
			limit = { has_planet_flag = planet_bajor }
			remove_planet_flag = occupation_target
			remove_modifier = occupation
			add_modifier = {
				modifier = pop_liberation_fever
				days = 1000
			}
			add_planet_devastation = 35
		}
	}
}

# ROW notified
country_event = {
	id = STH_bajoran_flavour.112
	title = "STH_bajoran_flavour.112.name"
	desc = "STH_bajoran_flavour.112.desc"
	picture = sth_GFX_evt_terok_nor
	show_sound = event_mystic_reveal

	is_triggered_only = yes

	option = { name = sth_understood }
}
# Cardassia notified
country_event = {
	id = STH_bajoran_flavour.113
	title = "STH_bajoran_flavour.113.name"
	desc = "STH_bajoran_flavour.113.desc"
	picture = sth_GFX_evt_terok_nor
	show_sound = event_mystic_reveal

	is_triggered_only = yes
	immediate = { 
		remove_modifier = em_occupying_force 
		add_opinion_modifier = {
			who = event_target:bajoran_republic
			modifier = opinion_broke_away_from
		}
	}

	option = { 
		name = sth_understood
		add_resource = { influence = -50 }
		set_country_flag = cardassian_bajoran_occupation_finished
	}
}
# Bajoran Republic
country_event = {
	id = STH_bajoran_flavour.114
	hide_window = yes
	show_sound = event_mystic_reveal

	is_triggered_only = yes
	immediate = {
		random_country = {
			limit = { has_country_flag = bajoran_republic }
			set_name = "Bajoran Republic"
		}
		random_planet = {
			limit = { has_planet_flag = planet_bajor }
			remove_modifier = occupation
			add_modifier = {
				modifier = pop_liberation_fever
				days = 1000
			}
		}
	}
}
# Cardassia defeat
country_event = {
	id = STH_bajoran_flavour.115
	title = "STH_bajoran_flavour.115.name"
	desc = "STH_bajoran_flavour.115.desc"
	picture = sth_GFX_evt_terok_nor
	show_sound = event_mystic_reveal

	is_triggered_only = yes
	immediate = { 
		remove_country_flag = world_occupier
		remove_modifier = em_occupying_force 
		add_opinion_modifier = {
			who = event_target:bajoran_republic
			modifier = opinion_broke_away_from
		}
	}

	option = { 
		name = sth_understood
		add_resource = { influence = -50 }
		set_country_flag = cardassian_bajoran_occupation_finished
		random_planet = {
			limit = { has_planet_flag = planet_bajor }
			remove_modifier = occupation
			add_modifier = {
				modifier = pop_liberation_fever
				days = 1000
			}
		}
	}
}

# New Bajoran Contact
country_event = {
	id = STH_bajoran_flavour.116
	hide_window = yes

	is_triggered_only = yes
	immediate = { 
		if = {
			limit = {
				exists = event_target:united_federation_of_planets
			}
			random_country = {
				limit = { has_country_flag = united_federation_of_planets }
				establish_communications_no_message = event_target:bajoran_republic
				establish_contact = {
					who = event_target:bajoran_republic
				}
			}
		}
		if = {
			limit = {
				exists = event_target:tamarian_unity
			}
			random_country = {
				limit = { has_country_flag = tamarian_unity }
				establish_communications_no_message = event_target:bajoran_republic
				establish_contact = {
					who = event_target:bajoran_republic
				}
			}
		}
		if = {
			limit = {
				exists = event_target:xepolite_guilds
			}
			random_country = {
				limit = { has_country_flag = xepolite_guilds }
				establish_communications_no_message = event_target:bajoran_republic
				establish_contact = {
					who = event_target:bajoran_republic
				}
			}
		}
	}
}

# Terror Events
country_event = {
	id = STH_bajoran_flavour.120
	hide_window = yes

	is_triggered_only = yes
	immediate = { 
		random_list = {
			20 = { country_event = { id = STH_bajoran_flavour.121 days = 5 random = 2 } } # Ship Explodes
			15 = { country_event = { id = STH_bajoran_flavour.122 days = 5 random = 2 } } # leader
			20 = { country_event = { id = STH_bajoran_flavour.123 days = 5 random = 2 } } # pop Explodes
			20 = { country_event = { id = STH_bajoran_flavour.124 days = 5 random = 2 } } # shipment
			25 = {}
		}
	}
}
#Ship Explodes
country_event = {
	id = STH_bajoran_flavour.121
	title = "STH_bajoran_flavour.121.name"
	desc = "STH_bajoran_flavour.121.desc"
	picture = sth_GFX_evt_exploding_ship
	show_sound = event_mystic_reveal

	is_triggered_only = yes
	immediate = { 
		random_owned_ship = {
			limit = {
				NOT = { exists = leader }
				is_hero_ship = no
				is_ship_size = science
			}
			destroy_ship = THIS
		}
	}
	option = { 
		name = STH_bajoran_flavour.121.a
		set_country_flag = bajoran_punishment_1
		add_resource = { influence = -50 }
		random_planet = {
			limit = { has_planet_flag = planet_bajor }
			random_pop = {
				limit = {
					is_species = BAJ
				}
				kill_pop = yes
			}
		}
	}
}
#leader Explodes
country_event = {
	id = STH_bajoran_flavour.122
	title = "STH_bajoran_flavour.122.name"
	desc = "STH_bajoran_flavour.122.desc"
	picture = sth_GFX_evt_leaderDeathFederation
	show_sound = event_mystic_reveal

	is_triggered_only = yes
	immediate = { 
		random_owned_leader = {
			limit = {
				is_hero = no
			}
			save_event_target_as = killed_leader
			kill_leader = {
				show_notification = yes
			}
		}
	}
	option = { 
		name = STH_bajoran_flavour.122.a
		add_resource = { influence = 50 }
		set_country_flag = bajoran_punishment_1
		random_owned_pop = {
			limit = {
				is_species_class = BAJ
				has_job = administrator_BAJ
			}
		}
	}
	option = { 
		name = STH_bajoran_flavour.124.b
		add_resource = { influence = 50 }
		set_country_flag = bajoran_ignore_1
	}
}
#Pop dies
country_event = {
	id = STH_bajoran_flavour.123
	title = "STH_bajoran_flavour.123.name"
	desc = "STH_bajoran_flavour.123.desc"
	picture = sth_GFX_evt_leaderDeathFederation
	show_sound = event_mystic_reveal

	is_triggered_only = yes
	immediate = { 
		random_owned_pop = {
			limit = {
				is_species = CAR
			}
			kill_pop = yes
		}
	}
	option = { 
		name = sth_understood
		set_country_flag = bajoran_punishment_2
		add_resource = { influence = -50 }
		random_planet = {
			limit = { has_planet_flag = planet_bajor }
			add_planet_devastation = 5
		}
	}
	option = { # Ignore
		name = STH_bajoran_flavour.124.b
		set_country_flag = bajoran_ignore_2
	}
}
country_event = {
	id = STH_bajoran_flavour.124
	title = "STH_bajoran_flavour.124.name"
	desc = "STH_bajoran_flavour.124.desc"
	picture = sth_GFX_evt_leaderDeathFederation
	show_sound = event_mystic_reveal

	is_triggered_only = yes
	immediate = {}
	option = { 
		name = STH_bajoran_flavour.124.a
		set_country_flag = bajoran_punishment_3
		random_list = {
			33 = { add_resource = { food = -200 } }
			33 = { add_resource = { alloys = -200 } }
			33 = { add_resource = { minerals = -200 } }
		}
		random_planet = {
			limit = { has_planet_flag = planet_bajor }
			add_planet_devastation = 3
		}
	}
	option = { # Ignore
		name = STH_bajoran_flavour.124.b
		set_country_flag = bajoran_ignore_3
	}
}
#STH_bajoran_flavour.125 - used

# Bajor passified
country_event = {
	id = STH_bajoran_flavour.130
	title = "STH_bajoran_flavour.130.name"
	desc = "STH_bajoran_flavour.130.desc"
	picture = sth_GFX_evt_bajor_occupation
	show_sound = event_mystic_reveal

	is_triggered_only = yes
	immediate = {}
	option = { 
		name = STH_bajoran_flavour.130.a
		add_resource = { influence = 100 food = 500	}
		random_planet = {
			limit = { has_planet_flag = planet_bajor }
			remove_modifier = pm_bajoran_resistance_cells
			remove_modifier = pm_bajoran_resistance_cells_large
			remove_planet_flag = bajoran_resistance
		}
	}
}

# Bajoran Sabotage
ship_event = {
	id = STH_bajoran_flavour.131
	title = "STH_bajoran_flavour.131.name"
	desc = "STH_bajoran_flavour.131.desc"
	picture = sth_GFX_evt_starshipExplosion
	show_sound = event_ship_explosion
	is_triggered_only = yes

	immediate = {
		leader = { save_event_target_as = assassinated_cardassian }
		save_event_target_as = assassinated_ship
		destroy_ship = THIS
	}
	option = { # Revenge
		name = STH_bajoran_flavour.131.a
		allow = { owner = { any_owned_planet = { has_planet_flag = planet_bajor any_pop = { is_species_class = BAJ } } } }
		random_planet = {
			limit = { has_planet_flag = planet_bajor }
			random_pop = {
				limit = { is_species_class = BAJ }
				kill_pop = yes
			}
			random_pop = {
				limit = { is_species_class = BAJ }
				kill_pop = yes
			}
			add_planet_devastation = 3
		}
		hidden_effect = {
			random_country = {
				limit = { has_country_flag = bajoran_overlords }
				random_list = {
					90 = { country_event = { id = STH_bajoran_flavour.109 days = 360 random = 30 }   }
					10 = { country_event = { id = STH_bajoran_flavour.130 days = 360 random = 30 }   }
				}
			}
		}
	}
	option = { # Attempt to root out resistance
		name = STH_bajoran_flavour.131.b
		hidden_effect = {
			random_country = {
				limit = { has_country_flag = bajoran_overlords }
				random_list = {
					60 = { country_event = { id = STH_bajoran_flavour.109 days = 360 random = 30 } }
					40 = { country_event = { id = STH_bajoran_flavour.130 days = 360 random = 30 } }
				}
			}
		}
	}
}

# Deal Reached
ship_event = {
	id = STH_bajoran_flavour.132
	title = "STH_bajoran_flavour.132.name"
	desc = "STH_bajoran_flavour.132.desc"
	picture = sth_GFX_evt_vedekAssembly
	show_sound = event_mystic_reveal
	is_triggered_only = yes

	immediate = {}

	option = { # Honour the Agreement
		name = STH_bajoran_flavour.132.a
		custom_tooltip = STH_bajoran_flavour.132.a.tooltip
		owner = {
			remove_modifier = em_occupying_force
			remove_country_flag = bajoran_overlords
			random_owned_planet = {
				limit = { has_planet_flag = planet_bajor }
				remove_modifier = pm_bajoran_resistance_cells
				remove_modifier = pm_bajoran_resistance_cells_large
				remove_modifier = pm_jammed_subspace_comms
				remove_modifier = occupation
			}
			random_owned_planet = {
				limit = { 
					OR = {
						has_planet_flag = planet_cardassia
						is_capital = yes
					}
				}
				remove_modifier = pm_havesting_occupied_world
			}
			every_country = { # NOTIFICATION
				limit = { has_communications = event_target:cardassian_union NOR = { has_country_flag = cardassian_union } }
				country_event = { id = STH_bajoran_flavour.133 days = 1 }
			}
		}
	}
	option = { # Betray the Bajorans
		name = STH_bajoran_flavour.132.b
		custom_tooltip = STH_bajoran_flavour.132.b.tooltip
		owner = {
			random_owned_planet = {
				limit = { has_planet_flag = planet_bajor }
				remove_modifier = occupation
				every_owned_pop = {
					limit = { is_species_class = BAJ }
					set_citizenship_type = {
						type = citizenship_slavery
						cooldown = yes 
					}
				}
			}
			every_country = { # NOTIFICATION
				limit = { has_communications = event_target:cardassian_union NOR = { has_country_flag = cardassian_union } }
				country_event = { id = STH_bajoran_flavour.134 days = 1 }
			}
		}
	}
}
# ROW notified
country_event = {
	id = STH_bajoran_flavour.133
	title = "STH_bajoran_flavour.133.name"
	desc = "STH_bajoran_flavour.133.desc"
	picture = sth_GFX_evt_terok_nor
	show_sound = event_mystic_reveal

	is_triggered_only = yes

	option = { name = sth_understood }
}
# ROW notified
country_event = {
	id = STH_bajoran_flavour.134
	title = "STH_bajoran_flavour.134.name"
	desc = "STH_bajoran_flavour.134.desc"
	picture = sth_GFX_evt_terok_nor
	show_sound = event_mystic_reveal

	is_triggered_only = yes

	option = { 
		name = STH_bajoran_flavour.134.a 
	}
	option = { #concerned
		name = STH_bajoran_flavour.134.b
		add_opinion_modifier = {
			who = event_target:cardassian_union
			modifier = opinion_concerned
		} 
	}
}




#Lost City of B'hala
planet_event = {
	id = STH_bajoran_flavour.200
	title = STH_bajoran_flavour.200.name
	desc = STH_bajoran_flavour.200.desc
	picture = sth_GFX_evt_vedekAssembly
	show_sound = event_activating_unknown_technology
	location = root
	fire_only_once = yes
	pre_triggers = {
		has_owner = yes
		has_ground_combat = no
	}
	trigger = {
		years_passed > 100
		has_planet_flag = bhavael_bajor
		any_pop = { is_species_class = BAJ }
		is_assimilated_planet = no
	}
	mean_time_to_happen = { years = 10 }
	immediate = {
		add_deposit = d_bajor_lostcity
		owner = {
			create_saved_leader = {
				key = bhala_discoverer
				creator = root.owner
				name = random
				species = root.owner_main_species
				class = governor
				skill = random
				set_age = 50
				traits = { trait = random_trait }
				effect = { save_global_event_target_as = bhala_discoverer }
			}
		}
	}
	option = {
		name = STH_bajoran_flavour.200.a
		tooltip = { add_deposit = d_bajor_lostcity }
	}
}
#Leader gets visions
country_event = {
	id = STH_bajoran_flavour.201
	title = STH_bajoran_flavour.201.name
	desc = STH_bajoran_flavour.201.desc
	picture = sth_GFX_evt_vedekAssembly
	location = root
	is_triggered_only = yes
	option = {
		name = STH_bajoran_flavour.201.a
		activate_saved_leader = { key = bhala_discoverer effect = { set_age = 50 save_global_event_target_as = bhala_discoverer } }
		hidden_effect = {
			every_relation = {
				limit = { has_normal_events = yes }
				country_event = { id = STH_bajoran_flavour.202 days = 3 scopes = { from = root fromfrom = root.from fromfromfrom = event_target:bhala_discoverer } }
			}
		}
		leave_alliance = { override_requirements = yes }
	}
	option = {
		name = STH_bajoran_flavour.201.b
		add_modifier = { modifier = em_ignored_prophecy days = 720 }
		hidden_effect = { remove_saved_leader = bhala_discoverer }
	}
}
#Countries Notified
country_event = {
	id = STH_bajoran_flavour.202
	title = STH_bajoran_flavour.202.name
	desc = STH_bajoran_flavour.202.desc
	picture = sth_GFX_evt_vedekAssembly
	location = root
	is_triggered_only = yes
	option = {
		name = STH_bajoran_flavour.202.a
		tooltip = { from = { leave_alliance = { override_requirements = yes } } }
	}
}
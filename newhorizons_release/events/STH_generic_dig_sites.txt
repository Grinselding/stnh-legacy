############################
#
# Iconia Probe Arch Site Events
#
# Written by Kodiak
#
############################

namespace = STH_site_iconia_probe

ship_event = {
	id = STH_site_iconia_probe.0
	hide_window = yes
	location = from
	is_triggered_only = yes
	immediate = { from = { create_archaeological_site = site_iconia_probe } }
}

#iconia probe - Site 1
fleet_event = {
	id = STH_site_iconia_probe.1
	title = STH_site_iconia_probe.1.name
	desc = STH_site_iconia_probe.1.desc
	picture = sth_GFX_evt_iconianProbe
	archaeology = yes
	is_triggered_only = yes
	immediate = { from = { set_site_progress_locked = yes } }
	after = { from = { set_site_progress_locked = no } }
	option = {
		name = STH_site_iconia_probe.1.a
		owner = { small_scaling_research_reward = { research_type = physics_research } }
	}
}

fleet_event = {
	id = STH_site_iconia_probe.2
	title = STH_site_iconia_probe.2.name
	desc = STH_site_iconia_probe.2.desc
	picture = sth_GFX_evt_iconianProbe
	archaeology = yes
	is_triggered_only = yes
	immediate = { from = { set_site_progress_locked = yes } }
	after = { from = { set_site_progress_locked = no } }
	option = {
		name = STH_site_iconia_probe.2.a
		owner = { small_scaling_research_reward = { research_type = engineering_research } }
	}
}

fleet_event = {
	id = STH_site_iconia_probe.3
	title = STH_site_iconia_probe.3.name
	desc = STH_site_iconia_probe.3.desc
	picture = sth_GFX_evt_iconianProbe
	archaeology = yes
	is_triggered_only = yes
	immediate = { from = { set_site_progress_locked = yes } }
	after = { from = { set_site_progress_locked = no } }
	option = {
		name = STH_site_iconia_probe.3.a
		owner = { small_scaling_research_reward = { research_type = physics_research } }
	}
}

fleet_event = {
	id = STH_site_iconia_probe.4
	title = STH_site_iconia_probe.4.name
	desc = STH_site_iconia_probe.4.desc
	picture = sth_GFX_evt_iconianProbe
	archaeology = yes
	is_triggered_only = yes
	immediate = { from = { set_site_progress_locked = yes } }
	after = { from = { set_site_progress_locked = no } }
	option = {
		name = STH_site_iconia_probe.4.a
		owner = { medium_scaling_research_reward = { research_type = society_research } }
	}
}

fleet_event = {
	id = STH_site_iconia_probe.5
	title = STH_site_iconia_probe.5.name
	desc = STH_site_iconia_probe.5.desc
	picture = sth_GFX_evt_iconianProbe
	archaeology = yes
	is_triggered_only = yes
	immediate = { from = { set_site_progress_locked = yes } }
	after = { from = { set_site_progress_locked = no } }
	option = {
		name = STH_site_iconia_probe.5.a
		hidden_effect = {
			owner = { 
				random_list = {
					10 = { country_event = { id = STH_site_iconia_probe.6 days = 360 random = 90 scopes = { from = root.from.planet } } }
					10 = { country_event = { id = STH_site_iconia_probe.7 days = 360 random = 90 scopes = { from = root.from.planet } } }
					10 = { country_event = { id = STH_site_iconia_probe.8 days = 360 random = 90 scopes = { from = root.from.planet } } }
				}
			}
		}
	}
}

country_event = {
	id = STH_site_iconia_probe.6
	title = STH_site_iconia_probe.6.name
	desc = STH_site_iconia_probe.6.desc
	picture = sth_GFX_evt_spaceBackground
	is_triggered_only = yes
	option = {
		name = STH_site_iconia_probe.6.a
		owner = { large_scaling_research_reward = { research_type = society_research } }
	}
}

country_event = {
	id = STH_site_iconia_probe.7
	title = STH_site_iconia_probe.7.name
	desc = STH_site_iconia_probe.7.desc
	picture = sth_GFX_evt_spaceBackground
	is_triggered_only = yes
	option = {
		name = STH_site_iconia_probe.7.a
		owner = { large_scaling_research_reward = { research_type = physics_research } }
	}
}

country_event = {
	id = STH_site_iconia_probe.8
	title = STH_site_iconia_probe.8.name
	desc = STH_site_iconia_probe.8.desc
	picture = sth_GFX_evt_spaceBackground
	is_triggered_only = yes
	option = {
		name = STH_site_iconia_probe.8.a
		owner = { large_scaling_research_reward = { research_type = engineering_research } }
	}
}


###############################
# Pahvo dig site by Kodiak    #
###############################

namespace = STH_site_pahvo_music

ship_event = {
	id = STH_site_pahvo_music.0
	hide_window = yes
	location = from
	is_triggered_only = yes
	immediate = { from = { create_archaeological_site = site_pahvo_music } }
}

#Pahvo
fleet_event = {
	id = STH_site_pahvo_music.1
	title = STH_site_pahvo_music.1.name
	desc = STH_site_pahvo_music.1.desc
	picture = sth_GFX_evt_pahvo_planet
	archaeology = yes
	is_triggered_only = yes
	immediate = { from = { set_site_progress_locked = yes } }
	after = { from = { set_site_progress_locked = no } }
	option = {
		name = STH_site_pahvo_music.1.a
		owner = { small_scaling_research_reward = { research_type = physics_research } }
	}
}

fleet_event = {
	id = STH_site_pahvo_music.2
	title = STH_site_pahvo_music.2.name
	desc = STH_site_pahvo_music.2.desc
	picture = sth_GFX_evt_pahvo_planet
	archaeology = yes
	is_triggered_only = yes
	immediate = { from = { set_site_progress_locked = yes } }
	after = { from = { set_site_progress_locked = no } }
	option = {
		name = STH_site_pahvo_music.2.a
		owner = { small_scaling_research_reward = { research_type = society_research } }
	}
}

fleet_event = {
	id = STH_site_pahvo_music.3
	title = STH_site_pahvo_music.3.name
	desc = STH_site_pahvo_music.3.desc
	picture = sth_GFX_evt_pahvo_planet
	archaeology = yes
	is_triggered_only = yes
	immediate = { from = { set_site_progress_locked = yes } }
	after = { from = { set_site_progress_locked = no } }
	option = {
		name = STH_site_pahvo_music.3.a
		owner = { small_scaling_research_reward = { research_type = engineering_research } }
	}
}

fleet_event = {
	id = STH_site_pahvo_music.4
	title = STH_site_pahvo_music.4.name
	desc = STH_site_pahvo_music.4.desc
	picture = sth_GFX_evt_pahvo_planet
	archaeology = yes
	is_triggered_only = yes
	immediate = { from = { set_site_progress_locked = yes } }
	after = { from = { set_site_progress_locked = no } }
	option = {
		name = STH_site_pahvo_music.4.a
		owner = { medium_scaling_research_reward = { research_type = society_research } }
	}
}

fleet_event = {
	id = STH_site_pahvo_music.5
	title = STH_site_pahvo_music.5.name
	desc = STH_site_pahvo_music.5.desc
	picture = sth_GFX_evt_pahvo_planet
	archaeology = yes
	is_triggered_only = yes
	immediate = { from = { set_site_progress_locked = yes } }
	after = { from = { set_site_progress_locked = no } }
	option = {
		name = STH_site_pahvo_music.5.a
		owner = { large_scaling_research_reward = { research_type = physics_research } }
		from.planet = {
			add_deposit = d_pahvo_spire
			add_deposit = d_pahvo_pahvans
        }
	}
}




###############################
# Iconian Gateways by Kodiak  #
###############################

namespace = sth_iconian_dig_site

fleet_event = {
    id = sth_iconian_dig_site.1
    title = sth_iconian_dig_site.1.name
    desc = sth_iconian_dig_site.1.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_iconian_dig_site.1.a
        owner = { 
            small_scaling_research_reward = { research_type = engineering_research }
        }
    }
}

fleet_event = {
    id = sth_iconian_dig_site.2
    title = sth_iconian_dig_site.2.name
    desc = sth_iconian_dig_site.2.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_iconian_dig_site.2.a
        owner = { add_resource = { minor_artifacts = 10 } }
    }
}

fleet_event = {
    id = sth_iconian_dig_site.3
    title = sth_iconian_dig_site.3.name
    desc = sth_iconian_dig_site.3.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_iconian_dig_site.3.a
        owner = { 
            small_scaling_research_reward = { research_type = engineering_research }
        }
    }
}

fleet_event = {
    id = sth_iconian_dig_site.4
    title = sth_iconian_dig_site.4.name
    desc = sth_iconian_dig_site.4.desc
    picture = sth_GFX_evt_iconianGateway
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_iconian_dig_site.4.a
        owner = { 
            medium_scaling_research_reward = { research_type = society_research }
        }
    }
}

fleet_event = {
    id = sth_iconian_dig_site.5
    title = sth_iconian_dig_site.5.name
    desc = sth_iconian_dig_site.5.desc
    picture = sth_GFX_evt_iconianGateway
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_iconian_dig_site.5.a
		owner = { 
            large_scaling_research_reward = { research_type = physics_research }
        }
        owner = { add_resource = { minor_artifacts = 50 } }
        from.planet = {
            add_deposit = d_iconian_gateway_deposit
        }
    }
}


#############
# Tkonian Strategic Location by Kodiak
#############

namespace = sth_military_complex_dig_site

fleet_event = {
    id = sth_military_complex_dig_site.1
    title = sth_military_complex_dig_site.1.name
    desc = sth_military_complex_dig_site.1.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = { #Assault the facility
        name = sth_military_complex_dig_site.1.a
        owner = { 
            small_scaling_research_reward = { research_type = engineering_research }
        }
    }
	option = { #Attempt to find a secondary entrance
        name = sth_military_complex_dig_site.1.b
        owner = { 
            small_scaling_research_reward = { research_type = engineering_research }
        }
    }
}

fleet_event = {
    id = sth_military_complex_dig_site.2
    title = sth_military_complex_dig_site.2.name
    desc = sth_military_complex_dig_site.2.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_military_complex_dig_site.2.a #Study facility technology to find a way inside
        owner = { add_resource = { minor_artifacts = 10 } }
    }
	option = {
        name = sth_military_complex_dig_site.2.b #Disassemble and destroy to find a way inside 
        owner = { 
            small_scaling_research_reward = { research_type = engineering_research }
        }
    }
}

fleet_event = {
    id = sth_military_complex_dig_site.3
    title = sth_military_complex_dig_site.3.name
    desc = sth_military_complex_dig_site.3.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = { #Use location
        name = sth_military_complex_dig_site.3.a
        from.planet = {
            add_deposit = d_strategic_location_deposit
        }
    }
	option = { #Dismantle location
        name = sth_military_complex_dig_site.3.b
		owner = { 
            small_scaling_research_reward = { research_type = physics_research }
        }
        owner = { add_resource = { minor_artifacts = 50 } }
    }
}


#############
# The Chase Dig Site by Kodiak
#############

namespace = sth_chase_dig_site

fleet_event = {
    id = sth_chase_dig_site.1
    title = sth_chase_dig_site.1.name
    desc = sth_chase_dig_site.1.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_chase_dig_site.1.a
        owner = { 
            small_scaling_research_reward = { research_type = society_research }
        }
    }
}

fleet_event = {
    id = sth_chase_dig_site.2
    title = sth_chase_dig_site.2.name
    desc = sth_chase_dig_site.2.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_chase_dig_site.2.a
        owner = { 
            small_scaling_research_reward = { research_type = engineering_research }
        }
    }
}

fleet_event = {
    id = sth_chase_dig_site.3
    title = sth_chase_dig_site.3.name
    desc = sth_chase_dig_site.3.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_chase_dig_site.3.a
        owner = { 
            small_scaling_research_reward = { research_type = physics_research }
        }
    }
}

fleet_event = {
    id = sth_chase_dig_site.4
    title = sth_chase_dig_site.4.name
    desc = sth_chase_dig_site.4.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_chase_dig_site.4.a
        owner = { 
            small_scaling_research_reward = { research_type = society_research }
        }
    }
}

fleet_event = {
    id = sth_chase_dig_site.5
    title = sth_chase_dig_site.5.name
    desc = sth_chase_dig_site.5.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = { ##Take research
        name = sth_chase_dig_site.5.a
        owner = { 
            medium_scaling_research_reward = { research_type = society_research }
			country_event = { id = sth_chase_dig_site.6 days = 7 }
        }
    }
	option = { ##Share research with race
		trigger = { owner = { NOT = { has_ethic = ethic_fanatic_xenophobe } } }
        name = sth_chase_dig_site.5.b
        owner = { 
            small_scaling_research_reward = { research_type = society_research }
			country_event = { id = sth_chase_dig_site.6 days = 7 }
        }
		every_playable_country = { 
			limit = { has_country_flag = alpha_beta_empire }
			add_opinion_modifier = { 
				modifier = triggered_the_chase_assisted
				who = ROOT.OWNER
			}
		}	
    }
	option = { ##Sabotage
		trigger = { owner = { NOT = { OR = { has_ethic = ethic_pacifist has_ethic = ethic_fanatic_pacifist } } } }
        name = sth_chase_dig_site.5.b
        owner = { 
            small_scaling_research_reward = { research_type = society_research }
			country_event = { id = sth_chase_dig_site.6 days = 7 }
        }
		every_playable_country = { 
			limit = { has_country_flag = alpha_beta_empire }
			add_opinion_modifier = { 
				modifier = triggered_the_chase_sabotage
				who = ROOT.OWNER
			}
		}	
    }
}

country_event = { ##Creation of site 2
	id = sth_chase_dig_site.6
	title = sth_chase_dig_site.6.name
	desc = sth_chase_dig_site.6.desc
	picture = sth_GFX_evt_theSwordOfKahless2 ##TODO
	is_triggered_only = yes
	location = event_target:theChase1Target
	immediate = {
		random_owned_planet  = {
			limit = { exists = owner is_colony = yes }
			save_event_target_as = theChase1Target
			set_planet_flag = theChase1Target
		}
	}
	option = {
		name = sth_chase_dig_site.6.a
		random_planet = {
			limit = { has_planet_flag = theChase1Target	}
			create_archaeological_site = sth_chase_2_dig_site_category
		}
	}
}

fleet_event = {
    id = sth_chase_dig_site.11
    title = sth_chase_dig_site.11.name
    desc = sth_chase_dig_site.11.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_chase_dig_site.11.a
        owner = { 
            small_scaling_research_reward = { research_type = society_research }
			add_resource = { minor_artifacts = 5 }
        }
    }
}

fleet_event = {
    id = sth_chase_dig_site.12
    title = sth_chase_dig_site.12.name
    desc = sth_chase_dig_site.12.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_chase_dig_site.12.a
        owner = { 
            small_scaling_research_reward = { research_type = engineering_research }
			add_resource = { minor_artifacts = 6 }
        }
    }
}

fleet_event = {
    id = sth_chase_dig_site.13
    title = sth_chase_dig_site.13.name
    desc = sth_chase_dig_site.13.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_chase_dig_site.13.a
        owner = { 
            small_scaling_research_reward = { research_type = engineering_research }
			add_resource = { minor_artifacts = 17 }
        }
    }
}

fleet_event = {
    id = sth_chase_dig_site.14
    title = sth_chase_dig_site.14.name
    desc = sth_chase_dig_site.14.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_chase_dig_site.14.a
        owner = { 
            small_scaling_research_reward = { research_type = engineering_research }
			add_resource = { minor_artifacts = 9 }
        }
    }
}

fleet_event = {
    id = sth_chase_dig_site.15
    title = sth_chase_dig_site.15.name
    desc = sth_chase_dig_site.15.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_chase_dig_site.15.a
        owner = { 
            medium_scaling_research_reward = { research_type = society_research }
			country_event = { id = sth_chase_dig_site.16 days = 7 }
        }
    }
}

country_event = { ##Creation of site 3
	id = sth_chase_dig_site.16
	title = sth_chase_dig_site.16.name
	desc = sth_chase_dig_site.16.desc
	picture = sth_GFX_evt_theSwordOfKahless2 ##TODO
	is_triggered_only = yes
	location = event_target:theChase2Target
	immediate = {
		random_owned_planet = {
			limit = { is_planet_class = pc_barren NOT = { has_planet_flag = theChase1Target  } }
			save_event_target_as = theChase2Target
			set_planet_flag = theChase2Target
		}
	}
	option = {
		name = sth_understood
		random_planet = {
			limit = { has_planet_flag = theChase2Target }
			create_archaeological_site = sth_chase_3_dig_site_category
		}
	}
}

fleet_event = {
    id = sth_chase_dig_site.21
    title = sth_chase_dig_site.21.name
    desc = sth_chase_dig_site.21.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_chase_dig_site.21.a
        owner = { 
            small_scaling_research_reward = { research_type = society_research }
			add_resource = { minor_artifacts = 3 }
        }
    }
}

fleet_event = {
    id = sth_chase_dig_site.22
    title = sth_chase_dig_site.22.name
    desc = sth_chase_dig_site.22.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_chase_dig_site.22.a
        owner = { 
            small_scaling_research_reward = { research_type = engineering_research }
			add_resource = { minor_artifacts = 8 }
        }
    }
}

fleet_event = {
    id = sth_chase_dig_site.23
    title = sth_chase_dig_site.23.name
    desc = sth_chase_dig_site.23.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_chase_dig_site.23.a
        owner = { 
            small_scaling_research_reward = { research_type = engineering_research }
			add_resource = { minor_artifacts = 11 }
        }
    }
}

fleet_event = {
    id = sth_chase_dig_site.24
    title = sth_chase_dig_site.24.name
    desc = sth_chase_dig_site.24.desc
    picture = sth_GFX_evt_undergroundTunnels
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_chase_dig_site.24.a
        owner = { 
            small_scaling_research_reward = { research_type = engineering_research }
			add_resource = { minor_artifacts = 4 }
        }
    }
}

fleet_event = {
    id = sth_chase_dig_site.25
    title = sth_chase_dig_site.25.name
    desc = sth_chase_dig_site.25.desc
    picture = sth_GFX_evt_theChase
	show_sound = event_finding_loot

    archaeology = yes
	is_triggered_only = yes
	
	immediate = {
		from = { set_site_progress_locked = yes }
	}

	after = { 
		from = { set_site_progress_locked = no } 
    }
    
    option = {
        name = sth_chase_dig_site.25.a
        owner = { 
            large_scaling_research_reward = { research_type = society_research }
			country_event = { id = sth_chase_dig_site.26 days = 3 } #keep self
        }
    }
	option = {
        name = sth_chase_dig_site.25.b
        owner = { 
			large_scaling_research_reward = { research_type = society_research }
			country_event = { id = sth_chase_dig_site.27 days = 3 } #share info
        }
    }
	option = {
        name = sth_chase_dig_site.25.c
        owner = { 
            large_scaling_research_reward = { research_type = society_research }
			country_event = { id = sth_chase_dig_site.28 days = 3 } #we are the superior race
        }
    }
	option = {
        name = sth_chase_dig_site.25.d
        owner = {
            large_scaling_research_reward = { research_type = society_research }
			country_event = { id = sth_chase_dig_site.29 days = 3 } #perhaps it can be weaponised
        }
    }
}


country_event = { ##Keep information for self
	id = sth_chase_dig_site.26
	title = sth_chase_dig_site.26.name
	desc = sth_chase_dig_site.26.desc
	picture = sth_GFX_evt_alien_planet ##TODO
	is_triggered_only = yes
	location = event_target:theChase2Target
	immediate = {
	}
	option = {
		name = sth_chase_dig_site.26.a	
		large_scaling_research_reward = { research_type = society_research }		
	}
}

country_event = { ##Share information freely
	id = sth_chase_dig_site.27
	title = sth_chase_dig_site.27.name
	desc = sth_chase_dig_site.27.desc
	picture = sth_GFX_evt_alien_planet ##TODO
	is_triggered_only = yes
	location = event_target:theChase2Target
	immediate = {
	}
	option = {
		allow = { NOT = { OR = { has_ethic = ethic_xenophobe has_ethic = ethic_fanatic_xenophobe } } }
		name = sth_chase_dig_site.27.a
		large_scaling_research_reward = { research_type = society_research }
		every_playable_country = { 
			limit = { has_country_flag = alpha_beta_empire }
			add_opinion_modifier = { 
				modifier = the_chase_shared_info 
				who = root 
			}
		}
	}
}

country_event = { ##This is nonsense! Unity boost
	id = sth_chase_dig_site.28
	title = sth_chase_dig_site.28.name
	desc = sth_chase_dig_site.28.desc
	picture = sth_GFX_evt_alien_planet ##TODO
	is_triggered_only = yes
	location = event_target:theChase2Target
	immediate = {
	}
	option = {
		allow = { OR = { has_ethic = ethic_authoritarian has_ethic = ethic_fanatic_authoritarian } }
		name = sth_chase_dig_site.28.a
		very_large_scaling_unity_reward = yes
	}
}

country_event = { #Perhaps it can be weaponised
	id = sth_chase_dig_site.29
	title = sth_chase_dig_site.29.name
	desc = sth_chase_dig_site.29.desc
	picture = sth_GFX_evt_alien_planet ##TODO
	is_triggered_only = yes
	location = event_target:theChase2Target
	immediate = {
	}
	option = {
		allow = { OR = { has_ethic = ethic_militarist has_ethic = ethic_fanatic_militarist } }
		name = sth_chase_dig_site.29.a
		very_large_scaling_research_reward = { research_type = society_research }
	}
}
############################
#
# Ship Component Events
#
#
############################


namespace = STH_components

# This = owner of fleet 1
# From = owner of fleet 2
# FromFrom = fleet 1
# FromFromFrom = fleet 2
country_event = {
	id = STH_components.1500
	hide_window = yes
	is_triggered_only = yes
	immediate = { 
		every_owned_ship = { 
			limit = { exists = fleet fleet = { is_same_value = root.fromfrom } }
			add_cloak_bonus = yes
		}
	}
}
portraits = {
	# yridian
	yridian_01 = {	
		entity = "portrait_yridian_male_01_entity" clothes_selector = "yridian_clothes_01" hair_selector = "yridian_lobes_01" greeting_sound = "human_male_greetings_01" 
		character_textures = {
			"gfx/models/portraits/yridian/yridian_male_body_01.dds"
		}
	}	
	yridian_02 = {	
		entity = "portrait_yridian_male_01_entity" clothes_selector = "yridian_clothes_01" hair_selector = "yridian_lobes_02" greeting_sound = "human_male_greetings_01" 
		character_textures = {
			"gfx/models/portraits/yridian/yridian_male_body_01b.dds"
		}
	}
	yridian_03 = {	
		entity = "portrait_yridian_male_01_entity" clothes_selector = "yridian_clothes_01" hair_selector = "yridian_lobes_03" greeting_sound = "human_male_greetings_01" 
		character_textures = {
			"gfx/models/portraits/yridian/yridian_male_body_01c.dds"
		}
	}
}

portrait_groups = {
	yridian = {
		default = yridian_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				portraits = {
					yridian_01
					yridian_02
					yridian_03
				}
			}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					yridian_01
					yridian_02
					yridian_03
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					yridian_01
					yridian_02
					yridian_03
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				portraits = {
					yridian_01
					yridian_02
					yridian_03
				}
			}
		}

			
		#leader scope 
		ruler = {
			add = {
				portraits = {
					yridian_01
					yridian_02
					yridian_03
				}
			}
		}
	}
}

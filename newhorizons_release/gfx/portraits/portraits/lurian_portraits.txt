portraits = {
	lurian_male_01 = {	
		entity = "portrait_lurian_male_01_entity"	clothes_selector = "lurian_clothes_01" 	hair_selector = "lurian_head_01"		greeting_sound = "grumpy_male_greetings_01" 
		character_textures = {
			"gfx/models/portraits/lurian/lurian_body_01.dds"
			"gfx/models/portraits/lurian/lurian_body_01b.dds"
			"gfx/models/portraits/lurian/lurian_body_01c.dds"
		}
	}
	lurian_female_01 = {	
		entity = "portrait_lurian_male_01_entity"	clothes_selector = "lurian_clothes_01" 	hair_selector = "lurian_head_01"		greeting_sound = "grumpy_female_greetings_01" 
		character_textures = {
			"gfx/models/portraits/lurian/lurian_body_01.dds"
			"gfx/models/portraits/lurian/lurian_body_01b.dds"
			"gfx/models/portraits/lurian/lurian_body_01c.dds"
		}
	}
}

portrait_groups = {
	lurian = {
		default = lurian_male_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					lurian_male_01
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					lurian_female_01
				}
			}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					lurian_male_01
					lurian_female_01
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					lurian_male_01
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					lurian_male_01
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					lurian_female_01
				}
			}
		}

			
		#leader scope 
		ruler = {
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					lurian_male_01
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					lurian_female_01
				}
			}
		}
	}	
}
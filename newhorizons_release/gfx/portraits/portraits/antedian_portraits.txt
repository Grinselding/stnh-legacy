portraits = {
	# antedian
	antedian_01 = {	
		entity = "portrait_human_male_01_entity"	clothes_selector = "antedian_clothes_01" hair_selector = "antedian_head_01" greeting_sound = "antedean_greetings_01" 
		character_textures = { "gfx/models/portraits/antedian/antedian_body_01.dds" }
	}
	antedian_02 = {	
		entity = "portrait_human_male_01_entity"	clothes_selector = "antedian_clothes_01" hair_selector = "antedian_head_02"	greeting_sound = "antedean_greetings_01" 
		character_textures = { "gfx/models/portraits/antedian/antedian_body_02.dds" }
	}
	antedian_03 = {	
		entity = "portrait_human_male_01_entity"	clothes_selector = "antedian_clothes_01" hair_selector = "antedian_head_03"	greeting_sound = "antedean_greetings_01" 
		character_textures = { "gfx/models/portraits/antedian/antedian_body_03.dds" }
	}
	antedian_04 = {	
		entity = "portrait_human_male_01_entity"	clothes_selector = "antedian_clothes_01" hair_selector = "antedian_head_04"	greeting_sound = "antedean_greetings_01" 
		character_textures = { "gfx/models/portraits/antedian/antedian_body_04.dds" }
	}
}

portrait_groups = {
	antedian = {
		default = antedian_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				portraits = {
					antedian_01
					antedian_02
					antedian_03
					antedian_04
				}
			}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					antedian_01
					antedian_02
					antedian_03
					antedian_04
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					antedian_01
					antedian_02
					antedian_03
					antedian_04
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				portraits = {
					antedian_01
					antedian_02
					antedian_03
					antedian_04
				}
			}
		}

			
		#leader scope 
		ruler = {
			add = {
				portraits = {
					antedian_01
					antedian_02
					antedian_03
					antedian_04
				}
			}
		}
	}	
}
portraits = {
	# tzenkethi
	tzenkethi_01 = {	entity = "portrait_tzenkethi_01_entity"	clothes_selector = "no_texture" hair_selector = "no_texture"  greeting_sound = "reptilian_01_greetings"
		character_textures = { "gfx/models/portraits/tzenkethi/tzenkethi_body_01.dds" }
	}
	tzenkethi_02 = {	entity = "portrait_tzenkethi_01_entity"	clothes_selector = "no_texture" hair_selector = "no_texture"  greeting_sound = "reptilian_01_greetings"
		character_textures = { "gfx/models/portraits/tzenkethi/tzenkethi_body_02.dds" }
	}
	tzenkethi_03 = {	entity = "portrait_tzenkethi_01_entity"	clothes_selector = "no_texture" hair_selector = "no_texture"  greeting_sound = "reptilian_01_greetings"
		character_textures = { "gfx/models/portraits/tzenkethi/tzenkethi_body_03.dds" }
	}
	tzenkethi_04 = {	entity = "portrait_tzenkethi_01_entity"	clothes_selector = "no_texture" hair_selector = "no_texture"  greeting_sound = "reptilian_01_greetings"
		character_textures = { "gfx/models/portraits/tzenkethi/tzenkethi_body_04.dds" }
	}
}


portrait_groups = {
	tzenkethi = {
		default = tzenkethi_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = { portraits = { tzenkethi_01 tzenkethi_02 tzenkethi_03 tzenkethi_04 } }
		}		
		
		#species scope
		species = { add = { portraits = { tzenkethi_01 tzenkethi_02 tzenkethi_03 tzenkethi_04 } } }		
		
		#pop scope
		pop = { add = { portraits = { tzenkethi_01 tzenkethi_02 tzenkethi_03 tzenkethi_04 } } }
		
		#leader scope
		leader = { add = { portraits = { tzenkethi_01 tzenkethi_02 tzenkethi_03 tzenkethi_04 } } }
		
		#leader scope 
		ruler = { add = { portraits = { tzenkethi_01 tzenkethi_02 tzenkethi_03 tzenkethi_04 } } }
	}		
}
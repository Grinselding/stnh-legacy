portraits = {
	# klingonAugmentHuman
	klingonAugmentHuman_female_04 = {	
		entity = "portrait_human_female_04_entity"	clothes_selector = "klingon_female_clothes_combined" 	hair_selector = "klingonAugmentHuman_female_hair_01"	greeting_sound = "klingon_female_greetings_01"
		character_textures = { "gfx/models/portraits/klingonAugmentHuman/klingonAugmentHuman_female_body_04.dds" }
	}
	klingonAugmentHuman_female_05 = {	
		entity = "portrait_human_female_05_entity"	clothes_selector = "klingon_female_clothes_combined" 	hair_selector = "klingonAugmentHuman_female_hair_01"	greeting_sound = "klingon_female_greetings_01"
		character_textures = { "gfx/models/portraits/klingonAugmentHuman/klingonAugmentHuman_female_body_05.dds" }
	}
	klingonAugmentHuman_male_04 = {	
		entity = "portrait_human_male_04_entity"	clothes_selector = "klingon_male_clothes_combined" 	hair_selector = "klingonAugmentHuman_male_hair_01"	greeting_sound = "klingon_male_greetings_01"
		character_textures = { "gfx/models/portraits/klingonAugmentHuman/klingonAugmentHuman_male_body_04.dds" }
	}
	klingonAugmentHuman_male_05 = {	
		entity = "portrait_human_male_05_entity"	clothes_selector = "klingon_male_clothes_combined" 	hair_selector = "klingonAugmentHuman_male_hair_01"	greeting_sound = "klingon_male_greetings_03"
		character_textures = { "gfx/models/portraits/klingonAugmentHuman/klingonAugmentHuman_male_body_05.dds" }
	}
}

portraits = {
	# klingon
	klingon_female_04 = {	
		entity = "portrait_human_female_04_entity"	clothes_selector = "klingon_female_clothes_combined" 	hair_selector = "klingon_female_hair_01"	greeting_sound = "klingon_female_greetings_01"
		character_textures = {
			"gfx/models/portraits/klingon civilian/klingon_female_body_04.dds"
		}
	}
	klingon_female_04b = {	
		entity = "portrait_human_female_04_entity"	clothes_selector = "klingon_female_clothes_combined" 	hair_selector = "klingon_female_hair_01"	greeting_sound = "klingon_female_greetings_01"
		character_textures = {
			"gfx/models/portraits/klingon civilian/klingon_female_body_04b.dds"
		}
	}
	klingon_female_05 = {	
		entity = "portrait_human_female_05_entity"	clothes_selector = "klingon_female_clothes_combined" 	hair_selector = "klingon_female_hair_02"	greeting_sound = "klingon_female_greetings_01"
		character_textures = {
			"gfx/models/portraits/klingon civilian/klingon_female_body_05.dds"
		}
	}
	klingon_female_06 = {	
		entity = "portrait_human_female_05_entity"	clothes_selector = "klingon_female_clothes_combined" 	hair_selector = "klingon_female_hair_02"	greeting_sound = "klingon_female_greetings_01"
		character_textures = {
			"gfx/models/portraits/klingon civilian/klingon_female_body_06.dds"
		}
	}
	klingon_male_04 = {	
		entity = "portrait_human_male_04_entity"	clothes_selector = "klingon_male_clothes_combined" 	hair_selector = "klingon_male_hair_01"	greeting_sound = "klingon_male_greetings_01"
		character_textures = {
			"gfx/models/portraits/klingon civilian/klingon_male_body_04.dds"
		}
	}
	klingon_male_04b = {	
		entity = "portrait_human_male_04_entity"	clothes_selector = "klingon_male_clothes_combined" 	hair_selector = "klingon_male_hair_01"	greeting_sound = "klingon_male_greetings_03"
		character_textures = {
			"gfx/models/portraits/klingon civilian/klingon_male_body_04b.dds"
		}
	}
	klingon_male_05 = {	
		entity = "portrait_human_male_05_entity"	clothes_selector = "klingon_male_clothes_combined" 	hair_selector = "klingon_male_hair_02"	greeting_sound = "klingon_male_greetings_05"
		character_textures = {
			"gfx/models/portraits/klingon civilian/klingon_male_body_05.dds"
		}
	}
	klingon_male_06 = {	
		entity = "portrait_human_male_05_entity"	clothes_selector = "klingon_male_clothes_combined" 	hair_selector = "klingon_male_hair_02"	greeting_sound = "klingon_male_greetings_07"
		character_textures = {
			"gfx/models/portraits/klingon civilian/klingon_male_body_06.dds"
		}
	}
}

portrait_groups = {
	klingon = {
		default = klingon_male_04
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = { ruler = { gender = male } }
				portraits = { klingon_male_04b klingon_male_05 klingon_male_06 klingon_male_04 }
			}
			add = {
				trigger = { ruler = { gender = female } }
				portraits = { klingon_female_04b klingon_female_05 klingon_female_06 klingon_female_04 }
			}
		}
		#species scope
		species = { #generic portrait for a species
			add = {
				trigger = { NOT = { has_global_flag = augmentVirus } }
				portraits = { klingon_female_04b klingon_female_05 klingon_female_06 klingon_female_04  klingon_male_04b klingon_male_05 klingon_male_06 klingon_male_04 }
			}
		}		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = { klingon_female_04b klingon_female_05 klingon_female_06 klingon_female_04  klingon_male_04b klingon_male_05 klingon_male_06 klingon_male_04 }
			}
		}
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = { gender = male }
				portraits = { klingon_male_04b klingon_male_05 klingon_male_06 klingon_male_04 }
			}
			add = {
				trigger = { gender = female }
				portraits = { klingon_female_04b klingon_female_05 klingon_female_06 klingon_female_04 }
			}
		}
		#leader scope 
		ruler = {
			add = {
				trigger = { gender = male }
				portraits = { klingon_male_04b klingon_male_05 klingon_male_06 klingon_male_04 }
			}
			add = {
				trigger = { gender = female }
				portraits = { klingon_female_04b klingon_female_05 klingon_female_06 klingon_female_04 }
			}
		}
	}	
}

portrait_groups = {
	klingon_human_augment = {
		default = klingonAugmentHuman_male_04
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = { ruler = { gender = male } }
				portraits = { klingonAugmentHuman_male_04 klingonAugmentHuman_male_05 klingon_male_06 }
			}
			add = {
				trigger = { ruler = { gender = female } }
				portraits = { klingonAugmentHuman_female_04 klingonAugmentHuman_female_05 }
			}
		}		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = { klingonAugmentHuman_female_04 klingonAugmentHuman_female_05 klingonAugmentHuman_male_04 klingonAugmentHuman_male_05 }
			}
		}		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = { klingonAugmentHuman_female_04 klingonAugmentHuman_female_05 klingonAugmentHuman_male_04 klingonAugmentHuman_male_05 }
			}
		}
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = { gender = female }
				portraits = { klingonAugmentHuman_female_04 klingonAugmentHuman_female_05 }
			}
			add = {
				trigger = { gender = male }
				portraits = { klingonAugmentHuman_male_04 klingonAugmentHuman_male_05 }
			}
		}
		#leader scope 
		ruler = {
			add = {
				trigger = { gender = female }
				portraits = { klingonAugmentHuman_female_04 klingonAugmentHuman_female_05 }
			}
			add = {
				trigger = { gender = male }
				portraits = { klingonAugmentHuman_male_04 klingonAugmentHuman_male_05 }
			}
		}
	}	
}
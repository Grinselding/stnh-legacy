portraits = {
	# kinshaya
	kinshaya_01 = {	
		entity = "portrait_kinshaya_01_entity"	clothes_selector = "kinshaya_clothes_01" hair_selector = "kinshaya_bald_01" greeting_sound = "gorn_male_greetings_01" 
		character_textures = {
			"gfx/models/portraits/kinshaya/kinshaya_normal_01_black.dds"
		}
	}
	kinshaya_02 = {	
		entity = "portrait_kinshaya_01_entity"	clothes_selector = "kinshaya_clothes_01" hair_selector = "kinshaya_bald_01" greeting_sound = "gorn_male_greetings_01" 
		character_textures = {
			"gfx/models/portraits/kinshaya/kinshaya_normal_01_blue.dds"
		}
	}
	kinshaya_03 = {	
		entity = "portrait_kinshaya_01_entity"	clothes_selector = "kinshaya_clothes_01" hair_selector = "kinshaya_bald_01" greeting_sound = "gorn_male_greetings_01" 
		character_textures = {
			"gfx/models/portraits/kinshaya/kinshaya_normal_01_pink.dds"
		}
	}
	kinshaya_04 = {	
		entity = "portrait_kinshaya_01_entity"	clothes_selector = "kinshaya_clothes_01" hair_selector = "kinshaya_bald_01" greeting_sound = "gorn_male_greetings_01" 
		character_textures = {
			"gfx/models/portraits/kinshaya/kinshaya_normal_01_purple.dds"
		}
	}
	kinshaya_05 = {	
		entity = "portrait_kinshaya_01_entity"	clothes_selector = "kinshaya_clothes_01" hair_selector = "kinshaya_bald_01" greeting_sound = "gorn_male_greetings_01" 
		character_textures = {
			"gfx/models/portraits/kinshaya/kinshaya_normal_01_red.dds"
		}
	}	
}

portrait_groups = {
	kinshaya = {
		default = kinshaya_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					kinshaya_01
					kinshaya_02
					kinshaya_05
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					kinshaya_03
					kinshaya_04
					kinshaya_05
				}
			}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					kinshaya_01
					kinshaya_02
					kinshaya_03
					kinshaya_04
					kinshaya_05
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					kinshaya_01
					kinshaya_02
					kinshaya_03
					kinshaya_04
					kinshaya_05
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					kinshaya_01
					kinshaya_02
					kinshaya_05
				}
			}
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					kinshaya_03
					kinshaya_04
					kinshaya_05
				}
			}
		}

			
		#leader scope 
		ruler = {
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					kinshaya_01
				}
			}
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					kinshaya_03
					kinshaya_04
					kinshaya_05
				}
			}
		}
	}	
}
portraits = {
	# baku
	baku_female_01 = {	
		entity = "portrait_human_female_01_entity" clothes_selector = "baku_female_clothes_01" hair_selector = "human_female_hair_01" greeting_sound = "human_female_greetings_03"
	}
	baku_female_02 = {	
		entity = "portrait_human_female_02_entity" clothes_selector = "baku_female_clothes_01" hair_selector = "human_female_hair_02" greeting_sound = "human_female_greetings_04"
	}
	baku_female_03 = {	
		entity = "portrait_human_female_03_entity" clothes_selector = "baku_female_clothes_01" hair_selector = "human_female_hair_03" greeting_sound = "human_female_greetings_05"
	}	
	baku_female_04 = {	
		entity = "portrait_human_female_04_entity" clothes_selector = "baku_female_clothes_01" hair_selector = "human_female_hair_04" greeting_sound = "human_female_greetings_01"
	}
	baku_female_05 = {	
		entity = "portrait_human_female_05_entity" clothes_selector = "baku_female_clothes_01" hair_selector = "human_female_hair_05" greeting_sound = "human_female_greetings_02"
	}
	baku_male_01 = {	
		entity = "portrait_human_male_01_entity" clothes_selector = "baku_male_clothes_01" hair_selector = "human_male_hair_01" greeting_sound = "human_male_greetings_03" 
	}	
	baku_male_02 = {	
		entity = "portrait_human_male_02_entity"	clothes_selector = "baku_male_clothes_01" hair_selector = "human_male_hair_02" greeting_sound = "human_male_greetings_04" 
	}
	baku_male_04 = {	
		entity = "portrait_human_male_03_entity" clothes_selector = "baku_male_clothes_01" hair_selector = "human_male_hair_04" greeting_sound = "human_male_greetings_01" 
	}
	baku_male_05 = {	
		entity = "portrait_human_male_04_entity" clothes_selector = "baku_male_clothes_01" hair_selector = "human_male_hair_05" greeting_sound = "human_male_greetings_02" 
	}
}

portrait_groups = {
	baku = {
		default = baku_female_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = { ruler = { gender = male } }
				portraits = {
					baku_male_01
					baku_male_02
					baku_male_04
					baku_male_05
				}
			}
			add = {
				trigger = { ruler = { gender = female } }
				portraits = {
					baku_female_01
					baku_female_02
					baku_female_03
					baku_female_04
					baku_female_05
				}
			}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					baku_female_01
					baku_female_02
					baku_female_03					
					baku_female_04
					baku_female_05
					baku_male_01
					baku_male_02				
					baku_male_04
					baku_male_05
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					baku_female_01
					baku_female_02
					baku_female_03				
					baku_female_04
					baku_female_05
					baku_male_01
					baku_male_02				
					baku_male_04
					baku_male_05
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = { gender = female }
				portraits = {
					baku_female_01
					baku_female_02
					baku_female_03					
					baku_female_04
					baku_female_05
				}
			}
			add = {
				trigger = { gender = male }
				portraits = {
					baku_male_01
					baku_male_02			
					baku_male_04
					baku_male_05
				}
			}
		}

			
		#leader scope 
		ruler = {
			add = {
				trigger = { gender = female }
				portraits = {
					baku_female_01
					baku_female_02
					baku_female_03					
					baku_female_04
					baku_female_05
				}
			}
			add = {
				trigger = { gender = male }
				portraits = {
					baku_male_01
					baku_male_02				
					baku_male_04
					baku_male_05
				}
			}
		}
	}	
}
# This is a template which multiple species can use. 

brunali_female_clothes_01 = {
	default = "gfx/models/portraits/brunali/civ_brunali_female_clothes_01.dds"
	
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/brunali/civ_brunali_female_clothes_01.dds"
	}
	
	#species scope
	species = { #generic portrait for a species
		default = "gfx/models/portraits/brunali/civ_brunali_female_clothes_01.dds"
		random = { 
	        trigger = { uses_borg_full_prosthetics = yes } 
	        list = {
		        "gfx/models/portraits/borg/borg_female_clothes_01.dds"
	        } 
        }
	}
	
	#pop scope
	pop = { #for a specific pop
		random = {
			trigger = { uses_borg_full_prosthetics = no } 
			list = {
				"gfx/models/portraits/brunali/civ_brunali_female_clothes_01.dds"
				"gfx/models/portraits/brunali/civ_brunali_female_clothes_02.dds"
				"gfx/models/portraits/brunali/civ_brunali_female_clothes_03.dds"
				"gfx/models/portraits/bajoran civilian/civ_bajoran_female_clothes_01.dds"
				"gfx/models/portraits/bajoran civilian/civ_bajoran_female_clothes_02.dds"
				"gfx/models/portraits/bajoran civilian/civ_bajoran_female_clothes_03.dds"
				"gfx/models/portraits/bajoran civilian/civ_bajoran_female_clothes_04.dds"
			}
		}

        random = { 
	        trigger = { uses_borg_full_prosthetics = yes } 
	        list = {
		        "gfx/models/portraits/borg/borg_female_clothes_01.dds"
	        } 
        } 
	}
	#leader scope
	leader = { #scientists, generals, admirals, governor
		#Brunali
		"gfx/models/portraits/brunali/civ_brunali_female_clothes_01.dds" = { leader_class = admiral NOT = { owner = { has_country_flag = united_federation_of_planets } } uses_borg_full_prosthetics = no }
		"gfx/models/portraits/brunali/civ_brunali_female_clothes_02.dds" = { leader_class = scientist NOT = { owner = { has_country_flag = united_federation_of_planets } } uses_borg_full_prosthetics = no }
		"gfx/models/portraits/brunali/civ_brunali_female_clothes_03.dds" = { leader_class = general NOT = { owner = { has_country_flag = united_federation_of_planets } } uses_borg_full_prosthetics = no }
		"gfx/models/portraits/brunali/civ_brunali_female_clothes_02.dds" = { leader_class = governor NOT = { owner = { has_country_flag = united_federation_of_planets } } uses_borg_full_prosthetics = no }	
		#Federation
		"gfx/models/portraits/starfleet_enterprise/ent_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes enterprise_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_enterprise/ent_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes enterprise_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_enterprise/ent_human_female_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes enterprise_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_enterprise/ent_human_female_admiral.dds" = { leader_class = governor uses_starfleet_uniform = yes enterprise_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet franklin/fra_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes franklin_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet franklin/fra_human_female_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes franklin_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet franklin/fra_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes franklin_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet franklin/fra_human_female_admiral.dds" = { leader_class = governor uses_starfleet_uniform = yes franklin_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet kelvin/kel_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes kelvin_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet kelvin/kel_human_female_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes kelvin_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet kelvin/kel_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes kelvin_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet kelvin/kel_human_female_admiral.dds" = { leader_class = governor uses_starfleet_uniform = yes kelvin_era = yes uses_borg_full_prosthetics = no }
# Discovery
		"gfx/models/portraits/starfleet_discovery/dis_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes is_medical_leader = no discovery_era = yes }
		"gfx/models/portraits/starfleet_discovery/dis_human_female_medical.dds" = { leader_class = scientist uses_starfleet_uniform = yes is_medical_leader = yes discovery_era = yes }
		"gfx/models/portraits/starfleet_discovery/dis_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_starfleet_uniform = yes discovery_era = yes }
		"gfx/models/portraits/starfleet_discovery/dis_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes discovery_era = yes }
		"gfx/models/portraits/starfleet_discovery/dis_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes discovery_era = yes }		
		"gfx/models/portraits/starfleet_discovery/dis_human_female_command.dds" = { leader_class = envoy uses_starfleet_uniform = yes discovery_era = yes }
# TOS		
		"gfx/models/portraits/starfleet_original_series/tos_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes original_series_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_original_series/tos_human_female_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes original_series_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_original_series/tos_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes original_series_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_original_series/tos_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes original_series_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes wrath_of_khan_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_female_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes wrath_of_khan_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes wrath_of_khan_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes wrath_of_khan_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet next generation 02/tng2_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes next_generation_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet next generation 02/tng2_human_female_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes next_generation_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet next generation 02/tng2_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes next_generation_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet next generation 02/tng2_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes next_generation_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_first_contact/fco_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes first_contact_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_first_contact/fco_human_female_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes first_contact_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_first_contact/fco_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes first_contact_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_first_contact/fco_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes first_contact_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_2380/2380_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes children_of_mars_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_2380/2380_human_female_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes children_of_mars_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_2380/2380_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes children_of_mars_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_2380/2380_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes children_of_mars_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_picard/pic_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes picard_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_picard/pic_human_female_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes picard_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_picard/pic_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes picard_era = yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_picard/pic_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes picard_era= yes uses_borg_full_prosthetics = no }
		"gfx/models/portraits/starfleet_all_good_things/agt_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes all_good_things_era = yes }
		"gfx/models/portraits/starfleet_all_good_things/agt_human_female_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes all_good_things_era = yes }
		"gfx/models/portraits/starfleet_all_good_things/agt_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes all_good_things_era = yes }
		"gfx/models/portraits/starfleet_all_good_things/agt_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes all_good_things_era = yes }		
        random = { 
	        trigger = { uses_borg_full_prosthetics = yes } 
	        list = {
		        "gfx/models/portraits/borg/borg_female_clothes_01.dds"
	        } 
        } 
	}

	#leader scope 
	ruler = { #for rulers
		#default = "gfx/models/portraits/brunali/civ_brunali_female_clothes_01.dds"
		random = {
			trigger = { uses_borg_full_prosthetics = no } 
			list = {
				"gfx/models/portraits/brunali/civ_brunali_female_clothes_01.dds"
				"gfx/models/portraits/brunali/civ_brunali_female_clothes_02.dds"
				"gfx/models/portraits/brunali/civ_brunali_female_clothes_03.dds"
				"gfx/models/portraits/bajoran civilian/civ_bajoran_female_clothes_01.dds"
				"gfx/models/portraits/bajoran civilian/civ_bajoran_female_clothes_02.dds"
				"gfx/models/portraits/bajoran civilian/civ_bajoran_female_clothes_03.dds"
				"gfx/models/portraits/bajoran civilian/civ_bajoran_female_clothes_04.dds"
			}
		}
        random = { 
	        trigger = { uses_borg_full_prosthetics = yes } 
	        list = {
		        "gfx/models/portraits/borg/borg_female_clothes_01.dds"
	        } 
        }
	}
}

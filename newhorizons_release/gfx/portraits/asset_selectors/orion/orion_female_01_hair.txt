# This is a template which multiple species can use. 
  
orion_female_hair_01 = {

	default = "gfx/models/portraits/all/human_female_hair_dark_black_style_01.dds"
	
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/all/human_female_hair_dark_black_style_01.dds"
	}
	
	#species scope
	species = { #generic portrait for a species
		default = "gfx/models/portraits/all/human_female_hair_dark_black_style_01.dds"
		random = {
			trigger = { uses_borg_prosthetics = yes } 
			list = {
				"gfx/models/portraits/borg/borg_female_prosthetic_01.dds"
				"gfx/models/portraits/borg/borg_female_prosthetic_02.dds"
				"gfx/models/portraits/borg/borg_female_prosthetic_03.dds"
			}
		}
	}
	
	#pop scope
	pop = { #for a specific pop
		default = "gfx/models/portraits/all/human_female_hair_dark_black_style_01.dds"
		random = {
			trigger = { uses_borg_prosthetics = no } 
			list = {
				"gfx/models/portraits/all/human_female_hair_dark_black_style_01.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_05.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_06.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_08.dds"
				"gfx/models/portraits/all/human_female_hair_orange_style_01.dds"
				"gfx/models/portraits/all/human_female_hair_orange_style_05.dds"
				"gfx/models/portraits/all/human_female_hair_orange_style_06.dds"
				"gfx/models/portraits/all/human_female_hair_orange_style_08.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_01.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_05.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_06.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_08.dds"
			}
		}	
		random = {
			trigger = { uses_borg_prosthetics = yes } 
			list = {
				"gfx/models/portraits/borg/borg_female_prosthetic_01.dds"
				"gfx/models/portraits/borg/borg_female_prosthetic_02.dds"
				"gfx/models/portraits/borg/borg_female_prosthetic_03.dds"
			}
		}
	}
	
	#leader scope
	leader = { #scientists, generals, admirals, governor
		default = "gfx/models/portraits/all/human_female_hair_dark_black_style_01.dds"
		random = {
			trigger = { uses_borg_prosthetics = no } 
			list = {
				"gfx/models/portraits/all/human_female_hair_dark_black_style_01.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_05.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_06.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_08.dds"
				"gfx/models/portraits/all/human_female_hair_orange_style_01.dds"
				"gfx/models/portraits/all/human_female_hair_orange_style_05.dds"
				"gfx/models/portraits/all/human_female_hair_orange_style_06.dds"
				"gfx/models/portraits/all/human_female_hair_orange_style_08.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_01.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_05.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_06.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_08.dds"
			}
		}
		random = {
			trigger = { uses_borg_prosthetics = yes } 
			list = {
				"gfx/models/portraits/borg/borg_female_prosthetic_01.dds"
				"gfx/models/portraits/borg/borg_female_prosthetic_02.dds"
				"gfx/models/portraits/borg/borg_female_prosthetic_03.dds"
			}
		}		
	}
  
	#leader scope 
	ruler = { #for rulers
		default = "gfx/models/portraits/all/human_female_hair_dark_black_style_01.dds"
		random = {
			trigger = { uses_borg_prosthetics = no } 
			list = {
				"gfx/models/portraits/all/human_female_hair_dark_black_style_01.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_05.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_06.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_08.dds"
				"gfx/models/portraits/all/human_female_hair_orange_style_01.dds"
				"gfx/models/portraits/all/human_female_hair_orange_style_05.dds"
				"gfx/models/portraits/all/human_female_hair_orange_style_06.dds"
				"gfx/models/portraits/all/human_female_hair_orange_style_08.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_01.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_05.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_06.dds"
				"gfx/models/portraits/all/human_female_hair_dark_black_style_08.dds"
			}
		}
		random = {
			trigger = { uses_borg_prosthetics = yes } 
			list = {
				"gfx/models/portraits/borg/borg_female_prosthetic_01.dds"
				"gfx/models/portraits/borg/borg_female_prosthetic_02.dds"
				"gfx/models/portraits/borg/borg_female_prosthetic_03.dds"
			}
		}		
	}
}
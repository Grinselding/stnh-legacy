orion_male_hair_01 = {
	default = "gfx/models/portraits/all/bald.dds"
	
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/all/bald.dds"
	}
	
	#species scope
	species = { #generic portrait for a species
		default = "gfx/models/portraits/all/bald.dds"
		random = {
			list = {
				"gfx/models/portraits/all/bald.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_01.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_02.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_01.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_10.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_03.dds"
				"gfx/models/portraits/all/bald.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_01.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_10.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_01.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_02.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_03.dds"
				"gfx/models/portraits/all/bald.dds"
			}
		}
		random = { 
	        trigger = { uses_borg_prosthetics = yes } 
	        list = { 
				"gfx/models/portraits/borg/borg_male_prosthetic_01.dds"
				"gfx/models/portraits/borg/borg_male_prosthetic_02.dds"
				"gfx/models/portraits/borg/borg_male_prosthetic_03.dds"
			}
        }		
	}
	
	#pop scope
	pop = { #for a specific pop
		default = "gfx/models/portraits/all/bald.dds"
		random = {
			trigger = { uses_borg_prosthetics = no } 
			list = {
				"gfx/models/portraits/all/bald.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_01.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_02.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_01.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_10.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_03.dds"
				"gfx/models/portraits/all/bald.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_01.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_10.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_01.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_02.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_03.dds"
				"gfx/models/portraits/all/bald.dds"
			}
		}
		random = { 
	        trigger = { uses_borg_prosthetics = yes } 
	        list = { 
				"gfx/models/portraits/borg/borg_male_prosthetic_01.dds"
				"gfx/models/portraits/borg/borg_male_prosthetic_02.dds"
				"gfx/models/portraits/borg/borg_male_prosthetic_03.dds"
			}
        } 
	}
	
	
	
	
	#leader scope
	leader = { #scientists, generals, admirals, governor
		default = "gfx/models/portraits/all/bald.dds"		
		random = {
			trigger = { uses_borg_prosthetics = no }
			list = {
				"gfx/models/portraits/all/bald.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_01.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_02.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_01.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_10.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_03.dds"
				"gfx/models/portraits/all/bald.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_01.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_10.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_01.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_02.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_03.dds"
				"gfx/models/portraits/all/bald.dds"
			}
		}
		random = {
	        trigger = { uses_borg_prosthetics = yes } 
	        list = { 
				"gfx/models/portraits/borg/borg_male_prosthetic_01.dds"
				"gfx/models/portraits/borg/borg_male_prosthetic_02.dds"
				"gfx/models/portraits/borg/borg_male_prosthetic_03.dds"
			}
        }		
	}
  
	#leader scope 
	ruler = { #for rulers
		default = "gfx/models/portraits/all/bald.dds"
		random = {
			trigger = { uses_borg_prosthetics = no }		
			list = {
				"gfx/models/portraits/all/bald.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_01.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_02.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_01.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_10.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_03.dds"
				"gfx/models/portraits/all/bald.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_orange_style_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_01.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_02.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_03.dds"
				"gfx/models/portraits/all/human_male_hair_dark_black_style_10.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_01.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_02.dds"
				"gfx/models/portraits/orion/orion_male_head_attach_03.dds"
				"gfx/models/portraits/all/bald.dds"
			}
		}
		random = {
	        trigger = { uses_borg_prosthetics = yes } 
			list = { 
				"gfx/models/portraits/borg/borg_male_prosthetic_01.dds"
				"gfx/models/portraits/borg/borg_male_prosthetic_02.dds"
				"gfx/models/portraits/borg/borg_male_prosthetic_03.dds"
			}
        }		
	}
}
  

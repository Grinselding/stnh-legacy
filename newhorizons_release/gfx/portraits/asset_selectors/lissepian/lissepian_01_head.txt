# This is a template which multiple species can use. 
  
lissepian_head_01 = {
	default = "gfx/models/portraits/lissepian/lissepian_head_02.dds"
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/lissepian/lissepian_head_02.dds"
	}
	species = { #generic portrait for a species
		default = "gfx/models/portraits/lissepian/lissepian_head_02.dds"
		random = { 
	        trigger = { uses_borg_prosthetics = yes } 
	        list = { "gfx/models/portraits/borg/borg_lissepian_prosthetic_01.dds" }
        }
	}
	pop = { #for a specific pop
		default = "gfx/models/portraits/lissepian/lissepian_head_02.dds"
		random = {
			trigger = { uses_borg_prosthetics = no } 
			list = { "gfx/models/portraits/lissepian/lissepian_head_01.dds" "gfx/models/portraits/lissepian/lissepian_head_02.dds" }
		}
		random = { 
	        trigger = { uses_borg_prosthetics = yes } 
	        list = { "gfx/models/portraits/borg/borg_lissepian_prosthetic_01.dds" }
        }
	}
	leader = { #scientists, generals, admirals, governor
		default = "gfx/models/portraits/lissepian/lissepian_head_02.dds"
		random = {
			trigger = { uses_borg_prosthetics = no } 
			list = { "gfx/models/portraits/lissepian/lissepian_head_01.dds" "gfx/models/portraits/lissepian/lissepian_head_02.dds" }
		}
		random = { 
	        trigger = { uses_borg_prosthetics = yes } 
	        list = { "gfx/models/portraits/borg/borg_lissepian_prosthetic_01.dds" }
        }
	}
	ruler = { #for rulers
		default = "gfx/models/portraits/lissepian/lissepian_head_02.dds"
		random = {
			trigger = { uses_borg_prosthetics = no } 
			list = { "gfx/models/portraits/lissepian/lissepian_head_01.dds" "gfx/models/portraits/lissepian/lissepian_head_02.dds" }
		}
		random = { 
	        trigger = { uses_borg_prosthetics = yes } 
	        list = { "gfx/models/portraits/borg/borg_lissepian_prosthetic_01.dds" }
        }			
	}
}
  
lissepian_head_01b = {
	default = "gfx/models/portraits/lissepian/lissepian_head_02b.dds"
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/lissepian/lissepian_head_02b.dds"
	}
	species = { #generic portrait for a species
		default = "gfx/models/portraits/lissepian/lissepian_head_02b.dds"
		random = { 
	        trigger = { uses_borg_prosthetics = yes } 
	        list = { "gfx/models/portraits/borg/borg_lissepian_prosthetic_01b.dds" }
        }
	}
	pop = { #for a specific pop
		default = "gfx/models/portraits/lissepian/lissepian_head_02b.dds"
		random = {
			trigger = { uses_borg_prosthetics = no } 
			list = { "gfx/models/portraits/lissepian/lissepian_head_01b.dds" "gfx/models/portraits/lissepian/lissepian_head_02b.dds" }
		}
		random = { 
	        trigger = { uses_borg_prosthetics = yes } 
	        list = { "gfx/models/portraits/borg/borg_lissepian_prosthetic_01b.dds" }
        }	
	}
	leader = { #scientists, generals, admirals, governor
		default = "gfx/models/portraits/lissepian/lissepian_head_02b.dds"
		random = {
			trigger = { uses_borg_prosthetics = no } 
			list = { "gfx/models/portraits/lissepian/lissepian_head_01b.dds" "gfx/models/portraits/lissepian/lissepian_head_02b.dds" }
		}
		random = { 
	        trigger = { uses_borg_prosthetics = yes } 
	        list = { "gfx/models/portraits/borg/borg_lissepian_prosthetic_01b.dds" }
        }	
	}
	ruler = { #for rulers
		default = "gfx/models/portraits/lissepian/lissepian_head_02b.dds"
		random = {
			trigger = { uses_borg_prosthetics = no } 
			list = { "gfx/models/portraits/lissepian/lissepian_head_01b.dds" "gfx/models/portraits/lissepian/lissepian_head_02b.dds" }
		}
		random = { 
	        trigger = { uses_borg_prosthetics = yes } 
	        list = { "gfx/models/portraits/borg/borg_lissepian_prosthetic_01b.dds" }
        }		
	}
}

lissepian_head_01c = {
	default = "gfx/models/portraits/lissepian/lissepian_head_02c.dds"
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/lissepian/lissepian_head_02c.dds"
	}
	species = { #generic portrait for a species
		default = "gfx/models/portraits/lissepian/lissepian_head_02c.dds"
		random = { 
	        trigger = { uses_borg_prosthetics = yes } 
	        list = { "gfx/models/portraits/borg/borg_lissepian_prosthetic_01c.dds" }
        }
	}
	pop = { #for a specific pop
		default = "gfx/models/portraits/lissepian/lissepian_head_02c.dds"
		random = {
			trigger = { uses_borg_prosthetics = no } 
			list = { "gfx/models/portraits/lissepian/lissepian_head_01c.dds" "gfx/models/portraits/lissepian/lissepian_head_02c.dds" }
		}
		random = { 
	        trigger = { uses_borg_prosthetics = yes } 
	        list = { "gfx/models/portraits/borg/borg_lissepian_prosthetic_01c.dds" }
        }
	}
	leader = { #scientists, generals, admirals, governor
		default = "gfx/models/portraits/lissepian/lissepian_head_02c.dds"
		random = {
			trigger = { uses_borg_prosthetics = no } 
			list = { "gfx/models/portraits/lissepian/lissepian_head_01c.dds" "gfx/models/portraits/lissepian/lissepian_head_02c.dds" }
		}
		random = { 
	        trigger = { uses_borg_prosthetics = yes } 
	       list = { "gfx/models/portraits/borg/borg_lissepian_prosthetic_01c.dds" }
        }
	}
	ruler = { #for rulers
		default = "gfx/models/portraits/lissepian/lissepian_head_02c.dds"
		random = {
			trigger = { uses_borg_prosthetics = no } 
			list = { "gfx/models/portraits/lissepian/lissepian_head_01c.dds" "gfx/models/portraits/lissepian/lissepian_head_02c.dds" }
		}
		random = { 
	        trigger = { uses_borg_prosthetics = yes } 
	        list = { "gfx/models/portraits/borg/borg_lissepian_prosthetic_01c.dds" }
        }		
	}
}
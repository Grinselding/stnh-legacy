human_female_clothes_starfleet_combined = { 
	default = "gfx/models/portraits/human_civilian/civ_human_female_clothes_01.dds"

	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist 
		default = "gfx/models/portraits/starfleet_enterprise/ent_human_female_command.dds" 
	} 
   
	#species scope 
	species = { #generic portrait for a species 
		default = "gfx/models/portraits/human_civilian/civ_human_female_clothes_01.dds"
		random = {  
			trigger = { uses_borg_full_prosthetics = yes }  
			list = { "gfx/models/portraits/borg/borg_female_clothes_01.dds" }
		}
	}
   
	#pop scope 
	pop = { #for a specific pop
		random = { 
			trigger = { has_pop_flag = cowboy_pop } 
			list = { "gfx/models/portraits/all/humanoid_female_cowboy_clothes_01.dds" "gfx/models/portraits/all/humanoid_female_cowboy_clothes_02.dds" "gfx/models/portraits/all/humanoid_female_cowboy_clothes_03.dds" }
		}
		"gfx/models/portraits/starfleet_enterprise/ent_human_female_security.dds" = { has_state_security_job = yes uses_starfleet_uniform_pop = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet franklin/fra_human_female_security.dds" = { has_state_security_job = yes uses_starfleet_uniform_pop = yes franklin_era = yes }
		"gfx/models/portraits/starfleet kelvin/kel_human_female_security.dds" = { has_state_security_job = yes uses_starfleet_uniform_pop = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet_discovery/dis_human_female_security.dds" = { has_state_security_job = yes uses_starfleet_uniform_pop = yes discovery_era = yes }
		"gfx/models/portraits/starfleet_original_series/tos_human_female_security.dds" = { has_state_security_job = yes uses_starfleet_uniform_pop = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_female_security.dds" = { has_state_security_job = yes uses_starfleet_uniform_pop = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet next generation 02/tng2_human_female_security.dds" = { has_state_security_job = yes uses_starfleet_uniform_pop = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet_first_contact/fco_human_female_security.dds" = { has_state_security_job = yes uses_starfleet_uniform_pop = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_2380/2380_human_female_security.dds" = { has_state_security_job = yes uses_starfleet_uniform_pop = yes children_of_mars_era = yes }
		"gfx/models/portraits/starfleet_picard/pic_human_female_security.dds" = { has_state_security_job = yes uses_starfleet_uniform_pop = yes picard_era = yes }
		"gfx/models/portraits/starfleet_all_good_things/agt_human_female_security.dds" = { has_state_security_job = yes uses_starfleet_uniform_pop = yes all_good_things_era = yes }
		random = { 
			trigger = { uses_native_clothes_pop = yes } 
			list = { "gfx/models/portraits/human_civilian/civ_human_female_clothes_01.dds" "gfx/models/portraits/human_civilian/civ_human_female_clothes_02.dds" "gfx/models/portraits/human_civilian/civ_human_female_clothes_03.dds" "gfx/models/portraits/human_civilian/civ_human_female_clothes_04.dds" "gfx/models/portraits/human_civilian/civ_human_female_clothes_05.dds" "gfx/models/portraits/human_civilian/civ_human_female_clothes_06.dds" }
		}		
		random = {  
			trigger = { uses_borg_full_prosthetics = yes }  
			list = { "gfx/models/portraits/borg/borg_female_clothes_01.dds" }
		}
	} 
     
	#leader scope   
	leader = { #scientists, generals, admirals, governor envoy
		
		
		#Starfleet
		"gfx/models/portraits/starfleet_enterprise/ent_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet_enterprise/ent_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet_enterprise/ent_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_starfleet_uniform = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet_enterprise/ent_human_female_admiral.dds" = { leader_class = governor uses_starfleet_uniform = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet franklin/fra_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes franklin_era = yes }
		"gfx/models/portraits/starfleet franklin/fra_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_starfleet_uniform = yes franklin_era = yes }
		"gfx/models/portraits/starfleet franklin/fra_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes franklin_era = yes }
		"gfx/models/portraits/starfleet franklin/fra_human_female_admiral.dds" = { leader_class = governor uses_starfleet_uniform = yes franklin_era = yes }
		"gfx/models/portraits/starfleet kelvin/kel_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet kelvin/kel_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet kelvin/kel_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet kelvin/kel_human_female_admiral.dds" = { leader_class = governor uses_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet kelvin/kel_human_female_admiral.dds" = { leader_class = envoy uses_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet_discovery/dis_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes is_medical_leader = no discovery_era = yes }
		"gfx/models/portraits/starfleet_discovery/dis_human_female_medical.dds" = { leader_class = scientist uses_starfleet_uniform = yes is_medical_leader = yes discovery_era = yes }
		"gfx/models/portraits/starfleet_discovery/dis_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_starfleet_uniform = yes discovery_era = yes }
		"gfx/models/portraits/starfleet_discovery/dis_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes discovery_era = yes }
		"gfx/models/portraits/starfleet_discovery/dis_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes discovery_era = yes }		
		"gfx/models/portraits/starfleet_original_series/tos_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_original_series/tos_human_female_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_original_series/tos_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_original_series/tos_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_original_series/tos_human_female_command.dds" = { leader_class = envoy uses_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_starfleet_uniform = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet next generation 02/tng2_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet next generation 02/tng2_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_starfleet_uniform = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet next generation 02/tng2_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet next generation 02/tng2_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet_first_contact/fco_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_first_contact/fco_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_starfleet_uniform = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_first_contact/fco_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_first_contact/fco_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes first_contact_era = yes }  
		"gfx/models/portraits/starfleet_2380/2380_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes children_of_mars_era = yes }
		"gfx/models/portraits/starfleet_2380/2380_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_starfleet_uniform = yes children_of_mars_era = yes }
		"gfx/models/portraits/starfleet_2380/2380_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes children_of_mars_era = yes }
		"gfx/models/portraits/starfleet_2380/2380_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes children_of_mars_era = yes } 	
		"gfx/models/portraits/starfleet_picard/pic_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes picard_era = yes }
		"gfx/models/portraits/starfleet_picard/pic_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_starfleet_uniform = yes picard_era = yes }
		"gfx/models/portraits/starfleet_picard/pic_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes picard_era = yes }
		"gfx/models/portraits/starfleet_picard/pic_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes picard_era = yes } 		
		"gfx/models/portraits/starfleet_all_good_things/agt_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes all_good_things_era = yes }
		"gfx/models/portraits/starfleet_all_good_things/agt_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_starfleet_uniform = yes all_good_things_era = yes }
		"gfx/models/portraits/starfleet_all_good_things/agt_human_female_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes all_good_things_era = yes }
		"gfx/models/portraits/starfleet_all_good_things/agt_human_female_command.dds" = { leader_class = governor uses_starfleet_uniform = yes all_good_things_era = yes } 

		#Mirror Starfleet
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_mirror_starfleet_uniform = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_mirror_starfleet_uniform = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_command.dds" = { is_hero_or_admiral = yes uses_mirror_starfleet_uniform = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_admiral.dds" = { leader_class = governor uses_mirror_starfleet_uniform = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_mirror_starfleet_uniform = yes franklin_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_mirror_starfleet_uniform = yes franklin_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_command.dds" = { is_hero_or_admiral = yes uses_mirror_starfleet_uniform = yes franklin_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_admiral.dds" = { leader_class = governor uses_mirror_starfleet_uniform = yes franklin_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_mirror_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_mirror_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_command.dds" = { is_hero_or_admiral = yes uses_mirror_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_admiral.dds" = { leader_class = governor uses_mirror_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_mirror_starfleet_uniform = yes discovery_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_mirror_starfleet_uniform = yes discovery_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_command.dds" = { is_hero_or_admiral = yes uses_mirror_starfleet_uniform = yes discovery_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_admiral.dds" = { leader_class = governor uses_mirror_starfleet_uniform = yes discovery_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_mirror_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_mirror_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_command.dds" = { is_hero_or_admiral = yes uses_mirror_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_admiral.dds" = { leader_class = governor uses_mirror_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_mirror_starfleet_uniform = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_mirror_starfleet_uniform = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_command.dds" = { is_hero_or_admiral = yes uses_mirror_starfleet_uniform = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_admiral.dds" = { leader_class = governor uses_mirror_starfleet_uniform = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet_next_generation_02_mirror/tng2_mirror_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_mirror_starfleet_uniform = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet_next_generation_02_mirror/tng2_mirror_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_mirror_starfleet_uniform = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet_next_generation_02_mirror/tng2_mirror_human_female_command.dds" = { is_hero_or_admiral = yes uses_mirror_starfleet_uniform = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet_next_generation_02_mirror/tng2_mirror_human_female_admiral.dds" = { leader_class = governor uses_mirror_starfleet_uniform = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet_first_contact_mirror/fco_mirror_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_mirror_starfleet_uniform = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_first_contact_mirror/fco_mirror_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_mirror_starfleet_uniform = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_first_contact_mirror/fco_mirror_human_female_command.dds" = { is_hero_or_admiral = yes uses_mirror_starfleet_uniform = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_first_contact_mirror/fco_mirror_human_female_command.dds" = { leader_class = governor uses_mirror_starfleet_uniform = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_all_good_things_mirror/agt_mirror_human_female_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_mirror_starfleet_uniform = yes all_good_things_era = yes }
		"gfx/models/portraits/starfleet_all_good_things_mirror/agt_mirror_human_female_security.dds" = { is_sec_ops = yes leader_class = general uses_mirror_starfleet_uniform = yes all_good_things_era = yes }
		"gfx/models/portraits/starfleet_all_good_things_mirror/agt_mirror_human_female_command.dds" = { is_hero_or_admiral = yes uses_mirror_starfleet_uniform = yes all_good_things_era = yes }
		"gfx/models/portraits/starfleet_all_good_things_mirror/agt_mirror_human_female_command.dds" = { leader_class = governor uses_mirror_starfleet_uniform = yes all_good_things_era = yes }
		
		random = {  
			trigger = { uses_borg_full_prosthetics = yes }  
			list = { "gfx/models/portraits/borg/borg_female_clothes_01.dds" }
		}
		
		"gfx/models/portraits/all/generic_female_uniform_01.dds" = { uses_starfleet_uniform = no }
	}   
 
	#leader scope  
	ruler = { #for rulers 
		#Agencies etc.
		"gfx/models/portraits/starfleet_enterprise/ent_human_female_admiral.dds" = { uses_starfleet_uniform_ruler = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet_enterprise/ent_human_female_security.dds" = { uses_starfleet_uniform_engineer = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet franklin/fra_human_female_admiral.dds" = { uses_starfleet_uniform_ruler = yes franklin_era = yes }
		"gfx/models/portraits/starfleet franklin/fra_human_female_security.dds" = { uses_starfleet_uniform_engineer = yes franklin_era = yes }
		"gfx/models/portraits/starfleet kelvin/kel_human_female_admiral.dds" = { uses_starfleet_uniform_ruler = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet kelvin/kel_human_female_security.dds" = { uses_starfleet_uniform_engineer  = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet_discovery/dis_human_female_command.dds" = { uses_starfleet_uniform_ruler = yes discovery_era = yes }	
		"gfx/models/portraits/starfleet_discovery/dis_human_female_security.dds" = { uses_starfleet_uniform_engineer = yes discovery_era = yes }	
		"gfx/models/portraits/starfleet_original_series/tos_human_female_command.dds" = { uses_starfleet_uniform_ruler = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_original_series/tos_human_female_security.dds" = { uses_starfleet_uniform_engineer = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_female_command.dds" = { uses_starfleet_uniform_ruler = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_female_security.dds" = { uses_starfleet_uniform_engineer = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet next generation 02/tng2_fehuman_male_command.dds" = { uses_starfleet_uniform_ruler = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet next generation 02/tng2_fehuman_male_security.dds" = { uses_starfleet_uniform_engineer = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet_first_contact/fco_human_female_command.dds" = { uses_starfleet_uniform_ruler = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_first_contact/fco_human_female_security.dds" = { uses_starfleet_uniform_engineer = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_2380/2380_human_female_command.dds" = { uses_starfleet_uniform_ruler = yes children_of_mars_era = yes }
		"gfx/models/portraits/starfleet_2380/2380_human_female_security.dds" = { uses_starfleet_uniform_engineer = yes children_of_mars_era = yes }
		"gfx/models/portraits/starfleet_picard/pic_human_female_command.dds" = { uses_starfleet_uniform_ruler = yes picard_era = yes }
		"gfx/models/portraits/starfleet_picard/pic_human_female_security.dds" = { uses_starfleet_uniform_engineer = yes picard_era = yes }
		"gfx/models/portraits/starfleet_all_good_things/agt_human_female_command.dds" = { uses_starfleet_uniform_ruler = yes all_good_things_era = yes }
		"gfx/models/portraits/starfleet_all_good_things/agt_human_female_security.dds" = { uses_starfleet_uniform_engineer = yes all_good_things_era = yes } 	
		
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_admiral.dds" = { uses_terran_uniform_ruler = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet_next_generation_02_mirror/tng2_mirror_human_female_admiral.dds" = { uses_terran_uniform_ruler = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet_first_contact_mirror/fco_mirror_human_female_command.dds" = { uses_terran_uniform_ruler = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_all_good_things_mirror/agt_mirror_human_female_command.dds" = { uses_terran_uniform_ruler = yes all_good_things_era = yes }
		
		#Civilian Leaders
		random = {
			trigger = { 
				uses_starfleet_uniform = yes
				OR = { enterprise_era = yes franklin_era = yes kelvin_era = yes original_series_era = yes }
			}
			list = {
				"gfx/models/portraits/human_civilian/human_president_female_1.dds"
				"gfx/models/portraits/human_civilian/human_president_female_2.dds"
				"gfx/models/portraits/human_civilian/human_president_female_3.dds"
				"gfx/models/portraits/human_civilian/human_president_female_4.dds"
			}
		}
		random = {
			trigger = { 
				uses_starfleet_uniform = yes
				is_ufp = yes
				OR = { wrath_of_khan_era = yes next_generation_era = yes first_contact_era = yes }
			}
			list = { "gfx/models/portraits/human_civilian/federation_president_female_1.dds" }
		}
		random = {
			trigger = { 
				uses_starfleet_uniform = yes
				is_ufp = no
				OR = { wrath_of_khan_era = yes next_generation_era = yes first_contact_era = yes }
			}
			list = {
				"gfx/models/portraits/human_civilian/human_president_female_1.dds"
				"gfx/models/portraits/human_civilian/human_president_female_2.dds"
				"gfx/models/portraits/human_civilian/human_president_female_3.dds"
				"gfx/models/portraits/human_civilian/human_president_female_4.dds"
			}
		}

		#Mirror Starfleet
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_admiral.dds" = { uses_mirror_starfleet_uniform = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_admiral.dds" = { uses_mirror_starfleet_uniform = yes franklin_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_admiral.dds" = { uses_mirror_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_admiral.dds" = { uses_mirror_starfleet_uniform = yes discovery_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_admiral.dds" = { uses_mirror_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_enterprise_mirror/ent_mirror_human_female_admiral.dds" = { uses_mirror_starfleet_uniform = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet_next_generation_02_mirror/tng2_mirror_human_female_admiral.dds" = { uses_mirror_starfleet_uniform = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet_first_contact_mirror/fco_mirror_human_female_command.dds" = { uses_mirror_starfleet_uniform = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_all_good_things_mirror/agt_mirror_human_female_command.dds" = { uses_mirror_starfleet_uniform = yes all_good_things_era = yes }
		
		random = {  
			trigger = { uses_borg_full_prosthetics = yes }  
			list = { "gfx/models/portraits/borg/borg_female_clothes_01.dds" }
		}
		
		"gfx/models/portraits/all/generic_female_uniform_01.dds" = { uses_starfleet_uniform = no }
	}
} 

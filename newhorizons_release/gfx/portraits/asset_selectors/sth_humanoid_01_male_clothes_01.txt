# This is a template which multiple species can use. 

sth_humanoid_01_male_clothes_01 = {
	default = "gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_01.dds"
	
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_01.dds"
	}
	
	#species scope
	species = { #generic portrait for a species
		default = "gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_01.dds"
		random = { 
			trigger = { uses_borg_full_prosthetics = yes }
			list = {
				"gfx/models/portraits/borg/borg_male_clothes_01.dds"
			} 
		}
	}
	
	#pop scope
	pop = { #for a specific pop
		default = "gfx/models/portraits/sth humanoid 04/sth_humanoid_04_male_clothes_05.dds"
		random = {
			trigger = { uses_borg_full_prosthetics = no }
			list = {
				"gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_01.dds"
				"gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_02.dds"
				"gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_03.dds"
				"gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_04.dds"
			}
		}
		random = { 
			trigger = { uses_borg_full_prosthetics = yes }
			list = {
				"gfx/models/portraits/borg/borg_male_clothes_01.dds"
			} 
		}
	}
	
	#leader scope
	leader = { #scientists, generals, admirals, governor
		#bajoran
		"gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_01.dds" = { leader_class = admiral uses_starfleet_uniform = no }
		"gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_02.dds" = { leader_class = scientist NOT = { owner = { has_country_flag = united_federation_of_planets } } }
		"gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_03.dds" = { leader_class = general NOT = { owner = { has_country_flag = united_federation_of_planets } } }
		"gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_04.dds" = { leader_class = governor NOT = { owner = { has_country_flag = united_federation_of_planets } } }	
		#Federation
		"gfx/models/portraits/starfleet_enterprise/ent_human_male_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet_enterprise/ent_human_male_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet_enterprise/ent_human_male_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet_enterprise/ent_human_male_admiral.dds" = { leader_class = governor uses_starfleet_uniform = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet franklin/fra_human_male_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes franklin_era = yes }
		"gfx/models/portraits/starfleet franklin/fra_human_male_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes franklin_era = yes }
		"gfx/models/portraits/starfleet franklin/fra_human_male_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes franklin_era = yes }
		"gfx/models/portraits/starfleet franklin/fra_human_male_admiral.dds" = { leader_class = governor uses_starfleet_uniform = yes franklin_era = yes }
		"gfx/models/portraits/starfleet kelvin/kel_human_male_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet kelvin/kel_human_male_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet kelvin/kel_human_male_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet kelvin/kel_human_male_admiral.dds" = { leader_class = governor uses_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet_original_series/tos_human_male_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_original_series/tos_human_male_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_original_series/tos_human_male_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_original_series/tos_human_male_command.dds" = { leader_class = governor uses_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_male_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_male_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_male_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_male_command.dds" = { leader_class = governor uses_starfleet_uniform = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet next generation 02/tng2_human_male_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet next generation 02/tng2_human_male_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet next generation 02/tng2_human_male_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet next generation 02/tng2_human_male_command.dds" = { leader_class = governor uses_starfleet_uniform = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet_first_contact/fco_human_male_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_first_contact/fco_human_male_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_first_contact/fco_human_male_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_first_contact/fco_human_male_command.dds" = { leader_class = governor uses_starfleet_uniform = yes first_contact_era = yes }
#Children of Mars		
		"gfx/models/portraits/starfleet_2380/2380_human_male_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes children_of_mars_era = yes }
		"gfx/models/portraits/starfleet_2380/2380_human_male_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes children_of_mars_era = yes }
		"gfx/models/portraits/starfleet_2380/2380_human_male_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes children_of_mars_era = yes }
		"gfx/models/portraits/starfleet_2380/2380_human_male_command.dds" = { leader_class = governor uses_starfleet_uniform = yes children_of_mars_era = yes } 
#Picard
		"gfx/models/portraits/starfleet_picard/pic_human_male_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes picard_era = yes }
		"gfx/models/portraits/starfleet_picard/pic_human_male_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes picard_era = yes }
		"gfx/models/portraits/starfleet_picard/pic_human_male_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes picard_era = yes }
		"gfx/models/portraits/starfleet_picard/pic_human_male_command.dds" = { leader_class = governor uses_starfleet_uniform = yes picard_era = yes } 
#All Good Things
		"gfx/models/portraits/starfleet_all_good_things/agt_human_male_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes all_good_things_era = yes }
		"gfx/models/portraits/starfleet_all_good_things/agt_human_male_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes all_good_things_era = yes }
		"gfx/models/portraits/starfleet_all_good_things/agt_human_male_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes all_good_things_era = yes }
		"gfx/models/portraits/starfleet_all_good_things/agt_human_male_command.dds" = { leader_class = governor uses_starfleet_uniform = yes all_good_things_era = yes }
	}

	#leader scope 
	ruler = { #for rulers
		#coridian
		"gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_01.dds" = { uses_starfleet_uniform = no }
		"gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_02.dds" = { uses_starfleet_uniform = no }
		"gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_03.dds" = { uses_starfleet_uniform = no }
		"gfx/models/portraits/bajoran civilian/civ_bajoran_male_clothes_04.dds" = { uses_starfleet_uniform = no }
		#Federation
		"gfx/models/portraits/starfleet_enterprise/ent_human_male_admiral.dds" = { uses_starfleet_uniform = yes enterprise_era = yes }
		"gfx/models/portraits/starfleet franklin/fra_human_male_admiral.dds" = { uses_starfleet_uniform = yes franklin_era = yes }
		"gfx/models/portraits/starfleet kelvin/kel_human_male_admiral.dds" = { uses_starfleet_uniform = yes kelvin_era = yes }
		"gfx/models/portraits/starfleet_original_series/tos_human_male_command.dds" = { uses_starfleet_uniform = yes original_series_era = yes }
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_male_command.dds" = { uses_starfleet_uniform = yes wrath_of_khan_era = yes }
		"gfx/models/portraits/starfleet next generation 02/tng2_human_male_command.dds" = { uses_starfleet_uniform = yes next_generation_era = yes }
		"gfx/models/portraits/starfleet_first_contact/fco_human_male_command.dds" = { uses_starfleet_uniform = yes first_contact_era = yes }
		"gfx/models/portraits/starfleet_2380/2380_human_male_command.dds" = { uses_starfleet_uniform = yes children_of_mars_era = yes }
		"gfx/models/portraits/starfleet_picard/pic_human_male_command.dds" = { uses_starfleet_uniform = yes picard_era = yes }
		"gfx/models/portraits/starfleet_all_good_things/agt_human_male_command.dds" = { uses_starfleet_uniform = yes all_good_things_era = yes }
	}
}

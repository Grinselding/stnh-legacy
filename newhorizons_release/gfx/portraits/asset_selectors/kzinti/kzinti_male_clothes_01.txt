# This is a template which multiple species can use. 

kzinti_male_clothes_01 = {
	default = "gfx/models/portraits/kzinti/kzinti_male_clothes_01.dds"
	
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/kzinti/kzinti_male_clothes_01.dds"
	}
	
	#species scope
	species = { #generic portrait for a species
		default = "gfx/models/portraits/kzinti/kzinti_male_clothes_02.dds"
		random = { 
	        trigger = { uses_borg_full_prosthetics = yes } 
	        list = { "gfx/models/portraits/borg/borg_male_clothes_01.dds" }
        }
	}
	
	#pop scope
	pop = { #for a specific pop
		default = "gfx/models/portraits/caitian/caitian_male_clothes_01.dds"
		random = {
			trigger = { uses_borg_full_prosthetics = no } 
			list = {
				"gfx/models/portraits/caitian/caitian_male_clothes_01.dds"
				"gfx/models/portraits/caitian/caitian_male_clothes_02.dds"
				"gfx/models/portraits/caitian/caitian_male_clothes_03.dds"
				"gfx/models/portraits/caitian/caitian_male_clothes_04.dds"
				"gfx/models/portraits/kzinti/kzinti_male_clothes_01.dds"
				"gfx/models/portraits/kzinti/kzinti_male_clothes_02.dds"
				"gfx/models/portraits/kzinti/kzinti_male_clothes_03.dds"
			}
		}
        random = { 
	        trigger = { uses_borg_full_prosthetics = yes } 
	        list = { "gfx/models/portraits/borg/borg_male_clothes_01.dds" }
        } 
	}
	#leader scope
	leader = { #scientists, generals, admirals, governor
		random = { 
			trigger = { leader_class = admiral uses_starfleet_uniform = no uses_borg_full_prosthetics = no NOT = { owner = { has_country_flag = united_federation_of_planets } } } 
			list = {
				"gfx/models/portraits/kzinti/kzinti_male_admiral_clothes.dds"
			} 
		}
		random = { 
			trigger = { leader_class = scientist uses_starfleet_uniform = no uses_borg_full_prosthetics = no NOT = { owner = { has_country_flag = united_federation_of_planets } } } 
			list = {
				"gfx/models/portraits/kzinti/kzinti_male_scientist_clothes.dds"
			} 
		}
		random = { 
			trigger = { leader_class = general uses_starfleet_uniform = no uses_borg_full_prosthetics = no NOT = { owner = { has_country_flag = united_federation_of_planets } } } 
			list = {
				"gfx/models/portraits/kzinti/kzinti_male_general_clothes.dds"
			} 
		}
		random = { 
			trigger = { leader_class = governor uses_starfleet_uniform = no uses_borg_full_prosthetics = no NOT = { owner = { has_country_flag = united_federation_of_planets } } } 
			list = {
				"gfx/models/portraits/kzinti/kzinti_male_governor_clothes.dds"
			} 
		}
		"gfx/models/portraits/starfleet_enterprise/ent_human_male_command.dds" = { 
			leader_class = admiral 
			AND = {
				owner = { has_country_flag = united_federation_of_planets }
				NOT = { years_passed > 14 }
			}
		}
		"gfx/models/portraits/starfleet_enterprise/ent_human_male_science.dds" = { 
			leader_class = scientist 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				NOT = {
					years_passed > 14
				}
			}
		}
		"gfx/models/portraits/starfleet_enterprise/ent_human_male_security.dds" = { 
			is_sec_ops = yes 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				NOT = {
					years_passed > 14
				}
			}
		}
		"gfx/models/portraits/starfleet_enterprise/ent_human_male_admiral.dds" = { 
			leader_class = governor 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				NOT = {
					years_passed > 14
				}
			}
		}
		"gfx/models/portraits/starfleet franklin/fra_human_male_science.dds" = { 
			leader_class = scientist 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 14
				years_passed < 50
			}
		}
		"gfx/models/portraits/starfleet franklin/fra_human_male_security.dds" = { 
			is_sec_ops = yes 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 14
				years_passed < 50
			}
		}
		"gfx/models/portraits/starfleet franklin/fra_human_male_command.dds" = { 
			leader_class = admiral 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 14
				years_passed < 50
			}
		}
		"gfx/models/portraits/starfleet franklin/fra_human_male_admiral.dds" = { 
			leader_class = governor 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 14
				years_passed < 50
			}
		}
		"gfx/models/portraits/starfleet kelvin/kel_human_male_science.dds" = { 
			leader_class = scientist 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 49
				years_passed < 100
			}
		}
		"gfx/models/portraits/starfleet kelvin/kel_human_male_security.dds" = { 
			is_sec_ops = yes 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 49
				years_passed < 100
			}
		}
		"gfx/models/portraits/starfleet kelvin/kel_human_male_command.dds" = { 
			leader_class = admiral 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 49
				years_passed < 100
			}
		}
		"gfx/models/portraits/starfleet kelvin/kel_human_male_admiral.dds" = { 
			leader_class = governor 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 49
				years_passed < 100
			}
		}	
		"gfx/models/portraits/starfleet_original_series/tos_human_male_science.dds" = { 
			leader_class = scientist 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 99
				years_passed < 120
			}
		}
		"gfx/models/portraits/starfleet_original_series/tos_human_male_security.dds" = { 
			is_sec_ops = yes 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 99
				years_passed < 120
			}
		}
		"gfx/models/portraits/starfleet_original_series/tos_human_male_command.dds" = { 
			leader_class = admiral 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 99
				years_passed < 120
			}
		}
		"gfx/models/portraits/starfleet_original_series/tos_human_male_command.dds" = { 
			leader_class = governor 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 99
				years_passed < 120
			}
		}
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_male_science.dds" = { 
			leader_class = scientist 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 119
				years_passed < 200
			}
		}
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_male_security.dds" = { 
			is_sec_ops = yes 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 119
				years_passed < 200
			}
		}
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_male_command.dds" = { 
			leader_class = admiral 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 119
				years_passed < 200
			}
		}
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_male_command.dds" = { 
			leader_class = governor 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 119
				years_passed < 200
			}
		}
		"gfx/models/portraits/starfleet next generation 02/tng2_human_male_science.dds" = { 
			leader_class = scientist 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 199
				years_passed < 216
			}
		}
		"gfx/models/portraits/starfleet next generation 02/tng2_human_male_security.dds" = { 
			is_sec_ops = yes 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 199
				years_passed < 216
			}
		}
		"gfx/models/portraits/starfleet next generation 02/tng2_human_male_command.dds" = { 
			leader_class = admiral 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 199
				years_passed < 216
			}
		}
		"gfx/models/portraits/starfleet next generation 02/tng2_human_male_command.dds" = { 
			leader_class = governor 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 199
				years_passed < 216
			}
		}	
		"gfx/models/portraits/starfleet_first_contact/fco_human_male_science.dds" = { 
			leader_class = scientist 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				first_contact_era = yes
			}
		}
		"gfx/models/portraits/starfleet_first_contact/fco_human_male_security.dds" = { 
			is_sec_ops = yes 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				first_contact_era = yes
			}
		}
		"gfx/models/portraits/starfleet_first_contact/fco_human_male_command.dds" = { 
			leader_class = admiral 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				first_contact_era = yes
			}
		}
		"gfx/models/portraits/starfleet_first_contact/fco_human_male_command.dds" = { 
			leader_class = governor 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				first_contact_era = yes
			}
		}
		"gfx/models/portraits/starfleet_2380/2380_human_male_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes children_of_mars_era = yes }
		"gfx/models/portraits/starfleet_2380/2380_human_male_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes children_of_mars_era = yes }
		"gfx/models/portraits/starfleet_2380/2380_human_male_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes children_of_mars_era = yes }
		"gfx/models/portraits/starfleet_2380/2380_human_male_command.dds" = { leader_class = governor uses_starfleet_uniform = yes children_of_mars_era = yes }
		
		"gfx/models/portraits/starfleet_picard/pic_human_male_science.dds" = { leader_class = scientist is_hero_or_admiral = no uses_starfleet_uniform = yes picard_era = yes }
		"gfx/models/portraits/starfleet_picard/pic_human_male_security.dds" = { is_sec_ops = yes uses_starfleet_uniform = yes picard_era = yes }
		"gfx/models/portraits/starfleet_picard/pic_human_male_command.dds" = { is_hero_or_admiral = yes uses_starfleet_uniform = yes picard_era = yes }
		"gfx/models/portraits/starfleet_picard/pic_human_male_command.dds" = { leader_class = governor uses_starfleet_uniform = yes picard_era= yes } 		
			
		random = { 
	        trigger = { uses_borg_full_prosthetics = yes } 
	        list = {
		        "gfx/models/portraits/borg/borg_male_clothes_01.dds"
	        }
        }
	}

	
	#leader scope 
	ruler = { #for rulers
		#default = "gfx/models/portraits/caitian/caitian_male_clothes_04.dds"
		"gfx/models/portraits/caitian/caitian_male_clothes_04.dds" = {
			NOT = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
			}
		}
		"gfx/models/portraits/starfleet_enterprise/ent_human_male_admiral.dds" = { 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				NOT = {
					years_passed > 14
				}
			}
		}
		"gfx/models/portraits/starfleet franklin/fra_human_male_admiral.dds" = { 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 14
				years_passed < 50
			}
		}
		"gfx/models/portraits/starfleet kelvin/kel_human_male_admiral.dds" = { 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 49
				years_passed < 100
			}
		}
		"gfx/models/portraits/starfleet_original_series/tos_human_male_command.dds" = { 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 99
				years_passed < 120
			}
		}
		"gfx/models/portraits/starfleet_wrath_of_khan/wok_human_male_command.dds" = { 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 119
				years_passed < 200
			}
		}
		"gfx/models/portraits/starfleet next generation 02/tng2_human_male_command.dds" = { 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				years_passed > 199
				years_passed < 216
			}
		}
		"gfx/models/portraits/starfleet_first_contact/fco_human_male_command.dds" = { 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				first_contact_era = yes
			}
		}
		"gfx/models/portraits/starfleet_2380/2380_human_male_command.dds" = { 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				children_of_mars_era = yes
			}
		}
		"gfx/models/portraits/starfleet_picard/pic_human_male_command.dds" = { 
			AND = {
				owner = {  
					has_country_flag = united_federation_of_planets
				}
				picard_era = yes
			}
		}
		random = { 
	        trigger = { uses_borg_full_prosthetics = yes } 
	        list = {
		        "gfx/models/portraits/borg/borg_male_clothes_01.dds"
	        }
        }			
	}
}


#Each non-alloy resource $recorcename should have a trigger of the form $s_resourcename_i
#AI will try to balance resources so it will increase production if $s_resourcename_2 = false
#and decrease if $resourcename_2 = false.
#triggers 0,1,3,4 are needed to make sure AI doesn't fall into loops with increasing-decreasing
#and to make sure that ai will make more drastic measures if production is far from level 2
#With resources you are supposed to keep around 0, I suggest 0 to be the level of $_1 trigger
#and difference between levels be slightly larger than grand admiral resource difference
#between having respective production building and not having it.

s_food_0 = {
    resource_income_compare = {
        resource = food
        value > -5
    }
    resource_stockpile_compare = {
        resource = food
        value > 200
    }
}
s_food_1 = {
    resource_income_compare = {
        resource = food
        value > 20
    }
    resource_stockpile_compare = {
        resource = food
        value > 2500
    }
}
s_food_2 = {
    resource_income_compare = {
        resource = food
        value > 60
    }
    resource_stockpile_compare = {
        resource = food
        value > 3000
    }
}
s_food_3 = {
    resource_income_compare = {
        resource = food
        value > 100
    }
    resource_stockpile_compare = {
        resource = food
        value > 4000
    }
}
s_food_4 = {
    resource_income_compare = {
        resource = food
        value > 300
    }
    resource_stockpile_compare = {
        resource = food
        value > 5000
    }
}


s_energy_0 = {
    resource_income_compare = {
        resource = energy
        value > 0
    }
}
s_energy_1 = {
    resource_income_compare = {
        resource = energy
        value > 30
    }
}
s_energy_2 = {
    resource_income_compare = {
        resource = energy
        value > 60
    }
}
s_energy_3 = {
    resource_income_compare = {
        resource = energy
        value > 150
    }
}
s_energy_4 = {
    resource_income_compare = {
        resource = energy
        value > 300
    }
}



s_minerals_0 = {
    if = {
        limit = {
            num_owned_planets < 3
        }
    resource_income_compare = {
        resource = minerals
        value > 20
       }
    }
    else_if = {
        limit = { num_owned_planets < 7 }
    resource_income_compare = {
        resource = minerals
        value > 50
       }
    }
    else_if = {
        limit = { num_owned_planets < 15 }
    resource_income_compare = {
        resource = minerals
        value > 100
       }
    }
    else = {
    resource_income_compare = {
        resource = minerals
        value > 200
       }
    }
}

s_minerals_1 = {
    if = {
        limit = {
            num_owned_planets < 3
        }
    resource_income_compare = {
        resource = minerals
        value > 40
       }
    }
    else_if = {
        limit = { num_owned_planets < 7 }
    resource_income_compare = {
        resource = minerals
        value > 100
       }
    }
    else_if = {
        limit = { num_owned_planets < 15 }
    resource_income_compare = {
        resource = minerals
        value > 200
       }
    }
    else = {
    resource_income_compare = {
        resource = minerals
        value > 400
       }
    }
}

s_minerals_2 = {
    if = {
        limit = {
            num_owned_planets < 3
        }
    resource_income_compare = {
        resource = minerals
        value > 100
       }
    }
    else_if = {
        limit = { num_owned_planets < 7 }
    resource_income_compare = {
        resource = minerals
        value > 200
       }
    }
    else_if = {
        limit = { num_owned_planets < 15 }
    resource_income_compare = {
        resource = minerals
        value > 300
       }
    }
    else = {
    resource_income_compare = {
        resource = minerals
        value > 400
       }
    }
}

s_minerals_3 = {
    if = {
        limit = {
            num_owned_planets < 3
        }
    resource_income_compare = {
        resource = minerals
        value > 200
       }
    }
    else_if = {
        limit = { num_owned_planets < 7 }
    resource_income_compare = {
        resource = minerals
        value > 400
       }
    }
    else_if = {
        limit = { num_owned_planets < 15 }
    resource_income_compare = {
        resource = minerals
        value > 800
       }
    }
    else = {
    resource_income_compare = {
        resource = minerals
        value > 1600
       }
    }
}

s_minerals_4 = {
    if = {
        limit = {
            num_owned_planets < 3
        }
    resource_income_compare = {
        resource = minerals
        value > 400
       }
    }
    else_if = {
        limit = { num_owned_planets < 7 }
    resource_income_compare = {
        resource = minerals
        value > 800
       }
    }
    else_if = {
        limit = { num_owned_planets < 15 }
    resource_income_compare = {
        resource = minerals
        value > 1500
       }
    }
    else = {
    resource_income_compare = {
        resource = minerals
        value > 3000
       }
    }
}

s_consumer_goods_1 = {
    resource_income_compare = {
        resource = consumer_goods
        value > 0
        
    }
}
s_consumer_goods_2 = {
    resource_income_compare = {
        resource = consumer_goods
        value > 10
        
    }
}
s_consumer_goods_3 = {
    resource_income_compare = {
        resource = consumer_goods
        value > 30
        
    }
}


s_nanites_0 = {
    resource_income_compare = {
        resource = nanites
        value > -5
    }
}
s_nanites_1 = {
    resource_income_compare = {
        resource = nanites
        value > 0
    }
}
s_nanites_2 = {
    resource_income_compare = {
        resource = nanites
        value > 10
    }
}
s_nanites_3 = {
    resource_income_compare = {
        resource = nanites
        value > 30
    }
}
s_nanites_4 = {
    resource_income_compare = {
        resource = nanites
        value > 40
    }
}


s_sr_cpm_0 = {
    resource_income_compare = {
        resource = sr_cpm
        value > -5
    }
}
s_sr_cpm_1 = {
    resource_income_compare = {
        resource = sr_cpm
        value > 0
    }
}
s_sr_cpm_2 = {
    resource_income_compare = {
        resource = sr_cpm
        value > 10
    }
}
s_sr_cpm_3 = {
    resource_income_compare = {
        resource = sr_cpm
        value > 30
    }
}
s_sr_cpm_4 = {
    resource_income_compare = {
        resource = sr_cpm
        value > 50
    }
}


s_sr_dilithium_raw_0 = {
    nand = {
    resource_income_compare = {
        resource = sr_dilithium_raw
        value < 0
    }
    resource_stockpile_compare = {
        resource = sr_dilithium_raw
        value < 100
    }
    }
}

s_sr_dilithium_raw_1 = {
    nand = {
    resource_income_compare = {
        resource = sr_dilithium_raw
        value < 5
    }
    resource_stockpile_compare = {
        resource = sr_dilithium_raw
        value < 500
    }
    }
}

s_sr_dilithium_raw_2 = {
    resource_income_compare = {
        resource = sr_dilithium_raw
        value > 16
    }
    resource_stockpile_compare = {
        resource = sr_dilithium_raw
        value > 500
    }
}
s_sr_dilithium_raw_3 = {
    resource_income_compare = {
        resource = sr_dilithium_raw
        value > 30
    }
    resource_stockpile_compare = {
        resource = sr_dilithium_raw
        value > 1000
    }
}




s_sr_dilithium_processed_2 = {
    or = {
    and = {
    resource_stockpile_compare = {
        resource = sr_dilithium_processed
        value > 1000
    }
    resource_income_compare = {
        resource = sr_dilithium_processed
        value > 20
    } }
    and = {
    resource_stockpile_compare = {
        resource = sr_dilithium_processed
        value > 2000
    }
    resource_income_compare = {
        resource = sr_dilithium_processed
        value > 10
    } }
    }
}

s_sr_dilithium_processed_3 = {
    resource_stockpile_compare = {
        resource = sr_dilithium_processed
        value > 4000
    }
    resource_income_compare = {
        resource = sr_dilithium_processed
        value > 20
    }
}

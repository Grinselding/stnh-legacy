##########################################################################
# Empire edicts
##########################################################################

@Edict1Cost = 100
@Edict2Cost = 200
@Edict3Cost = 300

@EdictDuration = 3600
@ambitionDuration = 3600

#######################
######CAMPAIGNS########   
####################### 

# Healthcare Reforms
edict_healthcare_campaign = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_healthcare_campaign }			
			}
			sr_cordrazine = 1000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_healthcare_campaign
			}
			influence = 100
		}
	}
      prerequisites = { "tech_society_new_worlds_1102" }
      modifier = {
         pop_growth_speed = 0.10
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
   
# Eductional Reforms
edict_education_campaign = {
   icon = "GFX_edict_type_policy"
       resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_education_campaign }			
			}
			influence = @Edict1Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_education_campaign
			}
			influence = 100
		}
	}
      prerequisites = { "tech_society_biology_1040" }
      modifier = {
         species_leader_exp_gain = 0.25
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# Ecological Campgain
edict_recycling_campaign = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_recycling_campaign }			
			}
			influence = @Edict3Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_recycling_campaign
			}
			influence = 100
		}
	}
      prerequisites = { "tech_society_new_worlds_1122" }
      modifier = {
         planet_pops_consumer_goods_upkeep_mult = -0.15
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
#Army Training
edict_war_drone_campaign = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_war_drone_campaign }			
			}
			influence = @Edict2Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_war_drone_campaign
			}
			influence = 100
		}
	}
      prerequisites = { "tech_society_combined_397" }
      modifier = {
         army_damage_mult = 0.15
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# Share the Burden
edict_share_burden_campaign = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_share_burden_campaign }			
			}
			influence = @Edict3Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_share_burden_campaign
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_that_487" }
    modifier = {
        planet_jobs_slave_minerals_produces_mult = 0.1
        planet_jobs_slave_food_produces_mult = 0.1
        pop_cat_slave_happiness = 0.5
    }
	length = @EdictDuration
    ai_weight = { weight = 1 }
}
   
# Forced Recruitment
edict_conscription_campaign = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_conscription_campaign }			
			}
			sr_luxuries = 1000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_conscription_campaign
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_special_395" }
    modifier = {
         country_sr_crew_produces_mult = 0.25
      }
	length = @EdictDuration
    ai_weight = { weight = 1 }
}



#############
# Resources #
#############

### Capacity Overload

# Capacity Overload
edict_capacity_overload = {
   icon = "GFX_edict_type_time"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_capacity_overload }			
			}
			sr_deuterium = 1000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_capacity_overload
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_planetary_367" }
    modifier = {
         country_energy_produces_mult = 0.20
      }
	length = 3600
      ai_weight = { weight = 1 }
}

# Capacity Overload - Machine
edict_capacity_overload_machine = {
   icon = "GFX_edict_type_time"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_capacity_overload_machine }			
			}
			sr_deuterium = @Edict3Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_capacity_overload_machine
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_02338" }
    modifier = {
         country_energy_produces_mult = 0.20
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
}

# Capacity Overload - Borg
edict_capacity_overload_borg = {
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_capacity_overload_borg }			
			}
			sr_deuterium = @Edict3Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_capacity_overload_borg
			}
			influence = 100
		}
	}
   icon = "GFX_edict_type_time"
    prerequisites = { "tech_society_12345" }
    modifier = {
         country_energy_produces_mult = 0.20
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
}

### Production Targets
   
# Production Targets - standard version
edict_production_targets = {
   icon = "GFX_edict_type_time"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_production_targets }			
			}
			sr_water = 1000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_production_targets
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_planetary_367" }
    modifier = { country_minerals_produces_mult = 0.20 }
	length = 3600
	potential = { is_non_standard_colonization = no }
	show_tech_unlock_if = {	is_non_standard_colonization = no }
    ai_weight = { weight = 1 }
}

# Production Targets - Machine
edict_production_targets_machine = {
   icon = "GFX_edict_type_time"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_production_targets_machine }			
			}
			influence = @Edict3Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_production_targets_machine
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_02338" }
    modifier = {
         country_minerals_produces_mult = 0.20
    }
	length = @EdictDuration
    ai_weight = { weight = 1 }
}

# Production Targets - Borg
edict_production_targets_borg = {
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_production_targets_borg }			
			}
			influence = @Edict3Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_production_targets_borg
			}
			influence = 100
		}
	}
   icon = "GFX_edict_type_time"
    prerequisites = { "tech_society_12345" }
    modifier = {
         country_minerals_produces_mult = 0.20
    }
	length = @EdictDuration
    ai_weight = { weight = 1 }
}

# Production Targets - atypical species
edict_production_targets_atypical = {
   icon = "GFX_edict_type_time"
        resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_production_targets_atypical }			
			}
			influence = @Edict3Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_production_targets_atypical
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_planetary_367" }
    modifier = { country_minerals_produces_mult = 0.20 }
	length = 3600
	potential = { is_non_standard_colonization = yes }
	show_tech_unlock_if = {	is_non_standard_colonization = yes }
    ai_weight = { weight = 1 }
}
   
# Farming Subsidies
edict_farming_subsidies = {
   icon = "GFX_edict_type_time"
     resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_farming_subsidies }			
			}
			sr_brizeen = 1000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_farming_subsidies
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_planetary_367" }
    modifier = {
         country_food_produces_mult = 0.20
      }
	length = 3600
	potential = { is_organic_species = yes NOT = { OR = { has_country_flag = hirogen_hunters has_country_flag = suliban_cabal } } }
	show_tech_unlock_if = { is_organic_species = yes NOT = { OR = { has_country_flag = hirogen_hunters has_country_flag = suliban_cabal } } }
    ai_weight = { weight = 1 }
}

# Farming Subsidies Nomadic Hirogen/Suliban
edict_farming_subsidies_nomad = {
   icon = "GFX_edict_type_time"
         resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_farming_subsidies_nomad }			
			}
			sr_water = 1000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_farming_subsidies_nomad
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_planetary_367" }
    modifier = {
         country_food_produces_mult = 0.20
      }
	length = 3600
	potential = { OR = { has_country_flag = hirogen_hunters has_country_flag = suliban_cabal } }
	show_tech_unlock_if = {	OR = { has_country_flag = hirogen_hunters has_country_flag = suliban_cabal } }
    ai_weight = { weight = 1 }
}
      
# Research Grants - Basic edict, available at game start
edict_research_focus = {
   icon = "GFX_edict_type_time"
      resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_research_focus }			
			}
			influence = @Edict2Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_research_focus
			}
			influence = 100
		}
	}
	prerequisites = { "tech_society_02217" }
    modifier = {
        all_technology_research_speed = 0.05
    }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# STEM Subsidies
edict_research_focus_2 = {
   icon = "GFX_edict_capacity"
   resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_research_focus_2 }			
			}
			influence = @Edict2Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_research_focus_2
			}
			influence = 100
		}
	}
    potential = {
            has_swapped_tradition = tr_diversity_4
      }
    modifier = {
         all_technology_research_speed = 0.1
      }
	length = @EdictDuration
    ai_weight = { weight = 1 }
}

# Key Drone Prioritization
edict_education_campaign_machine = {
   icon = "GFX_edict_capacity"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_education_campaign_machine }			
			}
			influence = @Edict1Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_education_campaign_machine
			}
			influence = 100
		}
	}
      prerequisites = { "tech_society_12346" }
      modifier = {
         species_leader_exp_gain = 0.25
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
   
# Prevention Protocols
edict_recycling_campaign_machine = {
   icon = "GFX_edict_capacity"
       resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_recycling_campaign_machine }			
			}
			influence = @Edict3Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_recycling_campaign_machine
			}
			influence = 100
		}
	}
      prerequisites = { "tech_society_12346" }
      modifier = {
         planet_amenities_no_happiness_mult = 0.1
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# Priority Combat Protocols
edict_war_drone_campaign_machine = {
   icon = "GFX_edict_capacity"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_war_drone_campaign_machine }			
			}
			influence = @Edict2Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_war_drone_campaign_machine
			}
			influence = 100
		}
	}
      prerequisites = { "tech_society_12356" }
      modifier = {
         army_damage_mult = 0.15
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
  
# Early Warnings
edict_early_warning_machine = {
   icon = "GFX_edict_type_policy"
          resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_early_warning_machine }			
			}
			influence = @Edict1Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_early_warning_machine
			}
			influence = 100
		}
	}
      prerequisites = { "tech_society_12356" }
      modifier = {
         planet_sensor_range_add = 1
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# Religious Unity - Generic unity edict. Available at game start.
edict_declare_saint = {
       resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_declare_saint }			
			}
			influence = @Edict2Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_declare_saint
			}
			influence = 100
		}
	}
   icon = "GFX_edict_type_policy"
    prerequisites = { "tech_society_12659" }
    modifier = {
        country_unity_produces_mult = 0.15
    }
	length = @EdictDuration
    ai_weight = { weight = 1 }
}
   
# Peace Festival - Generic happiness edict. Available at game start.
edict_peace_festivals = {
   icon = "GFX_edict_type_policy"
       resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_peace_festivals }			
			}
			sr_luxuries = 1000 
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_peace_festivals
			}
			influence = 100
		}
	}
	prerequisites = { "tech_society_02217" }
      potential = {
         has_unique_festival = no
      }
	  show_tech_unlock_if = { has_unique_festival = no }
      modifier = {
         pop_happiness = 0.10
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# First Contact Day - UE unique
edict_local_festival_1 = {
   icon = "GFX_edict_type_policy"
       resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_local_festival_1 }			
			}
			 sr_luxuries = 1000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_local_festival_1
			}
			influence = 100
		}
	}
	prerequisites = { "tech_society_02217" }
    potential = {
        has_country_flag = united_earth
    }
	show_tech_unlock_if = { has_country_flag = united_earth }
    modifier = {
        pop_happiness = 0.10
    }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# Kot'baval Festival - Klingon unique
edict_local_festival_2 = {
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_local_festival_2 }			
			}
			sr_luxuries = 1000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_local_festival_2
			}
			influence = 100
		}
	}
   icon = "GFX_edict_type_policy"
	prerequisites = { "tech_society_02217" }
      potential = {
         has_country_flag = klingon_empire
      }
	  show_tech_unlock_if = { has_country_flag = klingon_empire }
      modifier = {
         pop_happiness = 0.10
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# Kal Rekk Observation - Vulcan unique
edict_local_festival_3 = {
   icon = "GFX_edict_type_policy"
       resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_local_festival_3 }			
			}
			sr_luxuries = 1000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_local_festival_3
			}
			influence = 100
		}
	}
	prerequisites = { "tech_society_02217" }
      potential = {
         vulcan_empires = yes
      }
	  show_tech_unlock_if = { vulcan_empires = yes }
      modifier = {
         pop_happiness = 0.10
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# Peldor Gratitude Festival - Bajoran unique
edict_local_festival_4 = {
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_local_festival_4 }			
			}
			sr_luxuries = 1000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_local_festival_4
			}
			influence = 100
		}
	}
   icon = "GFX_edict_type_policy"
	prerequisites = { "tech_society_02217" }
      potential = {
         has_country_flag = bajoran_republic 
      }
	  show_tech_unlock_if = { has_country_flag = bajoran_republic }
      modifier = {
         pop_happiness = 0.10
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# Prixin Celebration - Talaxian unique
edict_local_festival_5 = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_local_festival_5 }			
			}
			sr_luxuries = 1000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_local_festival_5
			}
			influence = 100
		}
	}
	prerequisites = { "tech_society_02217" }
      potential = {
         has_country_flag = talaxian_empire
      }
	  show_tech_unlock_if = { has_country_flag = talaxian_empire }
      modifier = {
         pop_happiness = 0.10
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# Eitreih'hveinn Farmer Festival - Romulan unique
edict_local_festival_6 = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_local_festival_6 }			
			}
			sr_luxuries = 1000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_local_festival_6
			}
			influence = 100
		}
	}
	prerequisites = { "tech_society_02217" }
      potential = {
         has_country_flag = romulan_star_empire
      }
	  show_tech_unlock_if = { has_country_flag = romulan_star_empire }
      modifier = {
         pop_happiness = 0.10
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# Bread and Circuses - Terran empire unique
edict_local_festival_7 = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_local_festival_7 }			
			}
			sr_luxuries = 1000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_local_festival_7
			}
			influence = 100
		}
	}
	prerequisites = { "tech_society_02217" }
      potential = {
         has_country_flag = terran_empire
      }
	  show_tech_unlock_if = { has_country_flag = terran_empire }
      modifier = {
         pop_happiness = 0.10
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# Mother's Day - Lissepian empire unique
edict_local_festival_8 = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_local_festival_8 }			
			}
			sr_luxuries = 1000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_local_festival_8
			}
			influence = 100
		}
	}
	prerequisites = { "tech_society_02217" }
      potential = {
         has_country_flag = lissepian_parliaments
      }
	  show_tech_unlock_if = { has_country_flag = lissepian_parliaments }
      modifier = {
         pop_happiness = 0.10
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# Information Quarntine
edict_information_quarantine = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_information_quarantine }			
			}
			influence = @Edict2Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_information_quarantine
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_statecraft_991" }
    modifier = {
         pop_government_ethic_attraction = 0.33
         pop_ethics_shift_speed_mult = 0.33
    }
	length = @EdictDuration
    ai_weight = { weight = 1 }
}
   
#Encourage Free Thought
edict_encourage_free_thought = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_encourage_free_thought }			
			}
			influence = @Edict2Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_encourage_free_thought
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_statecraft_991" }
    modifier = {
         pop_ethics_shift_speed_mult = 0.5
    }
	length = @EdictDuration
    ai_weight = { weight = 1 }
}
   
#Map the Stars
edict_map_the_stars = {
   icon = "GFX_edict_type_policy"
          resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_map_the_stars }			
			}
			influence = @Edict1Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_map_the_stars
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_statecraft_991" }
    modifier = {
        science_ship_survey_speed = 0.25
        ship_anomaly_generation_chance_mult = 0.10
    }
	length = @EdictDuration
    ai_weight = { weight = 1 }
}
   
# Land of Opportunity
edict_land_of_opportunity = {
   icon = "GFX_edict_type_policy"
       resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_land_of_opportunity }			
			}
			influence = @Edict2Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_land_of_opportunity
			}
			influence = 100
		}
	}
      prerequisites = { "tech_society_statecraft_993" }
      potential = {
         has_comms_with_alien_empire = yes
      }
      modifier = {
         planet_immigration_pull_mult = 0.5
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# Interstellar Cultural Exchange
edict_diplomatic_grants = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_diplomatic_grants }			
			}
			influence = @Edict1Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_diplomatic_grants
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_statecraft_993" }
    potential = {
		is_xenophobe = no
      is_assimilator = no 
		NOT = { has_ethic = ethic_gestalt_consciousness }
		NOT = { has_valid_civic = civic_barbaric_despoilers }
        has_comms_with_alien_empire = yes
    }
    modifier = {
        country_trust_growth = 0.50
        country_trade_attractiveness = 0.10
		diplo_weight_mult = 0.10
		envoy_improve_relations_mult = 0.50
		envoy_harm_relations_mult = 0.50
    }
	length = @EdictDuration
    ai_weight = { weight = 1 }
}
   
# Early warnings
edict_early_warning = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_early_warning }			
			}
			influence = @Edict1Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_early_warning
			}
			influence = 100
		}
	}
      prerequisites = { "tech_society_statecraft_993" }
      modifier = {
         planet_sensor_range_add = 1
      }
	length = @EdictDuration
      ai_weight = { weight = 1 }
   }
   
# Full Research Footing
edict_research_footing = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_research_footing }			
			}
			influence = @Edict1Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_research_footing
			}
			influence = 100
		}
	}
      prerequisites = { "tech_society_02223" }
      modifier = {
        country_leader_pool_size = 1
		scientist_skill_levels = 1
		num_tech_alternatives_add = 1
        all_technology_research_speed = 0.05
      }
	length = @ambitionDuration
      is_ambition = yes   ai_weight = { weight = 1 }
   }
   
# Full War Footing
edict_desperate_measures = {
   icon = "GFX_edict_type_policy"
      	resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_desperate_measures }			
			}
			influence = @Edict2Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_desperate_measures
			}
			influence = 100
		}
	}
      prerequisites = { "tech_society_02223" }
      modifier = {
         starbase_defensive_war_ship_build_speed_mult = 1.00
         ship_home_territory_fire_rate_mult = 0.40
      }
	length = @ambitionDuration
      is_ambition = yes   ai_weight = { weight = 1 }
   }

   
# Architectural Renaissance
edict_architectural_renaissance = {
   icon = "GFX_edict_type_policy"
    resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_architectural_renaissance }			
			}
			influence = 2000
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_architectural_renaissance
			}
			influence = 100
		}
	}
      prerequisites = { "tech_society_02223" }
      modifier = {
         megastructure_build_speed_mult = 0.50
         planet_building_build_speed_mult = 1.00
      }
    length = @ambitionDuration
    is_ambition = yes   ai_weight = { weight = 1 }
   }
   
# Starbase Reinforcement
edict_fortress_proclamation = {
   icon = "GFX_edict_type_policy"
   	resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_fortress_proclamation }			
			}
			influence = 200
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_fortress_proclamation
			}
			influence = 100
		}
	}
      prerequisites = { "tech_society_02337" }
      modifier = {
         starbase_upgrade_speed_mult = 1.00
         shipsize_military_station_small_build_speed_mult = 1.00
      }
	length = @ambitionDuration
      is_ambition = yes   ai_weight = { weight = 1 }
   }
   
# Starbase Reinforcement machine
edict_fortress_proclamation_machine = {
	icon = "GFX_edict_type_policy"
	edict_cap_usage = 1
	resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_fortress_proclamation_machine }			
			}
			influence = @Edict2Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_fortress_proclamation_machine
			}
			influence = 100
		}
	}

      prerequisites = { "tech_society_12660" }
	  potential = { is_drone_authority = yes }
      modifier = {
         starbase_upgrade_speed_mult = 1.00
         shipsize_military_station_small_build_speed_mult = 1.00
      }
	length = @ambitionDuration
      is_ambition = yes   ai_weight = { weight = 1 }
   }
   
# Will to Power
edict_will_to_power = {
   icon = "GFX_edict_type_policy"
   	resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_will_to_power }			
			}
			influence = @Edict2Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_will_to_power
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_biology_1020" }
    modifier = {
         pop_government_ethic_attraction = 1.00
         pop_ethics_shift_speed_mult = 1.00
    }
    length = @ambitionDuration
    is_ambition = yes   ai_weight = { weight = 1 }
}
   
# The Grand Fleet
edict_grand_fleet = {
   icon = "GFX_edict_type_policy"
   resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_grand_fleet }			
			}
			influence = @Edict2Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_grand_fleet
			}
			influence = 100
		}
	}
    prerequisites = { "tech_society_02337" }
	potential = {
		OR = {
          has_technology = tech_society_02337
          has_swapped_tradition = tr_victory_4
		}
	}
    modifier = {
         country_naval_cap_mult = 0.20
         ships_upkeep_mult = -0.20
    }
    length = @ambitionDuration
    is_ambition = yes   ai_weight = { weight = 1 }
}

# Zhat Vash - Romulan Super secret agency
edict_zhat_vash = {
	resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_zhat_vash }			
			}
			influence = @Edict1Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_zhat_vash
			}
			influence = 400
		}
	}
	icon = "GFX_edict_type_policy"
	#prerequisites = { "tech_society_21550" }
    potential = {
		OR = {
			has_technology = tech_society_21550
			has_country_flag = romulan_star_empire
		}
    }
	show_tech_unlock_if = { has_country_flag = romulan_star_empire }
    modifier = { 
		pop_government_ethic_attraction = 0.05
		pop_happiness = 0.01
	}
	length = @EdictDuration
	ai_weight = { weight = 1000 }
}


# Dominion Shipbuilding
edict_dominion_shipbuilding = {
	icon = "GFX_edict_type_policy"
	resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { 
					has_edict = edict_dominion_shipbuilding
				}			
			}
			influence = 300
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_desperate_measures
			}
			influence = 100
		}
	}
	potential = {
        has_country_flag = the_dominion
    }
	show_tech_unlock_if = { has_country_flag = the_dominion }
	modifier = {
   		starbase_defensive_war_ship_build_speed_mult = 0.05
   		shipclass_military_build_speed_mult = 0.10
   		ship_home_territory_fire_rate_mult = 0.02
   		pop_happiness = -0.001
	}
	length = @EdictDuration   
	ai_weight = { weight = 100 }
}

# Borg Empire-Wide Mass Assimilation
edict_borg_mass_assimilation = {
	icon = "GFX_edict_type_policy"
	resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = edict_borg_mass_assimilation }
			}
			influence = @Edict1Cost
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_borg_mass_assimilation
			}
			influence = 50
		}
	}
	prerequisites = { "tech_society_12345" }
	modifier = { country_unity_produces_mult = -0.9 } #Penalizes passive Unity gain.
	length = @EdictDuration
	is_ambition = yes
	ai_weight = { 
		weight = 1 
		
		modifier = {
			factor = 1000
			AND = {
				is_assimilator = yes
				count_owned_pops = {
					limit = { is_assimilated_species = no }
					count > 40
				}
			}
		}
	}
}


greater_than_ourselves = {#Reenables important vanilla edict
	length = @EdictDuration
	icon = "GFX_edict_type_policy"
	edict_cap_usage = 1

	resources = {
		category = edicts
		cost = { #Activation Cost
			trigger = {
				NOT = { has_edict = greater_than_ourselves }			
			}
			influence = 300
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = greater_than_ourselves
			}
			influence = 200
		}
	}

	modifier = {
	}

	potential = {
		has_ascension_perk = ap_one_vision
		is_assimilator = no
	}

	ai_weight = { 
		weight = 100 
	}
}

#######################################
### Anti-Borg Combat Edicts - Xca|iber
#######################################

# Anti-Borg Drone Countermeasures
edict_anti_borg_assimilation = {
	icon = "GFX_edict_type_policy"
	resources = {
		category = edicts
		cost = { #Activation Cost (Minimal Investment)
			trigger = {
				has_policy_flag = anti_borg_1
				NOT = { has_edict = edict_anti_borg_assimilation }
			}
			influence = 120
		}
		cost = { #Activation Cost (Moderate Investment)
			trigger = {
				has_policy_flag = anti_borg_2
				NOT = { has_edict = edict_anti_borg_assimilation }
			}
			influence = 105
		}
		cost = { #Activation Cost (Aggressive Investment)
			trigger = {
				has_policy_flag = anti_borg_3
				NOT = { has_edict = edict_anti_borg_assimilation }
			}
			influence = 90
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_anti_borg_assimilation
			}
			influence = 50
		}
	}
	potential = {
		OR = {
			has_policy_flag = anti_borg_1
			has_policy_flag = anti_borg_2
			has_policy_flag = anti_borg_3
		}
		is_assimilator = no
	}
	allow = {
		custom_tooltip_fail = {
			text = edict_anti_borg_tooltip_fail
			OR = {
				# Minimal Policy Allows 1 Edict
				AND = {
					has_policy_flag = anti_borg_1
					NOR = {
						has_edict = edict_anti_borg_adaptation
						has_edict = edict_anti_borg_analysis
					}
				}
				# Moderate Policy Allows 2 Edicts
				AND = {
					has_policy_flag = anti_borg_2
					NAND = {
						has_edict = edict_anti_borg_adaptation
						has_edict = edict_anti_borg_analysis
					}
				}
				# Aggressive Policy allows all Edicts
				has_policy_flag = anti_borg_3
			}
		}
	}
	modifier = { }
	length = 720
	ai_weight = { 
		weight = 1 
		modifier = {
			factor = 100
			is_at_war_with = event_target:borgCollective
		}
	}
}

# Anti-Borg Frequency Remodulators
edict_anti_borg_adaptation = {
	icon = "GFX_edict_type_policy"
	resources = {
		category = edicts
		cost = { #Activation Cost (Minimal Investment)
			trigger = {
				has_policy_flag = anti_borg_1
				NOT = { has_edict = edict_anti_borg_adaptation }
			}
			influence = 120
		}
		cost = { #Activation Cost (Moderate Investment)
			trigger = {
				has_policy_flag = anti_borg_2
				NOT = { has_edict = edict_anti_borg_adaptation }
			}
			influence = 105
		}
		cost = { #Activation Cost (Aggressive Investment)
			trigger = {
				has_policy_flag = anti_borg_3
				NOT = { has_edict = edict_anti_borg_adaptation }
			}
			influence = 90
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_anti_borg_adaptation
			}
			influence = 50
		}
	}
	potential = {
		OR = {
			has_policy_flag = anti_borg_1
			has_policy_flag = anti_borg_2
			has_policy_flag = anti_borg_3
		}
		is_assimilator = no
	}
	allow = {
		custom_tooltip_fail = {
			text = edict_anti_borg_tooltip_fail
			OR = {
				# Minimal Policy Allows 1 Edict
				AND = {
					has_policy_flag = anti_borg_1
					NOR = {
						has_edict = edict_anti_borg_assimilation
						has_edict = edict_anti_borg_analysis
					}
				}
				# Moderate Policy Allows 2 Edicts
				AND = {
					has_policy_flag = anti_borg_2
					NAND = {
						has_edict = edict_anti_borg_assimilation
						has_edict = edict_anti_borg_analysis
					}
				}
				# Aggressive Policy allows all Edicts
				has_policy_flag = anti_borg_3
			}
		}
	}
	modifier = { }
	length = 720
	ai_weight = { 
		weight = 1 
		modifier = {
			factor = 100
			is_at_war_with = event_target:borgCollective
		}
	}
}

# Anti-Borg Tactical Innovations
edict_anti_borg_analysis = {
	icon = "GFX_edict_type_policy"
	resources = {
		category = edicts
		cost = { #Activation Cost (Minimal Investment)
			trigger = {
				has_policy_flag = anti_borg_1
				NOT = { has_edict = edict_anti_borg_analysis }
			}
			influence = 120
		}
		cost = { #Activation Cost (Moderate Investment)
			trigger = {
				has_policy_flag = anti_borg_2
				NOT = { has_edict = edict_anti_borg_analysis }
			}
			influence = 105
		}
		cost = { #Activation Cost (Aggressive Investment)
			trigger = {
				has_policy_flag = anti_borg_3
				NOT = { has_edict = edict_anti_borg_analysis }
			}
			influence = 90
		}
		cost = { #Deactivation Cost
			trigger = {
				has_edict = edict_anti_borg_analysis
			}
			influence = 50
		}
	}
	potential = {
		OR = {
			has_policy_flag = anti_borg_1
			has_policy_flag = anti_borg_2
			has_policy_flag = anti_borg_3
		}
		is_assimilator = no
	}
	allow = {
		custom_tooltip_fail = {
			text = edict_anti_borg_tooltip_fail
			OR = {
				# Minimal Policy Allows 1 Edict
				AND = {
					has_policy_flag = anti_borg_1
					NOR = {
						has_edict = edict_anti_borg_assimilation
						has_edict = edict_anti_borg_adaptation
					}
				}
				# Moderate Policy Allows 2 Edicts
				AND = {
					has_policy_flag = anti_borg_2
					NAND = {
						has_edict = edict_anti_borg_assimilation
						has_edict = edict_anti_borg_adaptation
					}
				}
				# Aggressive Policy allows all Edicts
				has_policy_flag = anti_borg_3
			}
		}
	}
	modifier = { }
	length = 720
	ai_weight = { 
		weight = 1 
		modifier = {
			factor = 100
			is_at_war_with = event_target:borgCollective
		}
	}
}
# All global modifiers are here.  They are applied from certain game-features.
#
#icon_frame = 1 # green frame
#icon_frame = 2 # yellow frame
#icon_frame = 3 # red frame
# Effects are fully scriptable here.

# The names can NOT be removed or changed since the code references them


##########################################################################
# Star Trek Horizons Modifiers
##########################################################################


sr_crew_deficit = {
	army_damage_mult = -0.75
	ship_weapon_damage = -0.75
}

sr_dilithium_deficit = {
	ship_fire_rate_mult = -0.75
	ship_shield_mult = -0.75
}

sr_brizeen_deficit = {
	planet_jobs_food_produces_mult = -0.25
}

sr_water_deficit = {
	planet_jobs_food_produces_mult = -0.25
}

sr_pergium_deficit = {
	planet_jobs_energy_produces_mult = -0.05
}

sr_boronite_deficit = {
	planet_jobs_energy_produces_mult = -0.05
}

sr_deuterium_deficit = {
	planet_jobs_energy_produces_mult = -0.25
}

sr_cordrazine_deficit = {
	pop_growth_speed_reduction = 0.5
}

sr_duranium_deficit = {
	ship_armor_mult = -0.5
}

sr_trellium_deficit = {
	ship_armor_mult = -0.5 ##TODO
}

sr_latinum_deficit = {
	planet_jobs_energy_produces_mult = -0.05
}

sr_time_crystal_deficit = {
	##TODO
}

sr_luxuries_deficit = {
	##TODO
}
po_food_deficit = {
	pop_happiness = -0.25
	pop_growth_speed = 0.02
	pop_growth_speed_reduction = 2
	bio_pop_growth_speed_reduction = 0.75
}

### Assault Army - Basic generic assault army
   assault_army = {
      damage = 1
      health = 1
      morale = 1
      morale_damage = 1
      collateral_damage = 1
      war_exhaustion = 1
      time = 90
      resources = {
         category = armies
         cost = { minerals = 100 }
         upkeep = { energy = 1 }
      }
      icon_frame = 2
      show_tech_unlock_if = { is_any_hive_species = no }
      potential = { owner = { is_any_hive_species = no } }
   }
   
### Robotic Army
   robotic_army = {
      damage = 1
      health = 2
      morale = 1
      morale_damage = 1
      collateral_damage = 1.5
      war_exhaustion = 0.5
      time = 90
      resources = {
         category = armies
         cost = { minerals = 150 }
         upkeep = { energy = 1.5 }
      }
      icon_frame = 4
      has_species = yes
      potential = { always = no }
   }
   
### Drone Army - Basic assault army, for Borg
   assault_army_borg = {
      damage = 1
      health = 2
      has_morale = no
      morale_damage = 1
      collateral_damage = 1.5
      war_exhaustion = 0.5
      time = 90
      resources = {
         category = armies
         cost = { minerals = 150 }
         upkeep = { energy = 1.5 }
      }
      icon_frame = 4
      show_tech_unlock_if = { is_borg_empire = yes }
      potential = { owner = { is_borg_empire = yes } }
      has_species = yes
   }
   
### Slave Army
   slave_army = {
      damage = 1
      health = 1
      morale = 0.75
      morale_damage = 0.75
      collateral_damage = 1.5
      war_exhaustion = 0.5
      time = 60
      resources = {
         category = armies
         cost = { sr_crew = 25 }
         upkeep = { sr_crew = 0.25 }
      }
      icon_frame = 10
      prerequisites = { "tech_society_that_487" }
      allow = { any_owned_pop = { is_pop_category = slave } }
   }
   
### Clone Army
   clone_army = {
      damage = 1
      health = 1
      morale = 1
      morale_damage = 1
      collateral_damage = 1.25
      war_exhaustion = 0.5
      time = 30
      resources = {
         category = armies
         cost = { food = 75 }
         upkeep = { food = 0.75 }
      }
      icon_frame = 7
      prerequisites = { "tech_society_selected_470" }
   }
   
### Telepathic Army
   psionic_army = {
      damage = 1.75
      health = 1.5
      morale = 2.25
      morale_damage = 1.4
      collateral_damage = 0.5
      war_exhaustion = 3
      time = 100
      resources = {
         category = armies
         cost = { sr_crew = 100 }
         upkeep = { sr_crew = 1 }
      }
      icon_frame = 9
      prerequisites = { "tech_telepathy" }
   }
   
### Xenomorphs
   xenomorph_army = {
      damage = 2
      health = 2
      has_morale = no
      collateral_damage = 5
      war_exhaustion = 0.25
      time = 100
      resources = {
         category = armies
         cost = { food = 200 }
         upkeep = { food = 2 }
      }
      icon_frame = 8
      prerequisites = { "tech_society_morphegenic_474" }
      has_species = no
   }
   
### Gene Warriors
   gene_warrior_army = {
      damage = 2
      health = 2.5
      morale = 2.5
      morale_damage = 1
      collateral_damage = 0.75
      war_exhaustion = 3
      time = 150
      resources = {
         category = armies
         cost = { food = 300 }
         upkeep = { food = 3 }
      }
      icon_frame = 6
      prerequisites = { "tech_society_the_476" }
   }
   
### Hunter-Killers - Basic assault army, for Pralor/Cravic
   machine_assault_1 = {
      damage = 1
      health = 1
      has_morale = no
      morale_damage = 1
      collateral_damage = 2
      war_exhaustion = 0.5
      time = 90
      resources = {
         category = armies
         cost = { minerals = 100 }
         upkeep = { energy = 1 }
      }
      icon_frame = 11
      show_tech_unlock_if = { is_machine_empire = yes }
      potential = { owner = { is_machine_empire = yes } }
      has_species = yes
   }
   
### Battle Frame
   machine_assault_2 = {
      damage = 1.5
      health = 2.5
      has_morale = no
      morale_damage = 1.5
      collateral_damage = 2
      war_exhaustion = 1
      time = 120
      resources = {
         category = armies
         cost = { consumer_goods = 100 }
         upkeep = { consumer_goods = 1 }
      }
      icon_frame = 11
      prerequisites = { "tech_society_12739" }
      show_tech_unlock_if = { is_machine_empire = yes }
      potential = { owner = { is_machine_empire = yes } }
      has_species = yes
   }
   
### Mechanized Combat Platform
   machine_assault_3 = {
      damage = 4
      health = 6
      has_morale = no
      morale_damage = 1.5
      collateral_damage = 4
      war_exhaustion = 4
      time = 500
      resources = {
         category = armies
         cost = { consumer_goods = 400 }
         upkeep = { consumer_goods = 4 }
      }
      icon_frame = 11
      prerequisites = { "tech_society_12743" }
      show_tech_unlock_if = { is_machine_empire = yes }
      potential = { owner = { is_machine_empire = yes } }
      has_species = no
   }
   
### Romulan Combat Platform
   war_machine = {
      damage = 3
      health = 4.5
      has_morale = no
      morale_damage = 1.5
      collateral_damage = 3
      war_exhaustion = 3
      time = 350
      resources = {
         category = armies
         cost = { minerals = 500 }
         upkeep = { energy = 5 }
      }
      icon_frame = 11
      prerequisites = { "tech_society_21364" }
      has_species = no
   }
   
### Deep Strike Commandos
   army_commandos = {
      damage = 1
      health = 1.25
      morale = 1
      morale_damage = 2.5
      collateral_damage = 1
      war_exhaustion = 1
      time = 120
      resources = {
         category = armies
         cost = { sr_crew = 100 }
         upkeep = { sr_crew = 1 }
      }
      icon_frame = 2
      prerequisites = { "tech_physics_01038" }
   }
   
### Time Commandos
   army_time_commandos = {
      damage = 1
      health = 1.5
      morale = 1
      morale_damage = 3
      collateral_damage = 1
      war_exhaustion = 1
      time = 120
      resources = {
         category = armies
         cost = { sr_time_crystal = 3 }
         upkeep = { sr_time_crystal = 0.03 }
      }
      icon_frame = 2
      prerequisites = { "tech_physics_20221" }
   }
   
### Mobile Energy Artillery
   army_artillery = {
      damage = 2
      health = 1
      morale = 1
      morale_damage = 1
      collateral_damage = 6
      war_exhaustion = 1
      time = 120
      resources = {
         category = armies
         cost = { sr_crew = 100 }
         upkeep = { sr_crew = 1 }
      }
      icon_frame = 2
      prerequisites = { "tech_society_combined_397" }
      show_tech_unlock_if = { is_machine_empire = no }
      potential = { owner = { is_machine_empire = no } }
   }
   
### Mobile Energy Artillery
   army_artillery_robot = {
      damage = 1.9
      health = 1
      has_morale = no
      morale_damage = 1
      collateral_damage = 6
      war_exhaustion = 1
      time = 120
      resources = {
         category = armies
         cost = { consumer_goods = 50 }
         upkeep = { consumer_goods = 0.5 }
      }
      icon_frame = 2
      prerequisites = { "tech_society_combined_397" }
      show_tech_unlock_if = { is_machine_empire = yes }
      potential = { owner = { is_machine_empire = yes } }
   }
   
### Heavy Tactical Drone Army
   army_borg_heavy = {
      damage = 1.5
      health = 2.5
      has_morale = no
      morale_damage = 1.5
      collateral_damage = 2
      war_exhaustion = 1
      time = 120
      resources = {
         category = armies
         cost = { consumer_goods = 100 }
         upkeep = { consumer_goods = 1 }
      }
      icon_frame = 11
      prerequisites = { "tech_society_statecraft_1765" }
      show_tech_unlock_if = { is_borg_empire = yes }
      potential = { owner = { is_borg_empire = yes } }
      has_species = yes
   }
   
### Terror Drone Army
   army_borg_terror = {
      damage = 4
      health = 6
      has_morale = no
      morale_damage = 1.5
      collateral_damage = 4
      war_exhaustion = 4
      time = 500
      resources = {
         category = armies
         cost = { consumer_goods = 400 }
         upkeep = { consumer_goods = 4 }
      }
      icon_frame = 11
      prerequisites = { "tech_society_statecraft_989" }
      show_tech_unlock_if = { is_borg_empire = yes }
      potential = { owner = { is_borg_empire = yes } }
      has_species = yes
   }
   
### Mercenries
   army_mercenry = {
      damage = 1
      health = 1
      morale = 1
      morale_damage = 1
      collateral_damage = 1
      war_exhaustion = 1
      time = 90
      resources = {
         category = armies
         cost = { sr_luxuries = 50 }
         upkeep = { food = 0.6 }
      }
      icon_frame = 2
      prerequisites = { "tech_society_12290" }
   }
   
### Bajoran Militia
   army_militia = {
      damage = 1
      health = 1
      morale = 0.75
      morale_damage = 0.75
      collateral_damage = 1.5
      war_exhaustion = 0.5
      time = 60
      resources = {
         category = armies
         cost = { unity = 30 }
         upkeep = { food = 0.6 }
      }
      icon_frame = 2
      prerequisites = { "tech_society_12284" }
   }
   
### Jem'Hadar Shock Troops
   army_jemhadar = {
      damage = 1.75
      health = 2.25
      morale = 2.25
      morale_damage = 1
      collateral_damage = 1
      war_exhaustion = 2.5
      time = 150
      resources = {
         category = armies
         cost = { consumer_goods = 100 }
         upkeep = { consumer_goods = 1 }
      }
      icon_frame = 6
      prerequisites = { "tech_ketracel_white" }
   }
   
apply_era_buff = {
	if = {
		limit = { owner = { OR = { has_country_flag = klingon_tos has_country_flag = romulan_tos has_country_flag = generic_tos has_country_flag = borg_tos } } }
		add_modifier = { modifier = "erabuff_tos" days = -1 }
	}
	if = {
		limit = { owner = { OR = { has_country_flag = klingon_tmp has_country_flag = romulan_tmp has_country_flag = generic_tmp has_country_flag = borg_tmp } } }
		add_modifier = { modifier = "erabuff_tmp" days = -1 }
	}
	if = {
		limit = { owner = { OR = { has_country_flag = klingon_tng has_country_flag = romulan_tng has_country_flag = generic_tng has_country_flag = borg_tng } } }
		add_modifier = { modifier = "erabuff_tng" days = -1 }
	}
}

apply_dom_weapon_buff = {
	if = {
		limit = { has_dom_weapons = yes }
		add_modifier = { modifier = sh_dom_polaron_effect days = -1 }
	}
}
	
cloneShipEffect = {
	##ONLY FOR KDF
	if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = corvette } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = corvette graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = saber } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = saber graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = sovereign } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = sovereign graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = steamrunner } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = steamrunner graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = adv_cruiser } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = adv_cruiser graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = assault_cruiser } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = assault_cruiser graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = strike } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = strike graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_gunboat_borghel_a } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_gunboat_borghel_a graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_gunboat_brel_a } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_gunboat_brel_a graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_bop_borghel } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_bop_borghel graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_bop_blasrika } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_bop_blasrika graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_bop_homcha } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_bop_homcha graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_bop_brel } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_bop_brel graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_bop_pagh } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_bop_pagh graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_raptor_somraw } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_raptor_somraw graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_raptor_d5 } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_raptor_d5 graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_raptor_jev } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_raptor_jev graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_raptor_hegh } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_raptor_hegh graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_raptor_mehadraw } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_raptor_mehadraw graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_raider_d6 } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_raider_d6 graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_raider_kvort } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_raider_kvort graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_battlecruiser_d7 } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_battlecruiser_d7 graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_battlecruiser_ktinga } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_battlecruiser_ktinga graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_battlecruiser_norgh } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_battlecruiser_norgh graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_battlecruiser_vorcha } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_battlecruiser_vorcha graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_warship_roj } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_warship_roj graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_warship_kvek } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_warship_kvek graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_warship_neghvar } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_warship_neghvar graphical_culture = event_target:cloneShipTemp.owner }
	}
	else_if = {
		limit = { event_target:cloneShipTemp = { is_ship_size = kdf_flagship_jehjhong } }
		create_ship = { name = event_target:cloneShipTemp random_existing_design = kdf_flagship_jehjhong graphical_culture = event_target:cloneShipTemp.owner }
	}
}
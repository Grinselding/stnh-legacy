root_get_exfiltration_bonus = {
	if = {
		limit = { check_variable = { which = years_infiltrating value > 20 } }
		root = { add_monthly_resource_mult = { resource = society_research value = 4 } }
	} 
	if = {
		limit = { check_variable = { which = years_infiltrating value > 15 } }
		root = { add_monthly_resource_mult = { resource = society_research value = 4 } }
	} 
	if = {
		limit = { check_variable = { which = years_infiltrating value > 10 } }
		root = { add_monthly_resource_mult = { resource = society_research value = 4 } }
	} 
	if = {
		limit = { check_variable = { which = years_infiltrating value > 5 } }
		root = { add_monthly_resource_mult = { resource = society_research value = 4 } }
	} 
	if = {
		limit = { check_variable = { which = years_infiltrating value > 0 } }
		root = { add_monthly_resource_mult = { resource = society_research value = 2 } }
	} 
}

root_get_fleet_exfiltration_bonus = {
	if = {
		limit = { check_variable = { which = years_infiltrating value > 20 } }
		root = { add_monthly_resource_mult = { resource = engineering_research value = 3 } }
	} 
	if = {
		limit = { check_variable = { which = years_infiltrating value > 15 } }
		root = { add_monthly_resource_mult = { resource = engineering_research value = 3 } }
	} 
	if = {
		limit = { check_variable = { which = years_infiltrating value > 10 } }
		root = { add_monthly_resource_mult = { resource = engineering_research value = 3 } }
	} 
	if = {
		limit = { check_variable = { which = years_infiltrating value > 5 } }
		root = { add_monthly_resource_mult = { resource = engineering_research value = 3 } }
	} 
	if = {
		limit = { check_variable = { which = years_infiltrating value > 0 } }
		log = "get something surely"
		root = { add_monthly_resource_mult = { resource = engineering_research value = 2 } }
		root = { add_monthly_resource_mult = { resource = society_research value = 2 } }
	} 
}

root_get_army_exfiltration_bonus = {
	if = {
		limit = { check_variable = { which = years_infiltrating value > 20 } }
		root = { add_monthly_resource_mult = { resource = engineering_research value = 3 } }
	} 
	if = {
		limit = { check_variable = { which = years_infiltrating value > 15 } }
		root = { add_monthly_resource_mult = { resource = engineering_research value = 3 } }
	} 
	if = {
		limit = { check_variable = { which = years_infiltrating value > 10 } }
		root = { add_monthly_resource_mult = { resource = engineering_research value = 3 } }
	} 
	if = {
		limit = { check_variable = { which = years_infiltrating value > 5 } }
		root = { add_monthly_resource_mult = { resource = engineering_research value = 3 } }
	} 
	if = {
		limit = { check_variable = { which = years_infiltrating value > 0 } }
		log = "get something surely"
		root = { add_monthly_resource_mult = { resource = engineering_research value = 2 } }
		root = { add_monthly_resource_mult = { resource = society_research value = 2 } }
	} 
}


set_secondment_flags = {
	if = {
		limit = { uses_starfleet_uniform = yes }
		set_leader_flag = starfleet_on_loan
	}
	if = {
		limit = { uses_mirror_starfleet_uniform = yes }
		set_leader_flag = mirror_starfleet_on_loan
	}
}


save_physics_scientist = { ###TODO - REVIEW THESE
	every_owned_leader = {
		limit = { owner = { research_leader = { area = physics is_same_value = prevprev } } }
		save_event_target_as = physicsLeader
	}
	if = {
		limit = { exists = event_target:physicsLeader }
		event_target:physicsLeader = { save_event_target_as = scientist }
	}
	else_if = {
		limit = { any_owned_leader = { leader_class = scientist } }
		random_owned_leader = { limit = { leader_class = scientist } save_event_target_as = scientist }
	}
	else = { }
}
save_engineering_scientist = {
	every_owned_leader = {
		limit = { owner = { research_leader = { area = engineering is_same_value = prevprev } } }
		save_event_target_as = physicsLeader
	}
	if = {
		limit = { exists = event_target:physicsLeader }
		event_target:physicsLeader = { save_event_target_as = scientist }
	}
	else_if = {
		limit = { any_owned_leader = { leader_class = scientist } }
		random_owned_leader = { limit = { leader_class = scientist } save_event_target_as = scientist }
	}
	else = { }
}
save_society_scientist = {
	every_owned_leader = {
		limit = { owner = { research_leader = { area = society is_same_value = prevprev } } }
		save_event_target_as = physicsLeader
	}
	if = {
		limit = { exists = event_target:physicsLeader }
		event_target:physicsLeader = { save_event_target_as = scientist }
	}
	else_if = {
		limit = { any_owned_leader = { leader_class = scientist } }
		random_owned_leader = { limit = { leader_class = scientist } save_event_target_as = scientist }
	}
	else = { }
}



ensure_male_antaak = {
	create_leader = { class = scientist species = last_created name = "Antaak" skill = 5
		traits = { trait = leader_trait_spark_of_genius trait = leader_trait_carefree trait = leader_trait_expertise_biology }
	}
	last_created_leader = {
		set_is_female = no
		set_leader_flag = antaak
		save_event_target_as = antaak
		exile_leader_as = antaak
	}
}


ensure_male_khan_governor = {
	create_species = { name = "Human" class = TREKHEROES portrait = KhanNoonienSingh traits = random }	
	create_leader = { class = governor species = last_created_species name = "Khan Noonien Singh" skill = 2 traits = { trait = leader_trait_army_veteran trait = leader_trait_retired_fleet_officer } }
	last_created_leader = {
		set_is_female = no
		set_leader_flag = khan_noonien_singh 
		save_global_event_target_as = khan_noonien_singh
	}
}

ensure_male_khan_scientist = {
	create_species = { name = "Human" class = TREKHEROES portrait = KhanNoonienSingh traits = random }	
	create_leader = { class = scientist species = last_created_species name = "Khan Noonien Singh" skill = 2 traits = { trait = leader_trait_army_veteran trait = leader_trait_retired_fleet_officer } }
	last_created_leader = {
		set_is_female = no
		set_leader_flag = khan_noonien_singh 
		save_global_event_target_as = khan_noonien_singh
	}
}

ensure_male_khan_admiral = {
	create_species = { name = "Human" class = TREKHEROES portrait = KhanNoonienSingh traits = random }	
	create_leader = { class = admiral species = last_created_species name = "Khan Noonien Singh" skill = 2 traits = { trait = leader_trait_army_veteran trait = leader_trait_retired_fleet_officer } }
	last_created_leader = {
		set_is_female = no
		set_leader_flag = khan_noonien_singh 
		save_global_event_target_as = khan_noonien_singh
	}
}

ensure_male_khan_general = {
	create_species = { name = "Human" class = TREKHEROES portrait = KhanNoonienSingh traits = random }	
	create_leader = { class = general species = last_created_species name = "Khan Noonien Singh" skill = 2 traits = { trait = leader_trait_army_veteran trait = leader_trait_retired_fleet_officer } }
	last_created_leader = {
		set_is_female = no
		set_leader_flag = khan_noonien_singh 
		save_global_event_target_as = khan_noonien_singh
	}
}


add_scaling_leader_influence = {
	switch = {
		trigger = has_skill
		1 = { owner = { add_resource = { influence = 50 } custom_tooltip = influence_gain_50 } }
		2 = { owner = { add_resource = { influence = 60 } custom_tooltip = influence_gain_60 } }
		3 = { owner = { add_resource = { influence = 75 } custom_tooltip = influence_gain_75 } }
		4 = { owner = { add_resource = { influence = 100 } custom_tooltip = influence_gain_100 } }
		5 = { owner = { add_resource = { influence = 125 } custom_tooltip = influence_gain_125 } }
		6 = { owner = { add_resource = { influence = 150 } custom_tooltip = influence_gain_150 } }
		7 = { owner = { add_resource = { influence = 175 } custom_tooltip = influence_gain_175 } }
		8 = { owner = { add_resource = { influence = 200 } custom_tooltip = influence_gain_200 } }
		9 = { owner = { add_resource = { influence = 225 } custom_tooltip = influence_gain_225 } }
		10 = { owner = { add_resource = { influence = 250 } custom_tooltip = influence_gain_250 } }
	}
}



create_changeling_leader = {
	ensure_changeling_exist = yes
	create_leader = { class = random species = event_target:founderSpecies name = random skill = 4 traits = { trait = leader_trait_shapeshifter trait = leader_trait_no_replace } event_leader = yes }
	last_created_leader = {
		remove_trait = leader_trait_no_replace
		random_list = {
			50 = {}
			5 = { set_name = "Odo" }
			5 = { set_name = "Laas" }
		}
		save_event_target_as = changeling_leader
	}
}


set_child_genius_age = {
	set_age = 16
}


give_bajoran_religion_trait_flag = {
	random_list = {
		10 = { set_leader_flag = gives_trait_ruler_fortifier }
		10 = { set_leader_flag = gives_trait_ruler_architectural_sense }
		10 = { set_leader_flag = gives_trait_ruler_frontier_spirit }
		10 = { set_leader_flag = gives_trait_ruler_world_shaper }
		10 = { set_leader_flag = gives_trait_ruler_eye_for_talent }
		10 = { set_leader_flag = gives_trait_ruler_champion_of_the_people }
		10 = { set_leader_flag = gives_trait_ruler_fertility_preacher }
		10 = { set_leader_flag = gives_trait_ruler_explorer }
		10 = { set_leader_flag = gives_trait_ruler_space_miner }
	}
}

transfer_vedek_candidate = {
	if = {
		limit = { NOT = { exists = event_target:bajoranVedek1 } }
		create_saved_leader = { 
			key = bajoranVedek1 name = random creator = root species = event_target:selectedVedekCandidate class = governor 
			effect = { 
				save_global_event_target_as = bajoranVedek1 set_leader_flag = bajoranVedek give_bajoran_religion_trait_flag = yes 
				set_name = event_target:selectedVedekCandidate
				if = { limit = { event_target:selectedVedekCandidate = { gender = male } } set_is_female = no }
				else_if = { limit = { event_target:selectedVedekCandidate = { gender = female } } set_is_female = yes }
				change_leader_portrait = event_target:selectedVedekCandidate
				event_target:selectedVedekCandidate = { set_leader_flag = silentDeath kill_leader = { show_notification = no } }
			}
		}
	}
	else_if = {
		limit = { NOT = { exists = event_target:bajoranVedek2 } }
		create_saved_leader = { 
			key = bajoranVedek2 name = random creator = root species = event_target:selectedVedekCandidate class = governor 
			effect = { 
				save_global_event_target_as = bajoranVedek2 set_leader_flag = bajoranVedek give_bajoran_religion_trait_flag = yes 
				set_name = event_target:selectedVedekCandidate
				if = { limit = { event_target:selectedVedekCandidate = { gender = male } } set_is_female = no }
				else_if = { limit = { event_target:selectedVedekCandidate = { gender = female } } set_is_female = yes }
				change_leader_portrait = event_target:selectedVedekCandidate
				event_target:selectedVedekCandidate = { set_leader_flag = silentDeath kill_leader = { show_notification = no } }
			}
		}
	}
	else_if = {
		limit = { NOT = { exists = event_target:bajoranVedek3 } }
		create_saved_leader = { 
			key = bajoranVedek3 name = random creator = root species = event_target:selectedVedekCandidate class = governor 
			effect = { 
				save_global_event_target_as = bajoranVedek3 set_leader_flag = bajoranVedek give_bajoran_religion_trait_flag = yes 
				set_name = event_target:selectedVedekCandidate
				if = { limit = { event_target:selectedVedekCandidate = { gender = male } } set_is_female = no }
				else_if = { limit = { event_target:selectedVedekCandidate = { gender = female } } set_is_female = yes }
				change_leader_portrait = event_target:selectedVedekCandidate
				event_target:selectedVedekCandidate = { set_leader_flag = silentDeath kill_leader = { show_notification = no } }
			}
		}
	}
}
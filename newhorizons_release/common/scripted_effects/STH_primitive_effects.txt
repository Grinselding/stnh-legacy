create_al_rijil_country = {
	create_species = {
		class = "CENTAUR"
		homeworld = THIS
		traits = { trait = "trait_adaptive" trait = "trait_nomadic" trait = "trait_wasteful" }
		namelist = "MAM1"
	}
	last_created_species = { save_global_event_target_as = centauranSpecies }
	create_country = {
		name = "Al Rijil"
		civics = { civic = civic_increasing_urbanization civic = civic_atmospheric_pollution }
		authority = auth_oligarchic
		species = event_target:centauranSpecies
		ethos = { ethic = "ethic_xenophile" ethic = "ethic_egalitarian" }
		flag = {
			icon = { category = "human" file = "flag_human_6.dds" }
			background = { category = "backgrounds" file = "circle.dds" }
			colors = { "dark_blue" "green" "null" "null" }
		}
		type = primitive
		origin = "origin_habitual"
		effect = {
			set_graphical_culture = industrial_01
			set_country_flag = warpcapable_age
			# set_country_flag = init_spawned
			set_primitive_age = warpcapable_age
			save_global_event_target_as = al_rijil
		}
	}
	set_owner = event_target:al_rijil
	create_fleet = { 
		name = "Space Station" 
		effect = {
			set_owner = event_target:al_rijil
			create_ship = { name = "Space Station" design = "NAME_Space_Station" }
			set_location = PREV
		}
	}
	generate_starting_pops = { pops_species_1 = 6 }
	add_building = building_primitive_capital
	set_name = "Al Rijil"
}
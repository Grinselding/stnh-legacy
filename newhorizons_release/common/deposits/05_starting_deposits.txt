## Possible Deposit Variables ##
# resources, resource and amount
# potential trigger (planet scope)
# planet_modifier - applied to planet only when deposit blocker has been cleared
# constant_modifier - applied to planet always
# blocker = <key/any/none> - default any
# station = station class in orbit to gather

@high = 16
@med = 8
@low = 4

@high_rare = 2
@med_rare = 1
@low_rare = 0.5

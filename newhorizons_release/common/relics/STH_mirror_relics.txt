###MIRROR RELICS

r_tantalus_device = {
	activation_duration = 3600
	portrait = "GFX_relic_tantalus_device"
	resources = { 
		category = relics 
		cost = { influence = 100 } 
	}
	ai_weight = { weight = 5 }
	triggered_country_modifier = { 
		potential = { always = yes }
		rivalries_influence_produces_mult = 0.15
	}
	score = 500
	active_effect = {
		custom_tooltip = tantalus_activate
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown"
				days = 3600
			}
		}
		country_event = { id = STH_relics_flavour.2101 }
	}
	possible = { 
	custom_tooltip = { fail_text = "requires_relic_no_cooldown" NOT = { has_modifier = relic_activation_cooldown } } }
}

r_terran_sword = { ##Needs active effect
	activation_duration = 3600
	sound = relic_activation_generic
	portrait = "GFX_relic_terran_sword"
	resources = { 
		category = relics 
		cost = { influence = 150 } 
		produces = { 
			trigger = {					
				OR = {
					is_species_class = FED					
					any_owned_pop = {	
						is_species_class = FED				
						has_citizenship_rights = yes						
					}
				}				
			}
			unity = 10 
		}
		produces = { 
			trigger = {	
				NOT = { is_species_class = FED }
			}
			society_research = 10 
		}
	}
	ai_weight = { weight = 10 }	
	score = 500
	active_effect = {
		custom_tooltip = terransword_activate
		large_scaling_crew_reward = yes
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown"
				days = 3600
			}
		}
	}
	possible = { custom_tooltip = { fail_text = "requires_relic_no_cooldown" NOT = { has_modifier = relic_activation_cooldown } } }
}

r_zefram_shotgun = { ##Needs active effect
	activation_duration = 3600
	sound = relic_activation_generic
	portrait = "GFX_relic_zefram_shotgun"
	resources = { 
		category = relics 
		cost = { influence = 150 } 
		produces = { 
			trigger = {					
				OR = {
					is_species_class = FED					
					any_owned_pop = {	
						is_species_class = FED				
						has_citizenship_rights = yes						
					}
				}				
			}
			unity = 10 
		}
		produces = { 
			trigger = {	
				NOT = { is_species_class = FED }
			}
			society_research = 10 
		}
	}
	ai_weight = { weight = 10 }	
	score = 500
	active_effect = {
		custom_tooltip = zefram_shotgun_activate
		large_scaling_unity_reward = yes
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown"
				days = 3600
			}
		}
	}
	possible = { custom_tooltip = { fail_text = "requires_relic_no_cooldown" NOT = { has_modifier = relic_activation_cooldown } } }
}

r_terran_badge = {
	activation_duration = @orb_activation_duration
	portrait = "GFX_relic_terranbadge"
	resources = { 
		category = relics 
		cost = { influence = 150 } 
	}
	ai_weight = { weight = 100 }
	triggered_country_modifier = { 
		potential = { always = yes }
		country_base_sr_time_crystal_produces_add = 1
	}
	score = 300
	active_effect = {
		custom_tooltip = mirrorbadge_activate
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown"
				days = 3600
			}
			country_event = { id = STH_relics_flavour.600 }
		}
	}
	possible = { custom_tooltip = { fail_text = "requires_relic_no_cooldown" NOT = { has_modifier = relic_activation_cooldown } } }
}
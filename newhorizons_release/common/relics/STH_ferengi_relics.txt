#Updated rules for potential that other races aquire them, appropriate produces and events, also many more rules
@activation_cost = 150
@triumph_duration = 3600


r_rules_of_acquisition = {
	activation_duration = @orb_activation_duration
	sound = relic_activation_generic
	portrait = "GFX_relic_ferengi_rules_of_acquisition"
	resources = { 
		category = relics 
		cost = { influence = 100 } 
		produces = { 
			trigger = {	
				OR = {
					is_species_class = FER
					any_owned_pop = {	
						is_species_class = FER				
						has_citizenship_rights = yes						
					}
				}			
			}
			unity = 10 
		}
		produces = { 
			trigger = {	
				NOT = { is_species_class = FER }
			}
			society_research = 10 
		}
	}
	ai_weight = { weight = 100 }	
	score = 1000
	active_effect = {
		custom_tooltip = rules_of_acquisition_activate
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown"
				days = 3600
			}
			if = {
				limit = {
					is_species_class = FER
				}
				country_event = { id = STH_relics_flavour.100 }
			}
			else = {
				country_event = { id = STH_relic_hunters.200 }
			}			
		}
	}
	possible = { custom_tooltip = { fail_text = "requires_relic_no_cooldown" NOT = { has_modifier = relic_activation_cooldown } } }
}
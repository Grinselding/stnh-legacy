# site_type_name = {				# Key of the site, used for name lookup etc.
#	desc = <triggered event desc>	# Description generator for the site, with scope this=archaeological site.
#	max_instances = <int>			# Max instances of this type a galaxy can have, only checked when using 'create_archaeological_site = random'
#	weight = <scriptable value>		# Weight used for random weight, only used when using 'create_archaeological_site = random'. scriptable value type is defined either by '<int>' or '<mean time to happen>'.
#	stages = <int>					# Should match number of defined stages below.
#	potential = <trigger>			# Trigger checking if a scope with this=fleet ,from=archaeological site is potential to excavate (this will add/remove this option without giving the player a reason).
#	allow = <trigger>				# Trigger checking if a scope with this=fleet ,from=archaeological site is allowed to excavate (this will toggle enable/disabled mode on buttons etc).
#	visible = <trigger>				# Trigger that checks if a scope with this=country can see the from=archaeological site
#	stage = {						# Stage definition, order dependent.
#		difficulty = <intervall int> 	# min max intervall type. intervall is defined either by '<int>' or '{ min = <int> max=<int> }' where the later will randomize a value between min and max.
#		icon = <string>				# rune icon gfx type.
#		event = <string>			# event to fire when finished the state.
#	}
#	stage = {...}					# Second stage, the total number of 'stage' entries should match value of 'stages'
#	on_roll_failed = <effect>			# Effect to fire when a roll fails, with scope this=fleet ,from=archaeological site.
#	on_create = <effect>			# Effect to fire upon site creation, with scope this=archaeological site.
#}


#Ship out of time
vaadwaur_ruins_1 = {
	desc = vaadwaur_ruins_1.desc
	picture = sth_GFX_evt_vaadwaurCity
	stages = 2
	max_instances = 1
	weight = { base = 0 }
	potential = { }
	visible = {
		default_site_visible_trigger = yes
	}
	on_visible = { }
	allow = {
		is_ship_class = shipclass_science_ship
		exists = leader
	}
	stage = {
		difficulty = 2
		event = STH_vaadwaur_story.110
		icon = GFX_archaeology_runes_F1
	}
	stage = {
		difficulty = 3
		event = STH_vaadwaur_story.120
		icon = GFX_archaeology_runes_F2
	}
	# stage = {
		# difficulty = 2
		# event = STH_archaeology_events.120
		# icon = GFX_archaeology_runes_F3
	# }
	# stage = {
		# difficulty = 3
		# event = STH_archaeology_events.130
		# icon = GFX_archaeology_runes_F4
	# }
	on_roll_failed = {
		from = { standard_archaeological_site_on_roll_failed = { RANDOM_EVENTS = sth_all_random_events } }
	}
}
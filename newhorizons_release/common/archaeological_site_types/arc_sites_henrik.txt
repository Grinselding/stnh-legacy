## Asteroid Command Center
# site_command_center = {
	# desc = "site_command_center_intro"
	# picture = GFX_evt_asteroid_field
	# stages = 4
	# weight = {
		# base = 0
		# modifier = {
			# add = 1
			# is_planet_class = pc_asteroid
		# }
	# }
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# default_site_visible_trigger = yes
	# }

	# stage = {
		# difficulty = 2
		# icon = unknown.dds
		# event = ancrel.6005
	# }

	# stage = {
		# difficulty = 3
		# icon = unknown.dds
		# event = ancrel.6010
	# }

	# stage = {
		# difficulty = 3
		# icon = unknown.dds
		# event = ancrel.6015
	# }

	# stage = {
		# difficulty = 4
		# icon = unknown.dds
		# event = ancrel.6020
	# }

	# on_roll_failed = {
		# from = {
			# standard_archaeological_site_on_roll_failed = { RANDOM_EVENTS = all_random_events }
		# }
	# }
# }

## Hunting Grounds
# site_hunting_ground = {
	# desc = "site_hunting_ground_intro"
	# stages = 5
	# weight = 0
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# default_site_visible_trigger = yes
	# }

	# stage = {
		# difficulty = 3
		# icon = unknown.dds
		# event = ancrel.6050
	# }

	# stage = {
		# difficulty = 4
		# icon = unknown.dds
		# event = ancrel.6055
	# }

	# stage = {
		# difficulty = 5
		# icon = unknown.dds
		# event = ancrel.6060
	# }

	# stage = {
		# difficulty = 6
		# icon = unknown.dds
		# event = ancrel.6065
	# }

	# stage = {
		# difficulty = 7
		# icon = unknown.dds
		# event = ancrel.6070
	# }

	# on_roll_failed = {
		# from = {
			# standard_archaeological_site_on_roll_failed = { RANDOM_EVENTS = all_random_events }
		# }
	# }
# }

## Zarqlan the Prophet
# site_zarqlan = {
	# desc = "site_zarqlan_intro"
	# stages = 6
	# weight = {
		# base = 0
	# }
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# default_site_visible_trigger = yes
	# }

	# stage = {
		# difficulty = 3
		# icon = unknown.dds
		# event = ancrel.6100
	# }

	# stage = {
		# difficulty = 4
		# icon = unknown.dds
		# event = ancrel.6105
	# }

	# stage = {
		# difficulty = 5
		# icon = unknown.dds
		# event = ancrel.6110
	# }

	# stage = {
		# difficulty = 6
		# icon = unknown.dds
		# event = ancrel.6115
	# }

	# stage = {
		# difficulty = 7
		# icon = unknown.dds
		# event = ancrel.6120
	# }

	# stage = {
		# difficulty = 8
		# icon = unknown.dds
		# event = ancrel.6125
	# }

	# on_roll_failed = {
		# from = {
			# standard_archaeological_site_on_roll_failed = { RANDOM_EVENTS = all_random_events }
		# }
	# }
# }

## Miniature Galaxy - Part I
# site_minigalaxy_1 = {
	# desc = "site_minigalaxy_1_intro"
	# stages = 3
	# weight = 0
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# default_site_visible_trigger = yes
	# }

	# stage = {
		# difficulty = 3
		# icon = unknown.dds
		# event = ancrel.6155
	# }

	# stage = {
		# difficulty = 4
		# icon = unknown.dds
		# event = ancrel.6160
	# }

	# stage = {
		# difficulty = 5
		# icon = unknown.dds
		# event = ancrel.6165
	# }

	# on_roll_failed = {
		# from = {
			# standard_archaeological_site_on_roll_failed = { RANDOM_EVENTS = all_random_events }
		# }
	# }
# }

## Miniature Galaxy - Part II
# site_minigalaxy_2 = {
	# desc = "site_minigalaxy_2_intro"
	# stages = 4
	# weight = 0
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# default_site_visible_trigger = yes
	# }

	# stage = {
		# difficulty = 5
		# icon = unknown.dds
		# event = ancrel.6170
	# }

	# stage = {
		# difficulty = 6
		# icon = unknown.dds
		# event = ancrel.6175
	# }

	# stage = {
		# difficulty = 7
		# icon = unknown.dds
		# event = ancrel.6180
	# }

	# stage = {
		# difficulty = 8
		# icon = unknown.dds
		# event = ancrel.6185
	# }

	# on_roll_failed = {
		# from = {
			# standard_archaeological_site_on_roll_failed = { RANDOM_EVENTS = all_random_events }
		# }
	# }
# }

## Star Petal
# site_star_petal = {
	# desc = "site_star_petal_intro"
	# stages = 4
	# weight = {
		# base = 0
		# modifier = {
			# add = 1
			# is_planet_class = pc_gas_giant
		# }
	# }
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# default_site_visible_trigger = yes
	# }

	# stage = {
		# difficulty = 2
		# icon = unknown.dds
		# event = ancrel.6200
	# }

	# stage = {
		# difficulty = 3
		# icon = unknown.dds
		# event = ancrel.6205
	# }

	# stage = {
		# difficulty = 3
		# icon = unknown.dds
		# event = ancrel.6210
	# }

	# stage = {
		# difficulty = 4
		# icon = unknown.dds
		# event = ancrel.6215
	# }

	# on_roll_failed = {
		# from = {
			# standard_archaeological_site_on_roll_failed = { RANDOM_EVENTS = all_random_events }
		# }
	# }
# }

## Robot Debris
# site_robot_debris = {
	# desc = "site_robot_debris_intro"
	# stages = 3
	# weight = {
		# base = 0
		# modifier = {
			# add = 1
			# OR = {
				# is_planet_class = pc_asteroid
				# is_planet_class = pc_ice_asteroid
			# }
		# }
	# }
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# default_site_visible_trigger = yes
	# }

	# stage = {
		# difficulty = 3
		# icon = unknown.dds
		# event = ancrel.6250
	# }

	# stage = {
		# difficulty = 4
		# icon = unknown.dds
		# event = ancrel.6255
	# }

	# stage = {
		# difficulty = 5
		# icon = unknown.dds
		# event = ancrel.6260
	# }

	# on_roll_failed = {
		# from = {
			# standard_archaeological_site_on_roll_failed = { RANDOM_EVENTS = all_random_events }
		# }
	# }
# }

## Test
# site_random = {
	# desc = "site_star_petal_intro"
	# stages = 4
	# weight = {
		# base = 0
	# }
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# default_site_visible_trigger = yes
	# }

	# stage = {
		# difficulty = 1
		# icon = unknown.dds
		# event = ancrel.6200
	# }

	# stage = {
		# difficulty = 1
		# icon = unknown.dds
		# event = ancrel.6205
	# }

	# stage = {
		# difficulty = 1
		# icon = unknown.dds
		# event = ancrel.6210
	# }

	# stage = {
		# difficulty = 1
		# icon = unknown.dds
		# event = ancrel.6215
	# }

	# on_roll_failed = {
		# from = {
			# standard_archaeological_site_on_roll_failed = { RANDOM_EVENTS = all_random_events }
		# }
	# }
# }
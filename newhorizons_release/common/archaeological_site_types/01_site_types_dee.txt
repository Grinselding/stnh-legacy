## site_type_name = {				### Key of the site, used for name and desc lookup etc.
##	max_instances = <int>			### Max instances of this type a galaxy can have, only checked when using `create_archaeological_site = random`
##	weight = <scriptable value>		### Weight used for random weight, only used when using `create_archaeological_site = random`. scriptable value type is defined either by '<int>' or `<mean time to happen>`.
##	stages = <int>					### Should match number of defined stages below.
##	potential = <trigger>			### Trigger checking if a scope with this=fleet ,prev=archaeological site is potential to excavate (this will add/remove this option without giving the player a reason).
##	allow = <trigger>				### Trigger checking if a scope with this=fleet ,prev=archaeological site is allowed to excavate (this will toggle enable/disabled mode on buttons etc).
##	stage = {						### Stage definition, order dependent.
##		difficulty = <intervall int> 	### min max intervall type. intervall is defined either by '<int>' or '{ min = <int> max=<int> }' where the later will randomize a value between min and max.
##		icon = <string>			### rune icon gfx type.
##		event = <string>			### event to fire when finished the state.
##	}
##	stage = {...}					### Second stage
##	on_roll_failed = <effect>			### effect to fire when a roll fails
##}

##The Baol Organism - Grunur Planet
# grunur_digsite_1 = {
	# desc = "grunur_digsite_1_desc"
	# stages = 2
	# max_instances = 1
	# weight = 0 ###Set by ancrel.2000
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# has_country_flag = baol_intro
		# default_site_visible_trigger = yes
	# }
	# stage = {
		# difficulty = 1
		# icon = GFX_archaeology_runes_C1
		# event = ancrel.2001
	# }
	# stage = {
		# difficulty = 1
		# icon = GFX_archaeology_runes_C2
		# event = ancrel.2002
	# }
	# on_roll_failed = {
		# from = { add_stage_clues = 5 }
	# }
# }

##The Baol Organism - The Barren
# baol_digsite_1 = {
	# desc = "baol_digsite_1_desc"
	# stages = 3
	# max_instances = 1
	# weight = 0 ###Set by ancrel.2000
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# has_country_flag = baol_intro
		# default_site_visible_trigger = yes
	# }
	# stage = {
		# difficulty = 1
		# icon = GFX_archaeology_runes_B1
		# event = ancrel.2004
	# }
	# stage = {
		# difficulty = 1
		# icon = GFX_archaeology_runes_B2
		# event = ancrel.2005
	# }
	# stage = {
		# difficulty = 1
		# icon = GFX_archaeology_runes_B3
		# event = ancrel.2006
	# }
	# on_roll_failed = {
		# from = { add_stage_clues = 5 }
	# }
# }

##The Baol Organism - The Shattered
# baol_digsite_2 = {
	# desc = "baol_digsite_2_desc"
	# stages = 4
	# max_instances = 1
	# weight = 0 ###Set by ancrel.2000
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# has_country_flag = baol_intro
		# default_site_visible_trigger = yes
	# }
	# stage = {
		# difficulty = 2
		# icon = GFX_archaeology_runes_B4
		# event = ancrel.2008
	# }
	# stage = {
		# difficulty = 2
		# icon = GFX_archaeology_runes_B5
		# event = ancrel.2009
	# }
	# stage = {
		# difficulty = 2
		# icon = GFX_archaeology_runes_B6
		# event = ancrel.2010
	# }
	# stage = {
		# difficulty = 2
		# icon = GFX_archaeology_runes_B1
		# event = ancrel.2011
	# }
	# on_roll_failed = {
		# from = { add_stage_clues = 5 }
	# }
# }

##The Baol Organism - The Silenced
# baol_digsite_3 = {
	# desc = "baol_digsite_3_desc"
	# stages = 2
	# max_instances = 1
	# weight = 0 ###Set by ancrel.2000
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# has_country_flag = baol_intro
		# default_site_visible_trigger = yes
	# }
	# stage = {
		# difficulty = 3
		# icon = GFX_archaeology_runes_B2
		# event = ancrel.2013
	# }
	# stage = {
		# difficulty = 3
		# icon = GFX_archaeology_runes_B3
		# event = ancrel.2024
	# }
	# on_roll_failed = {
		# from = { add_stage_clues = 5 }
	# }
# }


##City Of Bones
# city_of_bones = {
	# stages = 3
	# max_instances = 1
	# desc = {
		# trigger = {
			# planet = { is_colony = yes }
		# }
		# text = city_of_bones_desc
	# }
	# desc = {
		# trigger = {
			# planet = { is_colony = no }
		# }
		# text = city_of_bones_desc_uncol
	# }
	# picture = GFX_evt_alien_city
	# weight = {
		# base = 0
		# modifier = {
			# add = 1
			# OR = {
				# is_planet_class = pc_desert
				# is_planet_class = pc_arid
				# is_planet_class = pc_savannah
				# is_planet_class = pc_gaia
			# }
		# }
	# }
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# default_site_visible_trigger = yes
	# }
	# stage = {
		# difficulty = 1
		# icon = GFX_archaeology_runes_D1
		# event = ancrel.2041
	# }
	# stage = {
		# difficulty = 1
		# icon = GFX_archaeology_runes_D2
		# event = ancrel.2042
	# }
	# stage = {
		# difficulty = 1
		# icon = GFX_archaeology_runes_D3
		# event = ancrel.2043
	# }

	## Executes every time the research process doesn't go to another stage
	## From is the scope of the Archaeology Site itself
	# on_roll_failed = {
		# from = { add_stage_clues = 5 }
	# }
# }

##Mutation Vats
# mutation_vats = {
	# desc = "mutation_vats_desc"
	# stages = 3
	# max_instances = 1
	# weight = {
		# base = 0
		# modifier = {
			# add = 1
			# is_planet_class = pc_nuked
			# NOT = { has_planet_flag = ratling_planet }
		# }
	# }
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# default_site_visible_trigger = yes
	# }
	# stage = {
		# difficulty = 1
		# icon = unknown.dds ###TODO
		# event = ancrel.2045
	# }
	# stage = {
		# difficulty = 1
		# icon = unknown.dds ###TODO
		# event = ancrel.2046
	# }
	# stage = {
		# difficulty = 1
		# icon = unknown.dds ###TODO
		# event = ancrel.2047
	# }

	## Executes every time the research process doesn't go to another stage
	## From is the scope of the Archaeology Site itself
	# on_roll_failed = {
		# from = { add_stage_clues = 5 }
	# }
# }

##Planetary Mechanocalibrator
# planetary_mechanocalibrator = {
	# desc = "planetary_mechanocalibrator_desc"
	# picture = GFX_evt_city_ruins
	# stages = 6
	# max_instances = 1
	# weight = 0 ###Set in ancrel.2051
	# allow = {
		# exists = leader
		# leader = { leader_class = scientist }
	# }
	# visible = {
		# default_site_visible_trigger = yes
	# }
	# stage = {
		# difficulty = 1
		# icon = GFX_archaeology_runes_C3
		# event = ancrel.2051
	# }
	# stage = {
		# difficulty = 1
		# icon = GFX_archaeology_runes_C4
		# event = ancrel.2052
	# }
	# stage = {
		# difficulty = 1
		# icon = GFX_archaeology_runes_C5
		# event = ancrel.2053
	# }
	# stage = {
		# difficulty = 1
		# icon = GFX_archaeology_runes_C6
		# event = ancrel.2054
	# }
	# stage = {
		# difficulty = 1
		# icon = GFX_archaeology_runes_C1
		# event = ancrel.2055
	# }
	# stage = {
		# difficulty = 1
		# icon = GFX_archaeology_runes_C2
		# event = ancrel.2056
	# }

	## Executes every time the research process doesn't go to another stage
	## From is the scope of the Archaeology Site itself
	# on_roll_failed = {
		# from = { add_stage_clues = 5 }
	# }
# }
#disabled

tech_gargantuan_evolution = {
	cost = @tier3cost1
	area = society
	category = { biology }
	tier = 1
	is_rare = yes
	ai_update_type = all
	weight = 0
	potential = { always = no }
}

tech_repeatable_lcluster_clue = {
	area = engineering
	cost = 2000
	cost_per_level = @repeatableTechLevelCost
	tier = 2
	is_rare = yes
	category = { voidcraft }
	levels = -1
	weight = 0
	weight = 0
	potential = { always = no }
}

# ####################
# Sealed System Reward
# ####################

#Neural Tissue Engineering
tech_neuroregeneration = {
	cost = @tier4cost2
	area = society
	tier = 4
	category = { biology }
	weight = 0
	potential = { always = no }
}


# ####################
# Scavenger Bot Reward
# ####################

#Auto-Repair Module
tech_nanite_repair_system = {
	area = engineering
	cost = @tier5cost3
	tier = 5
	is_rare = yes
	category = { voidcraft }
	ai_update_type = military
	weight = 0
	potential = { always = no }
}
purge = {
	rank = 0
	# clothes_texture_index = 2
	
	assign_to_pop = {
		OR = {
			is_being_purged = yes
			has_pop_flag = event_purge
		}
	}
	weight = {
		weight = 20
	}
	
	# scope: pop to be resettled
	allow_resettlement = {
		always = yes
	}

	resettlement_cost = {
		energy = 100
	}
	
	pop_modifier = {
		pop_political_power = -1
	}
	resources = {
		category = pop_category_purge
		upkeep = {}
	}		
}

robot_servant = {
	rank = 0
	display_category = worker
	
	pop_modifier = {
		pop_political_power = -1
		pop_housing_usage_base = 0.5
		pop_amenities_usage_base = 0.5
	}
	
	assign_to_pop = {
		NAND = {
			exists = planet
			planet = {
				exists = owner
				owner = { is_gestalt = yes }
			}
		}
		OR = {
			is_shackled_robot = yes
			is_non_sapient_robot = yes
		}
	}
	weight = {
		weight = 15
	}	
	
	# scope: pop to be resettled
	allow_resettlement = {
		always = yes
	}

	resettlement_cost = {
		energy = 100
	}
	
	resources = {
		category = pop_category_robot
		upkeep = { trigger = { is_organic_species = yes } food = 1 } ### Organic Upkeep
		upkeep = { trigger = { is_assimilated_species = yes } consumer_goods = 1 } ### Borg Upkeep
		upkeep = { trigger = { is_changeling_species = yes } food = 0.5 minerals = 0.5 } ### Changeling Upkeep
		upkeep = { trigger = { is_lithovore_species = yes } minerals = 1 } ### Lithovore Upkeep
		upkeep = { trigger = { is_robotic_species = yes NOT = { is_assimilated_species = yes } } consumer_goods = 1 } ### Robotic Upkeep
		upkeep = { trigger = { has_ketracel_addiction = yes } consumer_goods = 1 } ### Ketracel White Upkeep
	}		
}

slave = {
	rank = 0
	# clothes_texture_index = 2
	display_category = worker
	
	assign_to_pop = {
		is_enslaved = yes
	}
	weight = {
		weight = 10
	}
	
	pop_modifier = {
		pop_political_power = -0.75
		pop_housing_usage_base = 0.75
		pop_amenities_usage_base = 0.75
	}
	
	# scope: pop to be resettled
	allow_resettlement = {
		always = yes
	}

	resettlement_cost = {
		energy = 50
	}	
	
	resources = {
		category = pop_category_slave
		upkeep = { trigger = { is_organic_species = yes } food = 1 } ### Organic Upkeep
		upkeep = { trigger = { is_assimilated_species = yes } consumer_goods = 1 } ### Borg Upkeep
		upkeep = { trigger = { is_changeling_species = yes } food = 0.5 minerals = 0.5 } ### Changeling Upkeep
		upkeep = { trigger = { is_lithovore_species = yes } minerals = 1 } ### Lithovore Upkeep
		upkeep = { trigger = { is_robotic_species = yes NOT = { is_assimilated_species = yes } } consumer_goods = 1 } ### Robotic Upkeep
		upkeep = { trigger = { has_ketracel_addiction = yes } consumer_goods = 1 } ### Ketracel White Upkeep
		
		# Living Standards Upkeep
		upkeep = {
			trigger = { 
				has_very_high_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_low
		}		
		upkeep = {
			trigger = { 
				has_high_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_low
		}		
		upkeep = {
			trigger = { 
				OR = {
					has_normal_living_standard_type = yes
					has_academic_living_standard_type = yes
				}
			}
			consumer_goods = @living_standard_luxuries_very_low
		}		
		upkeep = {
			trigger = { 
				OR = {
					has_low_living_standard_type = yes
					has_stratified_living_standard_type = yes
				}			
			}
		}	
	}		
}

bio_trophy = {
	rank = 0
	# clothes_texture_index = 1

	assign_to_pop = {
		has_citizenship_type = { type = citizenship_organic_trophy }
	}
	weight = {
		weight = 5
	}
	
	pop_modifier = {
		pop_housing_usage_base = 1
		pop_amenities_usage_no_happiness_base = 1
		pop_political_power = 2
	}

	# scope: pop to be resettled
	allow_resettlement = {
		always = yes
	}

	resettlement_cost = {
		energy = 50
	}
	resources = {
		category = pop_category_bio_trophy
		upkeep = { trigger = { is_organic_species = yes } food = 1 } ### Organic Upkeep
		upkeep = { trigger = { is_assimilated_species = yes } consumer_goods = 1 } ### Borg Upkeep
		upkeep = { trigger = { is_changeling_species = yes } food = 0.5 minerals = 0.5 } ### Changeling Upkeep
		upkeep = { trigger = { is_lithovore_species = yes } minerals = 1 } ### Lithovore Upkeep
		upkeep = { trigger = { is_robotic_species = yes NOT = { is_assimilated_species = yes } } consumer_goods = 1 } ### Robotic Upkeep
		upkeep = { trigger = { has_ketracel_addiction = yes NOT = { is_assimilated_species = yes } } consumer_goods = 1 } ### Ketracel White Upkeep
		
		upkeep = {
			consumer_goods = @living_standard_luxuries_very_high
		}
	}		
}

criminal = {
	rank = 1
	# clothes_texture_index = 2
	
	demotion_time = 0
	
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_shared_burden_living_standard_type = yes
		}
		trade_value_add = @trade_value_shared_burden
	}
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_very_high_living_standard_type = yes
		}
		trade_value_add = @trade_value_very_high
	}
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_high_living_standard_type = yes
		}
		trade_value_add = @trade_value_high
	}	
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			OR = {
				has_normal_living_standard_type = yes
				has_academic_living_standard_type = yes
			}
		}
		trade_value_add = @trade_value_normal
	}
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_stratified_living_standard_type = yes
		}
		trade_value_add = @trade_value_low
	}
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_low_living_standard_type = yes	
		}
		trade_value_add = @trade_value_very_low
	}	
	
	should_apply_unemployment_penalties = {
		NOR = {
			has_very_high_living_standard_type = yes
			has_high_living_standard_type = yes
		}
	}
	
	pop_modifier = {
		pop_housing_usage_base = 1
		pop_amenities_usage_base = 1
	}
	
	# scope: pop to be resettled
	allow_resettlement = {
		always = no
	}

	resources = {
		category = pop_category_workers
		upkeep = { trigger = { is_organic_species = yes } food = 1 } ### Organic Upkeep
		upkeep = { trigger = { is_assimilated_species = yes } consumer_goods = 1 } ### Borg Upkeep
		upkeep = { trigger = { is_changeling_species = yes } food = 0.5 minerals = 0.5 } ### Changeling Upkeep
		upkeep = { trigger = { is_lithovore_species = yes } minerals = 1 } ### Lithovore Upkeep
		upkeep = { trigger = { is_robotic_species = yes NOT = { is_assimilated_species = yes } } consumer_goods = 1 } ### Robotic Upkeep
		upkeep = { trigger = { has_ketracel_addiction = yes NOT = { is_assimilated_species = yes } } consumer_goods = 1 } ### Ketracel White Upkeep
		
		# Living Standards Upkeep
		produces = {
			trigger = {
				has_living_standard = { type = living_standard_utopian }
			}
			unity = 1
		}
		upkeep = {
			trigger = {
				has_shared_burden_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_shared_burden
		}			
		upkeep = {
			trigger = { 
				has_very_high_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_very_high
		}		
		upkeep = {
			trigger = { 
				has_high_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_high
		}		
		upkeep = {
			trigger = { 
				OR = {
					has_normal_living_standard_type = yes
					has_academic_living_standard_type = yes
				}
			}
			consumer_goods = @living_standard_luxuries_normal
		}		
		upkeep = {
			trigger = { 
				has_stratified_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_low
		}	
		upkeep = {
			trigger = { 
				has_low_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_very_low
		}
	}		
}

deviant_drone = {
	rank = 2
	# clothes_texture_index = 2
	
	demotion_time = 0
	
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_shared_burden_living_standard_type = yes
		}
		trade_value_add = @trade_value_shared_burden
	}
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_very_high_living_standard_type = yes
		}
		trade_value_add = @trade_value_very_high
	}
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_high_living_standard_type = yes
		}
		trade_value_add = @trade_value_high
	}	
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			OR = {
				has_normal_living_standard_type = yes
				has_academic_living_standard_type = yes
			}
		}
		trade_value_add = @trade_value_normal
	}
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_stratified_living_standard_type = yes
		}
		trade_value_add = @trade_value_low
	}
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_low_living_standard_type = yes	
		}
		trade_value_add = @trade_value_very_low
	}	
	
	should_apply_unemployment_penalties = {
		NOR = {
			has_very_high_living_standard_type = yes
			has_high_living_standard_type = yes
		}
	}
	
	pop_modifier = {
		pop_housing_usage_base = 1
		pop_amenities_usage_base = 1
	}
	
	# scope: pop to be resettled
	allow_resettlement = {
		always = no
	}

	resources = {
		category = pop_category_workers
		
		upkeep = { trigger = { is_organic_species = yes } food = 1 } ### Organic Upkeep
		upkeep = { trigger = { is_assimilated_species = yes } consumer_goods = 1 } ### Borg Upkeep
		upkeep = { trigger = { is_changeling_species = yes } food = 0.5 minerals = 0.5 } ### Changeling Upkeep
		upkeep = { trigger = { is_lithovore_species = yes } minerals = 1 } ### Lithovore Upkeep
		upkeep = { trigger = { is_robotic_species = yes NOT = { is_assimilated_species = yes } } consumer_goods = 1 } ### Robotic Upkeep
		upkeep = { trigger = { has_ketracel_addiction = yes NOT = { is_assimilated_species = yes } } consumer_goods = 1 } ### Ketracel White Upkeep
		
		# Living Standards Upkeep
		produces = {
			trigger = {
				has_living_standard = { type = living_standard_utopian }
			}
			unity = 1
		}
		upkeep = {
			trigger = {
				has_shared_burden_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_shared_burden
		}			
		upkeep = {
			trigger = { 
				has_very_high_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_very_high
		}		
		upkeep = {
			trigger = { 
				has_high_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_high
		}		
		upkeep = {
			trigger = { 
				OR = {
					has_normal_living_standard_type = yes
					has_academic_living_standard_type = yes
				}
			}
			consumer_goods = @living_standard_luxuries_normal
		}		
		upkeep = {
			trigger = { 
				has_stratified_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_low
		}	
		upkeep = {
			trigger = { 
				has_low_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_very_low
		}
	}		
}

corrupt_drone = {
	rank = 2
	# clothes_texture_index = 2
	
	demotion_time = 0
	
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_shared_burden_living_standard_type = yes
		}
		trade_value_add = @trade_value_shared_burden
	}
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_very_high_living_standard_type = yes
		}
		trade_value_add = @trade_value_very_high
	}
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_high_living_standard_type = yes
		}
		trade_value_add = @trade_value_high
	}	
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			OR = {
				has_normal_living_standard_type = yes
				has_academic_living_standard_type = yes
			}
		}
		trade_value_add = @trade_value_normal
	}
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_stratified_living_standard_type = yes
		}
		trade_value_add = @trade_value_low
	}
	triggered_planet_modifier = {
		potential = {
			can_generate_trade_value = yes
			has_low_living_standard_type = yes	
		}
		trade_value_add = @trade_value_very_low
	}	
	
	should_apply_unemployment_penalties = {
		NOR = {
			has_very_high_living_standard_type = yes
			has_high_living_standard_type = yes
		}
	}
	
	pop_modifier = {
		pop_housing_usage_base = 1
		pop_amenities_usage_base = 1
	}
	
	# scope: pop to be resettled
	allow_resettlement = {
		always = no
	}

	resources = {
		category = pop_category_workers
		
		upkeep = { trigger = { is_organic_species = yes } food = 1 } ### Organic Upkeep
		upkeep = { trigger = { is_assimilated_species = yes } consumer_goods = 1 } ### Borg Upkeep
		upkeep = { trigger = { is_changeling_species = yes } food = 0.5 minerals = 0.5 } ### Changeling Upkeep
		upkeep = { trigger = { is_lithovore_species = yes } minerals = 1 } ### Lithovore Upkeep
		upkeep = { trigger = { is_robotic_species = yes NOT = { is_assimilated_species = yes } } consumer_goods = 1 } ### Robotic Upkeep
		upkeep = { trigger = { has_ketracel_addiction = yes NOT = { is_assimilated_species = yes } } consumer_goods = 1 } ### Ketracel White Upkeep
		
		# Living Standards Upkeep
		produces = {
			trigger = {
				has_living_standard = { type = living_standard_utopian }
			}
			unity = 1
		}
		upkeep = {
			trigger = {
				has_shared_burden_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_shared_burden
		}			
		upkeep = {
			trigger = { 
				has_very_high_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_very_high
		}		
		upkeep = {
			trigger = { 
				has_high_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_high
		}		
		upkeep = {
			trigger = { 
				OR = {
					has_normal_living_standard_type = yes
					has_academic_living_standard_type = yes
				}
			}
			consumer_goods = @living_standard_luxuries_normal
		}		
		upkeep = {
			trigger = { 
				has_stratified_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_low
		}	
		upkeep = {
			trigger = { 
				has_low_living_standard_type = yes
			}
			consumer_goods = @living_standard_luxuries_very_low
		}
	}		
}

precursor = {
	rank = 3
	# clothes_texture_index = 1
	
	assign_to_pop = {
		exists = owner
		is_same_species = owner
		owner = { 
			OR = {
				is_country_type = fallen_empire 
				is_country_type = awakened_fallen_empire
			}
		}
	}
	weight = {
		weight = 100
	}
	
	pop_modifier = {
		pop_political_power = 9
		pop_housing_usage_base = 2
		pop_amenities_usage_base = 2
	}	
	
	# scope: pop to be resettled
	allow_resettlement = {
		always = yes
	}

	resettlement_cost = {
		energy = 100
	}	
	
	resources = {
		category = pop_category_precursor
		
		upkeep = { trigger = { is_organic_species = yes } food = 1 } ### Organic Upkeep
		upkeep = { trigger = { is_assimilated_species = yes } consumer_goods = 1 } ### Borg Upkeep
		upkeep = { trigger = { is_changeling_species = yes } food = 0.5 minerals = 0.5 } ### Changeling Upkeep
		upkeep = { trigger = { is_lithovore_species = yes } minerals = 1 } ### Lithovore Upkeep
		upkeep = { trigger = { is_robotic_species = yes NOT = { is_assimilated_species = yes } } consumer_goods = 1 } ### Robotic Upkeep
		upkeep = { trigger = { has_ketracel_addiction = yes NOT = { is_assimilated_species = yes } } consumer_goods = 1 } ### Ketracel White Upkeep
		
		# Other Upkeep
		upkeep = {
			trigger = { 
				exists = owner
				owner = { 
					is_spiritualist = no
				}
			}
			consumer_goods = 2.0
		}
		upkeep = {
			trigger = { 
				exists = owner
				owner = { 
					is_spiritualist = yes
				}
			}
			consumer_goods = 1.0
		}						
	}		
}

xeno_ward = {
	rank = 0
	# clothes_texture_index = 3
	
	pop_modifier = {
		pop_political_power = -1
		pop_housing_usage_base = 1
		pop_amenities_usage_base = 1
	}
	
	# scope: pop to be resettled
	allow_resettlement = {
		always = no
	}
	
	resources = {
		category = pop_category_xeno_ward
		
		upkeep = { trigger = { is_organic_species = yes } food = 1 } ### Organic Upkeep
		upkeep = { trigger = { is_assimilated_species = yes } consumer_goods = 1 } ### Borg Upkeep
		upkeep = { trigger = { is_changeling_species = yes } food = 0.5 minerals = 0.5 } ### Changeling Upkeep
		upkeep = { trigger = { is_lithovore_species = yes } minerals = 1 } ### Lithovore Upkeep
		upkeep = { trigger = { is_robotic_species = yes NOT = { is_assimilated_species = yes } } consumer_goods = 1 } ### Robotic Upkeep
		upkeep = { trigger = { has_ketracel_addiction = yes NOT = { is_assimilated_species = yes } } consumer_goods = 1 } ### Ketracel White Upkeep
		
		# Other Upkeep
		upkeep = {
			consumer_goods = @living_standard_luxuries_very_high
		}	
	}		
}
pre_sapients = {
	rank = 0
	# clothes_texture_index = 3
	
	pop_modifier = {
		pop_political_power = -1
		pop_housing_usage_base = -1
		pop_amenities_usage_no_happiness_base = -1
		pop_happiness = 1
	}
	
	# scope: pop to be resettled
	allow_resettlement = {
		always = no
	}
	
	resources = {
		category = pop_category_pre_sapients
		
		# Organic Upkeep
		upkeep = {
		}
	}		
}
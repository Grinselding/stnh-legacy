@distance = 30
@base_moon_distance = 10


# Valtese	
valt_homeworld = {			
	name = "Valt"		
	class = "sc_binary_mm"
	usage = custom_empire
	flags = { valt_homeworld }
	max_instances = 1
	asteroid_belt = { type = rocky_asteroid_belt radius = 80 }
	planet = { name = "Valt-A" class = pc_m_star orbit_distance = 20 orbit_angle = 90 size = 30 has_ring = no }
	planet = { name = "Valt-B" class = pc_m_star flags = { secondaryStar } orbit_distance = 30 orbit_angle = 270 size = 10 has_ring = no }
	planet = {
		name = "Valt Minor"
		class = "pc_arctic"
		orbit_distance = 40
		orbit_angle = -120
		size = 18
		starting_planet = yes
		has_ring = no
		deposit_blockers = none
		modifiers = none
		init_effect = { 
			prevent_anomaly = yes
			set_capital = yes
			if = {
				limit = { NOT = { any_country = { has_country_flag = valtese_senate } } }
				create_species = {
				    name = "Valtese"
				    class = VAL
				    portrait = valtese
				    homeworld = THIS
					namelist = "Valtese"
				    traits = {
				        trait = "trait_agrarian"
				        trait = "trait_talented"
				        trait = "trait_quarrelsome"
				        ideal_planet_class = "pc_arctic"
					}
				}
				last_created_species = { save_global_event_target_as = valteseSpecies }
				create_country = {
					name = "NAME_valtese_senate"
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = civic_valt_1 civic = civic_cutthroat_politics }
					authority = auth_democratic
					name_list = "Valtese"
					ethos = { ethic = "ethic_pacifist" ethic = "ethic_spiritualist" ethic = "ethic_egalitarian" }
					origin="origin_galactic_explorers"
					species = event_target:valteseSpecies
					flag = {
						icon = { category = "trek" file = "valtese.dds" }
						background = { category = "backgrounds" file = "00_solid.dds" }
						colors = { "orange" "orange" "null" "null" }
					}
					ship_prefix = "VS"
					effect = {
						set_graphical_culture = generic_05
						set_country_flag = valtese_senate
						set_country_flag = custom_start_screen
						set_country_flag = generic_ent
						set_country_flag = alpha_beta_empire
						set_country_flag = init_spawned
						set_country_flag = botf_minor
						save_global_event_target_as = valtese_senate
					}
				}
				
				set_owner = event_target:valtese_senate
			}
			add_modifier = { modifier = cold_snow_world days = -1 }
			set_capital = yes
			random_country = {
				limit = { has_country_flag = valtese_senate }
				save_global_event_target_as = valtese_senate
				add_appropriate_start_techs = yes
				give_technology = { tech = "tech_society_12372" message = no }
				species = { save_global_event_target_as = valteseSpecies }
				set_country_type = minorRace
			}
			set_owner = event_target:valtese_senate
			generate_starting_pops = { pops_species_1 = 20 }
			generate_start_buildings = yes
			generate_major_empire_start_fleets = yes
			add_deposit = d_animal_moonbeast
			set_name = "Valt Minor"
		}
	}
	planet = { 
		name = "Valt Major" 
		# class = rl_outer_unhabitable_planets 
		orbit_distance = 50 
		orbit_angle = -160 
		size = 20 
		has_ring = no 
		moon = { count = { min = 0 max = 1 } 
		class = rl_outer_moons 
		orbit_angle = { min = 90 max = 270 } 
		orbit_distance = 5 } 
	}
}
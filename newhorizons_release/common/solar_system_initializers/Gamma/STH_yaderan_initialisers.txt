@distance = 30
@base_moon_distance = 10

# Yaderan Homeworld
yaderan_homeworld = {
	name = "Yadera"
	class = sc_g
	flags = { yaderan_homeworld }
	usage = custom_empire
	max_instances = 1
	planet = { name = "Yadera" class = star size = { min = 20 max = 30 } orbit_distance = 0 orbit_angle = 1 has_ring = no }
	planet = {
		count = { min = 4 max = 8 }
		orbit_distance = { min = 25 max = 45 }
		# class = rl_inner_unhabitable_planets
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance
		moon = { count = { min = 0 max = 2 } class = rl_inner_moons orbit_angle = { min = 90 max = 270 } orbit_distance = 8 }
	}
	change_orbit = { min = 20 max = 30 }
	planet = {
		name = "Yadera Prime"
		class = "pc_continental"
		size = 18
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		has_ring = no
		init_effect = {
			prevent_anomaly = yes			
			if = {
				limit = { NOT = { any_country = { has_country_flag = yaderan_republic } } }
				create_species = { 
				    name = "Yaderan"
				    class = YAD 
				    portrait = yaderan 
				    homeworld = THIS 
					namelist = "Yaderan"
				    traits = { 
				        trait="trait_intelligent"
                    	trait="trait_communal"
                    	trait="trait_sedentary"
				        ideal_planet_class = "pc_continental" 
					} 
				}
				last_created_species = { save_global_event_target_as = yaderanSpecies }
				create_country = {
					name = "NAME_yaderan_republic"
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_virtuoso" civic = "civic_inwards_perfection" }
					origin="origin_galactic_explorers"
					authority = auth_democratic
					name_list = "Yaderan"
					ethos = { ethic = "ethic_xenophobe" ethic = "ethic_egalitarian" ethic = "ethic_pacifist" }
					species = event_target:yaderanSpecies
					flag = {
						icon={ category="trek" file="yaderan.dds" }
						background={ category="backgrounds" file="00_solid.dds" }
						colors={ "customcolor537" "blue" "null" "null" }
					}
					ship_prefix = ""
					effect = {
						set_graphical_culture = generic_01
						set_country_flag = yaderan_republic
						set_country_flag = custom_start_screen
						set_country_flag = generic_ent
						set_country_flag = gamma_empire
						set_country_flag = init_spawned
						save_global_event_target_as = yaderan_republic
					}
				}
				set_owner = event_target:yaderan_republic
			}
			add_modifier = { modifier = wet_karst_world days = -1 }
			set_capital = yes
			random_country = {
				limit = { has_country_flag = yaderan_republic }
				save_global_event_target_as = yaderan_republic
				add_appropriate_start_techs = yes
                give_technology = { tech = "tech_physics_holodeck_26" message = no }
				species = { save_global_event_target_as = yaderanSpecies }
				set_country_type = minorRace
			}
			set_owner = event_target:yaderan_republic
			add_deposit = d_tradegood_greenbread
			add_deposit = d_animal_gergher
			generate_starting_pops = { pops_species_1 = 22 }
			generate_start_buildings = yes
			add_building = building_holodeck_1
			generate_major_empire_start_fleets = yes
			set_name = "Yadera Prime"
		}
	}
}
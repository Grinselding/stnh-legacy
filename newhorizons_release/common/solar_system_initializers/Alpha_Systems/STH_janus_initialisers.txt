# Janus
janus_mm = {
	name = "Janus"
	class = sc_f
	planet = { 
        name = "Janus" class = pc_f_star orbit_distance = 0 orbit_angle = 1 size = 25 has_ring = no 
    }
    planet = { class = rl_inner_moons orbit_distance = 50 orbit_angle = 30 size = 12 has_ring = no }
    planet = { class = rl_inner_moons orbit_distance = 50 orbit_angle = 145 size = 8 has_ring = no }
    planet = { 
		class = rl_inner_moons orbit_distance = 70 orbit_angle = 30 size = 12 has_ring = no
		moon = {
			orbit_distance = 10
			class = rl_inner_moons
			size = 5
			orbit_angle = { min = 30 max = 270 }
		}
	}
	planet = { class = rl_inner_moons orbit_distance = 30 orbit_angle = 50 size = 10 has_ring = no }
	planet = { class = rl_inner_moons orbit_distance = 40 orbit_angle = 50 size = 8 has_ring = no }
	#JANUS VI
	planet = { 
		class = "pc_barren_cold" 
		orbit_distance = 70 
		orbit_angle = 50 
		size = 8 
		has_ring = no
		flags = { janus_vi }
		init_effect = { 
			clear_deposits = yes
		} 
	}
	#
	planet = { 
		# class = rl_gas_giants 
		orbit_distance = 80 orbit_angle = 30 size = 25 has_ring = yes
		moon = {
			orbit_distance = 20
			class = rl_outer_moons
			size = 7
			orbit_angle = { min = 30 max = 270 }
		}
		moon = {
			orbit_distance = 5
			class = rl_outer_moons
			size = 7
			orbit_angle = { min = 30 max = 270 }
		}
	}
	planet = { 
		# class = rl_gas_giants 
		orbit_distance = 50 orbit_angle = 30 size = 30 has_ring = no
		moon = {
			orbit_distance = 25
			class = rl_outer_moons
			size = 8
			orbit_angle = { min = 30 max = 270 }
		}
		moon = {
			orbit_distance = 5
			class = rl_outer_moons
			size = 4
			orbit_angle = { min = 30 max = 270 }
		}
		moon = {
			orbit_distance = 5
			class = rl_outer_moons
			size = 4
			orbit_angle = { min = 30 max = 270 }
		}
	}
	planet = { # class = rl_outer_unhabitable_rocky_planets 
	orbit_distance = 45 orbit_angle = 50 size = 10 has_ring = no }
	planet = { 
		# class = rl_outer_unhabitable_rocky_planets 
		orbit_distance = 75 orbit_angle = 50 size = 7 has_ring = no 
		moon = {
			orbit_distance = 10
			class = rl_outer_moons
			size = 4
			orbit_angle = { min = 30 max = 270 }
		}
	}
}
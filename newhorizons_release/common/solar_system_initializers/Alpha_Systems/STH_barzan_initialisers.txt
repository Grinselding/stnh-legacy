# Barzan
barzan_homeworld = {			
	name = "Barzan"		
	init_effect = { connect_neighbour_stars = yes }
	flags = { barzan }
	class = sc_f
	planet = {		
		name = "Barzan"	
		class = star	
		orbit_distance = 0	
		orbit_angle = 1	
		size = 25	
		has_ring = no	
	}		
	planet = {		
		# class = rl_inner_unhabitable_planets
		orbit_distance = 45
		size = 20
		moon = { class = rl_inner_moons orbit_distance = 9 size = 9 }
	}
	planet = {		
		# name = "Barzan II"
		class = "pc_arctic"	
		orbit_distance = 30	
		orbit_angle = 140	
		size = { min = 12 max = 14 }	
		flags = { barzanII }
		has_ring = no	
		deposit_blockers = none
		modifiers = none	
		init_effect = {
			prevent_anomaly = yes					
			create_species = {
				name = "Barzan"
				class = "BAR"
				portrait = "barzan"
				homeworld = THIS
				namelist = "Barzan"
				traits = {
					trait = "trait_agrarian"
					trait = "trait_charismatic"
					trait = "trait_breather_apparatus"
				}
			}
			last_created_species = { save_global_event_target_as = barzanSpecies }
			create_country = {
				name = "NAME_Barzan_Planetary_Republic"
				civics = { civic = civic_opportunistic_traders civic = civic_environmentalist }
				authority = auth_democratic
				species = event_target:barzanSpecies
				origin="origin_galactic_explorers"
				ethos = {
					ethic = "ethic_egalitarian"
					ethic = "ethic_fanatic_pacifist"
				}
				type = primitive
				flag = {
					icon = { category = "trek" file = "barzan.dds" }
					background = { category = "backgrounds" file = "circle_tall.dds" }
					colors = { "customcolor1515" "customcolor1693" "null" "null" }
				}
				effect = {
					set_graphical_culture = industrial_01
					set_country_flag = warpcapable_age
					set_country_flag = barzan_planetary_republic
					set_primitive_age = warpcapable_age
					save_global_event_target_as = barzan_planetary_republic
				}
			}
			add_modifier = { modifier = cold_high_desert_world days = -1 }
			set_owner = event_target:barzan_planetary_republic
			generate_starting_pops = { pops_species_1 = 3 }
			# add_building = building_primitive_capital
			# add_district = district_city
			# add_district = district_farming
			# add_district = district_generator
			set_name = "Barzan II"
		}
		moon = {
			name = "Amma"
			class = "pc_barren"
			size = 4
			orbit_distance = 8
			orbit_angle = 35
			has_ring = no
			deposit_blockers = none
		}
		moon = {
			name = "Tolpra"
			class = "pc_barren"
			size = 3
			orbit_distance = 12
			orbit_angle = 75
			has_ring = no
			deposit_blockers = none
		}
	}
	planet = {
		# class = rl_outer_unhabitable_rocky_planets
		orbit_distance = 40
		size = 20
	}
	planet = {
		# class = rl_gas_giants
		orbit_distance = 30
		size = 10
		moon = { class = rl_outer_moons orbit_distance = 15 size = 4 }
		moon = { class = rl_outer_moons orbit_distance = 10 size = 5 }
	}
	planet = {
		name = "Barzan VI Barycentre"
		class = pc_invisible_star
		orbit_distance = 60
		orbit_angle = 50
		size = 1
		has_ring = yes
		planet = {
			name = "Barzan VIa"
			# class = rl_outer_unhabitable_rocky_planets
			orbit_distance = 20
			size = 8
		}
		planet = {
			name = "Barzan VIb"
			# class = rl_outer_unhabitable_rocky_planets
			orbit_distance = 0
			orbit_angle = 180
			size = 8
		}
	}	
}
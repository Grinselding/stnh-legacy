#Kzinti Homeworld
ktarian_homeworld = {
	name = "Ktaris"
	class = "sc_f"
	flags = { ktarian_homeworld }
	usage = custom_empire
	max_instances = 1
	planet = { name = "Ktar" class = star orbit_distance = 0 orbit_angle = 1 size = { min = 20 max = 30 } has_ring = no 
		init_effect = {
		} 
	}
	planet = {
		count = { min = 2 max = 5 }
		orbit_distance = { min = 20 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		# class = rl_inner_unhabitable_planets
		moon = { count = { min = 2 max = 6 } orbit_angle = { min = 90 max = 270 } class = rl_inner_moons orbit_distance = 8 }
	}	
	planet = {		
		# name = "Ktaria"
		class = "pc_continental"
		orbit_distance = 30
		size = { min = 18 max = 20 }
		starting_planet = yes
		has_ring = no
		deposit_blockers = none
		flags = { ktarian_planet }
		init_effect = {
			prevent_anomaly = yes					
			create_species = {
				name = "Ktarian"
				class = "KTA"
				portrait = "ktarian" #TODO update
				homeworld = THIS
				namelist = "Ktarian"
				traits = {
					ideal_planet_class = "pc_continental"
					trait = "trait_charismatic"
					trait = "trait_agrarian"
				}
			}
			last_created_species = { save_global_event_target_as = ktarianSpecies }
			create_country = {
				name = "NAME_Ktarian_Coalition"
				type = primitive
				ignore_initial_colony_error = yes
				civics = { civic = "civic_efficient_bureaucracy" civic = "civic_functional_architecture" }
				authority = auth_democratic
				origin="origin_habitual"
				ethos = {
					ethic = "ethic_egalitarian"
					ethic = "ethic_fanatic_pacifist"
				}
				species = event_target:ktarianSpecies
				flag = {
					icon = { category = "trek" file = "turei.dds" } ##todo
					background ={ category="backgrounds" file="00_solid.dds" }
					colors= { "customcolor1512" "customcolor912" "null" "null" }
				}
				ship_prefix = KCC
				effect = {
					set_graphical_culture = industrial_01
					set_country_flag = ktarian_coalition
					set_primitive_age = warpcapable_age
					#set_country_flag = generic_ent
					set_country_flag = warpcapable_age
					save_global_event_target_as = ktarian_coalition
				}
			}
			add_modifier = { modifier = wet_alpine_world days = -1 }
			set_owner = event_target:ktarian_coalition
			generate_starting_pops = { pops_species_1 = 3 }
			clear_deposits = yes
			add_deposit = d_ktaria_arpasian_range
			add_deposit = d_ktaria_ktarian_glaciers
			add_deposit = d_ktaria_scenic_moonrise
			add_deposit = d_ktaria_black_grape_vineyard
			
			add_building = building_primitive_capital
			add_district = district_city
			add_district = district_farming
			add_district = district_generator
			set_name = "Ktaria"
		}
	}
	change_orbit = 80
	planet = {
		count = { min = 2 max = 4 }
		orbit_distance = { min = 20 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		# class = rl_outer_unhabitable_planets
		moon = { count = { min = 2 max = 3 } orbit_angle = { min = 90 max = 270 } class = rl_outer_moons orbit_distance = 9 }
	}
}
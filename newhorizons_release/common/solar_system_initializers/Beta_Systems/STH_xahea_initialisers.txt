#Xahean Homeworld
xahean_homeworld = {
	name = "Xahea"
	class = "sc_f"
	flags = { xahea_homeworld }
	usage = custom_empire
	max_instances = 1
	planet = { name = "Neketaka" class = star orbit_distance = 0 orbit_angle = 1 size = { min = 20 max = 30 } has_ring = no 
		init_effect = {
		} 
	}
	planet = {
		count = { min = 1 max = 4 }
		orbit_distance = { min = 20 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		# class = rl_inner_unhabitable_planets
		moon = { count = { min = 0 max = 1 } orbit_angle = { min = 90 max = 270 } class = rl_inner_moons orbit_distance = 5 }
	}	
	planet = {		
		name = "Xahea"	
		class = "pc_continental"
		orbit_distance = 30
		size = { min = 18 max = 20 }
		starting_planet = yes
		has_ring = yes
		deposit_blockers = none
		flags = { xahea_planet }
		init_effect = {
			prevent_anomaly = yes	
			save_event_target_as = xahea_planet
			add_modifier = { modifier = wet_mushroom_world days = -1 }
			create_ambient_object = { type = "sth_denorios_belt" location = this entity_offset = { min = 10 max = 20 } entity_offset_height = { min = -9 max = -11 } }
			last_created_ambient_object = { set_location = { target = event_target:xahea_planet distance = 10 angle = 0 } set_ambient_object_flag = xahea_belt }
			create_ambient_object = { type = "sth_denorios_belt" location = this entity_offset = { min = 10 max = 20 } entity_offset_height = { min = -9 max = -11 } }
			last_created_ambient_object = { set_location = { target = event_target:xahea_planet distance = 10 angle = 45 } set_ambient_object_flag = xahea_belt }
			create_ambient_object = { type = "sth_denorios_belt" location = this entity_offset = { min = 10 max = 20 } entity_offset_height = { min = -9 max = -11 } }
			last_created_ambient_object = { set_location = { target = event_target:xahea_planet distance = 10 angle = 90 } set_ambient_object_flag = xahea_belt }
			create_ambient_object = { type = "sth_denorios_belt" location = this entity_offset = { min = 10 max = 20 } entity_offset_height = { min = -9 max = -11 } }
			last_created_ambient_object = { set_location = { target = event_target:xahea_planet distance = 10 angle = 135 } set_ambient_object_flag = xahea_belt }
			create_ambient_object = { type = "sth_denorios_belt" location = this entity_offset = { min = 10 max = 20 } entity_offset_height = { min = -9 max = -11 } }
			last_created_ambient_object = { set_location = { target = event_target:xahea_planet distance = 10 angle = 180 } set_ambient_object_flag = xahea_belt }
			create_ambient_object = { type = "sth_denorios_belt" location = this entity_offset = { min = 10 max = 20 } entity_offset_height = { min = -9 max = -11 } }
			last_created_ambient_object = { set_location = { target = event_target:xahea_planet distance = 10 angle = 225 } set_ambient_object_flag = xahea_belt }
			create_ambient_object = { type = "sth_denorios_belt" location = this entity_offset = { min = 10 max = 20 } entity_offset_height = { min = -9 max = -11 } }
			last_created_ambient_object = { set_location = { target = event_target:xahea_planet distance = 10 angle = 270 } set_ambient_object_flag = xahea_belt }
			create_ambient_object = { type = "sth_denorios_belt" location = this entity_offset = { min = 10 max = 20 } entity_offset_height = { min = -9 max = -11 } }
			last_created_ambient_object = { set_location = { target = event_target:xahea_planet distance = 10 angle = 315 } set_ambient_object_flag = xahea_belt }
			create_ambient_object = { type = "sth_denorios_belt" location = this entity_offset = { min = 10 max = 20 } entity_offset_height = { min = -9 max = -11 } }
			if = {
				limit = { NOT = { any_country = { has_country_flag = xahean_council } } }
				create_species = { 
					name = "Xahean" 
					class = XAH
					portrait = ellora #todo
					homeworld = THIS
					namelist = "Xahean"
					traits = { 
						trait="trait_highly_talented"
	                    trait="trait_communal"
	                    trait="trait_reclusive"
						ideal_planet_class = "pc_continental"
					}
				}
				last_created_species = { save_global_event_target_as = xaheanSpecies }
				create_country = {
					name = "NAME_Xahean_Council"
					type = primitive
					ignore_initial_colony_error = yes
					civics = { civic = "civic_cutthroat_politics" civic = "civic_feudal_realm" }
					origin="origin_galactic_explorers"
					authority = auth_imperial
					name_list = "Xahean"
					ethos = { ethic = "ethic_authoritarian" ethic = ethic_fanatic_spiritualist }
					species = event_target:xaheanSpecies
					flag = {
						icon = { category = "trek" file = "hebitian.dds" } ##todo
						background ={ category="backgrounds" file="00_solid.dds" }
						colors= { "customcolor1912" "customcolor1912" "null" "null" }
					}
					ship_prefix = RXS
					effect = {
						set_graphical_culture = generic_01
						set_country_flag = xahean_council
						set_country_flag = custom_start_screen
						set_country_flag = generic_ent
						set_country_flag = alpha_beta_empire
						set_country_flag = init_spawned
						set_country_flag = botf_minor
						set_country_flag = early_space_age 
						set_primitive_age = early_space_age
						save_global_event_target_as = xahean_council
					}
				}
				set_owner = event_target:xahean_council
			}
			
			set_capital = yes
			random_country = {
				limit = { has_country_flag = xahean_council }
				save_global_event_target_as = xahean_council
				add_appropriate_start_techs = yes
				species = { save_global_event_target_as = xaheanSpecies }
			}
			set_owner = event_target:xahean_council
			generate_starting_pops = { pops_species_1 = 3 }
			add_building = building_primitive_capital
			add_district = district_farming
			add_district = district_farming
			add_deposit = d_sr_dilithium_surface_deposit
			set_name = "Xahea"	
		}
	}
	change_orbit = 80
	planet = {
		count = { min = 1 max = 4 }
		orbit_distance = { min = 20 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		# class = rl_outer_unhabitable_planets
		moon = { count = { min = 0 max = 1 } orbit_angle = { min = 90 max = 270 } class = rl_outer_moons orbit_distance = 8 }
	}
}
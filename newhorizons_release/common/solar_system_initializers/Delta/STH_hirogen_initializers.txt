@distance = 30
@base_moon_distance = 10

# hirogen
hirogen_homeworld = {			
	name = "Idrin's Star"
	class = "sc_g"
	flags = { hirogen_homeworld hunters_lodge_built }
	usage = custom_empire
	max_instances = 1
	asteroid_belt = { type = rocky_asteroid_belt radius = 88 }
	planet = { name = "Idrin's Star" class = star orbit_distance = 0 orbit_angle = 1 size = 30 has_ring = no }
	planet = { 
		class = "pc_asteroid" orbit_distance = 87 orbit_angle = -210 size = 2 has_ring = no 
		init_effect = { set_deposit = d_minerals_1 }
	}
	planet = { 
		class = "pc_asteroid" orbit_distance = 1 orbit_angle = -95 size = 2 has_ring = no 
		init_effect = { set_deposit = d_minerals_1 set_deposit = d_energy_1 }
	}
	planet = { 
		class = "pc_asteroid" orbit_distance = 1 orbit_angle = 100 size = 2 has_ring = no 
		init_effect = { set_deposit = d_sr_dilithium_orbit_deposit }
	}
	
	planet = {
		name = "Idrin's Lodge"
		class = "pc_hunters_lodge"
		orbit_distance = 1
		orbit_angle = 60
		size = 18
		has_ring = no
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		flags = { planet_idrin_lodge }
		init_effect = {
			prevent_anomaly = yes
			set_planet_entity = { entity = "orbital_habitat_entity" graphical_culture = hirogen_01 }
			set_planet_flag = megastructure
			set_planet_flag = habitat
			if = {
				limit = { NOT = { any_country = { has_country_flag = hirogen_hunters } } }
				create_species = { 
					name = "Hirogen" 
					class = HIR 
					portrait = hirogen 
					homeworld = THIS
					namelist = "Hirogen"					
					traits = { 
						trait="trait_consummate_warriors"
	                    trait="trait_skilled_hunters"
	                    trait="trait_solitary"
						ideal_planet_class = "pc_continental" 
					} 
				}
				last_created_species = { save_global_event_target_as = hirogenSpecies }
				create_country = {
					name = "NAME_hirogen_hunters"
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_nomadic" civic = "civic_bandits_hirogen" }
					authority = auth_oligarchic
					name_list = "Hirogen"
					ethos = { ethic = "ethic_fanatic_militarist" ethic = "ethic_authoritarian" }
					origin="origin_militaristic"
					species = event_target:hirogenSpecies
					flag = {
						icon = { category = "trek" file = "hirogen.dds" }
						background = { category = "backgrounds" file = "circle.dds" }
						colors = { "customcolor1902" "customcolor1890" "null" "null" }
					}
					ship_prefix = ""
					effect = {
						set_graphical_culture = hirogen_01
						set_country_flag = hirogen_hunters
						set_country_flag = custom_start_screen
						set_country_flag = generic_ent
						set_country_flag = delta_empire
						set_country_flag = init_spawned
						set_country_flag = first_alien_life
						set_country_flag = first_contact_event
						set_country_flag = sth_medium_galaxy
						set_country_flag = significant_power
						save_global_event_target_as = hirogen_hunters
					}
				}
				set_owner = event_target:hirogen_hunters
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = hirogen_hunters }
				save_global_event_target_as = hirogen_hunters
				add_appropriate_start_techs = yes
				species = { save_global_event_target_as = hirogenSpecies }
			}
			set_owner = event_target:hirogen_hunters
			generate_starting_pops = { pops_species_1 = 22 }
			clear_deposits = yes
			add_deposit = d_idrin_refuge
			add_deposit = d_asteroid_ice_field
			add_deposit = d_asteroid_cavern
			add_deposit = d_idrin_haven
			add_deposit = d_idrin_biome
			add_deposit = d_idrin_treasure
			generate_major_empire_start_fleets = yes
			add_building = building_orbital_capital_2
			add_building = building_naval_HQ_1_hirogen
			add_building = building_school_1
			add_building = building_foundry_1
			add_building = building_factory_1
			add_building = building_frontier_clinic_1
			add_building = space_building_temporal_1
			add_building = space_building_hydroponics_farm_1
			add_building = space_building_basic_mine
			add_building = building_power_plant_1
			add_building = building_holodeck_1_tsunkate
			add_building = building_collector_water_1
			add_district = district_orbital_city
			add_district = district_orbital_city
			add_district = district_farming_hydroponic
			add_district = district_farming_hydroponic
			set_name = "Idrin's Lodge" 
		}
	}
}

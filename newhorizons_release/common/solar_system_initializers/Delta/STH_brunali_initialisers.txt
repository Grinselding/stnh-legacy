@distance = 30
@base_moon_distance = 10

#Brunali
BrunaliHomeworld = {
	name = "Brunal"
	class = "rl_starting_stars"
	asteroid_belt = { type = rocky_asteroid_belt radius = 120 }
	flags = { brunali_homeworld }
	usage = custom_empire
	planet = { name = "Brunal" class = star orbit_distance = 0 orbit_angle = 1 size = { min = 20 max = 30 } has_ring = no }
	change_orbit = 50
	planet = {
		count = { min = 1 max = 2 }
		orbit_distance = 20
		# class = rl_inner_unhabitable_planets
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance
		moon = { count = { min = 0 max = 1 } class = rl_inner_moons orbit_angle = { min = 90 max = 270 } orbit_distance = 5 }
	}
	change_orbit = 30
	planet = {
		name = "Brunal Prime"
		home_planet = yes
		class = pc_continental
		orbit_distance = 10
		orbit_angle = { min = 90 max = 270 }
		size = 18
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		init_effect = { 
			prevent_anomaly = yes
			if = {
				limit = { NOT = { any_country = { has_country_flag = brunali_empire } } }
				create_species = {
					name = "Brunali"
					class = BRU
					portrait = brunali
					homeworld = THIS
					namelist = "Brunali"
					traits = {
						trait = "trait_natural_sociologists"
						trait = "trait_intelligent"
						trait = "trait_weak"
						trait = "trait_master_geneticist"
						ideal_planet_class = "pc_continental"
					}
				}
				last_created_species = { save_global_event_target_as = brunaliSpecies }
				create_country = {
					name = "NAME_brunali_empire"
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_agrarian_idyll" civic = "civic_environmentalist" }
					origin="origin_spiritualist"
					authority = auth_democratic
					name_list = "Brunali"
					ethos = { ethic = "ethic_fanatic_pacifist" ethic = "ethic_spiritualist" }
					species = event_target:brunaliSpecies
					flag = {
						icon = { category = "blocky" file = "flag_blocky_8.dds" }
						background = { category = "backgrounds" file = "new_dawn.dds" }
						colors = { "customcolor2000" "customcolor1809" "null" "null" }
					}
					ship_prefix = ""
					effect = {
						set_graphical_culture = generic_02
						set_country_flag = brunali_empire
						set_country_flag = custom_start_screen
						set_country_flag = generic_ent
						set_country_flag = delta_empire
						set_country_flag = init_spawned
						save_global_event_target_as = brunali_empire
					}
				}
				set_owner = event_target:brunali_empire
			}
			add_modifier = { modifier = wet_praire_world days = -1 }
			set_capital = yes
			random_country = {
				limit = { has_country_flag = brunali_empire }
				save_global_event_target_as = brunali_empire
				add_appropriate_start_techs = yes
				species = { save_global_event_target_as = brunaliSpecies }
            }
			set_owner = event_target:brunali_empire
			generate_starting_pops = { pops_species_1 = 20 }
			generate_start_buildings = yes
			generate_major_empire_start_fleets = yes
			set_name = "Brunal Prime" 	
		}
		change_orbit = @base_moon_distance
		moon = { count = { min = 0 max = 1 } class = rl_outer_moons orbit_angle = { min = 90 max = 270 } orbit_distance = 5 }
	}
	planet = {
		count = { min = 0 max = 2 }
		orbit_distance = 20
		# class = rl_outer_unhabitable_planets
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance
		moon = { count = { min = 0 max = 1 } class = rl_outer_moons orbit_angle = { min = 90 max = 270 } orbit_distance = 5 }
	}
	change_orbit = 25
	planet = { count = { min = 2 max = 3 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 40 max = 110 } }
	planet = {
		count = { min = 1 max = 3 }
		orbit_distance = 25
		# class = rl_outer_unhabitable_planets
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance
		moon = { count = { min = 0 max = 1 } class = rl_outer_moons orbit_angle = { min = 90 max = 270 } orbit_distance = 5 }
	}
	planet = {
		count = { min = 1 max = 2 }
		orbit_distance = 20
		class = pc_gas_giant
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance
		moon = { count = { min = 0 max = 3 } class = rl_outer_moons orbit_angle = { min = 90 max = 270 } orbit_distance = 5 }
	}
	planet = { count = { min = 0 max = 2 } orbit_distance = 20 # class = rl_outer_unhabitable_rocky_planets 
	orbit_angle = { min = 90 max = 270 } }
}
@distance = 30
@base_moon_distance = 10

#Vaadwaur
vaadwaur_homeworld = {
	class = "rl_starting_stars" #Todo: Make K class
	asteroid_belt = { type = rocky_asteroid_belt radius = 120 }
	flags = { vaadwaur_homeworld }
	usage = custom_empire
	max_instances = 1
	planet = { 
		class = star orbit_distance = 0 orbit_angle = 1 size = { min = 20 max = 30 } has_ring = no 
		flags = { vaadwaurManasa }
	}
	change_orbit = 50
	planet = {
		orbit_distance = 20
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance
		flags = { vaadwaurNehustan }
	}
	planet = {
		orbit_distance = 20
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance
		flags = { vaadwaurAhi }
		moon = { 
			class = rl_inner_moons orbit_angle = { min = 90 max = 270 } orbit_distance = 5 
			flags = { vaadwaurVritra }
		}
	}
	change_orbit = 30
	planet = {
		home_planet = yes
		class = pc_nuked
		flags = { vaadwaurPrime }
		orbit_distance = 10
		orbit_angle = { min = 90 max = 270 }
		size = { min = 22 max = 25 }
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		init_effect = { 
			prevent_anomaly = yes
			change_pc = pc_nuked
			random_country = {
				limit = { has_country_flag = vaadwaur_supremacy }
				save_global_event_target_as = vaadwaur_supremacy
				add_appropriate_start_techs = yes
				species = { save_global_event_target_as = vaadwaurSpecies }
			}
			if = {
				limit = { exists = event_target:vaadwaur_supremacy }
				set_owner = event_target:vaadwaur_supremacy
				clear_deposits = yes
				generate_starting_pops = { pops_species_1 = 2 }
				add_deposit = d_vaadwaur_ruined_capital_city
				while = { count = 5 add_deposit = d_vaadwaur_stasis_pods }
				while = { count = 4 add_deposit = d_vaadwaur_hangar_bay }
				add_deposit = d_underground_ore
				add_deposit = d_underground_thermal
				add_deposit = d_underground_soil
				add_deposit = d_city_ruins
				add_deposit = d_radioactive_wasteland
				add_deposit = d_bomb_crater
				add_building = building_capital_2
				add_planet_devastation = 90
			}
			else = {
				### STNH TODO: add special project /dig site
				add_anomaly = { category = STH_vaadwaur_colony_1_category }
				add_deposit = d_underground_ore
				add_deposit = d_underground_thermal
				add_deposit = d_underground_soil
				add_deposit = d_city_ruins
				add_deposit = d_radioactive_wasteland
				add_deposit = d_bomb_crater
				add_planet_devastation = 90
			}
		}
		change_orbit = @base_moon_distance
		moon = { 
			class = rl_inner_moons orbit_angle = { min = 90 max = 270 } orbit_distance = 5 
			flags = { vaadwaurAtum }
		}
	}
	planet = {
		orbit_distance = 20
		# class = rl_outer_unhabitable_planets
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance
		flags = { vaadwaurEobshin }
		moon = { 
			class = rl_outer_moons orbit_angle = { min = 90 max = 270 } orbit_distance = 5 
			flags = { vaadwaurSimbi }
		}
	}
	change_orbit = 25
	planet = { class = random_asteroid orbit_distance = 0 orbit_angle = { min = 40 max = 110 } }
	planet = { class = random_asteroid orbit_distance = 0 orbit_angle = { min = 40 max = 110 } }
	planet = { class = random_asteroid orbit_distance = 0 orbit_angle = { min = 40 max = 110 } }
	planet = {
		orbit_distance = 25
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance
		flags = { vaadwaurAapep }
	}
	planet = {
		orbit_distance = 25
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance
		flags = { vaadwaurMeretseger }
		moon = { 
			class = rl_outer_moons orbit_angle = { min = 90 max = 270 } orbit_distance = 5 
			flags = { vaadwaurNirah }
		}
	}
	planet = {
		orbit_distance = 20
		class = pc_gas_giant
		orbit_angle = { min = 90 max = 270 }
		change_orbit = @base_moon_distance
		flags = { vaadwaurNdengei }
		moon = { 
			class = rl_outer_moons orbit_angle = { min = 90 max = 270 } orbit_distance = 5 
			flags = { vaadwaurMeshussu }
		}
		moon = { 
			class = rl_outer_moons orbit_angle = { min = 90 max = 270 } orbit_distance = 5 
			flags = { vaadwaurNehebkau }
		}
	}
	planet = { 
		orbit_distance = 20 class = rl_outer_moons orbit_angle = { min = 90 max = 270 } 
		flags = { vaadwaurDamballa }
		class = pc_i_class
		init_effect = {
			set_deposit = d_sr_water_deposit
		}
	}
}
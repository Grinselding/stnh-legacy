#Deep space - Expeditions
deep_space_system_event_spawn = {
	name = "Somewhere in Deep Space"
	init_effect = { connect_neighbour_stars = no }
	usage = misc_system_init
	class = "sc_p_green"
	asteroid_belt = { type = rocky_asteroid_belt radius = 65 }
	flags = { deep_space_system hostile_system lcuster }
	change_orbit = 65
		planet = {
		name = "Expedition Mission"
		class = "pc_invisible_star"
		size = 1
		orbit_distance = 1
		orbit_angle = 1
		flags = { deep_space_flag }
		has_ring = no
		init_effect = {
			create_p_green_space = yes
			prevent_anomaly = yes
			clear_deposits = yes
		}
	}	
}
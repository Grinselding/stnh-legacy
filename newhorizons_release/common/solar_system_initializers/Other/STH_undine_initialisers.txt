# undine Enlightenment
### MAKE THIS A BETTER SYSTEM EVENTUALLY
undine_homeworld = {
	name = "Rachia"
	class = "sc_p_red"
	flags = { p_red undine_homeworld }
	usage = custom_empire
	max_instances = 1
	planet = {
		class = pc_nebula_red
		name = "Rachia"
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 20 max = 30 }
		has_ring = no
		init_effect = {
			create_p_red_space = yes
			prevent_anomaly = yes
		}
	}
	change_orbit = 50
	planet = {
		home_planet = yes
		class = pc_coral_asteroid
		name = "Limin"
		orbit_distance = 10
		orbit_angle = 120
		size = 20
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		init_effect = { 
			prevent_anomaly = yes
			random_country = {
				limit = { has_country_flag = undine_vanguard }
				save_global_event_target_as = undine_vanguard
				add_appropriate_start_techs = yes
				species = { save_global_event_target_as = undineSpecies }
			}
			set_owner = event_target:undine_vanguard
			set_capital = yes
			generate_starting_pops = { pops_species_1 = 15 }
			generate_major_empire_start_fleets = yes
			set_name = "Limin"
		}
	}
	planet = {
		class = pc_coral_asteroid
		name = "Ormin"
		orbit_distance = 10
		orbit_angle = 120
		size = 20
		deposit_blockers = none
		modifiers = none
		init_effect = { 
			prevent_anomaly = yes
			if = {
				limit = { exists = event_target:undine_vanguard exists = event_target:undineSpecies }
				create_colony = { owner = event_target:undine_vanguard species = event_target:undineSpecies ethos = owner }
				set_owner = event_target:undine_vanguard
				generate_starting_pops = { pops_species_1 = 10 }
			}
			set_name = "Ormin"
		}
	}
	planet = {
		class = pc_coral_asteroid
		name = "Schima"
		orbit_distance = 10
		orbit_angle = 120
		size = 20
		deposit_blockers = none
		modifiers = none
		init_effect = { 
			prevent_anomaly = yes
			if = {
				limit = { exists = event_target:undine_vanguard exists = event_target:undineSpecies }
				create_colony = { owner = event_target:undine_vanguard species = event_target:undineSpecies ethos = owner }
				set_owner = event_target:undine_vanguard
				generate_starting_pops = { pops_species_1 = 10 }
			}
			set_name = "Schima"
		}
	}
}
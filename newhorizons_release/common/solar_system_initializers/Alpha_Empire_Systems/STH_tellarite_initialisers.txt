@distance = 30
@base_moon_distance = 10

#Tellar
tellarian_homeworld = {
	name = "61 Cygni"
	class = "sc_binary_kk"
	flags = { tellarian_homeworld }
	usage = custom_empire
	max_instances = 1
	planet = { name = "Tellar" class = star orbit_distance = 0 orbit_angle = 1 size = 30 has_ring = no }
	planet = {
		count = 3
		# class = rl_inner_unhabitable_planets
		orbit_distance = 30
		orbit_angle = { min = 90 max = 270 }
		size = 10
		moon = { count = 1 class = rl_inner_moons orbit_angle = { min = 90 max = 270 } orbit_distance = 7 size = 3 }
	}
	planet = {
		name = "Tellar Prime"
		class = pc_continental
		flags = { planet_tellar }
		orbit_distance = 25
		orbit_angle = 220
		size = 18
		starting_planet = yes
		has_ring = no
		deposit_blockers = none
		modifiers = none
		init_effect = { 
			prevent_anomaly = yes
			if = {
				limit = { NOT = { any_country = { has_country_flag = tellarian_technocracy } } }
				create_species = {
				    name = "Tellarite"
				    class = TEL
				    portrait = tellarite
				    homeworld = THIS
					namelist = "Tellarite"
				    traits = {
				        trait="trait_charismatic"
	                    trait="trait_communal"
	                    trait="trait_natural_engineers"
	                    trait="trait_slow_breeders"
				        ideal_planet_class = "pc_continental"
					} 
				}
				last_created_species = { save_global_event_target_as = tellariteSpecies }
				create_country = {
					name = "NAME_tellarian_technocracy"
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_stubborn_pride" civic = "civic_cyclic_arguments" }
					origin="origin_galactic_explorers"
					authority = auth_democratic
					name_list = "Tellarite"
					ethos = { ethic = "ethic_xenophile" ethic = "ethic_fanatic_materialist" }
					species = event_target:tellariteSpecies
					flag = {
						icon = { category = "trek" file = "tellarite.dds" }
						background = { category = "backgrounds" file = "diamond.dds" }
						colors = { "customcolor1275" "customcolor221" "null" "null" }
					}
					ship_prefix = "TSC"
					effect = {
						set_graphical_culture = tellarite_01
						set_country_flag = tellarian_technocracy
						set_country_flag = custom_start_screen
						set_country_flag = federation_founder
						set_country_flag = generic_ent
						set_country_flag = alpha_beta_empire
						set_country_flag = init_spawned
						set_country_flag = first_alien_life
						set_country_flag = first_contact_event
						set_country_flag = sth_medium_galaxy
						set_country_flag = botf_minor
						save_global_event_target_as = tellarian_technocracy
					}
				}
				set_owner = event_target:tellarian_technocracy
			}
			add_modifier = { modifier = wet_alpine_world days = -1 }
			set_capital = yes
			random_country = {
				limit = { has_country_flag = tellarian_technocracy }
				save_global_event_target_as = tellarian_technocracy
				add_appropriate_start_techs = yes
				give_technology = { tech = "tech_physics_01351" message = no }
				species = { save_global_event_target_as = tellariteSpecies }
            }
			set_owner = event_target:tellarian_technocracy
			generate_starting_pops = { pops_species_1 = 21 }
			generate_start_buildings = yes
			generate_major_empire_start_fleets = yes
			set_name = "Tellar Prime"
		}
		moon = { name = "Kera" class = "pc_barren" size = 7 orbit_angle = { min = 90 max = 270 } orbit_distance = 7 }
		moon = { name = "Phinda" class = "pc_barren" size = 6 orbit_angle = { min = 90 max = 270 } orbit_distance = 7 }
	}
	planet = {
		count = 1
		# class = rl_outer_unhabitable_planets
		orbit_distance = 20
		orbit_angle = { min = 90 max = 270 }
		size = 12
	}
	planet = {
		count = 1
		# class = rl_gas_giants
		orbit_distance = 20
		orbit_angle = { min = 90 max = 270 }
		size = 20
		moon = { count = 1 class = rl_outer_moons orbit_angle = { min = 90 max = 270 } orbit_distance = 15 size = 3 }
	}
	change_orbit = 25
	planet = { name = "Tellamarkus" class = "pc_arctic" orbit_distance = 20 orbit_angle = { min = 90 max = 270 } size = 10 }
	change_orbit = 25
	planet = { name = "61 Cygni B" class = star flags = { secondaryStar } orbit_distance = 0 orbit_angle = { min = 90 max = 270 } size = 15 has_ring = no }
}
#Axanar Homeworld
#TODO make empire homeworld type
axanar_homeworld = {
	name = "Epsilon Eridani"									
	class = sc_k									
	planet = { name = "Delta Orcus" count = 1 class = star orbit_distance = 0 orbit_angle = 1 size = 33 has_ring = no }									
	planet = {		
		name = "Axanar"	
		class = "pc_continental"	
		orbit_distance = 40	
		size = { min = 12 max = 14 }
		has_ring = no	
		deposit_blockers = none
		flags = { sth_pcm } 
		modifiers = none		
		init_effect = {
			prevent_anomaly = yes
			random_list = {							
				50 = {
					create_species = {
						class = "AXA"
						portrait = "axanari"
						homeworld = THIS
						traits = { trait = random_traits }						
					}
					last_created_species = { save_global_event_target_as = axanariSpecies }
					create_country = {
						name = "Republic of Axanar"
						civics = { civic = civic_increasing_urbanization civic = civic_atmospheric_pollution }
						origin = "origin_militaristic"
						authority = auth_democratic
						species = event_target:axanariSpecies
						ethos = random
						type = primitive
						effect = {
							set_graphical_culture = industrial_01
							set_country_flag = warpcapable_age
							set_primitive_age = warpcapable_age
							save_global_event_target_as = republic_of_axanar
						}
					}
					set_owner = event_target:republic_of_axanar
					create_fleet = { 
						name = "Space Station"
						effect = {
							set_owner = event_target:republic_of_axanar
							create_ship = { name = "Space Station" design = "NAME_Space_Station" }
							set_location = PREV
						}
					}
					generate_starting_pops = { pops_species_1 = 6 }
					add_building = building_primitive_factory
					add_building = building_primitive_capital
					set_name = "Axanar"
				}
				50 = {}
			}
			add_modifier = { modifier = wet_high_jungle_world days = -1 }
		}
	}		
	planet = {
		name = "Delta Orcus II"
		# class = rl_outer_unhabitable_planets								
		orbit_distance = { min = 10 max = 20 }								
		size = { min = 5 max = 16 }
	}									
	planet = {		
		name = "Delta Orcus III"
		class = "pc_gas_giant"								
		orbit_distance = 50 								
		change_orbit = @base_moon_distance		
		size = 25
		has_ring = yes
		moon = { name = "Delta Orcus IIIa" orbit_angle = { min = 90 max = 270 } orbit_distance = { min = 7 max = 10 } }
		moon = { name = "Delta Orcus IIIb" orbit_angle = { min = 90 max = 270 } orbit_distance = { min = 7 max = 10 } }										
	}									
}
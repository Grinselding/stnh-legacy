@distance = 30
@base_moon_distance = 10

# Denobulan Homeworld
denobulan_homeworld = {
	name = "Iota Bootis"
	class = sc_trinary_gff
	flags = { denobulan_homeworld }
	usage = custom_empire
	max_instances = 1
	planet = {
		name = "Denobula Triaxa A"
		class = star size = 41
		orbit_distance = 150
		orbit_angle = 0
		has_ring = no

		planet = {
			name = "Denobula"
			class = "pc_continental"
			size = 18
			starting_planet = yes
			orbit_distance = 65
			orbit_angle = 185
			deposit_blockers = none
			modifiers = none
			has_ring = no
			flags = { planet_denobula }
			init_effect = {
				prevent_anomaly = yes			
				if = {
					limit = { NOT = { any_country = { has_country_flag = denobulan_unity } } }
					create_species = { 
						name = "Denobulan"
						class = DEN
						portrait = denobulan
						homeworld = THIS
						namelist = "Denobulan"
						traits = {
							trait="trait_quick_learners"
							trait="trait_charismatic"
							trait="trait_complex_family_structures"
							trait="trait_slow_breeders"
							ideal_planet_class = "pc_continental"
						}
					}
					last_created_species = { save_global_event_target_as = denobulanSpecies }
					create_country = {
						name = "NAME_denobulan_unity"
						type = default
						ignore_initial_colony_error = yes
						civics = { civic = "civic_philomaths" civic = "civic_beacon_of_liberty" }
						origin="origin_galactic_explorers"
						authority = auth_democratic
						name_list = "Denobulan"
						ethos = { ethic = "ethic_xenophile" ethic = "ethic_egalitarian" ethic = "ethic_pacifist" }
						species = event_target:denobulanSpecies
						flag = {
							icon = { category = "trek" file = "denobula.dds" }
							background = { category = "backgrounds" file = "double_hemispheres.dds" }
							colors = { "customcolor1600" "customcolor223" "null" "null" }
						}
						ship_prefix=""
						effect = {
							set_graphical_culture = generic_01
							set_country_flag = denobulan_unity
							set_country_flag = custom_start_screen
							set_country_flag = generic_ent
							set_country_flag = alpha_beta_empire
							set_country_flag = init_spawned
							set_country_flag = botf_minor
							save_global_event_target_as = denobulan_unity
						}
					}
					set_owner = event_target:denobulan_unity
				}
				add_modifier = { modifier = wet_praire_world days = -1 }
				set_capital = yes
				random_country = {
					limit = { has_country_flag = denobulan_unity }
					save_global_event_target_as = denobulan_unity
					species = { save_global_event_target_as = denobulanSpecies }
					add_appropriate_start_techs = yes
					give_technology = { tech = "tech_alien_life_studies" message = no }
					give_technology = { tech = "tech_society_02313" message = no }
					set_country_type = minorRace
				}
				set_owner = event_target:denobulan_unity
				generate_starting_pops = { pops_species_1 = 22 }
				add_deposit = d_denobula_kaybin
				add_deposit = d_tribbles_deposit
				add_deposit = d_medicinial_herbs
				generate_start_buildings = yes
				add_building = building_xeno_zoo_denobulan
				generate_major_empire_start_fleets = yes
				set_name = "Denobula"
			}
		}		
	}

	change_orbit = -150

	planet = { name = "Denobula Triaxa B" class = star size = 32 orbit_distance = 150 orbit_angle = -120 has_ring = no }

	change_orbit = -150

	planet = { name = "Denobula Triaxa C" class = star size = 21 orbit_distance = 150 orbit_angle = -120 has_ring = no }
}

### Cybernetic Imperative - for Borg Collective
ethic_cybernetic_consciousness = {
	cost = 1
	category = "hive"
	category_value = 0
	use_for_pops = no
	
	country_modifier = {
    	pop_government_ethic_attraction = 0.25
	}
	
	tags = {
	}
	country_attraction = {
		value = 2
		modifier = {
			factor = 0
			NOR = { has_trait = trait_synaptic_processors }
		}
	}
	pop_attraction = {
		value = 1
		modifier = {
			factor = 0
			NOR = { has_trait = trait_synaptic_processors }
		}
	}
	random_weight = {
		value = 0
		modifier = {
			factor = 0
			OR = { has_global_flag = game_started }
		}
	}
}

### Cybernetic Consensus - for Borg Cooperative / Unimatrix Zero / Bynar etc.
ethic_cybernetic_consensus = {
	cost = 1
	category = "hive"
	category_value = 1
	use_for_pops = no
	
	country_modifier = {
    	pop_government_ethic_attraction = 0.25
	}
	
	tags = {
	}
	country_attraction = {
		value = 2
		modifier = {
			factor = 0
			NOR = { has_trait = trait_synaptic_processors }
		}
	}
	pop_attraction = {
		value = 1
		modifier = {
			factor = 0
			NOR = { has_trait = trait_synaptic_processors }
		}
	}
	random_weight = {
		value = 0
		modifier = {
			factor = 0
			OR = { has_global_flag = game_started }
		}
	}
}

ethic_great_link = {
	cost = 1
	category = "hive"
	category_value = 0
	use_for_pops = no
	
	country_modifier = {
    	pop_government_ethic_attraction = 0.25
	}
	
	country_attraction = {
		value = 2
		modifier = {
			factor = 0
			NOR = { has_trait = trait_shapeshifter }
		}
	}
	pop_attraction = {
		value = 1
		modifier = {
			factor = 0
			NOR = { has_trait = trait_shapeshifter }
		}
	}
	random_weight = {
		value = 0
		modifier = {
			factor = 0
			OR = { has_global_flag = game_started }
		}
	}
}


ethic_psionic_consensus = {
	cost = 1
	category = "hive"
	category_value = 2
	use_for_pops = no
	country_modifier = {
    	pop_government_ethic_attraction = 0.25
	}
	random_weight = {
		value = 0
	}
}



ethic_genocidal = {
	cost = 1
	category = "xen"
	category_value = 0
	use_for_pops = no
	
		
	random_weight = {
		value = 0
	}
	
	country_attraction = {
		value = 1
		modifier = {
			factor = 0
			NOR = {
				has_country_flag = the_borg_collective
			}
		}
	}
	pop_attraction = {
		value = 1		
		modifier = {
			factor = 0
			from = {
				exists = owner
				owner = { has_country_flag = the_borg_collective }
			}
		}	
	}
}

ethic_fanatic_genocidal = {
	cost = 2
	category = "xen"
	category_value = 1
	use_for_pops = no
	
	random_weight = {
		value = 0
	}
}

ethic_neutral = {
	cost = 3
	category = "mil"
	category_value = 2
	use_for_pops = no
	random_weight = {
		value = 0
	}
}

# ethic_fanatic_extermination = {
	# cost = 1
	# category = "hive"
	# category_value = 1
	# use_for_pops = no
	
	# random_weight = {
		# value = 0
	# }
# }

ethic_ancient_gestalt_consciousness = {
	cost = 3
	category = "hive"
	category_value = 0
	use_for_pops = no
	
	country_modifier = {
		country_war_exhaustion_mult = -0.2
		country_base_influence_produces_add = 1
	}
	
	tags = {
		ETHIC_GESTALT_CONSCIOUSNESS_NO_TUTORIAL
		ETHIC_GESTALT_CONSCIOUSNESS_AUTHORITY
		ETHIC_GESTALT_CONSCIOUSNESS_IMMORTAL_RULER
		ETHIC_GESTALT_CONSCIOUSNESS_NO_FACTIONS
		ETHIC_GESTALT_CONSCIOUSNESS_DOMESTIC_POP_SURVIVAL
		ETHIC_ALLOW_NO_RETREAT	
	}
	
	random_weight = {
		value = 100
		modifier = {
			factor = 0
				has_global_flag = game_started # additional traits (trait_hive_mind, trait_machine_unit) are only assigned and verified for empire designs, no effect after game start
		}
		modifier = {
			factor = 2
			NOT = { has_global_flag = game_started }
		}
	}
}
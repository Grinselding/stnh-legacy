# from = country
# this = species

migration_control_yes = {
	token = migration_control_yes
	
	ai_will_do = {
		factor = 2
		modifier = {
			factor = 0.1
			from = { is_any_hive_species = no }		
		}
	}
}

migration_control_no = {
	token = migration_control_no

	potential = {
		exists = from
		from = { is_any_hive_species = no }
	}
	
	allow = {
		hidden_trigger = { exists = from }
		custom_tooltip = {
			NOR = {
				has_citizenship_type = { country = from type = citizenship_purge_machine }
				has_citizenship_type = { country = from type = citizenship_purge }
			}
			text = not_undesirables_tooltip
		}
		NAND = {
			has_citizenship_type = { country = from type = citizenship_slavery }
			OR = {
				has_slavery_type = { country = from type = slavery_livestock }
				has_slavery_type = { country = from type = slavery_matrix }
			}
		}
	}
	ai_will_do = {
		factor = 1
	}
}

population_control_yes = {
	token = population_control_yes
	
	pop_modifier = {
		pop_happiness = -0.1
	}

	allow = {
		hidden_trigger = { exists = from }
		from = {
			NOT = { has_policy_flag = population_controls_not_allowed }
		}
		if = { 
			limit = {
				OR = {
					has_citizenship_type = { country = from type = citizenship_purge }
					has_citizenship_type = { country = from type = citizenship_purge_machine }
				}			
			}
			always = yes
		}
		else = {
			custom_tooltip = {
				fail_text = same_species_disallowed
				NOT = { is_same_species = from }
			}
			custom_tooltip = {
				fail_text = empire_xenophile
				from = {
					NOR = {
						has_ethic = ethic_xenophile
						has_ethic = ethic_fanatic_xenophile
					}
				}
			}		
		}				
	}
	ai_will_do = {
		factor = 2
		modifier = {
			factor = 0
			NOR = {
				has_citizenship_type = { country = from type = citizenship_purge_machine }
				has_citizenship_type = { country = from type = citizenship_purge }
			}
		}
	}
}

population_control_no = {
	token = population_control_no

	potential = {
		always = yes
	}

	allow = {
		hidden_trigger = { exists = from }
		custom_tooltip = {
			NOR = {
				has_citizenship_type = { country = from type = citizenship_purge_machine }
				has_citizenship_type = { country = from type = citizenship_purge }
			}
			text = not_undesirables_tooltip
		}
		custom_tooltip = {
			fail_text = "HIVE_MIND_SPECIES_CITIZENS_ONLY"
			OR = {
				NOT = { from = { has_authority = auth_hive_mind } }
				has_trait = trait_hive_mind
			}
		}
	}
	ai_will_do = {
		factor = 1
	}
}

### This means you CAN'T colonize. Must be super optional
colonization_control_yes = {
	token = colonization_control_yes
	
	potential = {
		exists = from
		from = { is_assimilator = no }
		NAND = {
			from = { is_drone_authority = yes }
			OR = {
				has_trait = trait_mechanical
				has_trait = trait_machine_unit
				has_trait = trait_pc_assimilated_preference
			}
		}
	}

	allow = {
		hidden_trigger = { exists = from }
		custom_tooltip = {
			fail_text = is_slaves_not_battle_thralls
			NOR = {
				has_citizenship_type = { country = from type = citizenship_purge }
				has_citizenship_type = { country = from type = citizenship_purge_machine }
				has_citizenship_type = { country = from type = citizenship_slavery }
				has_citizenship_type = { country = from type = citizenship_robot_servitude }
			}
			
		}
		custom_tooltip = {
			fail_text = same_species_disallowed
			NOT = { is_same_species = from }
		}
	}

	ai_will_do = {
		factor = 1
		modifier = {
			factor = 100
				OR = { 
					has_trait = trait_shapeshifter
					has_trait = trait_ketracel_white_addiction
				}
		}
	}
}

### This means you CAN colonize. Must be the default
colonization_control_no = {
	token = colonization_control_no

	potential = {
		from = { is_assimilator = no }
		NAND = {
			from = { is_drone_authority = yes }
			OR = {
				has_trait = trait_mechanical
				has_trait = trait_machine_unit
				has_trait = trait_pc_assimilated_preference
			}
		}
	}

	allow = {
		hidden_trigger = { exists = from }
		custom_tooltip = {
			fail_text = not_undesirables_tooltip
			NOR = {
				has_citizenship_type = { country = from type = citizenship_purge_machine }
				has_citizenship_type = { country = from type = citizenship_purge }
			}
		}
		custom_tooltip = {
			fail_text = "HIVE_MIND_SPECIES_CITIZENS_ONLY"
			OR = {
				NOT = { from = { is_any_hive_species = yes } }
				has_trait = trait_hive_mind
			}
			is_any_mechanical_species = no
		}
		custom_tooltip = {
			fail_text = "SPECIES_CANNOT_COLONIZE"		
			NOR = {
				has_trait = trait_syncretic_proles
				has_trait = trait_nerve_stapled
				has_trait = trait_shapeshifter
				has_trait = trait_ketracel_white_addiction
			}	
		}
		custom_tooltip = {
			fail_text = is_slaves_not_battle_thralls
			NAND = {
				has_citizenship_type = { country = from type = citizenship_slavery }
				NOT = { has_slavery_type = { country = from type = slavery_military } }
			}
		}			
	}
	ai_will_do = {
		factor = 2
		modifier = {
			factor = 100
			is_species_class = VOR
			from = { 
				is_species_class = DOM
			}
		}
		modifier = {
			factor = 100
			is_xindi = yes
			from = { is_xindi = yes }
		}		
	}
}
### Planet Modifiers

pm_miniature_population = {
	spawn_chance = {
		#None - From anomaly 
	}
	modifier = "miniature_population"
}
pm_mars_colony = {
	spawn_chance = { }
	modifier = "mars_colony"
}

pm_rure_penthe_colony = {
	spawn_chance = { }
	modifier = "rure_colony"
}

pm_boreth_colony = {
	spawn_chance = { }
	modifier = "boreth_colony"
}

pm_surface_impact = {
	spawn_chance = {  }
	modifier = "surface_impact"
}

pm_janus_vi = {
	spawn_chance = {  }
	modifier = "janus_colony"
}

pm_iconia = {
	spawn_chance = {  }
	modifier = "iconia"
}

pm_pjem_colony = {
	spawn_chance = { }
	modifier = "pjem_colony"
}

###### Disabled as it was giving double modifiers to a number of planets - replaced by sth_start.90-93 ####

####################################
### Subclass planetary modifiers ###
####################################

### Class M (Swamp) = Ferenginar, Bolus
   # pm_wet_swamp_world = {
      # spawn_chance = {
         # modifier = { add = 100 is_planet_class = "pc_continental" }
         # modifier = { factor = 0 is_wet_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "wet_swamp_world"
   # }
   
## Class M (Alpine)
   # pm_wet_alpine_world = {
      # spawn_chance = {
         # modifier = { add = 100 is_planet_class = "pc_continental" }
         # modifier = { factor = 0 is_wet_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "wet_alpine_world"
   # }
   
## Class M (Tropical) = Q'unos
   # pm_wet_tropical_world = {
      # spawn_chance = {
         # modifier = { add = 100 is_planet_class = "pc_continental" }
         # modifier = { factor = 0 is_wet_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "wet_tropical_world"
   # }
   
## Class Q (Optimal)
   # pm_wet_gaia_world = {
      # spawn_chance = {
         # modifier = { add = 1 is_planet_class = "pc_continental" }
         # modifier = { factor = 0 is_wet_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "wet_gaia_world"
   # }
   
## Class O = Antedians, Benzite
   # pm_wet_ocean_world = {
      # spawn_chance = {
         # modifier = { add = 100 is_planet_class = "pc_continental" }
         # modifier = { factor = 0 is_wet_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "wet_ocean_world"
   # }
   
## Class Q (Variable)
   # pm_wet_super_habitable_world = {
      # spawn_chance = {
         # modifier = { add = 20 is_planet_class = "pc_continental" }
         # modifier = { factor = 0 is_wet_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "wet_super_habitable_world"
   # }
   
## Class M (Retinal)
   # pm_wet_retinal_world = {
      # spawn_chance = {
         # modifier = { add = 20 is_planet_class = "pc_continental" }
         # modifier = { factor = 0 is_wet_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "wet_retinal_world"
   # }
   
## Class M (Praire)
   # pm_wet_praire_world = {
      # spawn_chance = {
         # modifier = { add = 50 is_planet_class = "pc_continental" }
         # modifier = { factor = 0 is_wet_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "wet_praire_world"
   # }
   
## Class Q (Fungal)
   # pm_wet_mushroom_world = {
      # spawn_chance = {
         # modifier = { add = 20 is_planet_class = "pc_continental" }
         # modifier = { factor = 0 is_wet_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "wet_mushroom_world"
   # }
   
## Class M (Karst)
   # pm_wet_karst_world = {
      # spawn_chance = {
         # modifier = { add = 20 is_planet_class = "pc_continental" }
         # modifier = { factor = 0 is_wet_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "wet_karst_world"
   # }
   
## Class M (Sub-tropic)
   # pm_wet_high_jungle_world = {
      # spawn_chance = {
         # modifier = { add = 60 is_planet_class = "pc_continental" }
         # modifier = { factor = 0 is_wet_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "wet_high_jungle_world"
   # }
   
## Class R (Rogue) = Individually assigned
   # pm_wet_rogue_world = {
      # spawn_chance = {
         # modifier = { add = 0 is_planet_class = "pc_continental" }
         # modifier = { factor = 0 is_wet_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "wet_rogue_world"
   # }
   
## Class X (Tidal) = Remus
   # pm_wet_tidal_world = {
      # spawn_chance = {
         # modifier = { add = 1 is_planet_class = "pc_continental" }
         # modifier = { factor = 0 is_wet_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "wet_tidal_world"
   # }
   
## Class M (Steppe)
   # pm_wet_steppe_world = {
      # spawn_chance = {
         # modifier = { add = 20 is_planet_class = "pc_continental" }
         # modifier = { factor = 0 is_wet_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "wet_steppe_world"
   # }
   
## Class P (Subarctic)
   # pm_cold_subarctic_world = {
      # spawn_chance = {
         # modifier = { add = 100 is_planet_class = "pc_arctic" }
         # modifier = { factor = 0 is_cold_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "cold_subarctic_world"
   # }
   
## Class P (Antartic)
   # pm_cold_antartic_world = {
      # spawn_chance = {
         # modifier = { add = 20 is_planet_class = "pc_arctic" }
         # modifier = { factor = 0 is_cold_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "cold_antartic_world"
   # }
   
## Class P (Snow-Laden)
   # pm_cold_snow_world = {
      # spawn_chance = {
         # modifier = { add = 100 is_planet_class = "pc_arctic" }
         # modifier = { factor = 0 is_cold_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "cold_snow_world"
   # }
   
## Class M (Bog)
   # pm_cold_bog_world = {
      # spawn_chance = {
         # modifier = { add = 40 is_planet_class = "pc_arctic" }
         # modifier = { factor = 0 is_cold_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "cold_bog_world"
   # }
   
## Class M (Tundra)
   # pm_cold_tundra_world = {
      # spawn_chance = {
         # modifier = { add = 30 is_planet_class = "pc_arctic" }
         # modifier = { factor = 0 is_cold_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "cold_tundra_world"
   # }
   
## Class M (Frost Desert)
   # pm_cold_high_desert_world = {
      # spawn_chance = {
         # modifier = { add = 20 is_planet_class = "pc_arctic" }
         # modifier = { factor = 0 is_cold_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "cold_high_desert_world"
   # }
   
## Class Q (Geothermal)
   # pm_cold_geothermal_world = {
      # spawn_chance = {
         # modifier = { add = 20 is_planet_class = "pc_arctic" }
         # modifier = { factor = 0 is_cold_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "cold_geothermal_world"
   # }
   
## Class M (Arid)
   # pm_hot_arid_world = {
      # spawn_chance = {
         # modifier = { add = 100 is_planet_class = "pc_desert" }
         # modifier = { factor = 0 is_hot_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "hot_arid_world"
   # }
   
## Class H (Sandsea)
   # pm_hot_dune_world = {
      # spawn_chance = {
         # modifier = { add = 40 is_planet_class = "pc_desert" }
         # modifier = { factor = 0 is_hot_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "hot_dune_world"
   # }
   
## Class M (Savannah)
   # pm_hot_savannah_world = {
      # spawn_chance = {
         # modifier = { add = 100 is_planet_class = "pc_desert" }
         # modifier = { factor = 0 is_hot_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "hot_savannah_world"
   # }
   
## Class M (Oasis)
   # pm_hot_oasis_world = {
      # spawn_chance = {
         # modifier = { add = 40 is_planet_class = "pc_desert" }
         # modifier = { factor = 0 is_hot_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "hot_oasis_world"
   # }
   
## Class O (Desert Islands)
   # pm_hot_desert_island_world = {
      # spawn_chance = {
         # modifier = { add = 50 is_planet_class = "pc_desert" }
         # modifier = { factor = 0 is_hot_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "hot_desert_island_world"
   # }
   
## Class M (Mesa)
   # pm_hot_mesa_world = {
      # spawn_chance = {
         # modifier = { add = 50 is_planet_class = "pc_desert" }
         # modifier = { factor = 0 is_hot_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "hot_mesa_world"
   # }
   
## Class M (Mediterranean)
   # pm_hot_med_world = {
      # spawn_chance = {
         # modifier = { add = 50 is_planet_class = "pc_desert" }
         # modifier = { factor = 0 is_hot_world_subclass = yes }
         # modifier = { factor = 0 num_modifiers > 2 }
      # }
      # modifier = "hot_med_world"
   # }
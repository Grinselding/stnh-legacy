ufp_member_world = {
	has_difficulty_bonuses = yes
	counts_for_victory = yes
	pop_growth = yes
	playable = yes
	shuffle_pop_ethos = yes
	needs_survey = yes
	diplomatic_wars = yes
	ship_disengagement = yes
	army_disengagement = yes
	relations = yes
	share_communications = no	
	sub_title = "Federation Member World"
	observable = yes
	can_use_invalid_technology = no
	has_pulse_events = yes
	ai = {
		enabled = yes
		declare_war = no
		colonizer = no
		internal_policies = yes
		modification = yes
		government = yes
		traditions = yes
		construction = yes
		follow = yes
		can_use_market = yes
		modules = {
			military_minister_module
			interior_minister_module
			foreign_minister_module
		}
		ship_data = {
			constructor = { min = 0 max = 1 }
			starbase_outpost = { min = 0 max = 0 }
			science	= { min = 1 max = 1 }
			colonizer = { min = 0 max = 0 }
			assault_cruiser= {
				fraction = {
					modifier = { add = 15 has_technology = tech_engineering_assault_546 }
				}
			}
			strike= {
				fraction = {
					modifier = { add = 10 has_technology = tech_engineering_heavy_545 }
					modifier = { add = -2 has_technology = tech_engineering_assault_546 }
				}
			}
			adv_cruiser= {
				fraction = {
					modifier = { add = 30 has_technology = tech_engineering_battleship_544 }
					modifier = { add = -3 has_technology = tech_engineering_heavy_545 }
					modifier = { add = -4 has_technology = tech_engineering_assault_546 }
				}
			}
			steamrunner= {
				fraction = {
					modifier = { add = 14 has_technology = tech_engineering_battlecruiser_543 }
					modifier = { add = -4 has_technology = tech_engineering_battleship_544 }
					modifier = { add = -1 has_technology = tech_engineering_heavy_545 }
					modifier = { add = -1 has_technology = tech_engineering_assault_546 }
				}
			}
			sovereign= {
				fraction = {
					modifier = { add = 50 has_technology = tech_engineering_cruiser_542 }
					modifier = { add = -7 has_technology = tech_engineering_battlecruiser_543 }
					modifier = { add = -13 has_technology = tech_engineering_battleship_544 }
					modifier = { add = -3 has_technology = tech_engineering_heavy_545 }
					modifier = { add = -4 has_technology = tech_engineering_assault_546 }
				}
			}
			saber= {
				fraction = {
					modifier = { add = 33 has_technology = tech_engineering_destroyer_541 }
					modifier = { add = -16 has_technology = tech_engineering_cruiser_542 }
					modifier = { add = -3 has_technology = tech_engineering_battlecruiser_543 }
					modifier = { add = -4 has_technology = tech_engineering_battleship_544 }
					modifier = { add = -1 has_technology = tech_engineering_heavy_545 }
					modifier = { add = -1 has_technology = tech_engineering_assault_546 }
				}
			}
			corvette= {
				fraction = {
					modifier = { add = 100 has_technology = tech_patrol_frigate NOR = { is_borg_empire = yes has_technology = tech_emette_frigate has_technology = tech_rea_prey has_technology = tech_borghel_prey } }
					modifier = { add = -33 has_technology = tech_engineering_destroyer_541  }
					modifier = { add = -34 has_technology = tech_engineering_cruiser_542  }
					modifier = { add = -4 has_technology = tech_engineering_battlecruiser_543  }
					modifier = { add = -9 has_technology = tech_engineering_battleship_544  }
					modifier = { add = -2 has_technology = tech_engineering_heavy_545  }
					modifier = { add = -3 has_technology = tech_engineering_assault_546  }
				}
			}
			
			
			### Borg ships
			
			borg_probe= {
				fraction = {
					base = 100
					modifier = { add = -33 has_technology = tech_engineering_02931 }
					modifier = { add = -17 has_technology = tech_engineering_02892 }
					modifier = { add = -17 has_technology = tech_engineering_industry_1251 }
					modifier = { add = -4 has_technology = tech_engineering_industry_1252 }
					modifier = { add = -9 has_technology = tech_engineering_industry_1253 }
					modifier = { add = -4 has_technology = tech_engineering_industry_1254 }
					modifier = { add = -100 is_borg_empire = no }
				}
			}
				
			borg_scout= {
				fraction = {
					modifier = { add = 33 has_technology = tech_engineering_02931 }
					modifier = { add = -8 has_technology = tech_engineering_02892 }
					modifier = { add = -8 has_technology = tech_engineering_industry_1251 }
					modifier = { add = -3 has_technology = tech_engineering_industry_1252 }
					modifier = { add = -4 has_technology = tech_engineering_industry_1253 }
					modifier = { add = -2 has_technology = tech_engineering_industry_1254 }
				}
			}
				
			borg_pyramid= {
				fraction = {
					modifier = { add = 25 has_technology = tech_engineering_02892 }
					modifier = { add = -8 has_technology = tech_engineering_industry_1251 }
					modifier = { add = -3 has_technology = tech_engineering_industry_1252 }
					modifier = { add = -4 has_technology = tech_engineering_industry_1253 }
					modifier = { add = -2 has_technology = tech_engineering_industry_1254 }
				}
			}
				
			borg_sphere= {
				fraction = {
					modifier = { add = 33 has_technology = tech_engineering_industry_1251 }
					modifier = { add = -4 has_technology = tech_engineering_industry_1252 }
					modifier = { add = -9 has_technology = tech_engineering_industry_1253 }
					modifier = { add = -4 has_technology = tech_engineering_industry_1254 }
				}
			}
				
			borg_diamond= {
				fraction = {
					modifier = { add = 14 has_technology = tech_engineering_industry_1252 }
					modifier = { add = -4 has_technology = tech_engineering_industry_1253 }
					modifier = { add = -2 has_technology = tech_engineering_industry_1254 }
				}
			}
				
			borg_cube= {
				fraction = {
					modifier = { add = 30 has_technology = tech_engineering_industry_1253 }
					modifier = { add = -6 has_technology = tech_engineering_industry_1254 }
				}
			}
				
			borg_tactical= {
				fraction = {
					modifier = { add = 19 has_technology = tech_engineering_industry_1254 }
				}
			}
			
			#### Fed ships
			
			fed_frigate_emette = {
			      fraction = {
			         modifier = {
			            add = 100
			            has_technology = tech_emette_frigate
			         }
			         modifier = {
			            add = -50
			            has_technology = tech_engineering_03503
			         }
			         modifier = {
			            add = -16
			            has_technology = tech_engineering_03504
			         }
			         modifier = {
			            add = -9
			            has_technology = tech_engineering_03506
			         }
			         modifier = { # Emette is now obselete
			            add = -25
			            has_technology = tech_engineering_03509
			         }
			      }
			   }
			   fed_frigate_engle = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03509
			         }
			         modifier = { # Engle is now obselete
			            add = -20
			            has_technology = tech_engineering_03514
			         }
			      }
			   }
			   fed_frigate_saladin = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03514
			         }
			         modifier = { # Saladin is now obselete
			            add = -20
			            has_technology = tech_engineering_03516
			         }
			      }
			   }
			   fed_frigate_akula = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03516
			         }
			         modifier = {
			            add = -10
			            has_technology = tech_engineering_03517
			         }
			         modifier = { # Akula is now obselete
			            add = -10
			            has_technology = tech_engineering_13519
			         }
			      }
			   }
			   fed_frigate_challenger = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_13519
			         }
			      }
			   }
			   fed_light_cruiser_intrepid_type = {
			      fraction = {
			         modifier = {
			            add = 50
			            has_technology = tech_engineering_03503
			         }
			         modifier = {
			            add = -17
			            has_technology = tech_engineering_03504
			         }
			         modifier = {
			            add = -8
			            has_technology = tech_engineering_03506
			         }
			         modifier = { # Intrepid_Type is now obselete
			            add = -25
			            has_technology = tech_engineering_03507
			         }
			      }
			   }
			   fed_light_cruiser_bonaventure = {
			      fraction = {
			         modifier = {
			            add = 25
			            has_technology = tech_engineering_03507
			         }
			         modifier = { # Bonaventure is now obselete
			            add = -25
			            has_technology = tech_engineering_03509
			         }
			      }
			   }
			   fed_light_cruiser_shepard = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03509
			         }
			         modifier = { # Shepard is now obselete
			            add = -20
			            has_technology = tech_engineering_03512
			         }
			      }
			   }
			   fed_light_cruiser_kelvin = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03512
			         }
			         modifier = { # Kelvin is now obselete
			            add = -20
			            has_technology = tech_engineering_03516
			         }
			      }
			   }
			   fed_light_cruiser_centaur = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03516
			         }
			         modifier = { # Centaur is now obselete
			            add = -20
			            has_technology = tech_engineering_03517
			         }
			      }
			   }
			   fed_light_cruiser_curry_type = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03517
			         }
			         modifier = { # Curry_type is now obselete
			            add = -20
			            has_technology = tech_engineering_13519
			         }
			      }
			   }
			   fed_light_cruiser_niagara = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_13519
			         }
			         modifier = {
			            add = -10
			            has_technology = tech_engineering_03520
			         }
			         modifier = { # Niagara is now obselete
			            add = -10
			            has_technology = tech_engineering_13523
			         }
			      }
			   }
			   fed_light_cruiser_steamrunner = {
			      fraction = {
			         modifier = {
			            add = 5
			            has_technology = tech_engineering_13523
			         }
			      }
			   }
			   fed_light_cruiser_saber = {
			      fraction = {
			         modifier = {
			            add = 5
			            has_technology = tech_engineering_13523
			         }
			      }
			   }
			   fed_explorer_nx = {
			      fraction = {
			         modifier = {
			            add = 33
			            has_technology = tech_engineering_03504
			         }
			         modifier = {
			            add = -8
			            has_technology = tech_engineering_03506
			         }
			         modifier = { # NX is now obselete
			            add = -25
			            has_technology = tech_engineering_03510
			         }
			      }
			   }
			   fed_explorer_hoover = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_03510
			         }
			         modifier = { # Hoover is now obselete
			            add = -10
			            has_technology = tech_engineering_12508
			         }
			      }
			   }
			   fed_explorer_cardenas = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_03510
			         }
			         modifier = { # Cardenas is now obselete
			            add = -10
			            has_technology = tech_engineering_12508
			         }
			      }
			   }
			   fed_explorer_miranda = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_12508
			         }
			         modifier = { # Miranda is now obselete
			            add = -20
			            has_technology = tech_engineering_13519
			         }
			      }
			   }
			   fed_explorer_new_orleans = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_13519
			         }
			         modifier = { # New_Orleans is now obselete
			            add = -20
			            has_technology = tech_engineering_13522
			         }
			      }
			   }
			   fed_explorer_intrepid_class = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_13522
			         }
			      }
			   }
			   fed_mmv_poseidon = {
			      fraction = {
			         modifier = {
			            add = 25
			            has_technology = tech_engineering_03506
			         }
			         modifier = { # Poseidon is now obselete
			            add = -25
			            has_technology = tech_engineering_03508
			         }
			      }
			   }
			   fed_mmv_walker = {
			      fraction = {
			         modifier = {
			            add = 25
			            has_technology = tech_engineering_03508
			         }
			         modifier = {
			            add = -5
			            has_technology = tech_engineering_03509
			         }
			         modifier = { # Walker is now obselete
			            add = -20
			            has_technology = tech_engineering_03517
			         }
			      }
			   }
			   fed_mmv_constellation = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03517
			         }
			         modifier = { # Constellation is now obselete
			            add = -20
			            has_technology = tech_engineering_03518
			         }
			      }
			   }
			   fed_mmv_nebula = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03518
			         }
			         modifier = { # Nebula is now obselete
			            add = -20
			            has_technology = tech_engineering_13521
			         }
			      }
			   }
			   fed_mmv_akira = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_13521
			         }
			      }
			   }
			   fed_mmv_norway = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_13521
			         }
			      }
			   }
			   fed_heavy_cruiser_nimitz = {
			      fraction = {
			         modifier = {
			            add = 15
			            has_technology = tech_engineering_03509
			         }
			         modifier = {
			            add = -5
			            has_technology = tech_engineering_03510
			         }
			         modifier = { # Nimitz is now obselete
			            add = -10
			            has_technology = tech_engineering_12508
			         }
			      }
			   }
			   fed_heavy_cruiser_ares = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_12508
			         }
			         modifier = { # Ares is now obselete
			            add = -10
			            has_technology = tech_engineering_03513
			         }
			      }
			   }
			   fed_heavy_cruiser_constitution = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_03513
			         }
			         modifier = { # Constitution is now obselete
			            add = -10
			            has_technology = tech_engineering_13522
			         }
			      }
			   }
			   fed_heavy_cruiser_sovereign = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_13522
			         }
			      }
			   }
			   fed_advanced_cruiser_crossfield = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_03510
			         }
			         modifier = { # Crossfield is now obselete
			            add = -10
			            has_technology = tech_engineering_03515
			         }
			      }
			   }
			   fed_advanced_cruiser_excelsior = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_03515
			         }
			         modifier = { # Excelsior is now obselete
			            add = -10
			            has_technology = tech_engineering_03524
			         }
			      }
			   }
			   fed_advanced_cruiser_vesta = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_03524
			         }
			      }
			   }
			   fed_exploration_cruiser_ambassador = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_03517
			         }
			         modifier = { # Ambassador is now obselete
			            add = -10
			            has_technology = tech_engineering_03518
			         }
			      }
			   }
			   fed_exploration_cruiser_galaxy = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_03518
			         }
			      }
			   }
			   fed_heavy_escort_defiant = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_03520
			         }
			         modifier = { # Defiant is now obselete
			            add = -10
			            has_technology = tech_engineering_03524
			         }
			      }
			   }
			   fed_heavy_escort_prometheus = {
			      fraction = {
			         modifier = {
			            add = 10
			            has_technology = tech_engineering_03524
			         }
			      }
			   }
			   #### Klingon ships
			   kdf_bop_borghel = {
			      fraction = {
			         modifier = {
			            add = 100
			            has_technology = tech_borghel_prey
			         }
			         modifier = {
			            add = -50
			            has_technology = tech_engineering_03602
			         }
			         modifier = { # Borghel is now obselete
			            add = -50
			            has_technology = tech_engineering_03604
			         }
			      }
			   }
			   kdf_bop_homcha = {
			      fraction = {
			         modifier = {
			            add = 34
			            has_technology = tech_engineering_03604
			         }
			         modifier = {
			            add = -9
			            has_technology = tech_engineering_03605
			         }
			         modifier = { # HomCha is now obselete
			            add = -25
			            has_technology = tech_engineering_03607
			         }
			      }
			   }
			   kdf_bop_brel = {
			      fraction = {
			         modifier = {
			            add = 25
			            has_technology = tech_engineering_03607
			         }
			         modifier = {
			            add = -5
			            has_technology = tech_engineering_03608
			         }
			         modifier = { # Brel is now obselete
			            add = -20
			            has_technology = tech_engineering_03611
			         }
			      }
			   }
			   kdf_bop_pagh = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03611
			         }
			      }
			   }
			   kdf_raptor_somraw = {
			      fraction = {
			         modifier = {
			            add = 50
			            has_technology = tech_engineering_03602
			         }
			         modifier = { # Somraw is now obselete
			            add = -50
			            has_technology = tech_engineering_03603
			         }
			      }
			   }
			   kdf_raptor_d5 = {
			      fraction = {
			         modifier = {
			            add = 50
			            has_technology = tech_engineering_03603
			         }
			         modifier = { # D5 is now obselete
			            add = -50
			            has_technology = tech_engineering_03604
			         }
			      }
			   }
			   kdf_raptor_jev = {
			      fraction = {
			         modifier = {
			            add = 33
			            has_technology = tech_engineering_03604
			         }
			         modifier = {
			            add = -8
			            has_technology = tech_engineering_03605
			         }
			         modifier = { # Jev is now obselete
			            add = -25
			            has_technology = tech_engineering_03608
			         }
			      }
			   }
			   kdf_raptor_hegh = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03608
			         }
			         modifier = { # Hegh is now obselete
			            add = -20
			            has_technology = tech_engineering_03614
			         }
			      }
			   }
			   kdf_raptor_mehadraw = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03614
			         }
			      }
			   }
			   kdf_raider_d6 = {
			      fraction = {
			         modifier = {
			            add = 33
			            has_technology = tech_engineering_03604
			         }
			         modifier = {
			            add = -8
			            has_technology = tech_engineering_03605
			         }
			         modifier = {
			            add = -5
			            has_technology = tech_engineering_03608
			         }
			         modifier = { # D6 is now obselete
			            add = -20
			            has_technology = tech_engineering_03609
			         }
			      }
			   }
			   kdf_raider_kvort = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03609
			         }
			      }
			   }
			   kdf_battlecruiser_d7 = {
			      fraction = {
			         modifier = {
			            add = 25
			            has_technology = tech_engineering_03605
			         }
			         modifier = { # D7 is now obselete
			            add = -25
			            has_technology = tech_engineering_03606
			         }
			      }
			   }
			   kdf_battlecruiser_ktinga = {
			      fraction = {
			         modifier = {
			            add = 25
			            has_technology = tech_engineering_03606
			         }
			         modifier = { # Ktinga is now obselete
			            add = -25
			            has_technology = tech_engineering_03608
			         }
			      }
			   }
			   kdf_battlecruiser_norgh = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03608
			         }
			         modifier = { # Norgh is now obselete
			            add = -20
			            has_technology = tech_engineering_03610
			         }
			      }
			   }
			   kdf_battlecruiser_vorcha = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03610
			         }
			      }
			   }
			   kdf_warship_roj = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03608
			         }
			         modifier = { # Roj is now obselete
			            add = -20
			            has_technology = tech_engineering_03610
			         }
			      }
			   }
			   kdf_warship_kvek = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03610
			         }
			         modifier = { # Kvek is now obselete
			            add = -20
			            has_technology = tech_engineering_03611
			         }
			      }
			   }
			   kdf_warship_neghvar = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03611
			         }
			      }
			   }
			   #### romulan ships
			   rom_bop_rea = {
			   	    fraction = {

			         modifier = {
			            add = 100
			            has_technology = tech_rea_prey
			         }
			         modifier = {
			            add = -50
			            has_technology = tech_engineering_03653
			         }
			         modifier = {
			            add = -16
			            has_technology = tech_engineering_03654
			         }
			         modifier = {
			            add = -9
			            has_technology = tech_engineering_03655
			         }
			         modifier = { # rom_bop_rea is now obselete
			            add = -25
			            has_technology = tech_engineering_03658
			         }
			      }
			   }
			   rom_bop_stanet = {
			      fraction = {
			         modifier = {
			            add = 25
			            has_technology = tech_engineering_03658
			         }
			         modifier = {
			            add = -5
			            has_technology = tech_engineering_03659
			         }
			         modifier = { # rom_bop_stanet is now obselete
			            add = -20
			            has_technology = tech_engineering_03661
			         }
			      }
			   }
			   rom_bop_jorek = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03661
			         }
			         modifier = { # rom_bop_jorek is now obselete
			            add = -20
			            has_technology = tech_engineering_03663
			         }
			      }
			   }
			   rom_bop_arhael = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03663
			         }
			      }
			   }
			   rom_interceptor_tvaro = {
			      fraction = {
			         modifier = {
			            add = 50
			            has_technology = tech_engineering_03653
			         }
			         modifier = {
			            add = -17
			            has_technology = tech_engineering_03654
			         }
			         modifier = {
			            add = -8
			            has_technology = tech_engineering_03655
			         }
			         modifier = { # rom_interceptor_tvaro is now obselete
			            add = -25
			            has_technology = tech_engineering_03657
			         }
			      }
			   }
			   rom_interceptor_tliss = {
			      fraction = {
			         modifier = {
			            add = 25
			            has_technology = tech_engineering_03657
			         }
			         modifier = {
			            add = -5
			            has_technology = tech_engineering_03659
			         }
			         modifier = { # rom_interceptor_tliss is now obselete
			            add = -20
			            has_technology = tech_engineering_03661
			         }
			      }
			   }
			   rom_interceptor_tmara = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03661
			         }
			         modifier = { # rom_interceptor_tmara is now obselete
			            add = -20
			            has_technology = tech_engineering_03665
			         }
			      }
			   }
			   rom_interceptor_tdair = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03665
			         }
			      }
			   }
			   rom_warbird_tellus = {
			      fraction = {
			         modifier = {
			            add = 33
			            has_technology = tech_engineering_03654
			         }
			         modifier = {
			            add = -8
			            has_technology = tech_engineering_03655
			         }
			         modifier = { # rom_warbird_tellus is now obselete
			            add = -25
			            has_technology = tech_engineering_03656
			         }
			      }
			   }
			   rom_warbird_stasek = {
			      fraction = {
			         modifier = {
			            add = 25
			            has_technology = tech_engineering_03656
			         }
			         modifier = { # rom_warbird_stasek is now obselete
			            add = -25
			            has_technology = tech_engineering_03659
			         }
			      }
			   }
			   rom_warbird_sharien = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03659
			         }
			         modifier = { # rom_warbird_sharien is now obselete
			            add = -20
			            has_technology = tech_engineering_03660
			         }
			      }
			   }
			   rom_warbird_kanassarum = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03660
			         }
			         modifier = { # rom_warbird_kanassarum is now obselete
			            add = -20
			            has_technology = tech_engineering_03663
			         }
			      }
			   }
			   rom_warbird_valdore = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03663
			         }
			      }
			   }
			   rom_starbird_tshen = {
			      fraction = {
			         modifier = {
			            add = 25
			            has_technology = tech_engineering_03655
			         }
			         modifier = { # rom_starbird_tshen is now obselete
			            add = -25
			            has_technology = tech_engineering_03658
			         }
			      }
			   }
			   rom_starbird_debrune = {
			      fraction = {
			         modifier = {
			            add = 25
			            has_technology = tech_engineering_03658
			         }
			         modifier = {
			            add = -5
			            has_technology = tech_engineering_03659
			         }
			         modifier = { # rom_starbird_debrune is now obselete
			            add = -20
			            has_technology = tech_engineering_03660
			         }
			      }
			   }
			   rom_starbird_valkis = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03660
			         }
			         modifier = { # rom_starbird_valkis is now obselete
			            add = -20
			            has_technology = tech_engineering_03663
			         }
			      }
			   }
			   rom_starbird_kaskara = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03663
			         }
			      }
			   }
			   rom_battleship_pontilus = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03659
			         }
			         modifier = { # rom_battleship_pontilus is now obselete
			            add = -20
			            has_technology = tech_engineering_03662
			         }
			      }
			   }
			   rom_battleship_dderidex = {
			      fraction = {
			         modifier = {
			            add = 20
			            has_technology = tech_engineering_03662
			         }
			      }
			      }

		}
		army_data = {
			defense_army = { fraction = { factor = 50 } }
		}
	}
	
	trade_routes_available = {
		is_gestalt = no
	}
	faction = {
		needs_colony = no
		needs_border_access = yes
	}
	modules = {
		standard_event_module = {}
		standard_economy_module = {}
		standard_leader_module = {}
		exclusive_diplomacy_module = {
			can_receive = {
				action_declare_war
				action_offer_peace
				action_offer_trade_deal
				action_cancel_trade_deal
			}
			can_send = {
				action_declare_war
				action_offer_peace
				action_offer_trade_deal
				action_cancel_trade_deal
			}
		}
		standard_technology_module = {}
		standard_trade_routes_module = {}
		standard_species_rights_module = {}
	}
	resources = {
		category = country_base
		produces = { ### Tier 3 empires
			energy = 35
			minerals = 35
			physics_research = 5
            society_research = 5
            engineering_research = 5
			influence = 3
			unity = 5
			alloys = 1
			sr_dilithium = 10 ### Slightly buffed to ensure FED members can maintain fleets
			sr_luxuries = 5
			consumer_goods = 10
		}
		produces = {
			trigger = { has_country_flag = significant_power } ### Tier 2 empires
			energy = 5
			consumer_goods = 5
			alloys = 5
			sr_crew = 5
			sr_dilithium = 5
		}
		produces = {
			trigger = { has_country_flag = major_faction } ### Tier 1 empires - only the 7 major ones, bonus does not stack with tier 2. Have their own sources of extra crew/dilithium
			energy = 10
			consumer_goods = 5
			alloys = 1
		}
		produces = {
			trigger = { is_borg_empire = yes } ### strongest empire, bonus stacks up with tier 1
			energy = 20
			minerals = 20
			consumer_goods = 20
			sr_dilithium = 10
		}
		produces = {
			trigger = { has_country_flag = the_dominion }
			consumer_goods = 20
			
		}
		produces = {
			trigger = { is_organic_species = yes }
			food = 25
			sr_water = 5
		}
		produces = {
			trigger = { AND = { is_organic_species = yes is_nomadic_empire = no } }
			sr_brizeen = 5
		}
		produces = {
			trigger = { is_lithovore_species = yes }
			minerals = 25
		}
		produces = {
			trigger = { is_machine_empire = yes }
			energy = 10
			minerals = 10
			consumer_goods = 40
		}
	}
	
	# planet scope
	branch_office_resources = {
		category = planet_branch_offices
		cost = {
			energy = 1000
			influence = 50
		}
		
		upkeep = {}
		
		produces = {}		
	}
}

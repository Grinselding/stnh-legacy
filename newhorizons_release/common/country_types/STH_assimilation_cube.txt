assimilation_cube = {
	government = no
	uses_origins = no
	observable = no
	ai = {
		enabled = no
	}
	faction = {
		hostile = yes
		needs_border_access = no
		generate_borders = no
		needs_colony = no
		auto_delete = no
	}
	modules = {
		exclusive_diplomacy_module = {} # Nothing is allowed, but we need this for relationships.
		all_technology_module = {}
		standard_event_module = {}
		standard_leader_module = {}
	}
}
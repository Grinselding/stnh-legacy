cb_house_feud = {
	show_in_diplomacy = no
	show_notification = no
	potential = {
		always = no
	}
	is_valid = {
		always = no
	}
}

cb_civil_war_rebel = {
	show_in_diplomacy = no
	show_notification = no
	potential = {
		always = no
	}
	is_valid = {
		always = no
	}
}

cb_civil_war_loyalist = {
	show_in_diplomacy = no
	show_notification = no
	potential = {
		always = no
	}
	is_valid = {
		always = no
	}
}

cb_total_conquest = {
	show_in_diplomacy = no
	show_notification = no
	potential = {
		always = no
	}
	is_valid = {
		always = no
	}
}

cb_borg_assimilation = {
	show_in_diplomacy = no
	show_notification = no
	potential = {
		is_assimilator = yes
	}
	is_valid = {
		has_total_war_cb = yes
		NOT = { is_overlord_to = FROM }
		OR = {
			is_subject = no
			overlord = { NOT = { is_same_value = from } }
		}		
		is_neighbor_of = from
	}
}

cb_borg_assimilation_defence = {
	show_in_diplomacy = yes
	show_notification = no
	potential = {
		is_normal_country = yes
		is_assimilator = no
	}
	is_valid = {
		has_total_war_cb = yes
		from = { is_assimilator = yes }
		OR = {
			is_neighbor_of = from
			has_country_flag = "colonyScooped:@FROM"
		}
	}
}

cb_undine_purge = {
	show_in_diplomacy = no
	show_notification = no
	potential = {
		is_infester = yes
	}
	is_valid = {
		is_neighbor_of = from
	}
}

cb_undine_purge_defence = {
	show_in_diplomacy = yes
	show_notification = no
	potential = {
		is_infester = no
	}
	is_valid = {
		from = { is_infester = yes }
		OR = {
			is_neighbor_of = from
			# has_country_flag = "colonyScooped:@FROM"
		}
	}
}


cb_universe_defence = {
	show_in_diplomacy = no
	show_notification = no
	potential = {
		always = no
	}
	is_valid = {
		always = no
	}
}


cb_demilitarisation = {
	show_in_diplomacy = yes
	show_notification = yes
	potential = {
		is_normal_country = yes
		is_assimilator = no
		NOR = { 
			has_ethic = ethic_gestalt_consciousness
			has_ethic = ethic_ancient_gestalt_consciousness
			}
	}
	is_valid = {	
		has_policy_flag = liberation_wars
		is_neighbor_of = FROM
		from = { is_normal_country = yes }
		from = { NOR = { has_ethic = ethic_gestalt_consciousness has_ethic = ethic_ancient_gestalt_consciousness } }
		has_total_war_cb = no	
	}
}

cb_trade_embargo = {
	show_in_diplomacy = yes
	show_notification = yes
	potential = {
		is_normal_country = yes
		is_assimilator = no
		NOR = { has_ethic = ethic_gestalt_consciousness has_ethic = ethic_ancient_gestalt_consciousness }
	}
	is_valid = {	
		has_policy_flag = liberation_wars
		is_neighbor_of = FROM
		from = { is_normal_country = yes }
		from = { NOR = { has_ethic = ethic_gestalt_consciousness has_ethic = ethic_ancient_gestalt_consciousness } }
		has_total_war_cb = no	
	}
}


###ABRONATH

cb_abronath = {
    show_in_diplomacy = yes
    show_notification = yes
    potential = {
        is_normal_country = yes
		is_assimilator = no
		NOT = { has_ethic = ethic_gestalt_consciousness }
    }
    is_valid = {
        from = {
			OR = { 
				AND = {
					has_relic = r_abronath_1
					has_relic = r_abronath_2
					has_relic = r_abronath_3
					}
				has_relic = r_abronath_full_inactive
				has_relic = r_abronath_full
			}
        }
        #has_total_war_cb = no
        NOT = { is_overlord_to = FROM }
    }
}

# Revenge (Honour)
cb_honour_revenge = {
    show_in_diplomacy = yes
    show_notification = yes
    potential = {
        is_normal_country = yes
		is_assimilator = no
		OR = {
			has_trait = "trait_consummate_warriors"
			has_civic = "civic_warrior_kahless"
		}
		has_country_flag = been_dishonoured
    }
    is_valid = {
        from = {
			has_country_flag = country_dishonoured
        }
        NOT = { is_overlord_to = FROM }
    }
}

# Revenge
cb_revenge = {
    show_in_diplomacy = yes
    show_notification = yes
    potential = {
        is_normal_country = yes
		is_assimilator = no
		NOR = {
			has_trait = "trait_consummate_warriors"
			has_civic = "civic_warrior_kahless"
		}
		 has_country_flag = country_betrayed
    }
    is_valid = {
        from = {
			has_country_flag = country_betrayer
        }
        NOT = { is_overlord_to = FROM }
    }
}

#cb_occupation
cb_occupation = {
	show_in_diplomacy = yes
	show_notification = yes
	potential = {
		OR = {
			has_country_flag = world_occupier
		}
		NOT = { is_in_federation_with = from }
		is_subject = no
	}
	is_valid = {
		NOT = { is_overlord_to = FROM }
		is_country_type = awakened_fallen_empire
		NOT = { has_authority = auth_machine_intelligence }
	}
}

cb_occupation_subject = {
	show_in_diplomacy = no
	show_notification = no
	potential = {
		has_country_flag = occupied_country
		is_subject = yes
	}
	is_valid = {
		overlord = { is_same_value = from }
	}
}

# Disarm trilithium weapons
cb_system_denial_disarm = {
    show_in_diplomacy = yes
    show_notification = yes
    potential = {
        is_normal_country = yes
        is_assimilator = no	       
    }
    is_valid = {       
        OR = {
            has_country_flag = STH_system_denial_mechanics_opinion_heliocide_neighboring_system 
            has_country_flag = STH_system_denial_mechanics_opinion_heliocide_colony_system
        }		       
        from = {
            has_country_flag = STH_system_denial_mechanics_heliocide
            NOT = { has_country_flag = STH_system_denial_mechanics_disarmed }
        }
    }
}

# Eternal Revenge!
cb_system_denial_eternal_revenge = {
    show_in_diplomacy = yes
    show_notification = yes
    potential = {
        is_normal_country = yes
		is_assimilator = no			
    }
    is_valid = {        
        has_country_flag = STH_system_denial_mechanics_opinion_heliocide_home_system
        from = {
			has_country_flag = STH_system_denial_mechanics_heliocide
        }
    }
}

#Reconnect System
cb_system_denial_reconnect = {
    show_in_diplomacy = yes
    show_notification = yes
    potential = {
        is_normal_country = yes
        is_assimilator = no	       
    }
    is_valid = { 
        has_technology = tech_engineering_rocketry_1602      
        any_system = {
            has_star_flag = STH_system_denial_mechanics_isolated_system
            root = {
                has_claim = prev
            }            
        }      		       
        from = {
            has_country_flag = STH_system_denial_mechanics_determined_isolationist            
        }
    }
}

#Reclaim our heritage that was stolen from us
cb_relic_hunters_reclamation = {
    show_in_diplomacy = yes
    show_notification = yes
    potential = {
        is_normal_country = yes
        is_assimilator = no
    }
    is_valid = {
      has_country_flag = STH_relic_hunters_relic_thief_known
      is_normal_country = yes
      holds_stolen_heritage = yes		
    }
}

#Sample Primitive species for Preservation
cb_relic_hunters_sampling = {
  show_in_diplomacy = yes
  show_notification = no
  potential = {    
      is_normal_country = yes      
      is_species_class = HUR
      exists = from
      from = {
        NOT = { is_species_class = HUR }
        is_normal_country = yes                
      }
  }
  is_valid = {
    from = { 
      OR = {
        num_owned_relics > 0          
        any_owned_pop_species = {
          NOT = {
            ROOT = {
              any_owned_pop_species = {
                is_same_species = PREVPREV
              }                
            }
          }
        }
      }          
    }    
  }
}

# Quadrant Invasion
cb_quadrant_invasion = {
	show_in_diplomacy = yes
	show_notification = yes
	potential = {
		has_country_flag = the_dominion
	}
	is_valid = {
		FROM = {
			is_normal_country = yes
			is_subject = no
			OR = {
				has_country_flag = quadrant_invasion_target
				has_country_flag = quadrant_invasion_gatekeeper
			}
		}
	}
}

# Quadrant Invasion
cb_quadrant_defender = {
	show_in_diplomacy = yes
	show_notification = yes
	potential = {
		is_normal_country = yes
		is_subject = no
		OR = {
			has_country_flag = quadrant_invasion_target
			has_country_flag = quadrant_invasion_gatekeeper
		}
	}
	is_valid = {
		FROM = {
			is_normal_country = yes
			has_country_flag = the_dominion
			has_country_flag = quadrant_invasion_invader
		}
	}
}
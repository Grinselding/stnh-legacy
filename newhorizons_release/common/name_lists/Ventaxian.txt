﻿###Ventaxian
Ventaxian = {
	### SHIPS
	ship_names = {

		generic = {
		}
	}

	fleet_names = {
		sequential_name = "%O% Fleet"
	}
	
	army_names = {
		defense_army = {
			sequential_name = "%O% Guards"
		}
		assault_army = {
			sequential_name = "%O% Assault Force"
		}
		slave_army = {
			sequential_name = "%O% Indentured Defence Force"
		}
		clone_army = { 
			sequential_name = "%O% Clone Defence Force"
		}
		robotic_army = {
			sequential_name = "%O% Mechanised Defence Force"
		}
		psionic_army = { 
			sequential_name = "%O% Psi Defence Force"
		}
		xenomorph_army = {
			sequential_name = "%O% Bio-Warfare Defence Force"
		}
		gene_warrior_army = {
			sequential_name = "%O% Bio-Engineered Defence Force"
		}
	}
		

	### PLANETS

	planet_names = {
		generic = {
			names = {}
		}
	}

	### CHARACTERS

	character_names = {
		default = {
			# Always combined with a second name
			first_names_male = {
				Acost Honst Ardrost Jrost Reist Garv Drevinora Lrora Srari Armanira Trarta Renntra Krentra Braelkra Crokra Srarkra Krojora Sronekra Dralra Erushra Mrera Grartra Hrentra Brarkra Brasidra
			}
			first_names_female = {
				Kalev Bhev Nev Jhuliv Dhannav Lhev Ihiv Ehev Dhamiv Dhav Yhav Jhrev Mhav Nhriv Rhov Mhiv Ahnev Ghendev Mhallev Rhinev Khestev Lhaxev Shorev
			}
			# Always combined with a first name
			second_names = {
				Jared Etes Niar Adaj Adia Amabala Aron Lexa Lazeh Nah Neeli Nevah Revilo Siri Zeni Siana
			}
		}
	}
}
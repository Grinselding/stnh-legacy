﻿###Ardanan
Ardanan = {
	### SHIPS

	ship_names = {

		generic = {
		}
	}

	fleet_names = {
		sequential_name = "%O% Fleet"
	}
	
	army_names = {
		defense_army = {
			sequential_name = "%O% Guards"
		}
		assault_army = {
			sequential_name = "%O% Assault Force"
		}
		slave_army = {
			sequential_name = "%O% Indentured Defence Force"
		}
		clone_army = { 
			sequential_name = "%O% Clone Defence Force"
		}
		robotic_army = {
			sequential_name = "%O% Mechanised Defence Force"
		}
		psionic_army = { 
			sequential_name = "%O% Psi Defence Force"
		}
		xenomorph_army = {
			sequential_name = "%O% Bio-Warfare Defence Force"
		}
		gene_warrior_army = {
			sequential_name = "%O% Bio-Engineered Defence Force"
		}
	}
		

	### PLANETS

	planet_names = {
		generic = {
			names = {}
		}
	}

	### CHARACTERS

	character_names = {
		default = {
			# Always combined with a second name
			first_names_male = {
				Plasus Midro Anka Branka Tanka Klanka Danka Ganka Tranka Garus Draylus Srabus Ardro Jruro Reitto Drevino Lro Sradro Armaro Tramo Rennanus Krenthus Bratro Crortus Shatro Krojo Stonekro Drakro Erushus Mrerus Grartus Hrentus Brarus Brazus
			}
			first_names_female = {
				Vanna Droxine Rianna Bhavanna Naxine Jhulinna Dhanna Lhelline Ihirrine Ehexine Minxine Dhanna Haxine Jhrenna Mharanna Ahrinna Rhonna Mhiraxine Ahnissanna Ghendanna Mhallanna Rhinananna Khestanna Lhaxine Shoraxine
			}
			# Always combined with a first name
			second_names = {
				"i'Stratos" "e'Tropos" "i'Mesos" "i'Thermos" "i'Exos" "i'Ionos" "e'Ozonos" "e'Magnetos" "i'Auroros" "i'Turbos" "e'Streamos" "e'Atmos"
			}
		}
	}
}
﻿### Klingon Empire
Klingon = {
	randomized = no

	ship_names = {
		generic = {
			Sha'go Grin'zel Swi'tsel Wah'sHiqus Ma'tix Bortras Amar B'Moth Bortas Buruk Ch'Tang Drovana Hor'Cha Ki'tang  Koraga Korinar Maht-H'a Malpara Negh'Var Ning'tao Orantho Pagh Par'tok P'Rang Qu'Vat Somraw Slivin T'Acog Toh'Kaht Vor'nak Y'tem Ya'Vang K'vort Charghwl' K'T'Inga Ll'wl May'Duj B'rel DughDuy Dughjup Dughla' DughQu' Dughro' DughwoQ DughyaS Etam GhungDol Ghunglom GhungQa' GhungQogh Hegh'ta Hembu' HemDuy Hemghol Hemlom Hovpeng M'Char NaSbutlh NaSDol NaSDup NaSgho NaSghong NaStaj Rotarran Vorn QaDwI' SuDwI' ChungwI' ChavwI' VangwI' Chu'wI' VaQwI' GhuHwI' QeHwI' NuQwI' GhoHwI' HIvwI' QabwI' QImwI' ChopwI' BotwI' JaqwI' MeQwI' MolwI' JonwI' QengwI' CharghwI' SeHwI' QughwI' HubwI' PejwI' HeghwI' Qaw'wI' VerghwI' YotwI' Aktuh Azetbur B'iJik Chang Divok Fek'lhr Gi'ral Gorkon J'Ddan Kahless Kang K'Ehleyr Klag K'mpec K'nera Korris Konmel K'Ratak Koloth Koord Kor K'Temoc Kurn Kruge L'Kor Lorgh Maltz Melota Morath Nu'Daq DajDuS Dajghol Dajghu' Dochbeq Dochqup DoqbaS Doqcha DoqghuH DoqHegh DoqHo' DoqmaS Doqram HurghSan Husghaj Le'batlh Le'chav Le'Hov Le'qorDu' Qanmang Qanvaj QIjbaS QIjcha QIjHegh QIjHo' QIjmaS QIjram Qutbe' QutHol Qutqempa' QutSa' BIvwI' BoQwI' BoSwI' BuvwI' ChelwI' ChenwI' ChoHwI' ChovwI' DaqwI' GhorwI' HeQwI' HujwI' LI'wI' LoHwI' MIywI' MobwI' NajwI' NgIlwI' NgongwI' QaHwI' QemwI' QeSwI' QIchwI' Qochbe'wI' QolwI' SepwI' Ta'wI' TaymoHwI' TlheDwI' VI'wI' BoHcha BoHDol BoHDegh BoHDuS BoHtIq BoHpeng BoHyaS Ngojcha NgojDegh NgojDol NgojDuS Ngojpeng Ngojporgh Ngotlhbe' NgotlhchoH NgotlhDa' NgotlhDol Ngotlhpuq Noycha'puj NoyDaS NoyDuj Noyqech NoyvI' VaQbach VaQDoch VaQDol VaQDup VaQpeng VaQto' VaQyaS BIrHegh BIrHom BIrruv BochbaS BochHeH BochHov BochlIy Bochqut Bochtev HoSbatlh HoSbegh HoSDuj HoSjagh HoSloD HoSmu' HoSqempa' Jaqbach Jaqbeq Jaqbutlh JaqDup JaqHoq JaqtIq QarbeH QarchetvI' QarDuS QarHIch QarmIn QarnuH Qarpeng Boreth DighreS Drovna Erikang Gre'thor Khitomer Kirom Kri'stak Lursor Morska No'Mat P'Rang Praxis QalIa'pe' Quin'lat QuprIp Qui'Tu Ro'kegh T'Ong VorcaS Vor'cha Vornak Yavang Bortasqu' Kal'Ruq
		}
		corvette = {
		}
		saber = {
		}
		sovereign = {
		}
		steamrunner = {
		}		
		adv_cruiser = {
		}
		strike = {
		}
		kdf_gunboat_borghel_a = { "Borghel" }
		kdf_gunboat_brel_a = { "BaHwil'" "Begh'poQ" "BImoqu'" "BInep" "BI'ngo'" "BIQ'a'ngo'" "Bllugh" "Bowie'e" "B'rel" "Buruk" "Chong'pogh" "Ch'Tang" "Dakh'yi'DIL" "Dakronh" "Kla'DIyuS" "Deb'choS" "Doh'" "Do'Ha'" "Dom'SIS" "DughDuy" "Dughjup" "Dughla'" "DughQu'" "Dughro'" "DughwoQ" "DughyaS" "Dun'wo'" "Duy'Hub" "Em'ree" "Etam" "Ghar'Qotlh" "Gho'be'" "Ghoch'ragh" "GhoH'Sot" "GhungDol" "Ghunglom" "GhungQa'" "GhungQogh" "Gon'dev" "Grerr" "guhMoh" "Hegh'QeDp" "Hegh'ta" "Hembu'" "HemDuy" "Hemghol" "Hemlom" "Hijol'peng" "Hob'DIS" "Hovpeng" "Huj'a'SIp" "Hurgh'ragh" "HuS'ngeb" "Jib'lalDan" "Jiyajbe'" "Jol ylchu'" "Jor" "JuS'mung" "Kad'nra" "Kahless Ro" "Khich" "Korinar" "Koroth" "Krogshat" "Kruge" "K'ti'suka" "LoS'maH" "Luq'argh" "MajQa'be'" "Malpara" "Mav'jop" "Maw'HeSwI'" "M'Char" "Mich'pagh" "Mok'tal" "NaSbutlh" "NaSDol" "NaSDup" "NaSgho" "NaSghong" "NaS'puchpa'" "NaStaj" "Nay'par" "Ning'tao" "Ning'tau" "N'Kghar" "PiqaD'nem" "Plath" "Qap'ghargh" "Qo'" "Qovin" "Qugh'tung" "QuHvaj'Qob" "Qul'bIQ" "Quoc'Truong" "Qup'SoH" "Qut'Such" "RanKuf" "RaQpo'juS" "Rotarran" "Sap'leng" "SID'ghom" "Slivin" "TaD'moH" "TajHu'" "Tar'be'" "Targ" "Tazhat" "TIH'Hich" "TIQ'IwSev" "TlhIH'Hu" "Trayor" "Tu'a'rop" "VIghajbe'" "Vorka" "Vorn" "Vut'bol" "Wo'bortas" "Wuv'a'tem" "Yaj'a'" "YijatlhQo'" "Ylje'" "Y'tem" }
		kdf_bop_borghel = { "Borghel" }
		kdf_bop_homcha = { "Hom Cha'Par" }
		kdf_bop_brel = { "BaHwil'" "Begh'poQ" "BImoqu'" "BInep" "BI'ngo'" "BIQ'a'ngo'" "Bllugh" "Bowie'e" "B'rel" "Buruk" "Chong'pogh" "Ch'Tang" "Dakh'yi'DIL" "Dakronh" "Kla'DIyuS" "Deb'choS" "Doh'" "Do'Ha'" "Dom'SIS" "DughDuy" "Dughjup" "Dughla'" "DughQu'" "Dughro'" "DughwoQ" "DughyaS" "Dun'wo'" "Duy'Hub" "Em'ree" "Etam" "Ghar'Qotlh" "Gho'be'" "Ghoch'ragh" "GhoH'Sot" "GhungDol" "Ghunglom" "GhungQa'" "GhungQogh" "Gon'dev" "Grerr" "guhMoh" "Hegh'QeDp" "Hegh'ta" "Hembu'" "HemDuy" "Hemghol" "Hemlom" "Hijol'peng" "Hob'DIS" "Hovpeng" "Huj'a'SIp" "Hurgh'ragh" "HuS'ngeb" "Jib'lalDan" "Jiyajbe'" "Jol ylchu'" "Jor" "JuS'mung" "Kad'nra" "Kahless Ro" "Khich" "Korinar" "Koroth" "Krogshat" "Kruge" "K'ti'suka" "LoS'maH" "Luq'argh" "MajQa'be'" "Malpara" "Mav'jop" "Maw'HeSwI'" "M'Char" "Mich'pagh" "Mok'tal" "NaSbutlh" "NaSDol" "NaSDup" "NaSgho" "NaSghong" "NaS'puchpa'" "NaStaj" "Nay'par" "Ning'tao" "Ning'tau" "N'Kghar" "PiqaD'nem" "Plath" "Qap'ghargh" "Qo'" "Qovin" "Qugh'tung" "QuHvaj'Qob" "Qul'bIQ" "Quoc'Truong" "Qup'SoH" "Qut'Such" "RanKuf" "RaQpo'juS" "Rotarran" "Sap'leng" "SID'ghom" "Slivin" "TaD'moH" "TajHu'" "Tar'be'" "Targ" "Tazhat" "TIH'Hich" "TIQ'IwSev" "TlhIH'Hu" "Trayor" "Tu'a'rop" "VIghajbe'" "Vorka" "Vorn" "Vut'bol" "Wo'bortas" "Wuv'a'tem" "Yaj'a'" "YijatlhQo'" "Ylje'" "Y'tem" "Cha'Joh" }
		kdf_bop_pagh = { "Pagh Haw'" "Chav Ralqu'je" "ChenmoHmeh" "Chochot'a" "Daghajchugh" "Dah Maw" "DaraDmo" "Dara'pu'bogh" "DaqunmeH" "DunaDchoh" "Hegh Ghah" "Herghvetlh" "Hlingong" "Hu'tegh" "MaQmIgh" "QamchoH" "QaQbogh" "Qong'Dlj" "Qu'nasvad" "ShoSqu'qech" "SoSll'Daq" "SuH DaH" "Sumchugh Vay" "Yachchuq" }
		kdf_raptor_somraw = { "Amw'I" "Balth" "Barak" "Bokor" "Dit'kra" "Gantin" "K'araH" "K'eylat" "Nu'paH" "Nu'Tal" "Patan" "R'mora" "Rok'lor" "Somraw" "Veng" }
		kdf_raptor_d5 = { "PeD NIHwI'" "Che'leth" "Bortas" }
		kdf_raptor_jev = { "Jev Cha'par" Klothos "Gr'oth" }
		kdf_raptor_hegh = { "Hegh Jop" "S'Cha" }
		kdf_raptor_mehadraw = { "Me'Had'Raw" "Chora'chugh" "DaDabej" "Daw'meh DuhlvDi" "Dol MuSHa'bogh" "Dotlh" "Ha'QeDjaj" "Heghta'meH" "IwwIj'ay'Hom" "Jawbogh" "Lununglaw'Hoch" "NabVad" "Num'eghmeh Duj" "Qochbe'meH" "Qo'Joh" "Qo'noS Targhmey" "QongDaqDajDaq" "QumHa'bogh" "QuSmey" "Qu'ta'Dl'Hegh" "Satlho'Qu'tlh" "Sepbogh Yabvad" "TuQmeh" "Vuplu'bejnes" "Waghqu'ngeh" }
		kdf_raider_d6 = { "Kut'luch" }
		kdf_raider_kvort = { "Aktuh" "Azetbur" "Begh'poQ" "B'iJik" "Buruk" "Chang" "Chong'pogh" "Deb'choS" "Divok" "Duy'Hub" "Fek'lhr" "Gho'be'" "GhoH'Sot" "Gi'ral" "Gorkon" "Gro'kan" "Hegh'ta" "Hob'DIS" "Hurgh'ragh" "J'Ddan" "Kahless" "Kang" "K'Ehleyr" "Ki'tang" "Klag" "K'mpec" "K'nera" "Koloth" "Konmel" "Kor" "Koraga" "Kormag" "Korris" "K'Ratak" "Kreltek" "Kruge" "K'Temoc" "Kurn" "K'vort" "L'Kor" "Lorgh" "Lukara" "Luq'argh" "MajQa'be'" "Maltz" "Melota" "Morath" "NaS'puchpa'" "Nay'par" "Nu'Daq" "Pagh" "PiqaD'nem" "Qugh'tung" "Qup'SoH" "Qut'Such" "TaD'moH" "Tagak" "Taj" "TajHu" "Tar'be" "Vorn" "Wuv'a'tem" }
		kdf_battlecruiser_d7 = { "Begh'poQ" "Blortlh" "Chong'pogh" "Deb'choS" "D'k Tahg" "Duy'Hub" "Ectacus" "Gho'be'" "GhoH'Sot" "Gr'oth" "Hob'DiS" "Hurgh'ragh" "Korthos" "Luq'argh" "MajQa'be'" "NaS'puchpa'" "Nay'par" "'OghwI'" "PiqaD'nem" "Qaw'qay" "Qugh'tung" "Qup'SoH" "Qut'Such" "Quv" "Roney" "TaD'moH" "TajHu" "Tar'be" "Tr'loth" "Varchas" "Voq'leng" "Wuv'a'tem" }
		kdf_battlecruiser_ktinga = { "Arekkieh" "Bardur" "Begh'poQ" "B'Moth" "Bortas" "Chargh" "Chong'pogh" "Deb'choS" "DajDuS" "Dajghol" "Dajghu'" "Dochbeq" "Dochqup" "DoqbaS" "Doqcha" "DoqghuH" "DoqHegh" "DoqHo'" "DoqmaS" "Doqram" "Duy'Hub" "Ghaklor" "GhIqtal" "Gho'be'" "GhoH'Sot" "Gr'oth" "Ghob" "HaH'vat" "K'elric" "Va'tal" "Hakkarl" "Hob'DIS" "Hurgh'ragh" "HurghSan" "Husghaj" "LI'batlh" "LI'chav" "Le'Hov" "Le'qorDu'" "Kartadza" "Kluggoth" "Kol'Targh" "Korvat" "K'tanco" "K't'inga" "Luq'argh" "MajQa'be'" "Melikaphkaz" "NaS'puchpa'" "Nay'par" "Pefak" "PiqaD'nem" "QaD" "Qanmang" "Qanvaj" "Qapla" "QIjbaS" "QIjcha" "QIjHegh" "QIjHo'" "QIjmaS" "QIjram" "QItI'nga" "Qob" "Qugh'tung" "Qup'SoH" "Qutbe'" "QutHol" "Qutqempa'" "QutSa'" "Qut'Such" "T'Acog" "T'Nek" "TaD'moH" "TajHu" "Tar'be" "Tebtivu" "Tewniwa" "Tevekh" "T'Ong" "VaH" "Vo'taq" "Wuv'a'tem" "Ya'Vang" "Zajikh" }
		kdf_battlecruiser_norgh = { "Norgh" }
		kdf_battlecruiser_vorcha = { "'Ach loDnI'" "'Avwi" "Akua" "Akva" "Amar" "Ars'lek" "Baqghol" "Begh'poQ" "Bej'joq" "BighHa'" "BIQDep" "Boreth" "Bortas" "BortaS'tej" "Chen'a'meQ" "Chong'pogh" "Daqchov" "DaQ'Qat" "DaSpu'Dal" "Deb'choS" "DighreS" "Dop'puS" "Drovana" "Duy'Hub" "Erikang" "Gar'Tukh" "Gho'be'" "GhoH'Sot" "Gre'thor" "Hegh'ta" "Hej'leng" "Hob'DIS" "Ho'hegh" "HoSghaj" "Hurgh'a'" "Hurgh'ragh" "Jan" "JaqHom" "JonKa" "JorwI'Hegh" "Luq'argh" "K'elest" "Kerla" "Key'vong" "Khitomer" "Kirom" "Kohna" "Kri'stak" "K'Tang" "LengwI'qet" "Lursor" "Mahk'tar" "MaHmuSSoH" "Maht-H'a" "MajQa'be'" "May'morgh" "Mep'reH" "M'ganath" "MI'va'" "Morska" "Nagh'nuS" "NaS'puchpa'" "Nav'Don" "Nay'par" "Neng-ta" "No'Mat" "Nong'ngIv" "'O'lav" "PiqaD'nem" "PIvghor" "Poj'qumpa" "P'Rang" "Praxis" "QalIa'pe'" "Qam-Chee" "Qap'Hurgh" "Qay'lIng" "Qigh'yoD" "Qit'ong" "Qogh'a'Huy" "Quin'lat" "Qui'Tu" "Qu'Vat" "QueloDmI'" "Qugh'tung" "QuprIp" "Qup'SoH" "Qut'Such" "Quv Patlh" "R'kang" "RoghvaH" "Ro'kegh" "Seg'pa" "SIH'chor" "Sto Vo Kor" "TajHu" "T'Acog" "TaD'moH" "Tar'be" "Taral" "Tcha'voth" "Ti'voH" "T'Kora" "'TlhuH" "Toh'Kaht" "T'Ong" "VatlhvI'" "veScharg'a" "VorcaS" "Vor'cha" "Vor'nak" "Vum'ghargh" "Vup'Quj" "WamwI'" "Waq'bach" "Wuv'a'tem" "Yab'loD" "Ya'Vang" }
		kdf_warship_roj = { "Roj Par" }
		kdf_warship_kvek = { "K'vek" }
		kdf_warship_neghvar = { "BajnISlu'" "Bey" "Bom" "CharghwI'" "Chon bom" "Chang" "Chong" "Chot" "Gha'tlhIq" "Ghob" "Ghol'quS" "GhuH'nov" "Giz'ta'bek" "Hargh" "Hegh'mar" "HI'a'pIm" "Ho'a'chaID" "Hurgh'ragh" "JeH'jhong" "Kang" "K'mpec" "Koloth" "Kormat" "Loj'boq" "Lop" "LoS'SaD" "Ma'tlhej" "Mogh" "Nan" "Negh'Var" "Bagh'Var" "Nov'Hoch" "Pe'egh" "Pey'Suq" "PoHmey'vav" "QajunpaQ" "QaStaHvIS" "Qa'Vak" "Qetbogh" "QIghpej" "Qom" "Satlh'QaH" "SIQwI'" "Ta'rom" "Vangshu'a'" "VaQ" "Yinlu'taH" "YoHwI'" }
		kdf_flagship_jehjhong = { "JeH'jhong" }
	}
	
	ship_class_names = {
		kdf_gunboat_borghel_a = { "Borghel A" }
		kdf_gunboat_brel_a = { "B'Rel A" }
		kdf_bop_borghel = { "Borghel" }
		kdf_bop_blasrika = { "Blas Rika" }
		kdf_bop_homcha = { "Hom Cha'Par" }
		kdf_bop_brel = { "B'Rel" }
		kdf_bop_pagh = { "Pagh Haw'" }
		kdf_raptor_somraw = { "Somraw" }
		kdf_raptor_d5 = { "D5-Type" }
		kdf_raptor_jev = { "Jev Cha'par" } 
		kdf_raptor_hegh = { "Hegh Jop" }
		kdf_raptor_mehadraw = { "Me'Had'Raw" }
		kdf_raider_d6 = { "D6-Type" } 
		kdf_raider_kvort = { "K'Vort" }
		kdf_battlecruiser_d7 = { "D7-Type" }
		kdf_battlecruiser_ktinga = { "K't'inga" }
		kdf_battlecruiser_norgh = { "Norgh" }
		kdf_battlecruiser_vorcha = { "Vor'cha" }
		kdf_warship_roj = { "Roj Par" } 
		kdf_warship_kvek = { "K'vek" }
		kdf_warship_neghvar = { "Negh'Var" }
		kdf_flagship_jehjhong = { "JeH'jhong" }
		military_defense_kdf_1 = { DuQwl }
		military_defense_kdf_2 = { B'rel }
	}

	fleet_names = {
		sequential_name = "%O% Fleet"
	}
	
	army_names = {
		defense_army = {
			sequential_name = "%O% Planetary Guard"
		}
		assault_army = {
			sequential_name = "%O% Expeditionary Force"
		}
		slave_army = {
			sequential_name = "%O% Indentured Rifles"
		}
		clone_army = { 
			sequential_name = "%O% Clone Army"
		}
		robotic_army = {
			sequential_name = "%O% Hunter-Killer Group"
		}
		psionic_army = { 
			sequential_name = "%O% Psi Commando"
		}
		xenomorph_army = {
			sequential_name = "%O% Bio-Warfare Division"
		}
		gene_warrior_army = {
			sequential_name = "%O% Bio-Engineered Squadron"
		}
	}

	planet_names = {
		generic = {
			names = { 
			}
		}
	}


	### CHARACTERS

	character_names = {
		names1 = {
			weight = 100
			first_names_male = {
				Be'etor Torgh R'tani Mordok Karthag Worf Martok Gowron Amar Antaak "A'trom" Atul "Bar'el" Barot "Bei'ju" Korath Koral Kor "K'mpec" Ortakin Mogh Koth Kolor Chang Darok Rodek Pahash "M'Rek" "Ch'olid" Erud Ehac Olkuk "K'chazol" Hamtow "K'modoz" Vernak Veso Ch'klon "Utr'ik" Otaz "T'uthunga" Uwok H'huntow Laluth Rekorrd Dami Stada Ba'ktor B'iJik Bo'rak Brok'tan Ch'Pok Ch'Targh Chu'vok D'Ghor Divok Drex Goroth Grafk G'trok Hij'qa H'ohk Hon-Tihl H'ta Huraga Japar Ja'rod J'Dan Jodmos Kobor Kahmar Kamarag Kargan Katogh Kaybok Kerla Klaa Klaang K'mtar K'Nera Kohlar Kolax Konmel Kornan Koral Korok Korris K'Ratak Kruge K'Tal K'Temang K'Temoc K'Trelan Kulge Kunivas Leskit Kurn Lorgh Magh Maltz Morag Morak Morka Mow'ga Noggra Nu'Daq Or'Eq Orak Ortikan M'Kota Rejac Rorg Rurik Rynar Sarpek ShiVang Shovak Silrek Sorval Stex T'vis Tanas Tel-Peh T'Greth Thopok T'Kar Toq Toral Torath Torg Torin Tumek Ujilli Vagh Voq Vorok W'Mar Yeto Tenavik Jelor Dehus Charkvanek Kavin Javen
			}
			first_names_female = {
				J'hara Solia K'tarla Korila Sirella "B'Elanna" "Ba'el" B'Etor Grilka L'Kor Larna Leskit Linkasa Shenara Karana Mara Kerla Vixis Azetbur Tavana "D'egronga" "K'evew'a" Shutha "H'pirsisha" Opija Dava Drovila Yadmust "Uts'an" Grovowe Jan Noh Stuxonga "D'apothi" Gruga Nolkrah Milvi Kalara "K'nera" Katrene Askade Kuri Bu'kaH Ch'Rega Dennas Doran Gi'ral Huss Kahlest Karana Keedera K'Ehleyr Vixis Kohlana Krelik K'Rene Valkris Kurak L'Naan Lukara M'Nea Miral N'Garen L'Rell L'Dan L'iJik Synon Talij Tavana Turla Vekma Zegov  "Jeh'l" "Tal'Q" Lursa
			}
			second_names = {
				" "
			}
			regnal_first_names_male = {
				Be'etor Torgh R'tani Mordok Karthag Worf Martok Gowron Amar Antaak "A'trom" Atul "Bar'el" Barot "Bei'ju" Korath Koral Kor "K'mpec" Ortakin Mogh Koth Kolor Chang Darok Rodek Pahash "M'Rek" "Ch'olid" Erud Ehac Olkuk "K'chazol" Hamtow "K'modoz" Vernak Veso Ch'klon "Utr'ik" Otaz "T'uthunga" Uwok H'huntow Laluth Rekorrd Dami Stada Ba'ktor B'iJik Bo'rak Brok'tan Ch'Pok Ch'Targh Chu'vok D'Ghor Divok Drex Goroth Grafk G'trok Hij'qa H'ohk Hon-Tihl H'ta Huraga Japar Ja'rod J'Dan Jodmos Kobor Kahmar Kamarag Kargan Katogh Kaybok Kerla Klaa Klaang K'mtar K'Nera Kohlar Kolax Konmel Kornan Koral Korok Korris K'Ratak Kruge K'Tal K'Temang K'Temoc K'Trelan Kulge Kunivas Leskit Kurn Lorgh Magh Maltz Morag Morak Morka Mow'ga Noggra Nu'Daq Or'Eq Orak Ortikan M'Kota Rejac Rorg Rurik Rynar Sarpek ShiVang Shovak Silrek Sorval Stex T'vis Tanas Tel-Peh T'Greth Thopok T'Kar Toq Toral Torath Torg Torin Tumek Ujilli Vagh Voq Vorok W'Mar Yeto Tenavik Jelor Dehus Charkvanek Kavin Javen
			}
			regnal_first_names_female = {
				J'hara Solia K'tarla Korila Sirella "B'Elanna" "Ba'el" B'Etor Grilka L'Kor Larna Leskit Linkasa Shenara Karana Mara Kerla Vixis Azetbur Tavana "D'egronga" "K'evew'a" Shutha "H'pirsisha" Opija Dava Drovila Yadmust "Uts'an" Grovowe Jan Noh Stuxonga "D'apothi" Gruga Nolkrah Milvi Kalara "K'nera" Katrene Askade Kuri Bu'kaH Ch'Rega Dennas Doran Gi'ral Huss Kahlest Karana Keedera K'Ehleyr Vixis Kohlana Krelik K'Rene Valkris Kurak L'Naan Lukara M'Nea Miral N'Garen L'Rell L'Dan L'iJik Synon Talij Tavana Turla Vekma Zegov "Jeh'l" "Tal'Q" Lursa
			}
			regnal_second_names = {
				" "
			}
		}
	}
}
﻿### Talosian
Talosian = {
	randomized = no
	ship_names = {
		generic = {
			Watcher
		}
		corvette = {
		}
		saber = {
		}
		sovereign = {
		}
		steamrunner = {
		}		
		adv_cruiser = {
		}
		strike = {
		}	
		constructor = { }
		colonizer = { }
		science = { }
		transport = { }

	}

	fleet_names = {
		sequential_name = "%O% Fleet"
	}
	
	army_names = {
		defense_army = {
			sequential_name = "%O% Guard"
		}
		assault_army = {
			sequential_name = "%O% Defence Force"
		}
		slave_army = {
			sequential_name = "%O% Indentured Defence Force"
		}
		clone_army = { 
			sequential_name = "%O% Clone Defence Force"
		}
		robotic_army = {
			sequential_name = "%O% Mechanical Defence Force"
		}
		psionic_army = { 
			sequential_name = "%O% Psi Defence Force"
		}
		xenomorph_army = {
			sequential_name = "%O% Bio-Warfare Defence Force"
		}
		gene_warrior_army = {
			sequential_name = "%O% Bio-Engineered Defence Force"
		}
	}

	planet_names = {
		generic = {
			names = { 
			}
		}
	}


	### CHARACTERS

	character_names = {
		names1 = {
			weight = 100
			first_names_male = {
				Keeper Guardian Protector Guard Preserver Watcher Minder Escort Archivist Librarian Curator Conservator Administrator Goalkeeper Timekeeper Custodian Warder Steward Attendant Concierge Cotter Deliverer Holder Factor Sentinel Handler Castellan Operator Onlooker Observer Witness Bystander Beholder
			}
			first_names_female = {
				Keeper Guardian Protector Guard Preserver Watcher Minder Escort Archivist Librarian Curator Conservator Administrator Goalkeeper Timekeeper Custodian Warder Steward Attendant Concierge Cotter Deliverer Holder Factor Sentinel Handler Castellan Operator Onlooker Observer Witness Bystander Beholder
			}
			second_names = {
				" "
			}
			regnal_first_names_male = {
				Keeper Guardian Protector Guard Preserver Watcher Minder Escort Archivist Librarian Curator Conservator Administrator Goalkeeper Timekeeper Custodian Warder Steward Attendant Concierge Cotter Deliverer Holder Factor Sentinel Handler Castellan Operator Onlooker Observer Witness Bystander Beholder
			}
			regnal_first_names_female = {
				Keeper Guardian Protector Guard Preserver Watcher Minder Escort Archivist Librarian Curator Conservator Administrator Goalkeeper Timekeeper Custodian Warder Steward Attendant Concierge Cotter Deliverer Holder Factor Sentinel Handler Castellan Operator Onlooker Observer Witness Bystander Beholder
			}
			regnal_second_names = {
				" "
			}
		}
	}
}
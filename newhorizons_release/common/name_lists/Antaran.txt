﻿###Antaran
Antaran = {
	### SHIPS
	ship_names = {
		generic = {
		}
	}

	fleet_names = {
		sequential_name = "%O% Fleet"
	}
	
	army_names = {
		defense_army = {
			sequential_name = "%O% Guards"
		}
		assault_army = {
			sequential_name = "%O% Assault Force"
		}
		slave_army = {
			sequential_name = "%O% Indentured Defence Force"
		}
		clone_army = { 
			sequential_name = "%O% Clone Defence Force"
		}
		robotic_army = {
			sequential_name = "%O% Mechanised Defence Force"
		}
		psionic_army = { 
			sequential_name = "%O% Psi Defence Force"
		}
		xenomorph_army = {
			sequential_name = "%O% Bio-Warfare Defence Force"
		}
		gene_warrior_army = {
			sequential_name = "%O% Bio-Engineered Defence Force"
		}
	}
		

	### PLANETS

	planet_names = {
		generic = {
			names = {}
		}
	}

	### CHARACTERS

	character_names = {
		default = {
			# Always combined with a second name
			first_names_male = {
				Hudak Abak Lirik Pitark Ribark Velk Krok Bark Lupark Koruk Acok Honka Ardaruk Jroruk Reik Gavuk Drevuk Soriark Arik Armuk Cariacak Renuk Krenuk Braecak Crok Stak Krojuk Skrak Daluk Eruk Uwuk Gark Hariark Bariak Siduk
			}
			first_names_female = {
				Azen Tincoo Vinka Pirinka Kirinka Lavicoo Kalcoo Bhezen Nezen Jhezen Dhancoo Lhenka Ihilka Ehezen Dhezen Dhalcoo Yhalcoo Jhrelcoo Mhanka Nhrinka Rhonka Mhinka Ahnka Ghenka Mhazen Rhinka Khezen Lhaxen Shenka Sharika Shencoo Berncoo Penka Karizen
			}
			# Always combined with a first name
			second_names = {
				Jared Etes Niar Adaj Adia Amabala Aron Lexa Lazeh Nah Neeli Nevah Revilo Siri Zeni Siana Woil "Ner Naba" Jyusa Illuss Noille Illum "Ner Shada" "Ner Hudda" "Orr Mandell" "Orr Mentell" "Orr Anorr" "Orr Antaha" "Orr Bini" "Orr Buri" "Orr Cane" "Orr Cell" "Orr Carda" "Orr Carpag" "Orr Celbu" "Orr Cests" "Orr Dat" "Orr Dols" "Orr Dolas" "Orr Dicol" "Orr Enleh" "Orr Fanta" "Ner Mandell" "Ner Mentell" "Ner Adiner" "Ner Antaha" "Ner Bini" "Ner Buri" "Ner Cane" "Ner Cell" "Ner Carda" "Ner Carpag" "Ner Celbu" "Ner Cests" "Ner Dat" "Ner Dols" "Ner Dolas" "Ner Dicol" "Mar Enleh" "Ner Fanta"
			}
		}
	}
}
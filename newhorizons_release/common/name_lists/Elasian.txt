﻿### Elasian
Elasian = {
	### SHIPS

	ship_names = {
		generic = {
		}
	}

	fleet_names = {
		sequential_name = "%O% Task Force"
	}
	
	army_names = {
		defense_army = {
			sequential_name = "%O% Defensive Regulars"
		}
			
		assault_army = {
			sequential_name = "%O% Attack Dragoons"
		}
		
		slave_army = {
			sequential_name = "%O% Battle Thralls"
		}
		
		clone_army = { 
			sequential_name = "%O% Warrior Clones"
		}
		
		robotic_army = {
			sequential_name = "%O% Battle Droid Legion"
		}
		
		psionic_army = { 
			sequential_name = "%O% Psionic Cadre"
		}
		
		xenomorph_army = {
			sequential_name = "%O% Destroyer Brood"
		}
		
		gene_warrior_army = {
			sequential_name = "%O% Augmented Vanguard"
		}
	}
		

	### PLANETS

	planet_names = {
		generic = {
			names = {}
		}
	}

	### CHARACTERS

	character_names = {
		default = {
			# Always combined with a second name
			first_names_male = {
				Kryton Philomelos Agamedes Iolaos Baltasaros Eteokles Timotheos Iseas Tydeos Ambrotos Lysippos Sinon Aketes Aristogiton Kleidemos Kleomenes Polyeuktes Speusippos Eurymedon Erasmios Adrastos
			}
			first_names_female = {
				Elaan Persis Nereis Amymone Phyllis Theora Kreusa Idole Koronis Thele Elais Palaemona Thetis Alisia Thymele Phaethusa Lykopis Barkida Kltemnestra Glykera Nanno Metaneira Krokale Semele
			}
			# Always combined with a first name
			second_names = {
				"ia Samorena" "ia Eugenielle" "ia Kouria" "ia Ela" "ia Schoi" "ia Pasca" "ia Thaki" "ia Ros" "ia Nadi" "ia Kardo" "ia Esco" "ia Elvi"
			}
		}
	}
}
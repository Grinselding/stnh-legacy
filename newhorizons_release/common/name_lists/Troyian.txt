﻿### Troyian
Troyian = {
	### SHIPS

	ship_names = {

		generic = {
			Aegina Agathonissi Agiaeirini Agistri Agreloussa Agriomandra Aitoliko Alimia Alonissos Ammouliani Amorgos Anafi Ananes Anavatis Andros Antikythera Antimilos Antiparos Antipaxi Antipsara Antitilos Antitrikeri Arefoussa Arhangelos Arkoi Arkoudi Armathia Arnaouti Artemis Askania Astakida Astypalaia Atalanti Atokos Avgo Cephalonia Chios Chrysi Corfu Crete Dasia Daskaleia Delos Despotiko Dia Diaplo Diapori Dionysades Dokos Dolmas Donoussa Dragonada Drakonera Elafonisi Elafonisos Elafonissos Elasa Ereikoussa Erinia Eschati Euboea Falkonera Faradonesia Farmakonisi Fleves Fokionissia Folegandros Fotia Fragos Gaidouronisi Gavdopoula Gavdos Gianysada Gioura Glaronisi Glaronisi Grammeza Gramvoussa Grandes Gyali Gyaros Halavra Halki Hersonisi Hondro Hristiana Htenies Hydra Ikaria Imia Ios Iraklia Ithaca Ithaki Kalamos Kalogiros Kalogiros Kalolimnos Kalovolos Kalydon Kalymnos Kamilonisi Kandeloussa Karavi Kardak Kardiotissa Karga Karlonisi Karpathos Kasos Kastellorizo Kastos Katergo Kavalliani Kavallos Kea Kefali Kefalonia Kelifos Keros Kimolos Kinaros Kitriani Kleisova Kolokythas Kos Koubelonisi Koufonisi Koufonisia Kouloundros Kounoupoi Koursaroi Koutsomytis Kramvoniss Kravia Kyriamadi Kythira Kythnos Kythros Lagousa Lamprinos Lazaretta Lazaretto Lefkada Leipsoi Lekhoussa Lemnos Leon Leros Lesbos Levitha Lichades Lithari Loutro Madouri Makares Makri Makronissos Makropoula Makroulo Mandilou Manolia Marathos Marmaras Marmaro Mathraki Mavros Megalo Megalonisi Meganisi Megatzedes Mikronisi Milos Mochlos Mykonos Naxos Nero Nikolos Nikouria Nimos Nisyros Oinousses Omfori Othonoi Oxeia Palaiosouda Papadoplaka Paros Pasas Patmos Patroklou Paxi Paximada Paximadaki Paximadia Pergoussa Peristera Peristeri Peristerovrachoi Petalas Petalida Petalioi Petalouda Piganoussa Piperi Pistros Pitta Platia Polyaigos Pontikaki Pontikonisi Pontikos Pontikoussa Poros Prasokissamou Prasonisi Prasouda Prokopanistos Prosfora Provati Psara Psarocharako Psathoura Pseira Pserimos Psili Psyllos Psyttaleia Repio Revythoussa Rhineia Rho Rhodos Romvi Safonidi Salamina Saliagos Samiopoula Samos Samothraki Santorini Sapientza Sarakino Saria Schistonisi Schiza Schoinoussa Seirina Serifopoula Serifos Sesklio Sideros Sifnos Sikinos Skandili Skantzoura Skiathos Skopelos Skorpidi Skorpios Skyropoula Skyros Sofia Souda Spetses Spetsopoula Sphacteria Spinalonga Stouronisi Strofades Stroggyli Strongyli Symi Syrna Syros Telendos Thasos Thera Thetis Thilia Thirasia Thymaina Tilos Tinos Tourlida Trachilos Trafos Tragonisi Tsougria Tsougriaki Valaxa Valenti Vasiladi Vidos Vous Vromonas Vryonisi Zaforas Zakynthos
		}
	}

	fleet_names = {
		sequential_name = "%O% Task Force"
	}
	
	army_names = {
		defense_army = {
			sequential_name = "%O% Defensive Regulars"
		}
			
		assault_army = {
			sequential_name = "%O% Attack Dragoons"
		}
		
		slave_army = {
			sequential_name = "%O% Battle Thralls"
		}
		
		clone_army = { 
			sequential_name = "%O% Warrior Clones"
		}
		
		robotic_army = {
			sequential_name = "%O% Battle Droid Legion"
		}
		
		psionic_army = { 
			sequential_name = "%O% Psionic Cadre"
		}
		
		xenomorph_army = {
			sequential_name = "%O% Destroyer Brood"
		}
		
		gene_warrior_army = {
			sequential_name = "%O% Augmented Vanguard"
		}
	}
		

	### PLANETS

	planet_names = {
		generic = {
			names = {}
		}
	}

	### CHARACTERS

	character_names = {
		default = {
			# Always combined with a second name
			first_names_male = {
				Purdi Petric Hector Pandarus Asius Acamas Mesthles Antiphus Nastes Sarpedon Glaucus Aeneas Pylaeus Chromis Ennomus Phorcys Ascanius Sarus Acamasthles Acanius Hectorcys Pandarpedon Ennomis Phor Pylaucus Panius Ascandarus Chromus Glaeus
			}
			first_names_female = {
				Purdilla Petria Hectai Pandara Asie Acama Mesthlai Antipha Nasta Sarpedai Glauce Aenea Pylaea Chroma Ennoma Phorca Ascane Sara Acamasthla Acania Hectorca Pandarpeda Ennoma Phara Pylauca Paniua Ascandara Chromusa Glaeusa
			}
			# Always combined with a first name
			second_names = {
				"ei Zeleia" "ei Adresteia" "ei Pityeia" "ei Percote" "ei Sestus" "ei Larissa" "ei Tmolus" "ei Xanthus" "ei Amydon" "ei Arisbe" "ei Erythini" "ei Cromna"
			}
		}
	}
}
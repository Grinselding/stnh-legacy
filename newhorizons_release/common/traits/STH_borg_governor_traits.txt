leader_trait_resource_extraction_bias = {
	cost = 1
	modification = no
	icon = "gfx/interface/icons/traits/leader_traits/trait_ruler_resource_extraction_bias.dds"
	leader_potential_add = {
		from = { has_authority = auth_cybernetic_consciousness }
	}
	modifier = {
		planet_jobs_minerals_produces_mult = 0.1
		planet_jobs_energy_produces_mult = 0.1
	}
	leader_trait = { governor }
	leader_class = { governor }
}

leader_trait_resource_nanite_bias = {
	cost = 1
	modification = no
	icon = "gfx/interface/icons/traits/leader_traits/trait_ruler_resource_nanite_bias.dds"
	leader_potential_add = {
		from = { has_authority = auth_cybernetic_consciousness }
	}
	modifier = {
		planet_jobs_consumer_goods_produces_mult = 0.1
	}
	leader_trait = { governor }
	leader_class = { governor }
}

leader_trait_resource_crew_bias = {
	cost = 1
	modification = no
	icon = "gfx/interface/icons/traits/leader_traits/trait_ruler_deep_connections.dds"
	leader_potential_add = {
		from = { has_authority = auth_cybernetic_consciousness }
	}
	modifier = {
		planet_jobs_sr_crew_produces_mult = 0.1
	}
	leader_trait = { governor }
	leader_class = { governor }
}

leader_trait_tactical_drone_bias = {
	cost = 1
	modification = no
	icon = "gfx/interface/icons/traits/leader_traits/leader_trait_synthetic.dds"
	leader_potential_add = {
		from = { has_authority = auth_cybernetic_consciousness }
	}
	modifier = {
		planet_army_build_speed_mult = 0.25
	}
	leader_trait = { governor }
	leader_class = { governor }
}
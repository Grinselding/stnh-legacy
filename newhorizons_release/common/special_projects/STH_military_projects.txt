# Admrial
special_project = {
	key = "MILITARY_ESCORT_1A_PROJECT"
	days_to_research = 40
	timelimit = 1900
	tech_department = society_technology
	picture = sth_GFX_evt_heroDiplomacy1
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_military = 1
		leader = admiral
		#has_trait = leader_trait_hero ##Needs rejigging completely for hero ships
	}
	on_success = {
		fleet = { 
			random_list = {
				15 = { #Bad
					modifier = { factor = 2 leader = { has_level < 2 } }
					fleet_event = { id = STH_militaryship.113 days = 2 }
					owner = { abort_special_project = { type = "MILITARY_ESCORT_1B_PROJECT" location = event_target:militarymission_escort_target1 } }
				} 
				15 = { # Average
				modifier = { factor = 2 leader = { has_level < 2 } }
				fleet_event = { id = STH_militaryship.102 days = 2 } 
				owner = { abort_special_project = { type = "MILITARY_ESCORT_1B_PROJECT" location = event_target:militarymission_escort_target1 } }
			} 
				15 = { #Good
					modifier = { factor = 70 leader = { OR = { is_hero = yes has_level >= 4 } } }
					fleet_event = { id = STH_militaryship.105 } 
					owner = { abort_special_project = { type = "MILITARY_ESCORT_1B_PROJECT" location = event_target:militarymission_escort_target1 } }
				} 
			}
		}
	}
	on_fail = {
		fleet = {
			fleet_event = {
				id = STH_militaryship.110 days = 3
			}
		}
		owner = { abort_special_project = { type = "MILITARY_ESCORT_1B_PROJECT" location = event_target:militarymission_escort_target1 } }
	}
}

# Flagship - 
special_project = {
	key = "MILITARY_ESCORT_1B_PROJECT"
	days_to_research = 40
	timelimit = 1900
	tech_department = society_technology
	picture = sth_GFX_evt_heroDiplomacy1
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_science_ship = 1
		leader = scientist
		has_trait = leader_trait_hero_ship_admiral
	}
	on_success = {
		fleet = { 
			random_list = { 
				10 = { #Average
					fleet_event = { id = STH_militaryship.113 } 
					owner = { abort_special_project = { type = "MILITARY_ESCORT_1A_PROJECT" location = event_target:militarymission_escort_target1 } }
				} 
				90 = { #Great
					fleet_event = { id = STH_militaryship.105 }
					owner = { abort_special_project = { type = "MILITARY_ESCORT_1A_PROJECT" location = event_target:militarymission_escort_target1 } }
				} 
			}
		}
	}
	on_fail = {
		fleet = {
			fleet_event = { id = STH_militaryship.110 days = 3 }
		}
		owner = { abort_special_project = { type = "MILITARY_ESCORT_1A_PROJECT" location = event_target:militarymission_escort_target1 } }
	}
}


special_project = {
	key = "MILITARY_ESCORT_2A_PROJECT"
	days_to_research = 40
	timelimit = 1900
	tech_department = society_technology
	picture = sth_GFX_evt_heroDiplomacy1
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_military = 1
		leader = admiral
		has_trait = leader_trait_envoy_courier
	}
	on_success = {
		owner = { abort_special_project = { type = "MILITARY_ESCORT_2B_PROJECT" location = event_target:militarymission_escort_target2 } }
		fleet = { 
			random_list = {
				40 = { #Bad
					modifier = { factor = 2 leader = { has_level < 2 } }
					fleet_event = { id = STH_militaryship.119 days = 2 } 
				} 
				60 = { #Average
					modifier = { factor = 100 leader = { OR = { is_hero = yes has_level = 3 } } }
					fleet_event = { id = STH_militaryship.117 } 
				}
				60 = { #Good
					modifier = { factor = 100 leader = { OR = { is_hero = yes has_level >= 4 } } }
					fleet_event = { id = STH_militaryship.115 } 
				}  
			}
		}
	}
	on_fail = {
		owner = { abort_special_project = { type = "MILITARY_ESCORT_2B_PROJECT" location = event_target:militarymission_escort_target2 } }
		fleet = {
			fleet_event = {
				id = STH_militaryship.119 days = 3
			}
		}
	}
}

# Flagship - 
special_project = {
	key = "MILITARY_ESCORT_2B_PROJECT"
	days_to_research = 40
	timelimit = 1900
	tech_department = society_technology
	picture = sth_GFX_evt_heroDiplomacy1
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_science_ship = 1
		leader = scientist
		has_trait = leader_trait_envoy_courier
	}
	on_success = {
		owner = { abort_special_project = { type = "MILITARY_ESCORT_2A_PROJECT" location = event_target:militarymission_escort_target2 } }
		fleet = { 
			random_list = {
				40 = { #Bad
					modifier = { factor = 2 leader = { has_level < 2 } }
					fleet_event = { id = STH_militaryship.119 days = 2 } 
				} 
				60 = { #Average
					modifier = { factor = 100 leader = { OR = { is_hero = yes has_level = 3 } } }
					fleet_event = { id = STH_militaryship.117 } 
				}
				60 = { #Good
					modifier = { factor = 100 leader = { OR = { is_hero = yes has_level >= 4 } } }
					fleet_event = { id = STH_militaryship.115 } 
				}  
			}
		}
	}
	on_fail = {
		owner = { abort_special_project = { type = "MILITARY_ESCORT_2A_PROJECT" location = event_target:militarymission_escort_target2 } }
		fleet = {
			fleet_event = { id = STH_militaryship.119 days = 3 }
		}
		
	}
}

###############
# Research SOS
###############

# Regular - 
special_project = {
	key = "MILITARY_STARBASE_1A_PROJECT"
	days_to_research = 20
	timelimit = 800
	tech_department = society_technology
	picture = sth_GFX_evt_satellite_in_orbit
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_military = 1
		leader = admiral
		#has_trait = leader_trait_hero_ship_admiral
	}
	on_success = {
		owner = { 
			random_list = { 
				25 = { modifier = { factor = 5 leader = { has_level >= 5 } }
					country_event = { id = STH_militaryship.134 } } #Great
				25 = { modifier = { factor = 5 leader = { has_level >= 3 } }
					country_event = { id = STH_militaryship.136 } } #Average
				25 = { modifier = { factor = 2 leader = { has_level <= 2 } }
					country_event = { id = STH_militaryship.137 } } #Bad
				25 = { modifier = { factor = 2 leader = { has_level = 1 } }
					country_event = { id = STH_militaryship.138 } } #Awful 
			}
		}
		fleet = { fleet_event = { id = STH_militaryship.139 } }
	}
	on_fail = {
		owner = {
			country_event = { id = STH_militaryship.133 days = 3 }
		}
	}
}

# Hero Admiral - 
special_project = {
	key = "MILITARY_STARBASE_1B_PROJECT"
	days_to_research = 20
	timelimit = 800
	tech_department = society_technology
	picture = sth_GFX_evt_satellite_in_orbit
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_military = 1
		leader = admiral
		has_trait = leader_trait_hero_ship_admiral
	}
	on_success = {
		owner = { 
			random_list = { 
				40 = { country_event = { id = STH_militaryship.134 } } #Great
				30 = { country_event = { id = STH_militaryship.135 } } #Good
				20 = { country_event = { id = STH_militaryship.136 } } # Nothing
				05 = { country_event = { id = STH_militaryship.137 } } #Bad
				05 = { country_event = { id = STH_militaryship.138 } } #Awful 
			}
		}
		fleet = { fleet_event = { id = STH_militaryship.139 } }
		owner = { abort_special_project = { type = "MILITARY_STARBASE_1C_PROJECT" location = event_target:research_target_1 } }
	}
	on_fail = {
		owner = {
			country_event = { id = STH_militaryship.133 days = 3 }
		}
	}
}

# Hero Scientist - 
special_project = {
	key = "MILITARY_STARBASE_1C_PROJECT"
	days_to_research = 20
	timelimit = 800
	tech_department = society_technology
	picture = sth_GFX_evt_satellite_in_orbit
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_science_ship = 1
		leader = scientist
		has_trait = leader_trait_hero_ship_admiral
	}
	on_success = {
		owner = { 
			random_list = { 
				40 = { country_event = { id = STH_militaryship.134 } } #Great
				30 = { country_event = { id = STH_militaryship.135 } } #Good
				20 = { country_event = { id = STH_militaryship.136 } } #Average
				05 = { country_event = { id = STH_militaryship.137 } } #Bad
				05 = { country_event = { id = STH_militaryship.138 } } #Awful 
			}
		}
		fleet = { fleet_event = { id = STH_militaryship.139 } }
		owner = { abort_special_project = { type = "MILITARY_STARBASE_1B_PROJECT" location = event_target:research_target_1 } }
	}
	on_fail = {
		owner = {
			country_event = { id = STH_militaryship.133 days = 3 }
		}
	}
}

# Hero Engineer - 
special_project = {
	key = "MILITARY_STARBASE_1D_PROJECT"
	days_to_research = 20
	timelimit = 800
	tech_department = society_technology
	picture = sth_GFX_evt_satellite_in_orbit
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_military = 1
		leader = admiral
		has_trait = leader_trait_hero_ship_engineer
	}
	on_success = {
		owner = { 
			random_list = { 
				30 = { country_event = { id = STH_militaryship.140 } } # Engineer
				40 = { country_event = { id = STH_militaryship.134 } } #Great
				10 = { country_event = { id = STH_militaryship.135 } } #Good
				15 = { country_event = { id = STH_militaryship.136 } } #Average
				05 = { country_event = { id = STH_militaryship.137 } } #Bad
			}
		}
		fleet = { fleet_event = { id = STH_militaryship.139 } }
	}
	on_fail = {
		owner = {
			country_event = { id = STH_militaryship.133 days = 3 }
		}
	}
}

# Hero Tal Shiar - 
special_project = {
	key = "MILITARY_STARBASE_1E_PROJECT"
	days_to_research = 20
	timelimit = 800
	tech_department = society_technology
	picture = sth_GFX_evt_satellite_in_orbit
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_military = 1
		leader = admiral
		has_trait = leader_trait_talshiar
	}
	on_success = {
		owner = { 
			random_list = { 
				30 = { country_event = { id = STH_militaryship.140 } } # Tal'Shiar
				40 = { country_event = { id = STH_militaryship.134 } } #Great
				10 = { country_event = { id = STH_militaryship.135 } } #Good
				15 = { country_event = { id = STH_militaryship.136 } } #Average
				05 = { country_event = { id = STH_militaryship.137 } } #Bad
			}
		}
		fleet = { fleet_event = { id = STH_militaryship.139 } }
	}
	on_fail = {
		owner = {
			country_event = { id = STH_militaryship.133 days = 3 }
		}
	}
}

###############
# Starbase SOS
###############

special_project = {
	key = "MILITARY_SOS_1_PROJECT"
	days_to_research = 20
	timelimit = 800
	tech_department = society_technology
	picture = sth_GFX_evt_starbase74
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_military = 1
		leader = admiral
	}
	on_success = {
		owner = { 
			random_list = { 
				10 = { modifier = { factor = 50 leader = { is_hero = yes } } country_event = { id = STH_militaryship.152 } } #  Saved
				10 = { modifier = { factor = 20 leader = { has_level > 4 } } country_event = { id = STH_militaryship.152 } } #  Saved
				10 = { modifier = { factor = 50 leader = { has_trait = leader_trait_talshiar } } country_event = { id = STH_militaryship.152 } } #  Saved
				10 = { country_event = { id = STH_militaryship.153 } } # Minor Damage
				05 = { country_event = { id = STH_militaryship.154 } } # Major Damage
			}
		}
		fleet = { fleet_event = { id = STH_militaryship.155 } }
	}
	on_fail = {
		owner = { country_event = { id = STH_militaryship.154 days = 3 } }
	}
}

special_project = {
	key = "MILITARY_SOS_2_PROJECT"
	days_to_research = 10
	timelimit = 800
	tech_department = society_technology
	picture = sth_GFX_evt_starbase74
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_military = 1
		leader = admiral
	}
	on_success = {
		owner = { 
			country_event = { id = STH_militaryship.159 days = 3 }
		}
		fleet = { fleet_event = { id = STH_militaryship.155 } }
	}
	on_fail = {}
}

# Investigate stranded ship
special_project = {
	key = "MILITARY_SOS_3_PROJECT"
	days_to_research = 10
	timelimit = 1000
	tech_department = society_technology
	picture = sth_GFX_evt_fedshipDestroyed
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_military = 1
		leader = admiral
	}
	on_success = {
		owner = {
			random_list = {
				10 = { modifier = { factor = 50 leader = { is_hero = yes } } 
					random_list = {
						20 = { country_event = { id = STH_militaryship.171 } } # Saved
						30 = { country_event = { id = STH_militaryship.174 } } # Pirates 1
						50 = { country_event = { id = STH_militaryship.175 } } # Pirates 2
					}
				}
				10 = { modifier = { factor = 20 leader = { has_level > 4 } }
					random_list = {
						50 = { country_event = { id = STH_militaryship.171 } } # Saved
						50 = { country_event = { id = STH_militaryship.175 } } # Pirates 2
					}
				}	
				10 = { modifier = { factor = 50 leader = { has_trait = leader_trait_talshiar } } 
					random_list = {
						50 = { country_event = { id = STH_militaryship.171 } } # Saved
						50 = { country_event = { id = STH_militaryship.175 } } # Pirates 2
					}
				}
				10 = { country_event = { id = STH_militaryship.171 } } # Minor Damage
				10 = { country_event = { id = STH_militaryship.172 } } # Minor Damage
				10 = { country_event = { id = STH_militaryship.173 } } # Explodes
			}
		}
		fleet = { fleet_event = { id = STH_militaryship.155 } }
	}
	on_fail = {
		owner = {
			country_event = { id = STH_militaryship.173 } # Explodes
		}
	}
}
# Investigate Asteroid
special_project = {
	key = "MILITARY_SOS_4_PROJECT"
	days_to_research = 10
	#timelimit = 900
	tech_department = society_technology
	picture = sth_GFX_evt_doomsday_remnant
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_transport = 1
		leader = general
	}
	on_success = {
		owner = { 
			random_list = {
				60 = { country_event = { id = STH_militaryship.176 days = 3 } }
				40 = { country_event = { id = STH_militaryship.178 days = 3 } }
			}
			
		}
		fleet = { fleet_event = { id = STH_militaryship.155 } }
	}
	on_fail = {}
}

# Raid Base
special_project = {
	key = "MILITARY_SOS_5_PROJECT"
	days_to_research = 60
	tech_department = engineering_technology
	picture = sth_GFX_evt_groundBattle1
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_transport = 1
		leader = general
	}
	on_success = {
		owner = { 
			country_event = {
				id = STH_militaryship.177
				days = 3
			}
		}
		fleet = { fleet_event = { id = STH_militaryship.155 } }
	}
	on_fail = {}
}


### LOWER DECKS
special_project = {
	key = "SECOND_CONTACT_1_PROJECT"
	days_to_research = 60
	timelimit = 1000
	tech_department = society_technology
	picture = sth_GFX_evt_second_contact
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_military = 1
		leader = admiral
	}
	on_success = {
		owner = { 
			random_list = {
				60 = { #Good 
					modifier = { factor = 20 leader = { has_level < 2 } }
					modifier = { factor = 10 leader = { is_hero = yes } }
					country_event = { id = STH_militaryship.202 days = 2 } 
				} 
				20 = { #bad
					country_event = { id = STH_militaryship.203 } 
				}
				20 = { #awful
					country_event = { id = STH_militaryship.204 } 
				}  
			}
		}
	}
	on_fail = {
		owner = {
			country_event = {
				id = STH_militaryship.205 days = 3
			}
		}
	}
}

### LOWER DECKS
special_project = {
	key = "SECOND_CONTACT_2_PROJECT"
	days_to_research = 60
	timelimit = 1000
	tech_department = society_technology
	picture = sth_GFX_evt_second_contact
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_military = 1
		leader = admiral
	}
	on_success = {
		owner = { 
			random_list = {
				20 = { #Good 
					modifier = { factor = 40 leader = { has_level < 2 } }
					modifier = { factor = 30 leader = { is_hero = yes } }
					country_event = { id = STH_militaryship.202 days = 2 } 
				} 
				45 = { #bad
					country_event = { id = STH_militaryship.203 } 
				}
				45 = { #awful
					country_event = { id = STH_militaryship.204 } 
				}  
			}
		}
	}
	on_fail = {
		owner = {
			country_event = {
				id = STH_militaryship.205 days = 3
			}
		}
	}
}

### Medical Conference
special_project = {
	key = "MILITARY_4_PROJECT"
	days_to_research = 60
	timelimit = 1000
	tech_department = society_technology
	picture = sth_GFX_evt_virus1
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event	
	requirements = {
		shipclass_science_ship = 1
		leader = scientist
		has_trait = leader_trait_doctor
	}
	on_success = {
		random_list = {
			20 = { #Good 
				modifier = { factor = 2 leader = { has_level > 3 } }
				ship_event = { id = STH_militaryship.181 days = 2 } 
			} 
			20 = { #bad
				ship_event = { id = STH_militaryship.182 days = 2 } 
			}
			2 = { # research
				modifier = { 
					factor = 0 
					owner = {
						has_technology = tech_tanerian_plague_1 
						has_technology = tech_tanerian_plague_2 
						has_technology = tech_laurentian_plague_1 
						has_technology = tech_laurentian_plague_2
						has_technology = tech_seripian_flu_1
						has_technology = tech_seripian_flu_2
						has_technology = tech_centauran_flu_1
						has_technology = tech_centauran_flu_2
						has_technology = tech_ynorian_pox_1
						has_technology = tech_ynorian_pox_2
						has_technology = tech_kentaran_pox_1
						has_technology = tech_kentaran_pox_2
					}
				} 
				ship_event ={ id = STH_militaryship.183 days = 2 }   
			}
		}
	}
	on_fail = {}
}
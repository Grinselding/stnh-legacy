### Kelpien Cival War Projects

special_project = {
	key = "KELPIEN_1A_PROJECT"
	tech_department = society_technology
	picture = sth_GFX_evt_kaminar1
	days_to_research = 30
	timelimit = 1080
	event_scope = ship_event
	requirements = {
		shipclass_science_ship = 1
		leader = scientist
			has_trait = leader_trait_hero_ship_admiral
	}

	on_success = {
		random_list = {
			70 = { owner = { country_event = { id = STH_kelpien_flavour.7 days = 10 }}} # Best
			30 = { owner = { country_event = { id = STH_kelpien_flavour.8 days = 10 }}} # Good
        }
		owner = { abort_special_project = { type = "KELPIEN_1B_PROJECT" location = event_target:kaminarPlanet } }
	}
	on_fail = {
		owner = { country_event = { id = STH_kelpien_flavour.200 days = 10 }}
	}
}

special_project = {
	key = "KELPIEN_1B_PROJECT"
	tech_department = society_technology
	picture = sth_GFX_evt_kaminar1
	days_to_research = 30
	timelimit = 1080
	event_scope = ship_event
	requirements = {
		shipclass_military = 1
		leader = admiral
			has_trait = leader_trait_hero_ship_admiral
	}

	on_success = {
		random_list = {
			70 = { owner = { country_event = { id = STH_kelpien_flavour.7 days = 10 }}} # Best
			30 = { owner = { country_event = { id = STH_kelpien_flavour.8 days = 10 }}} # Good
        }
		owner = { abort_special_project = { type = "KELPIEN_1A_PROJECT" location = event_target:kaminarPlanet } }
	}
	on_fail = {
		owner = { country_event = { id = STH_kelpien_flavour.200 days = 10 }}
	}
}
# Tal Shiar
special_project = {
	key = "KELPIEN_1C_PROJECT"
	tech_department = society_technology
	picture = sth_GFX_evt_kaminar1
	days_to_research = 30
	timelimit = 1080
	event_scope = ship_event
	requirements = {
		shipclass_science_ship = 1
		leader = scientist
			has_trait = leader_trait_talshiar
	}

	on_success = {
		random_list = {
			60 = { owner = { country_event = { id = STH_kelpien_flavour.7 days = 10 }}} # Best
			40 = { owner = { country_event = { id = STH_kelpien_flavour.8 days = 10 }}} # Good
        }
	}
	on_fail = {
		owner = { country_event = { id = STH_kelpien_flavour.200 days = 10 }}
	}
}

# Send Troops
special_project = {
	key = "KELPIEN_1D_PROJECT"
	tech_department = society_technology
	picture = sth_GFX_evt_kaminar1
	days_to_research = 30
	timelimit = 1080
	event_scope = ship_event
	requirements = {
		shipclass_transport  = 1
		leader = general
	}

	on_success = {
		random_list = {
			30 = {
                modifier = { add = 20 leader = { has_skill >= 4 } }
                modifier = { add = 40 leader = { has_skill >= 5 } }
                modifier = { add = 200 leader = { is_sec_ops = yes } }
                owner = { country_event = { id = STH_kelpien_flavour.7 days = 10 }} # Best
			}
			30 = {
				modifier = { add = 5 leader = { has_skill >= 1 } }
                modifier = { add = 10 leader = { has_skill >= 2 } }
                owner = { country_event = { id = STH_kelpien_flavour.8 days = 10 }} # Good
			}
			30 = { owner = { country_event = { id = STH_kelpien_flavour.9 days = 10 }} # Bad
			}
		}
	}
	on_fail = {
		owner = { country_event = { id = STH_kelpien_flavour.200 days = 10 }}
	}
}

# Vahar'ai
special_project = {
	key = "KELPIEN_2A_PROJECT"
	tech_department = society_technology
	picture = sth_GFX_evt_kaminar1
	days_to_research = 30
	timelimit = 2000
	event_scope = ship_event
	requirements = {
		shipclass_science_ship = 1
		leader = scientist
	}

	on_success = {
		random_list = {
			30 = {
                modifier = { add = 20 leader = { has_skill >= 4 } }
                modifier = { add = 40 leader = { has_skill >= 5 } }
                modifier = { add = 200 leader = { has_trait = leader_trait_hero_ship_admiral } }
				modifier = { add = 200 leader = { has_trait = leader_trait_talshiar } }
                owner = { country_event = { id = STH_kelpien_flavour.16 days = 10 }} # Best
			}
			30 = {
				modifier = { add = 5 leader = { has_skill >= 1 } }
                modifier = { add = 10 leader = { has_skill >= 2 } }
                owner = { country_event = { id = STH_kelpien_flavour.17 days = 10 }} # Neutral
			}
			25 = { owner = { country_event = { id = STH_kelpien_flavour.18 days = 10 }} # Bad
			}
			15 = { owner = { country_event = { id = STH_kelpien_flavour.100 days = 10 }} # Battle
			}
		}
	}	
	on_fail = {
		owner = { country_event = { id = STH_kelpien_flavour.200 days = 10 }}
	}
}

special_project = {
	key = "KELPIEN_3A_PROJECT"
	tech_department = society_technology
	picture = sth_GFX_evt_kaminar1
	days_to_research = 30
	timelimit = 1080
	event_scope = ship_event
	requirements = {
		shipclass_science_ship = 1
		leader = scientist
			has_trait = leader_trait_hero_ship_admiral
	}

	on_success = {
		random_list = {
			70 = { owner = { country_event = { id = STH_kelpien_flavour.51 days = 10 }}} # Best
			30 = { owner = { country_event = { id = STH_kelpien_flavour.52 days = 10 }}} # Good
        }
		owner = { abort_special_project = { type = "KELPIEN_3B_PROJECT" location = event_target:kaminarPlanet } }
	}
	on_fail = {
		owner = { country_event = { id = STH_kelpien_flavour.200 days = 10 }}
	}
}

special_project = {
	key = "KELPIEN_3B_PROJECT"
	tech_department = society_technology
	picture = sth_GFX_evt_kaminar1
	days_to_research = 30
	timelimit = 1080
	event_scope = ship_event
	requirements = {
		shipclass_military = 1
		leader = admiral
			has_trait = leader_trait_hero_ship_admiral
	}

	on_success = {
		random_list = {
			70 = { owner = { country_event = { id = STH_kelpien_flavour.51 days = 10 }}} # Best
			30 = { owner = { country_event = { id = STH_kelpien_flavour.52 days = 10 }}} # Good
        }
		owner = { abort_special_project = { type = "KELPIEN_3A_PROJECT" location = event_target:kaminarPlanet } }
	}
	on_fail = {
		owner = { country_event = { id = STH_kelpien_flavour.200 days = 10 }}
	}
}
# Tal Shiar
special_project = {
	key = "KELPIEN_3C_PROJECT"
	tech_department = society_technology
	picture = sth_GFX_evt_kaminar1
	days_to_research = 30
	timelimit = 1080
	event_scope = ship_event
	requirements = {
		shipclass_science_ship = 1
		leader = scientist
			has_trait = leader_trait_talshiar
	}

	on_success = {
		random_list = {
			60 = { owner = { country_event = { id = STH_kelpien_flavour.51 days = 10 }}} # Best
			40 = { owner = { country_event = { id = STH_kelpien_flavour.52 days = 10 }}} # Good
        }
	}
	on_fail = {
		owner = { country_event = { id = STH_kelpien_flavour.200 days = 10 }}
	}
}

# Send Troops
special_project = {
	key = "KELPIEN_3D_PROJECT"
	tech_department = society_technology
	picture = sth_GFX_evt_kaminar1
	days_to_research = 30
	timelimit = 1080
	event_scope = ship_event
	requirements = {
		shipclass_transport  = 1
		leader = general
	}

	on_success = {
		random_list = {
			30 = {
                modifier = { add = 20 leader = { has_skill >= 4 } }
                modifier = { add = 40 leader = { has_skill >= 5 } }
                modifier = { add = 200 leader = { is_sec_ops = yes } }
                owner = { country_event = { id = STH_kelpien_flavour.51 days = 10 }} # Best
			}
			30 = {
				modifier = { add = 5 leader = { has_skill >= 1 } }
                modifier = { add = 10 leader = { has_skill >= 2 } }
                owner = { country_event = { id = STH_kelpien_flavour.52 days = 10 }} # Good
			}
			30 = { owner = { country_event = { id = STH_kelpien_flavour.53 days = 10 }} # Bad
			}
		}
	}
	on_fail = {
		owner = { country_event = { id = STH_kelpien_flavour.200 days = 10 }}
	}
}

# Vahar'ai
special_project = {
	key = "KELPIEN_4A_PROJECT"
	tech_department = society_technology
	picture = sth_GFX_evt_kaminar1
	days_to_research = 30
	timelimit = 2000
	event_scope = ship_event
	requirements = {
		shipclass_science_ship = 1
		leader = scientist
	}

	on_success = {
		random_list = {
			30 = {
                modifier = { add = 20 leader = { has_skill >= 4 } }
                modifier = { add = 40 leader = { has_skill >= 5 } }
                modifier = { add = 200 leader = { has_trait = leader_trait_hero_ship_admiral } }
				modifier = { add = 200 leader = { has_trait = leader_trait_talshiar } }
                owner = { country_event = { id = STH_kelpien_flavour.55 days = 10 }} # Best
			}
			30 = {
				modifier = { add = 5 leader = { has_skill >= 1 } }
                modifier = { add = 10 leader = { has_skill >= 2 } }
                owner = { country_event = { id = STH_kelpien_flavour.56 days = 10 }} # Neutral
			}
			25 = { owner = { country_event = { id = STH_kelpien_flavour.57 days = 10 }} # Bad
			}
			15 = { owner = { country_event = { id = STH_kelpien_flavour.100 days = 10 }} # Battle
			}
		}
	}	
	on_fail = {
		owner = { country_event = { id = STH_kelpien_flavour.200 days = 10 }}
	}
}

# Available requirements:
#
#	SKILLS
#	skill = 2
#
#	LEADER
#	leader = scientist
#	leader = admiral
#	leader = general
#	leader = ruler
#	leader = governor
#	
#	MILITARY SHIPS
#	shipclass_military = 1
#	
#	CIVILIAN SHIPS
#	shipclass_constructor = 2
#	shipclass_science_ship = 1
#	shipclass_colonizer = 2
#	
#	ARMIES
#	shipclass_transport = 1
#	assault_armies = 2
#	defense_armies = 1
#	
#	STATIONS
#	research_station = yes
#	mining_station = yes
#	observation_station = yes
#
#	SCOPES
#	abort_trigger
#	this = country (project owner)
#	from = event scope (planet or ship, MIGHT NOT EXIST)
#	fromfrom = project creation scope (usually equals location)
#	
#	on_success
#	this = event scope (ship or planet)
#	from = project creation scope (usually equals location)
#	
#	on_fail
#	this = country (project owner)
#	from = project creation scope (usually equals location)

#Use Admiral

# Bajoran Temple - Scientist
special_project = {
	key = "WARPCAPABLE_1_PROJECT"
	days_to_research = 30
	timelimit = 900
	tech_department = society_technology
	picture = sth_GFX_evt_vedekAssembly
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event
	requirements = {
		shipclass_science_ship = 1
		leader = scientist
	}
	on_success = {
		ship_event = { id = STH_warpcapable.11 days = 1 }
		owner = { abort_special_project = { type = "WARPCAPABLE_2_PROJECT" } }
	}
	on_fail = {
		
	}
}
# Bajoran Temple - Admiral
special_project = {
	key = "WARPCAPABLE_2_PROJECT"
	days_to_research = 30
	timelimit = 900
	tech_department = society_technology
	picture = sth_GFX_evt_vedekAssembly
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event
	requirements = {
		shipclass_military = 1
		leader = admiral
	}
	on_success = {
		ship_event = { id = STH_warpcapable.11 days = 1 }
		owner = { abort_special_project = { type = "WARPCAPABLE_1_PROJECT" } }
	}
	on_fail = {
		
	}
}

## Cave Visit
special_project = {
	key = "WARPCAPABLE_3_PROJECT"
	days_to_research = 60
	timelimit = 900
	tech_department = society_technology
	picture = sth_GFX_evt_trill
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event
	requirements = {
		shipclass_science_ship = 1
		leader = scientist
	}
	on_success = {
		ship_event = { id = STH_warpcapable.18 days = 1 }
	}
	on_fail = {
		
	}
}

##Generic Conference
special_project = {
	key = "WARPCAPABLE_4_PROJECT"
	days_to_research = 60
	timelimit = -1
	tech_department = society_technology
	picture = sth_GFX_evt_federationCouncil
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event
	requirements = {
		shipclass_science_ship = 1
		leader = scientist
	}
	on_success = {
		random_list = {
			33 = { ship_event = { id = STH_warpcapable.22 days = 2 } }
			33 = { ship_event = { id = STH_warpcapable.23 days = 2 } }
			33 = { ship_event = { id = STH_warpcapable.24 days = 2 } }
		}
	}
	on_fail = {}
}

## Risa Shore leave
special_project = {
	key = "WARPCAPABLE_5_PROJECT"
	days_to_research = 30
	timelimit = -1
	tech_department = society_technology
	picture = sth_GFX_evt_risa
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event
	requirements = {
		shipclass_science_ship = 1
		leader = scientist
	}
	on_success = {
		random_list = {
			80 = { ship_event = { id = STH_warpcapable.26 days = 2 } }
			20 = { ship_event = { id = STH_warpcapable.27 days = 2 } }
		}
		owner = { 
			abort_special_project = { type = "WARPCAPABLE_6_PROJECT" }
			abort_special_project = { type = "WARPCAPABLE_7_PROJECT" }
		}
	}
	on_fail = {}
}
## Risa Shore leave
special_project = {
	key = "WARPCAPABLE_6_PROJECT"
	days_to_research = 30
	timelimit = -1
	tech_department = society_technology
	picture = sth_GFX_evt_risa
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event
	requirements = {
		shipclass_military = 1
		leader = admiral
	}
	on_success = {
		random_list = {
			80 = { ship_event = { id = STH_warpcapable.26 days = 2 } }
			20 = { ship_event = { id = STH_warpcapable.27 days = 2 } }
		}
		owner = { 
			abort_special_project = { type = "WARPCAPABLE_5_PROJECT" }
			abort_special_project = { type = "WARPCAPABLE_7_PROJECT" }
		}
	}
	on_fail = {}
}
## Risa Shore leave
special_project = {
	key = "WARPCAPABLE_7_PROJECT"
	days_to_research = 30
	timelimit = -1
	tech_department = society_technology
	picture = sth_GFX_evt_risa
	icon = "gfx/interface/icons/situation_log/situation_log_quest.dds"
	event_scope = ship_event
	requirements = {
		shipclass_transport = 1
		leader = general
	}
	on_success = {
		random_list = {
			80 = { ship_event = { id = STH_warpcapable.26 days = 2 } }
			20 = { ship_event = { id = STH_warpcapable.27 days = 2 } }
		}
		owner = { 
			abort_special_project = { type = "WARPCAPABLE_5_PROJECT" }
			abort_special_project = { type = "WARPCAPABLE_6_PROJECT" }
		}
	}
	on_fail = {}
}
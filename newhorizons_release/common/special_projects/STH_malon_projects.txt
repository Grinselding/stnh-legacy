# Waste Removal 
special_project = {
	key = "MALON_1_PROJECT"
	days_to_research = 30
	timelimit = 900
	tech_department = society_technology
	picture = sth_GFX_evt_malonFreighe2
	event_scope = ship_event
	requirements = {
		shipclass_military = 1
		leader = admiral
		has_trait = leader_trait_hauling_waste
	}
	on_start = {
		ship_event = { id = STH_malon_flavour.10 }
	}
	on_success = {
		random_country = {
			limit = { has_country_flag = malon_sanctity }
			random_list = {
				90 = { country_event = { id = STH_malon_flavour.5 days = 3 } } # Success
				10 = { country_event = { id = STH_malon_flavour.11 days = 3 } } # Explodes
			}
		}
	}
	on_fail = { country_event = { id = STH_malon_flavour.7 }
	}
}
special_project = {
	key = "MALON_2_PROJECT"
	days_to_research = 30
	timelimit = 900
	tech_department = society_technology
	picture = sth_GFX_evt_malonFreighe2
	event_scope = ship_event
	requirements = {
		shipclass_military = 1
		leader = admiral
		has_trait = leader_trait_hauling_waste
	}
	on_success = {
		random_country = {
			limit = { has_country_flag = malon_sanctity }
			random_list = {
				70 = { country_event = { id = STH_malon_flavour.8 days = 3 } } # Success
				30 = { 
					modifier = {
						factor = 0
						any_system = {
							OR = {
								has_star_flag = dumping_location_2
								has_star_flag = dumping_location_3
							}
							has_owner = no
						}
					}
					country_event = { id = STH_malon_flavour.9 days = 3 } } # Caught
				5 = { country_event = { id = STH_malon_flavour.11 days = 3 } } # Explodes
			}
		}
		random_country = {
			limit = { has_country_flag = wasted_dump_target }
			country_event = { id = STH_malon_flavour.6 days = 4 }
		}
	}
	on_fail = { country_event = { id = STH_malon_flavour.7 }
	}
}
# Waste Pickup waste 
special_project = {
	key = "MALON_3_PROJECT"
	days_to_research = 30
	timelimit = 900
	tech_department = society_technology
	picture = sth_GFX_evt_malonFreighe2
	event_scope = ship_event
	requirements = {
		shipclass_military = 1
		leader = admiral
	}
	on_success = {
		random_country = {
			limit = { has_country_flag = malon_sanctity }
			country_event = { id = STH_malon_flavour.4 days = 3 }
		}
		ship_event = { id = STH_malon_flavour.12 }
	}
	on_fail = { country_event = { id = STH_malon_flavour.11 }
	}
}

# Waste management - research solution
special_project = {
	key = "MALON_4_PROJECT"
	days_to_research = 600
	timelimit = -1
	tech_department = society_technology
	picture = sth_GFX_evt_malonFreighe2
	event_scope = planet_event
	requirements = {}

	on_success = {
		random_country = {
			limit = { has_country_flag = malon_sanctity }
			country_event = { id = STH_malon_flavour.101   }
		}
	}
	on_fail = {}
}

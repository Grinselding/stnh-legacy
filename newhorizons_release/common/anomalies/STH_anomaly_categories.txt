############################
###  ANOMALY CATEGORIES  ###
############################

STH_sleepingDogs_category = {
	desc = STH_sleepingDogs_category_desc
	picture = GFX_evt_ship_in_orbit
	level = 3
	spawn_chance = {
		modifier = {
			add = 10
			is_gas_giant = yes
			from = { owner = { has_normal_anomalies = yes } }
		}
	}
	max_once = yes
	on_success = { 
		20 = STH_sleepingDogs_event.1
		2 = STH_sleepingDogs_fail.1
		1 = STH_sleepingDogs_fail.2
	}
}

STH_arsenalOfFreedom_category = {
	desc = STH_arsenalOfFreedom_category_desc
	picture = GFX_evt_ship_in_orbit
	level = 2
	spawn_chance = {
		modifier = {
			add = 10
			is_planet_class = pc_tropical
			from = { owner = { has_normal_anomalies = yes } }
		}
	}
	max_once = yes
	on_success = { 
		20 = STH_arsenalOfFreedom_event.1
		2 = STH_arsenalOfFreedom_fail.1
		1 = STH_arsenalOfFreedom_fail.2
	}
}

STH_theTerratinIncident_category = {
	desc = STH_theTerratinIncident_category_desc
	picture = GFX_evt_ship_in_orbit
	level = 2
	spawn_chance = {
		modifier = {
			add = 10
			is_planet_class = pc_g_class
			from = { owner = { has_normal_anomalies = yes } }
		}
	}
	max_once = yes
	on_success = { 
		20 = STH_theTerratinIncident_event.1
		2 = STH_theTerratinIncident_fail.1
		1 = STH_theTerratinIncident_fail.2
	
	}
}


STH_theSilentEnemy_category = {
	desc = STH_theSilentEnemy_category_desc
	picture = GFX_evt_ship_in_orbit
	level = 2
	spawn_chance = {
		modifier = {
			add = 10
			years_passed > 1
			from = { owner = { has_normal_anomalies = yes } }
		}
	}
	max_once = yes
	on_success = { 
		20 = STH_theSilentEnemy_event.1
		2 = STH_theSilentEnemy_fail.1
		1 = STH_theSilentEnemy_fail.2
	}
}

STH_strangeNewWorld_category = {
	desc = STH_strangeNewWorld_category_desc
	picture = GFX_evt_ship_in_orbit
	level = 2
	spawn_chance = {
		modifier = {
			add = 10
			is_m_class = yes
			from = { owner = { has_normal_anomalies = yes } }
		}
	}
	max_once = yes
	on_success = { 
		20 = STH_strangeNewWorld_event.1
		2 = STH_strangeNewWorld_event.17
		1 = STH_strangeNewWorld_event.18
	}
}

STH_gamestersOfTriskelion_category = {
	desc = STH_gamestersOfTriskelion_category_desc
	picture = GFX_evt_ship_in_orbit
	level = 4
	spawn_chance = {
		modifier = {
			add = 10
			is_m_class = yes
			from = { owner = { has_normal_anomalies = yes } }
		}
	}
	max_once = yes
	on_success = { 
		20 = STH_gamestersOfTriskelion_event.3
		2 = STH_gamestersOfTriskelion_event.2
		1 = STH_gamestersOfTriskelion_event.1
	}
}

STH_mirrorMirror_category = {
	desc = STH_mirrorMirror_category_desc
	picture = GFX_evt_ship_in_orbit
	level = 2
	spawn_chance = {
		modifier = { 
			add = 10
			is_asteroid = no
			is_star = no
			from = { owner = { NOT = { has_country_flag = terran_empire } has_transporters = yes } }
			from = { owner = { has_normal_anomalies = yes } }
		}
	}
	max_once = yes
	on_success = { 
		20 = STH_mirrorMirror_event.1000
		2 = STH_mirrorMirror_event.9 
		1 = STH_mirrorMirror_event.9
	}
}

STH_theSkinOfEvil_category = {
    desc = STH_theSkinOfEvil_category_desc
    picture = sth_GFX_evt_theSkinOfEvil1
    level = 1
    spawn_chance = {
        modifier = {
            add = 10
            is_m_class = yes
			from = { owner = { has_normal_anomalies = yes } }
        }
    }
	max_once = yes
	on_success = { 
		20 = STH_theSkinOfEvil_event.1
		2 = STH_theSkinOfEvil_fail.1
		1 = STH_theSkinOfEvil_fail.2
	}
}

STH_space_seed_category = {
	desc = STH_space_seed_category_desc
	picture = sth_GFX_evt_space_seed_1
	level = 1
	spawn_chance = {
		modifier = {
            add = 5
			is_star = no
			from = { 
				owner = {
					OR = { 
						earth_empires = yes
						is_species_class = FED
					}
					has_country_flag = cold_storage_finished
				} 
			}
			from = { owner = { has_normal_anomalies = yes } }
        }
    }
	max_once = yes
	on_success = { 
		50 = {
			modifier = { factor = 0 owner = { has_country_flag = STH_space_seed_BotanyBay } }
			anomaly_event = STH_space_seed_event.1
		}
		20 = {
			modifier = { factor = 0 owner = { has_country_flag = STH_space_seed_Bartlet } }
			anomaly_event = STH_space_seed_event.1000
		}
		20 = {
			modifier = { factor = 0 owner = { has_country_flag = STH_space_seed_McGarry } }
			anomaly_event = STH_space_seed_event.2000
		}
		20 = {
			modifier = { factor = 0 owner = { has_country_flag = STH_space_seed_Lyman } }
			anomaly_event = STH_space_seed_event.3000
		}
		20 = {
			modifier = { factor = 0 owner = { has_country_flag = STH_space_seed_Ziegler } }
			anomaly_event = STH_space_seed_event.4000
		}
		20 = {
			modifier = { factor = 0 owner = { has_country_flag = STH_space_seed_Marbury } }
			anomaly_event = STH_space_seed_event.5000
		}
		10 = {
			anomaly_event = STH_space_seed_event.10000
		}
		2 = STH_space_seed_event_fail.1
		1 = STH_space_seed_event_fail.2
	}
}

STH_terra_nova_category = {
	desc = STH_terra_nova_category_desc  ###TODO add proper description gating for UE - Walshicus
	picture = sth_GFX_evt_terra_nova_1
	level = 1
    spawn_chance = {		
		modifier = {
			factor = 0
		}
	}
	max_once = yes
	on_success = {
		20 = {
			modifier = { factor = 0 owner = { NOT = { has_country_flag = united_earth } } }
			anomaly_event = STH_terra_nova_event.1
		}
		2 = STH_terra_nova_event_fail.1
		1 = STH_terra_nova_event_fail.2
	}
}

STH_kaminar_kelpien_category = {
	desc = STH_kaminar_kelpien_category_desc
	picture = sth_GFX_evt_kaminar1
	level = 2
    spawn_chance = {		
		modifier = {
			factor = 0
		}
	}
	max_once = yes
	on_success = {
		40 = {
			modifier = { factor = 0 owner = { NOR = { has_country_flag = united_earth has_country_flag = united_federation_of_planets } } }
			anomaly_event = STH_brightest_star.1
		}
		2 = STH_brightest_star_fail.1
		1 = STH_brightest_star_fail.2
	}
}

STH_de_declaratem_category = {
	desc = STH_de_declaratem_category_desc
	picture = sth_GFX_evt_MirrorUniverse1
	level = 1
    spawn_chance = {
        modifier = {
            add = 50
            is_planet_class = pc_black_hole
            from = { owner = { earth_empires = yes } }
        }
		modifier = {
            add = 50
            is_planet_class = pc_black_hole
            years_passed > 25
			from = { owner = { has_normal_anomalies = yes } }
        }
    }
	max_once_global = yes
	on_success = {  
		40 = STH_deDeclaratem_event.1
		2 = STH_deDeclaratem_event.100
		1 = STH_deDeclaratem_event.200
	}
}


STH_samaritan_snare_category = {
	desc = STH_samaritan_snare_category_desc
	picture = sth_GFX_evt_samaritanSnare1
	level = 2
	spawn_chance = {
		modifier = {
			add = 10
			is_colonizable = no
			is_star = no
			from = { owner = { has_normal_anomalies = yes } }
		}
	}
	max_once = yes
	on_success = { 
		20 = STH_samaritan_snare_event.1
		2 = STH_samaritan_snare_event.100
		1 = STH_samaritan_snare_event.100
	}
}

STH_the_hundred_category = {
	desc = STH_the_hundred_category_desc
	picture = sth_GFX_evt_the_hundred_1
	level = 2
	spawn_chance = {		
		modifier = {
			factor = 0
		}
	}
	on_success = { 
		10 = {
			modifier = { factor = 0 owner = { has_normal_anomalies = no } }
			anomaly_event = STH_dominion_story.105
		}
		20 = {
			modifier = { factor = 0 owner = { has_normal_anomalies = yes } }
			anomaly_event = STH_dominion_story.101
		}
		3 = {
			modifier = { factor = 0 owner = { has_normal_anomalies = yes } }
			anomaly_event = STH_dominion_story.100
		}
	}
}


STH_the_ship_category = {
	desc = STH_the_ship_category_desc
	picture = sth_GFX_evt_theShip1
	level = 3
	spawn_chance = {		
		modifier = {
			factor = 0
		}
	}
	max_once = yes
	on_success = { 
		20 = STH_dominion_flavour.2020
		2 = STH_dominion_flavour.2010
		1 = STH_dominion_flavour.2010
	}
}

STH_the_android_category = {
	desc = STH_the_android_category_desc
	picture = sth_GFX_evt_theAndroid1
	level = 1
	spawn_chance = {		
		modifier = {
			add = 0
			has_global_flag = STH_theAndroid_started
		}
	}
	max_once = yes
	on_spawn = { set_global_flag = STH_the_android_flag }
	on_success = { 
		20 = STH_theAndroid_event.1
		2 = STH_theAndroid_event.1000
		1 = STH_theAndroid_event.1000
	}
}

STH_secondChance_category = {
	desc = STH_secondChance_category_desc
	picture = GFX_evt_society_research
	level = 1
	spawn_chance = {
		modifier = { 
			add = 10
			years_passed > 100
			uninhabitable_regular_planet = yes
			from = { owner = { has_normal_anomalies = yes } }
		}
	}
	max_once = yes
	on_success = STH_secondChance_event.1
}


STH_innerLight_category = {
	desc = STH_innerLight_category_desc
	picture = sth_GFX_evt_innerLight1
	level = 2
	spawn_chance = {
		modifier = { 
			add = 3
			uninhabitable_regular_planet = yes
			from = { owner = { has_normal_anomalies = yes } }
		}
	}
	max_once = yes
	on_success = STH_innerLight_event.1
}

STH_silence_category = {
	desc = STH_silence_category_desc
	picture = sth_GFX_evt_blackHole
	level = 3
	spawn_chance = {
		modifier = { #only appears on black holes, once
			add = 50
			is_star_class = sc_black_hole			
			from = { owner = { has_normal_anomalies = yes } }
		}
	}
	max_once = yes
	on_success = STH_silence_event.1
}

STH_theCrossing_category = {
	desc = STH_theCrossing_category_desc
	picture = sth_GFX_evt_spaceBackground
	level = 2
	spawn_chance = {
		modifier = { #anywhere but low chance
			add = 1
			from = { owner = { has_normal_anomalies = yes } }
		}
	}
	max_once = yes
	on_success = STH_theCrossing_event.1
}

STH_memorial_category = {
	desc = STH_memorial_category_desc
	picture = sth_GFX_evt_spaceBackground
	level = 2
	spawn_chance = {
		modifier = { #anywhere but low chance
			add = 1
			is_m_class = yes
			years_passed > 20
			from = { owner = { has_normal_anomalies = yes } }
		}
	}
	max_once_global = yes
	on_success = STH_memorial_event.1
}

STH_iconia_probe_category = {
	desc = STH_iconia_probe_category_desc
	picture = sth_GFX_evt_iconianProbe
	level = 1
	spawn_chance = {		
		modifier = {
			add = 0
		}
	}
	max_once = yes
	on_success = STH_site_iconia_probe.0
}

STH_pahvo_music_category = {
	desc = STH_pahvo_music_category_desc
	picture = sth_GFX_evt_pahvo_planet
	level = 1
	spawn_chance = {		
		modifier = {
			add = 0
		}
	}
	max_once = yes
	on_success = STH_site_pahvo_music.0
}
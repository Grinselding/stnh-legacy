### System Resources

time = {
}


### Basic Resources

energy ={
	tradable = yes
	max = 15000
	deficit_modifier = energy_deficit #### found in static modifiers
	tooltip_decimals = 1
	ai_weight = { weight = 1.5 }
	ai_wants = { base = 500 }
}

minerals ={
	tradable = yes
	market_amount = 100
	market_price = 100
	max = 15000
	deficit_modifier = minerals_deficit #### found in static modifiers
	ai_weight = { weight = 1 }
	ai_wants = { base = 1000 }
}

food ={
	tradable = yes
	market_amount = 100
	market_price = 100
	max = 15000
	deficit_modifier = food_deficit #### found in static modifiers
	ai_weight = { weight = 1 }
	ai_wants = { base = 500 }
}

physics_research = {
	ai_weight = {
		weight = 1
	}
}

society_research = {
	ai_weight = {
		weight = 1
	}
}

engineering_research = {
	ai_weight = {
		weight = 1
	}
}

influence ={
	max = 1000
	fixed_max_amount = yes
	tooltip_decimals = 1
	deficit_modifier = influence_deficit #### found in static modifiers
	ai_weight = { weight = 1 }
}

unity = {
	ai_weight = {
		weight = 1
	}
}

###########################
### Manufactured Resources  ###
###########################

alloys ={
	tradable = yes
	market_amount = 25
	market_price = 100
	max = 20000
	deficit_modifier = alloys_deficit #### found in static modifiers
	ai_weight = { weight = 5 }
	ai_wants = { base = 1500 
	modifier = {
		add = 1000
		resource_stockpile_compare = {
			resource = alloys 
			value > 1200
		}
	}	
	}

}

consumer_goods ={
	tradable = yes
	market_amount = 50
	market_price = 100
	max = 15000
	deficit_modifier = consumer_goods_deficit #### found in static modifiers
	ai_weight = { weight = 2 }
	ai_wants = { base = 500 }
}

###########################
### Advanced Resources  ###
###########################

volatile_motes = {
	tradable = yes
	market_amount = 10
	market_price = 100
	max = 10000
	
	prerequisites = { "tech_mine_volatile_motes" }
	visibility_prerequisite = { always = no }
	
	deficit_modifier = volatile_motes_deficit #found in static modifiers
	
	ai_weight = {
		weight = 10
	}
}

exotic_gases = {
	tradable = yes
	market_amount = 10
	market_price = 100
	max = 10000
	
	prerequisites = { "tech_mine_exotic_gases" }
	visibility_prerequisite = { always = no }
	
	deficit_modifier = exotic_gases_deficit #found in static modifiers
	
	ai_weight = {
		weight = 10
	}
}

rare_crystals = {
	tradable = yes
	market_amount = 10
	market_price = 100
	max = 10000
	
	prerequisites = { "tech_mine_rare_crystals" }
	visibility_prerequisite = { always = no }
	
	deficit_modifier = rare_crystals_deficit #found in static modifiers
	
	ai_weight = {
		weight = 10
	}
}


###########################
###    Rare Resources   ###
###########################

# Living Metal
sr_living_metal = {
	tradable = yes
	market_amount = 1
	market_price = 100
	max = 10000
	
	prerequisites = { "tech_mine_living_metal" }
	visibility_prerequisite = { always = no }
	
	ai_weight = {
		weight = 100
	}
}

# Zro
sr_zro = {
	tradable = yes
	market_amount = 1
	market_price = 100
	max = 10000
	
	prerequisites = { "tech_mine_zro" }
	visibility_prerequisite = { always = no }

	ai_weight = {
		weight = 100
	}
}

# Dark Matter
sr_dark_matter = {
	tradable = yes
	market_amount = 1
	market_price = 100
	max = 10000

	prerequisites = { "tech_mine_dark_matter" }
	visibility_prerequisite = { always = no }
	
	ai_weight = {
		weight = 100
	}
}

minor_artifacts = {
	tradable = no
	
	ai_weight = {
		weight = 1
	}
}
